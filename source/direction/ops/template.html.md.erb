---
layout: markdown_page
title: "Product Section Vision - Ops"
---

- TOC
{:toc}

## Ops Section Overview
<!-- Provide a general overview of the section, what is covered within it and introduce themes.
Include details on our current market share (if available), the total addressable market (TAM), our competitive position, and high level feedback from customers on current features -->
The Ops Section comprises the [Configure](/direction/configure) and [Monitor](/direction/monitor) [stages of the DevOps lifecycle](/stages-devops-lifecycle/).
[Market analysts (i)](https://drive.google.com/file/d/1B2KCSj_PF8ZhZqx_8Q6zFU2vldMu94lT/view) often describe these stages as IT automation and configuration management (ITACM, analogous to Configure) and IT operations management (ITOM, partially analogous to Monitor). 

The [total addressable market (TAM) for DevOps tools targeting these stages (internal - i)](https://drive.google.com/file/d/1B2KCSj_PF8ZhZqx_8Q6zFU2vldMu94lT/view) is $2.2B today and expected to grow to $5.3B in 2023 (18.8% CAGR). 
This is a deep value pool and represents a significant portion of GitLab's expanding addressable market.
The [market is well established (i)](https://drive.google.com/file/d/1VvnJ5Q5PJzPKZ_oYBHGNuc6D7mtMmIZ_/view) with players such as Splunk, New Relic and Datadog dominating market share in the Monitor stage and IBM (+RedHat), Hashicorp, Puppet and Chef splitting a large chunk of the fragmented market in the Configure stage. 

With so many existing strong players, GitLab's market share in these stages is effectively zero.
Our customers do utilize [Configure](https://app.periscopedata.com/app/gitlab/462967/Configure-Metrics) and Monitor capabilities today.
Primarily they:
* Use AutoDevOps and Serverless for quickly getting started with non-production, no-ops projects
* Occasionally use Auto DevOps and Serverless for production workloads.
* Use Auto DevOps as a starting point to design modern devops workflows.
* Utilize GitLab's core features in Create and Verify to manage Infrastructure as Code projects ([internal example](https://gitlab.com/gitlab-com/dev-resources))
* Value our integration with Kubernetes and Prometheus 
* Are interested in bringing Incident Management processes into GitLab to encourage collaboration

Our current R&D investment in Configure and Monitor stages is [40 team members](/company/team/?department=ops-section) (on a [plan to grow 205% Y/Y in 2019](/handbook/hiring/charts/ops-section/)).
The [Configure stage is considered in Year 3 maturity](/direction/maturity/#configure) and the [Monitor stage is considered in Year 1 maturity](/direction/maturity/#monitor).

## Challenges
<!-- What are our constraints?  (team size, product maturity, lack of brand, GTM challenges, etc). What are our market/competitive challenges? -->
Despite significant growth in our R&D investment in 2019, we are constrained relative to our competition in our investment amount in the Ops section. This constraint coupled with our lack of brand identification with Enterprise Ops section buyers means we need to be very strategic in building our product capabilities. 

While the market for the Configure stage has many strong incumbent players, it is splintered across many traditional IT management tools (Ansible, Chef, Puppet). There are [emerging trends (i)](https://drive.google.com/file/d/1ZAqTIiSfpHKyVFpgMnJoOBnyCQ-aF3ej/view) showing a consolidation on [containers (i)](https://docs.google.com/presentation/d/1tt6-XqnhCmsx8Jfy39hit8OgggeaFUUxxU7TnNsG4Qk/edit#slide=id.p9) and [specifically Kubernetes (i)](https://docs.google.com/presentation/d/1tt6-XqnhCmsx8Jfy39hit8OgggeaFUUxxU7TnNsG4Qk/edit#slide=id.p9) for cloud-native application development. This will have the effect of stymying commercial
 players from locking in market share based on proprietary tools. So while there are strong players, the current market dynamics means there is [upside](#opportunities) for us.

The market for the Monitor stage is aligned around key players [New Relic, Splunk, ServiceNow and Datadog](http://videos.cowen.com/detail/videos/recent/video/6040056480001?autoStart=true). Datadog, New Relic and Splunk are all headed towards integrated observability platforms based on logging and metrics. [ServiceNow (i)](https://drive.google.com/file/d/1VvnJ5Q5PJzPKZ_oYBHGNuc6D7mtMmIZ_/view) is considered a standard for help-desk and Incident Management workflows. Strong existing players with large sustained R&D investment will make it difficult for GitLab to compete head-to-head for Monitor stage market share.

Competing tools are marketed as stand-alone point solutions while GitLab's Ops section capabilities will require customers to be using other stages of GitLab. This creates a narrow funnel for adoption with potential users coming only from the segment of the market already using, or willing to use, other stages of GitLab rather than broad-based market fit. This smaller adoption funnel will also affect the rate of learning. 

Outside of market challenges we have some internal ones as well. In general, we are not [dogfooding](/handbook/values/#dogfooding) the Ops section features sufficiently. This has the effect of [slowing our rate of learning](/handbook/product/#dogfood-everything), and putting us in the position of not having immediate real world validation of our product initiatives.

## Opportunities
As noted above, given strong existing players, and our relatively low R&D investment, product maturity and brand awareness compared to the competition, GitLab must be targeted in our investment. Here are some key opportunities we must take advantage of:

* **Increasing Developer buying power:** In the move to DevOps, [tools focused on enabling developers will garner increased share of wallet](http://videos.cowen.com/detail/videos/recent/video/6040056480001?autoStart=true). [71% of organizations (i)](https://docs.google.com/presentation/d/1tt6-XqnhCmsx8Jfy39hit8OgggeaFUUxxU7TnNsG4Qk/edit#slide=id.p4) will achieve full DevOps adoption on some projects within a year. [35% of DevOps implementations include Monitor stage capabilities, 30% include Configure stage ones (i)](https://docs.google.com/presentation/d/1tt6-XqnhCmsx8Jfy39hit8OgggeaFUUxxU7TnNsG4Qk/edit#slide=id.p14). 
* **IT operations skills gap:** DevOps transformations are hampered by a [lack of IT operations skills (i)](https://docs.google.com/presentation/d/1tt6-XqnhCmsx8Jfy39hit8OgggeaFUUxxU7TnNsG4Qk/edit#slide=id.p19). Most organizations have taken to creating [infrastructure platform teams](https://hackernoon.com/how-to-build-a-platform-team-now-the-secrets-to-successful-engineering-8a9b6a4d2c8) to [minimize the amount of operations expertise required by DevOps team members (i)](https://drive.google.com/file/d/1GexcUo4FzmhmV30tnjn7x3hyk1plEK4-/view).  
* **Clear winner in Kubernetes:** Driven by software-defined infrastructure, cost management, and resiliency, organizations [are flocking to cloud-native application architectures (i)](https://drive.google.com/file/d/1ZAqTIiSfpHKyVFpgMnJoOBnyCQ-aF3ej/view). Kubernetes is the clear winner in container orchestration platforms. [Gartner recommends (i)](https://drive.google.com/file/d/19MZsKTc8tvAAFQEWuxDGoTW1f_kCZ6GI/view) organizations choose vendors that support open-source software projects in the cloud native ecosystem ([CNCF](https://www.cncf.io/)). 
* **IaC and Continuous Configuration Management are precursors to AIOps:** AIOps strives to provide self-learning, self-healing IT operations massively impacting IT operations professionals and their tools. However, it is [early in the hype cycle (i)](https://drive.google.com/file/d/1GexcUo4FzmhmV30tnjn7x3hyk1plEK4-/view) and being successful will depend on DevOps teams first defining their infrastructure, operations and observability as code.

## Vision Themes
Our vision for Ops is to enable today's modern best-practices in operations without the burden of
specialized teams, multiple tools and heavy workflows that are the largest barriers to adoption in these stages of the DevOps lifecycle. Our goal is to empower DevOps teams to own their code in production and have the tools to contribute not just to application feature development but to the characteristics of their applications as experienced by their end-users.

### Theme: Infrastructure and Observability as Code
<!-- Introduce theme, stages and categories it applies to and reference liberally. Highlight
important features, epics and issues from that theme specifically. Concentrate not on what we will do
but also what we won't do over the next 12 months. -->
There have been two major trends impacting operations; these are essential to seeding the DevOps
transformation within teams. They are Infrastructure as Code (IAC) and Observability as Code (OAC).

#### Infrastructure as Code

[IAC](https://en.wikipedia.org/wiki/Infrastructure_as_code) provides DevOps teams with a method to maintain consistency with the infrastructure their applications run on.
The best practices defined in IAC prevent configuration drift and allow teams to maintain consistent performance and
security characteristics without the need for ongoing manual intervention. 

IAC was made possible by the availability of rapidly deployed cloud infrastructure (IaaS platforms) and the corresponding
build up of tools to enable generic usage and consistent deployment to that infrastructure (Container Management Platforms).

With our roots as a source code management system, we will build on our existing workflows for code management and
extend our ability to define and manage infrastructure configuration in your project repositories so you can achieve reliability
and consistency in your application.

As examples, GitLab will:
* Auto-detect Terraform templates and deploy them as part of Auto DevOps
* Allow users to define a connection between a project that houses application code, and a project that houses infrastructure definition which that application runs on

#### Observability and Operations as Code

Observability is a measure of your application which defines how well you can understand the state of your production system from its external outputs.
The more observable your application is the more reliably you can operate it, both manually and through automation.
Our vision is to allow you to define your observability and operational automation in code, alongside your application code.
Whether that is the configuration of dashboards, metrics, alerts, runbooks, automation scripts or incident issues - GitLab will source-control those configurations and ensure they are reliably deployed to your production systems.

As examples, GitLab will:
* Auto-detect alert-manager configuration files and deploy them to managed Prometheus
* Enable source-controlled dashboards and metric definitions
* Source-control runbooks and executable runbooks alongside infrastructure definition code

### Theme: Smart Feedback Loop
<!-- Introduce theme, stages and categories it applies to and reference liberally. Highlight
important features, epics and issues from that theme specifically. Concentrate not on what we will do
but also what we won't do over the next 12 months. -->
Defining your infrastructure, observability, and operations as code is just the first step.
The real magic happens when you rapidly iterate on those definitions.
As a single-application for the entire DevOps lifecycle GitLab completes what is commonly a major gap in the DevOps loop - the feedback from ops back into planning. 

Our vision is to provide not just an easy ability to identify, suggest, complete, and deploy changes to your production system but also enable new insights that other tools can't.
During incident response, GitLab can quickly identify the last deployed commit, lines of code changed, and author to guide the response to the right place.
During deployment, GitLab can quickly tell you if your production system is already out-of-bounds from defined service-level indicator (SLI) metrics and roll back.
During post-mortems, GitLab can suggest runbook or alert changes.
We are very excited about enabling DevOps teams with these smart additions.

As examples, GitLab will:
* Provide your last deployed commit info in standard incident templates
* Auto-roll back if production SLIs alert
* Suggest alert and runbook changes during Incident after action
* Provide a consolidated timeline view in Incident after action reports

### Theme: Operations for All
<!-- Introduce theme, stages and categories it applies to and reference liberally. Highlight
important features, epics and issues from that theme specifically. Concentrate not on what we will do
but also what we won't do over the next 12 months. -->
Truly integrated DevOps teams are difficult to create.
While large organizations have the budget to staff dedicated teams with roles like "Cost Optimization Engineer" and "Site Reliability Engineer" smaller teams require "Jack of All Trades" engineers.

Our vision at GitLab is that everyone can contribute, and that small teams, without specialization can leverage and adopt the same tools as those in larger organizations.
By building on our preference for convention over configuration we can take the guess work out of operations.
We will enable you and your team to quickly build a production application and iterate on it.
Not just on its features, but on the overall experience delivered to your users. Rapidly.

As examples, GitLab will:
* Consistently create smart defaults for Auto DevOps, runbooks, infrastructure monitoring, alert configuration and incident templates
* Continue to expand the capabilities of Auto Devops to support a wider array of application types
* Provide auto-infrastructure management tools such as auto0-service restart and cluster cost optimization
* Integrate incident management tasks with developers traditional workflow, namely issues and merge requests


### Theme: No-Ops
<!-- Introduce theme, stages and categories it applies to and reference liberally. Highlight
important features, epics and issues from that theme specifically. Concentrate not on what we will do
but also what we won't do over the next 12 months. -->
While many teams will continue to employ operations best practices, there is a growing movement to use stricter conventions to reduce the need for operations tasks almost entirely.
We'll continue to support this pattern via our investment in Auto DevOps, Serverless and PaaS iniatives. 

At GitLab we believe more and more organizations will adopt centralized platform teams to manage and provide infrastructure to their development teams. 
By providing useful tools for those platform teams to function, we can assist them in reducing the operational burden on their development teams.

As examples, GitLab will:
* Expand our support for serverless and function based applications
* Provide additional tools for group and instance level Kubernetes clusters
* Always ship with smart defaults for Kubernetes cluster, and GitLab managed app configuration


## 3 Year Strategy
In three years the Ops Section market will:
* Consolidate around Kubernetes as the defacto abstraction layer which application delivery teams interact with to deploy and maintain software
* Be clearly segmented between infrastructure platform delivery professionals, and DevOps application delivery professionals
* Have adapted DevOps processes to also include infrastructure and observability into CI/CD pipelines providing more responsive, more secure and higher quality production applications

As a result, in three years, Gitlab will:
* Be considered a critical tool for infrastructure platform delivery teams
* Enable strong collaboration between application and platform teams
* Provide easy adoption of IaC deployment patterns so DevOps teams have more insight and responsiveness to their application's production capabilities

## What's Next for Ops
<!-- Conclude with What must we get done in the next 12 months? What won't be done? Then
transition into direction items highlighting those themes across the relevant stages -->
In the next 12 months the Ops Section must focus on:
* **Dogfooding** - To immediately increase our rate of learning
* **Logging** - In order to build out a complete tool for operational tasks we will improve our logging capabilities based on common open-source standards
* **Infrastructure Platform Teams** - In order to lean into our differentiated investment in Kubernetes first, we should provide tools for the platform teams in larger development organizations
* **Incident Management** - In order to complete the feedback loop for our customers and provide a complete DevOps view to leaders, we will meet our users where they are in their DevOps journey and create experiences to integrate existing monitoring tools into Incident Management workflows
* **IaC** - In order to [skate where the puck is headed](/handbook/product/#why-cycle-time-is-important) we will improve the experience of using GitLab to source control your infrastructure and observability configuration

In doing so we will choose NOT to:
* Invest heavily in our own dashboards and visualization tools
* Invest heavily in ease of use and experience for individuals attaching Kubernetes clusters to projects
* Invest heavily in enhanced out-of-the-box configuration of monitoring including auto-anomoly detection and synthetic monitoring
* Invest in app instrumentation across a number of legacy programming languages

<%= direction %>
