---
layout: markdown_page
title: "Category Vision - Container Registry"
---

- TOC
{:toc}

## Container Registry

The GitLab Container Registry is a secure and private registry for Docker images. Built on open source software, the GitLab Container Registry is completely integrated with GitLab.  Easily use your images with GitLab CI, create images specific for tags or branches and much more.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Container%20Registry)
- [Overall Vision](https://about.gitlab.com/direction/package/)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/593)

Interested in joining the conversation for this category? Please join us in our
[public epic](https://gitlab.com/groups/gitlab-org/-/epics/1287) where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

## What's Next & Why

We are currently working on expanding the Container Registry API, [gitlab-ce#26866](https://gitlab.com/gitlab-org/gitlab-ce/issues/26866) which will give users the ability to view all of their Docker images/tags at the group level. Our goal is give administrators more visibility and control into their container registry at the group and instance level. 

We are also working on improving the experience and functionality of deleting images/tags. [gitlab-ce#24705](https://gitlab.com/gitlab-org/gitlab-ce/issues/24705) will add the the ability to select and delete multiple images/tags from the user interface. While [gitlab-ce#21405](https://gitlab.com/gitlab-org/gitlab-ce/issues/21405) addresses an issue where deleting an image/tag deletes all images/tags with the same `image_id`. Finally, [gitlab-ce#40096](https://gitlab.com/gitlab-org/gitlab-ce/issues/40096) will extend the [CI job permisisons model](https://docs.gitlab.com/ee/user/project/new_ci_build_permissions_model.html) to allow `CI_REGISTRY_USER` to untag images via GitLab CI. 


## Maturity Plan

This category is currently at the "Viable" maturity level, and
our next maturity target is Complete (see our [definitions of maturity levels](/direction/maturity/)).
Key deliverables to achieve this are:
- [Storage tracking and limits for the Container Registry](https://gitlab.com/gitlab-org/gitlab-ce/issues/59232)
- [Implement in-line garbage collection](https://gitlab.com/gitlab-org/gitlab-ce/issues/57897)
- [Retention / expiration policy](https://gitlab.com/gitlab-org/gitlab-ce/issues/20247)
- [Automatically clean container registry based on policies](https://gitlab.com/gitlab-org/gitlab-ce/issues/60337)
- [Tag pruning and deletion](https://gitlab.com/gitlab-org/gitlab-ce/issues/21405)
- [Sort images list](https://gitlab.com/gitlab-org/gitlab-ce/issues/20216)
- [Filter images list](https://gitlab.com/gitlab-org/gitlab-ce/issues/62309)

## Competitive Landscape

[JFrog](https://jfrog.com/artifactory/) and [Sonatype](https://www.sonatype.com/nexus-repository-sonatype) both offer support for building and deployin Docker images. GitHub has a product in [beta](https://help.github.com/en/articles/configuring-docker-for-use-with-github-package-registry) that allows users to authenticate, publish and install packages utilizing Docker. 

Container registries such as [Docker Hub](https://hub.docker.com/) and [Quay](https://quay.io/) offer users a single location to build, analyze and distribute their container images. 

GitLab provides an improved experience by being the single location for the entire DevOps Lifecycle, not just a portion of it. We will provide many of the features expected of a Package Management tool, but without the weight and complexity of a single-point solution. We will prioritize security, performance and integration without sacrificing user experience. 

## Top Customer Success/Sales Issue(s)

The top Customer Success / Sales issue is to improve the visibility and management layer of the Container Registry. The goal of [gitlab-ce#29639](https://gitlab.com/gitlab-org/gitlab-ce/issues/29639) is to improve the tracking and display of data to provide a more seamless user experience within GitLab. By completing this issue we will:
-  Allow  metadata to be stored and removed
-  Make it possible to easily track what data is stored in the registry
-  Make it possible to introduce retention policies for images stored in the registry


## Top Customer Issue(s)

The top customer issue is [gitlab-ce#21405](https://gitlab.com/gitlab-org/gitlab-ce/issues/21405), which addresses issues with removing tags. Currently, the deletion of a single tag, will delete all tags associated with that image. Resolving this issue will establish an improved tag pruning and deletion process for the GitLab Container Registry.

There are additional top TAM issues identified which are popular amongst our customers:

- [GitLab Registry available images list/search](https://gitlab.com/gitlab-org/gitlab-ce/issues/26866)
- [Retention/expiration policy for container images](https://gitlab.com/gitlab-org/gitlab-ce/issues/20247)
- [Create API endpoint to list the packages of a group](https://gitlab.com/gitlab-org/gitlab-ee/issues/10003)

## Top Internal Customer Issue(s)

The top internal customer issue is tied to storage optimization. [gitlab-ce#57897](https://gitlab.com/gitlab-org/gitlab-ce/issues/57897) will allow the Infrastructure team to lower the total cost of the GitLab.com Container Registry by implementing in-line garbage collection and removal of blobs. 

## Top Vision Item(s)

As we see increased adoption and usage of the Container Registry, the need for an improved user interface becomes more important. [gitlab-ee#3597](https://gitlab.com/gitlab-org/gitlab-ee/issues/3597) establishes a high-level vision for the future user experience of the Container Registry. In the coming months, we will break this feature into actionable issues and conduct user research to ensure we provide the best experience possible. 
