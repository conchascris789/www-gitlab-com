---
layout: markdown_page
title: "Sales Territories "
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Process to Update   

1. Create MR with requested changes  
2. Assign to `@jjcordz` & `@atkach1` 
3. `@jjcordz` to update LeanData
4. `@atkach1` to update SFDC
5. MR will be merged by `@jjcordz` when changes have been made   

#### Updating these tables without updating Operations will not be reflected in our various systems causing all reports and routing to be incorrect!   
{:.no_toc}

**Questions?** Ask in `#sales` slack channel pinging `@sales-ops`. 



## Region/Vertical
{:.no_toc}
  * **VP Commercial Sales** ([Mid-Market](#mid-market-segment) & [Small Business](#small-business-segment)): Ryan O'Nell
  * **[Europe, Middle East and Africa](#emea)**: Richard Pidgeon, Regional Director
  * **[North America - US East](#us-east)**: Mark Rogge, Regional Director
  * **[North America - US West](#us-west)**: Haydn Mackay, Regional Director
  * **[Public Sector](#public-sector)**: Paul Almeida, Director of Federal Sales

## Territories

### World
![](/images/handbook/business-ops/FY20_Q1_WW_Large.png)

| Territory    | Region | Large                              | Mid-Market     | Sales Development Representative (SDR) |
|:------------ | :----- | :--------------------------------- | :------------- | :--------- |
| Brazil       | LATAM  | **Jim Torres**   | Romer Gonzalez | Bruno Lazzarin |
| SoCenAmer    | LATAM  | **Jim Torres**   | Romer Gonzalez | Bruno Lazzarin |


### APAC
![](/images/handbook/business-ops/FY20_Q1_APAC_Large.png)

| Territory    | Region | Large        | Mid-Market   |
| :----------- | :----- | :----------- | :----------- |
| ANZ          | APAC   | Sarah Orell  | Julie Manalo |
| Central Asia | APAC   | Sarah Orell  | Julie Manalo |
| China        | APAC   | Sarah Orell  | Julie Manalo |
| Japan        | APAC   | Sarah Orell  | Julie Manalo |
| Korea        | APAC   | Sarah Orell  | Julie Manalo |
| SE Asia      | APAC   | Sarah Orell  | Julie Manalo |
| South Asia   | APAC   | Sarah Orell  | Julie Manalo |


### EMEA
![](/images/handbook/business-ops/FY20_Q1_EMEA_Large.png)

| Territory       | Region | Large (**SAL** - SDR)                                  | Mid-Market  |
| :-------------- | :----- | :----------------------------------------------------- | :---------- |
| BeNeLux         | EMEA | **Nasser Mohunol**<br>Camilo Villanueva                  | Conor Brady |
| Germany         | EMEA | **Timo Schuit**<br>**Rene Hoferichter**<br>Peter Kunkli  | Conor Brady |
| Eastern Europe  | EMEA | **Vadim Rusin (temp)**<br>Camilo Villanueva         | Conor Brady |
| France          | EMEA | **Hugh Christey**<br>**Aleksander Bosnic**<br>Anthony Seguillon                 | Conor Brady |
| MEA             | EMEA | **Sascha Scholing**<br>Camilo Villanueva               | Conor Brady |
| Southern Europe | EMEA | **Vadim Rusin**<br>Camilo Villanueva               | Conor Brady |
| Nordics         | EMEA | **Annette Kristensen**<br>Camilo Villanueva                 | Conor Brady |
| Switzerland and Austria  | EMEA | **Thomas Bosshard**<br>Peter Kunkli                | Conor Brady |
| UK&I            | EMEA | **Justin Haley**<br>**Simon Williams**<br>**Anja Löether**<br>**Steve Challis**<br>Chris Loudon | Conor Brady |
| EMEA Named      | EMEA | **Phil Mckenzie Smith**<br>Camilo Villanueva             | Conor Brady |
| EMEA Named      | EMEA | **Hailey Pobanz (maternity leave)**<br>Camilo Villanueva             | Conor Brady |

### NORAM

For the United States, the following rules apply to all accounts except government agencies or publicly-funded educational institutions or departments, including those at private universities (Johns Hopkins Applied Physics Lab, for example. Government agencies and publicly-funded educational institutions will be managed by our [Public Sector](#public-sector) team.

For other countries in NORAM, governments agencies will be handled by the territory owner.

#### Large Segment
![](/images/handbook/business-ops/FY20_Q1_NORAM_Large.png)
![](/images/handbook/business-ops/FY20_Q1_NORAMCalifornia_Large.png)

##### US East

| Territory                 | Region - SubRegion | Strategic Account Leader (SAL) | Sales Development Rep (SDR) |
| :------------------------ | :----------------- | :----------------------------- | :-------------------------- |
| Central-Gulf-Carolinas    | NORAM - US East    | **Sean Billow**                | Kevin McKinley              |
| Sunshine Peach            | NORAM - US East    | **Jim Bernstein**              | Steven Cull                 |
| Manhattan                 | NORAM - US East    | **Liz Corring**                | Ryan Kimball (temp)         |
| Mid-Atlantic              | NORAM - US East    | **Katherine Evans**            | Kelsey Steyn                |
| Mid-Central               | NORAM - US East    | **Mark Bell**                  | Matthew Walsh               |
| New England - East Canada | NORAM - US East    | **Tony Scafidi**               | Kaleb Hill                  |
| North Central - Ontario   | NORAM - US East    | **Jordan Goodwin**             | Marcus Stangl               |
| NY State - New Jersey     | NORAM - US East    | **Paul Duffy**                 | Ryan Kimball                |
| North TX-LA               | NORAM - US East    | **Chip Digirolamo**            | Morgen Smith                |
| South TX                  | NORAM - US East    | **Matt Petrovick**            | Marcus Stangl (temp)        |
| Named Accounts            | NORAM - US East    | **John May**                   | Matt Walsh                  |
| Named Accounts            | NORAM - US East    | **David Wells**                | Steven Cull                |
| Named Accounts            | NORAM - US East    | **Larry Biegel**               | Kevin McKinley              |

##### US West

| Area | Area Sales Manager (ASM) | Territory | Strategic Account Leader (SAL) | Sales Development Rep (SDR) |
| :--- | :----------------------- | :-------- | :----------------------------- | :-------------------------- |
| Rockies/SoCal | James Roberts | Colorado | **Chris Cornaccia** | Shawn Winters |
| Rockies/SoCal | James Roberts | Arizona / New Mexico | **James Roberts (temp)** | Suzy Verdin |
| Rockies/SoCal | James Roberts | Utah / Nevada | **James Roberts (temp)** | Suzy Verdin |
| Rockies/SoCal | James Roberts | Orange County / San Diego | **Brad Downey** | Mike LeBeau |
| Rockies/SoCal | James Roberts | Greater LA | **Robert Hyry** | David Fisher |
| Bay Area | Alan Cooke | San Francisco | **Alan Cooke (temp)** | Hannah Schuler (temp) |
| Bay Area | Alan Cooke | Santa Clara | **Robert van Leer** | Brandon Greenwell |
| Bay Area | Alan Cooke | Named Accounts | **Mike Walsh** | Danae Villareal |
| Bay Area | Alan Cooke | Named Accounts | **Joe Miklos** | Danae Villareal |
| Bay Area | Alan Cooke | Named Accounts | **Nico Ochoa** | Madison Taft |
| Bay Area | Alan Cooke | San Mateo / East Bay | **Alan Cooke (temp)** | Isaac Mondesir |
| PNW/MidWest | Haydn Mackay (temp) | Big Sky | **Adam Olson** | Paul Oakley |
| PNW/MidWest | Haydn Mackay (temp) | Named Accounts | **Dee George** | Paul Oakley |
| PNW/MidWest | Haydn Mackay (temp) | Named Accounts | **Joe Drumtra** | Matt Macfarlane |
| PNW/MidWest | Haydn Mackay (temp) | Seattle | **Haydn Mackay (temp)** | Eduardo Gonzales |


##### Public Sector

| Territory                             | Region - SubRegion | Strategic Account Leader        | Inside Sales Rep     |
| :------------------------------------ | :----------------- | :------------------------------ | :------------------- |
| Federal - Civilian-1                  | NORAM - PubSector  | **Philip Nannery**              | Christine Saah       |
| Federal - Civilian-2                  | NORAM - PubSector  | **Susannah Reed**               | Christine Saah       |
| Federal - Civilian-3                  | NORAM - PubSector  | **Kyle Goodwin**                | Bill Duncan          |
| Federal - Civilian-4                  | NORAM - PubSector  | **Russ Wilson**                 | Bill Duncan          |
| Federal - Civilian-5                  | NORAM - PubSector  | **Jim Riley (temp)**            | Bill Duncan          |
| Federal - Civilian-6                  | NORAM - PubSector  | **Jim Riley (temp)**            | Christine Saah       |
| Federal - Civilian-7                  | NORAM - PubSector  | **Jim Riley (temp)**            | Christine Saah       |
| Federal - Civilian-8                  | NORAM - PubSector  | **Jim Riley (temp)**            | Bill Duncan          |
| State and Local (SLED East)           | NORAM - PubSector  | **Carrie David**                | Alexis Shaw          |
| State and Local (SLED West)           | NORAM - PubSector  | **Dave Lewis**                  | Alexis Shaw          |
| Federal - DoD-Air Force-1             | NORAM - PubSector  | **Ralph Kompare**               | Craig Pepper         |
| Federal - DoD-Air Force-2             | NORAM - PubSector  | **Ralph Kompare**               | Craig Pepper         |
| Federal - DoD-Unified Commands        | NORAM - PubSector  | **Ralph Kompare**               | Craig Pepper         |
| Federal - DoD-Navy-1                  | NORAM - PubSector  | **Melissa Ard**                 | Craig Pepper         |
| Federal - DoD-Navy-2                  | NORAM - PubSector  | **Melissa Ard**                 | Craig Pepper         |
| Federal - DoD-Army-1                  | NORAM - PubSector  | **Wes Nagel**                   | Margaret Sheridan    |
| Federal - DoD-Army-2                  | NORAM - PubSector  | **Wes Nagel**                   | Margaret Sheridan    |
| Federal - DoD-Agencies                | NORAM - PubSector  | **Dan Carroll**                 | Margaret Sheridan    |
| Federal - Intel-1                     | NORAM - PubSector  | **Marc Kriz**                   | Jay Jewell           |
| Federal - Intel-2                     | NORAM - PubSector  | **Marc Kriz**                   | Jay Jewell           |
| Federal - Channel (System Integrators)| NORAM - PubSector  | **Mark Robinson**               | Jay Jewell           |


#### Mid-Market Segment
*  The Mid-Market Segment is broken down into two types of territories within their regions, Majors and Minors. Each Major territory is broken down into 5-6 Minor Territories. The following maps and tables break down the regions into their respective Majors and Minors and how they pertain to ownership.

##### Mid-Market Majors

![](/images/handbook/business-ops/FY20_Q1_NORAM_MMmajors.png)
![](/images/handbook/business-ops/FY20_Q1_NORAMCalifornia_MM.png)

| Territory       | Mid-Market Account Manager           |
| :-------------- | :----------------------------------- |
| Northeast^      | Alyssa Belardi                       |
| Great Lakes^    | *Great Lakes Minors Map and table*   |
| Southeast^^     | Jeff Lackey                          |
| Mountain        | Matthew Doerfler                     |
| Northwest       | *Northwest Minors Map and table*     |
| Southwest       | *Southwest Minors Map and table*     |

* ^ **Northeast** includes New York City only, the rest of NY State belongs to Great Lakes      
* ^^ **Southeast** includes all of Mexico, South America down to the Falkland Islands       

| Zip Code Format     | Territory |
| :------------------ | :-------- |
| 940xx-961xx^^^      | Northwest |
| 943xx, 950xx, 951xx | Southwest |
| 900xx-939xx         | Southwest |

* ^^^ **Northwest** is range of all zip codes that start with the format of 940xx, 941xx, 942xx between 940xx and 961xx. The exceptions are highlighted in the table above.      

##### Mid-Market Minors
![](/images/handbook/business-ops/FY20_Q1_NORAM_MMminors.png)
* Please refer to the Minors details below
* Northeast, EMEA and APAC Minors to come

**Great Lake Minors**  

| Great Lakes Minor | Mid-Market AE    | Territory Code |
| :---------------- | :--------------- | :------------- |  
| MN-IL^            | Matthew Doerfler | MM-GL-MnIL     |
| Chicago^          | James Komara     | MM-GL-Chicago  |
| WI-PA^^           | Jeff Lackey      | MM-GL-WiPa     |
| Ontario           | Philip Camillo   | MM-GL-Ontario  |
| NY+Phili^^^       | Alyssa Belardi   | MM-GL-NYPhili  |

* ^ **Chicago** Chicago is it's own minor and is composed of all Zips that start with: 600,601,602,603,604,605,606,607,608     
* ^ **MN - IL** Is proportioned to be the size of two Minors (to be split at a future date)       
* ^^ **Philadelphia** Philadelphia is a part of the NY+Phili Minor. Phili is composed of all Zips that start with: 175,176,180,189,190,191,193,194,195,196        
* ^^^ **NYC** is a part of the [Northeast Major](#mid-market-majors)       

**Northwest Minors**  

| Northwest Minor   | Mid-Market AE   | Territory Borders                                               | Territory Code   |
| :---------------- | :-------------- | :-------------------------------------------------------------- | :--------------- |  
| Minor-1           | Philip Camillo  | CA Zips start with-942,944-949,952,953-961                      | MM-NW-Jefferson  |
| Minor-2           | Leigh Motheral  | Washington and Alaska                                           | MM-NW-WaAk       |
| Minor-3           | Philip Camillo  | CA Zips start with-940                                          | MM-NW-Peninsula  |
| Minor-4           | Philip Camillo  | CA Zips-94102, 94103, 94104, 94105, 94107                       | MM-NW-DowntownSF |
| Minor-5/6         | Leigh Motheral  | CA Zips-94108, 94110, 94111, 94114, 94117, 94118, 94129, 94133, 94158   | MM-NW-SF         |

**Southwest Minors**  

| Southwest Minor     | Mid-Market AE | Territory Borders                                             | Territory Code      |
| :------------------ | :------------ | :------------------------------------------------------------ | :------------------ |  
| AZ,NV,HI Greater LA | James Komara  | Arizona, Nevada, Hawaii & CA Zips Start in range of 902-912   | MM-SW-HANLA         |
| LA Core             | James Komara  | CA Zips Start with 900                                        | MM-SW-LA            |
| SoCal/Irvine        | Rashad Bartholomew  | CA Zips Start in range of 913-926                             | MM-SW-IrvineSoCal   |
| Thousand Oaks       | James Komara  | CA Zips Start in range of 927-939, CA Zip Starts with 950     | MM-SW-Thousand Oaks |
| Palo Alto           | James Komara  | CA Zip Starts with 943,951                                    | MM-SW-Palo Alto     |

**Mountain Minors**  

| Southwest Minor     | Mid-Market AE | Territory Borders                                                                          | Territory Code      |
| :------------------ | :------------ | :----------------------------------------------------------------------------------------- | :------------------ |
| Utah                | Matt Doerfler | Utah                                                                                       | MM-Mt-Utah          |
| Greater Austin      | Matt Doerfler | Texas Zips that start with 787, Excluding 78700-78705                                      | MM-Mt-Grater Austin |
| West/TX             | Matt Doerfler | Montana, New Mexico, Wyoming, Texas^ (See Texas exclusions)                                | MM-Mt-Wext/TX       |
| East/Dallas         | Scott Larson  | Kansas, Nebraska, North Dakota, Oklahoma, South Dakota, Texas Zips that Start with 750XX   | MM-Mt-East/Dallas   |
| IdOr                | Matt Doerfler | Idaho, Oregon                                                                              | MM-Mt-IdOr          |
| CoCa                | Matt Doerfler | Colorado, Alberta, British Columbia, Manitoba, Saskatchewan, All 3 Canadian Territories    | MM-Mt-CoCa          |

* ^ **Texas Exclusions** See Greater Austin and East/Dallas Minors for the portions of Texas that are not included in the territory 

**Southeast Minors**  

| Southwest Minor     | Mid-Market AE | Territory Borders                                                                         | Territory Code      |
| :------------------ | :------------ | :---------------------------------------------------------------------------------------- | :------------------ |
| LATAM               | Jeff Lackey   | All LATAM countries                                                                       | MM-SE-LATAM         |
| West                | Jeff Lackey   | Alabama, Arkansas, Louisiana, Mississippi, Missouri, North Carolina, Puerto Rico, South Carolina | MM-SE-West          |
| East                | Jeff Lackey   | Delaware, Kentucky, Maryland, Tennessee, Virginia, West Virginia                               | MM-SE-East          |
| NJ/DC               | Jeff Lackey   | New Jersey, District of Columbia                                                          | MM-SE-NJ/DC         |
| PeterTampa          | Jeff Lackey   | FL Zips starting with and including 335XX-349XX                                           | MM-SE-PeterTampa    |
| SunshinePeach       | Jeff Lackey   | Georgia & Florida^ (^Excluding PeterTampa Minor)                                          | MM-SE-SunshinePeach |

**EMEA/APAC Territory Codes**

| Territory       | Territory Code       |
| :-------------- | :------------------- |
| BeNeLux         | MM-EMEA-NorBeNeLux   |
| DACH            | MM-EMEA-DACH         |
| Eastern Europe  | MM-EMEA-EEMEA        |
| France          | MM-EMEA-South Europe |
| MEA             | MM-EMEA-EEMEA        |
| Nordics         | MM-EMEA-NorBeNeLux   |
| Southern Europe | MM-EMEA-South Europe |
| UK&I            | MM-EMEA-UKI          |
| APAC (Region)   | MM-APAC              |


### Small Business Segment
TBA - Maps for SMB Segment


#### World

| Territory    | Region | Small Business     |
|:------------ | :----- | :------------- |
| Brazil       | LATAM  | Romer Gonzalez |
| SoCenAmer    | LATAM  | Romer Gonzalez |

#### APAC 

| Territory    | Region | Small Business  |
| :----------- | :----- | :------------- |
| ANZ          | APAC   | Michael Miranda    |
| Central Asia | APAC   | Michael Miranda    |
| China        | APAC   | Michael Miranda    |
| Japan        | APAC   | Michael Miranda    |
| Korea        | APAC   | Michael Miranda    |
| SE Asia      | APAC   | Michael Miranda    |
| South Asia   | APAC   | Michael Miranda    |

#### EMEA

| Territory       | Region | Small Business      |
| :-------------- | :----- | :------------------ |
| BeNeLux         | EMEA   | Vilius Kavaliauskas |
| DACH            | EMEA   | Vilius Kavaliauskas |
| Eastern Europe  | EMEA   | Vilius Kavaliauskas |
| France          | EMEA   | Ross Mawhinney |
| MEA             | EMEA   | Vilius Kavaliauskas |
| Nordics         | EMEA   | Vilius Kavaliauskas |
| Southern Europe | EMEA   | Ross Mawhinney |
| UK&I            | EMEA   | Vilius Kavaliauskas |


#### NORAM

| Territory       | Small Business         |
| :-------------- | :----------------------------------- |
| Northeast       | Michael Miranda                      |
| Great Lakes     | Michael Miranda                      |
| Southeast       | Kaley Johnson                      |
| Mountain        | Adam Pestreich                       |
| Northwest (excluding Washington)       | Marsja Jones                       |
| Southwest (excluding Texas)| Adam Pestreich          |
| Washington           | Brooke Williamson                  |
| Texas           | Brooke Williamson                  |


