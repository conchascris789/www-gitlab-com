---
layout: markdown_page
title: "KPI Index"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Updating

1. This page is the list of [KPIs](/handbook/ceo/kpis/) and links to the definitions in the handbook.
1. In order to maintain a [SSOT](/handbook/handbook-usage/#style-guide-and-information-architecture), KPI definitions live only in the functional section in the handbook, as close to the work as possible. So define performance indicators in the relevant page (for example the adoption page that also contains how the team works and has relevant videos) instead of a (K)PI page.
1. The Data Team is responsible for maintaining this page.
1. If a KPI is not defined, create an MR to propose a definition and ask the appropriate business stakeholder to review and merge.
1. If you remove, add, or change the essence of a KPI, the merge request needs to be approved by the function head and the CEO. Last person approving should merge the MR.

## Working with the Data Team

Please see [the Data Team Handbook](/handbook/business-ops/data-team/).

## GitLab KPIs

GitLab KPIs are duplicates of goals of the reports further down this page.
GitLab KPIs are the most important indicators of company performance.

1. [IACV](/handbook/sales/#incremental-annual-contract-value-iacv) vs. plan > 1 (P0)
1. [Capital Consumption](/handbook/finance/accounting/#capital-consumption) < Plan (P1)
1. [Sales efficiency](/handbook/sales/#sales-efficiency-ratio) > 1 (P0)
1. [Pipe generated](https://about.gitlab.com/handbook/sales/#pipeline-generation) vs. plan > 1 (P0)
1. [Wider community merged MRs per release](/handbook/marketing/community-relations/#community-relations-kpis) (dependent on GitLab.com data) (out of scope for Q2)
1. [LTV / CAC](https://about.gitlab.com/handbook/sales/#ltv-to-cac-ratio) ratio > 4 (Out of Scope for Q2)
1. [Candidate ASAT](/handbook/hiring/metrics/#candidate-nps) >4.1 (out of scope for Q2)
1. [Hires vs. plan](/handbook/hiring/metrics/#hires-vs-plan) > 0.9  (P0)
1. [12 month team member turnover](/handbook/people-operations/people-operations-metrics/#team-member-turnover) < 16% (Bamboo) (P0)
1. [Merge Requests per release per engineer in product development](/handbook/engineering/performance-indicators/#average-mrs-dev-month) > 10 (GitLab.com) (P2)
1. [Uptime GitLab.com](/handbook/engineering/performance-indicators/#gitlab-com-availability) > 99.95% (dependent on PD) (out of scope for Q2)
1. [Active users per hosting platform](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25237/diffs) (out of scope for Q2)
1. [Support Satisfaction](/handbook/support/#customer-satisfaction-ssat) (P2)
1. [Runway](/handbook/finance/operating-metrics/#cash-burn-average-cash-burn-and-runway) > 12 months (P1)
1. [MAUI](http://www.meltano.com/docs/roadmap.html#maui) (Meltano so not part of the GitLab Executive Team KPIs) > 10% WoW

## Sales KPIs

1. [IACV](/handbook/sales/#incremental-annual-contract-value-iacv) vs. plan > 1 (P0)
1. [Field efficiency ratio](/handbook/sales/#field-efficiency-ratio) > 2 (P0)
1. [TCV](/handbook/sales/#total-contract-value-tcv) vs. plan > 1 (P0)
1. [ARR](/handbook/sales/#annual-recurring-revenue-arr) YoY > 190% (P0)
1. [Win rate](/handbook/sales/#win-rate) > 30% (P1)
1. [% of ramped reps at or above quota](/handbook/sales/#quota-ramp) > 0.7 (P1)
1. [Net Retention](/handbook/customer-success/vision/#measurement-and-kpis) > 2 (P0)
1. [Gross Retention](/handbook/customer-success/vision/#measurement-and-kpis) > 0.9 (P0)
1. Rep [IACV  per comp](https://about.gitlab.com/handbook/sales/#measuring-sales-rep-productivity) > 5 (P1)
1. [ProServe](/handbook/customer-success/professional-services-engineering/#long-term-profitability-targets) revenue vs. cost > 1.1 (P1)
1. [Services attach rate for strategic](/handbook/customer-success/professional-services-engineering/#Services-Attach-Rate-for-Strategic-Accounts) > 0.8 (P1)
1. [Self-serve sales ratio](/handbook/sales/#self-serve-sales-ratio) > 0.3 (P1)
1. [Licensed users](/handbook/sales/#licensed-users) (P1)
1. [ARPU](/handbook/sales/#revenue-per-licensed-user-also-known-as-arpu) (P0)
1. [New hire location factor](/handbook/people-operations/global-compensation/#location-factor) < 0.72 (P0)

## Marketing KPIs

1.	[Net New Business Pipeline Created ($s)](/handbook/marketing/revenue-marketing/#revenue-marketing-kpi-definitions) v Plan > 1 (P0)
1.	[Pipe-to-spend per marketing activity](/handbook/marketing/revenue-marketing/#revenue-marketing-kpi-definitions) > 5  (P0)
1.	[Marketing efficiency ratio > 2](/handbook/finance/operating-metrics/#marketing-efficiency-ratio) - P0
1.	[Opportunities per XDR per month created](/handbook/marketing/revenue-marketing/#revenue-marketing-kpi-definitions) (P0)
1.	[LTV / CAC](/handbook/sales/#ltv-to-cac-ratio) >4 (P1)
1.  [Social Media Follower Growth](/handbook/marketing/revenue-marketing/digital-marketing-programs/#digital-marketing-kpis) (out of scope for Q2)
1.  [New Unique Web Visitors (about.gitlab.com)](/handbook/marketing/revenue-marketing/digital-marketing-programs/#digital-marketing-kpis) (Out of Scope)
1.	[Total Web Sessions (about.gitlab.com)](/handbook/marketing/revenue-marketing/digital-marketing-programs/#digital-marketing-kpis) (Out of Scope)
1.	[Meetups per month](/handbook/marketing/community-relations/#community-relations-kpis) (out of scope for Q2)
1.  [Wider Community merged MRs per release](/handbook/marketing/community-relations/#community-relations-kpis) (out of scope for Q2)
1.	[Community Channel Response Time](/handbook/marketing/community-relations/#community-relations-kpis) (out of scope for Q2)
1.	[New hire location factor](/handbook/people-operations/people-operations-metrics/#average-location-factor) (P0)
1.	[Pipeline coverage:2X for current quarter, 1X for next quarter, and .5 for 2 QTRs out](https://about.gitlab.com/handbook/sales/#pipeline-coverage-ratio) (P1)
1.	[Total # of MQLs by month](/handbook/marketing/revenue-marketing/#revenue-marketing-kpi-definitions) (P0)
1.	[Lead follow-up response time](/handbook/marketing/revenue-marketing/#revenue-marketing-kpi-definitions) (P1)
1.	[Qty. of Case Studies published per month](/handbook/marketing/product-marketing/#product-marketing-kpis) (Out of Scope)

## People Group KPIs

1. [Hires vs. plan](/handbook/hiring/metrics/#hires-vs-plan) > 0.9 (P0)
1. [Apply to Offer Accept (Days)](/handbook/hiring/metrics/#apply-to-offer-accept-days) < 30 (P0)
1. [Offer acceptance rate](/handbook/hiring/metrics/#offer-acceptance-rate)> 0.9 (P1)
1. [Candidate ASAT](/handbook/hiring/metrics/#candidate-nps) > 4.1 (out of scope for Q2)
1. [Average location factor](/handbook/people-operations/people-operations-metrics/#average-location-factor) < 0.65 (P0)
1. [Percent of team members over compensation band](/handbook/people-operations/people-operations-metrics/#percent-over-compensation-band) = 0% (Out of scope for Q2)
1. [New hire location factor](/handbook/hiring/metrics/#new-hire-location-factor) < 0.72 (P0)
1. [12 month team member turnover](/handbook/people-operations/people-operations-metrics/#team-member-turnover) < 16% (P0)
1. [12 month voluntary team member turnover](/handbook/people-operations/people-operations-metrics/#team-member-turnover) < 10% (P0)
1. [Candidates Sourced versus Candidates Hired](/handbook/hiring/metrics/#candidates-sourced-vs-candidates-hired) > 0.28 (P1)
1. [Onboarding TSAT](/handbook/people-operations/people-operations-metrics/#onboarding-enps) > 4 (out of scope for Q2)
1. [Diversity lifecycle: applications, recruited, interviews, offers, acceptance, retention](/handbook/hiring/metrics/#diversity-lifecycle-applications-recruited-interviewed-offers-extended-offers-accepted-and-retention) (out of scope for Q2)
1. [PeopleOps cost per team member](/handbook/people-operations/people-operations-metrics/#cost-per-team-member) (out of scope for Q2)
1. [Discretionary bonus](/handbook/incentives/#discretionary-bonuses) per employee per month > 0.1 (P1)

## Finance KPIs

1. [IACV](/handbook/sales/#incremental-annual-contract-value-iacv) per [capital consumed](/handbook/finance/accounting/#capital-consumption) > 2 
2. [Sales efficiency](/handbook/sales/#sales-efficiency-ratio) > 1.0 
3. [Magic number](/handbook/sales/#magic-number) > 1.1 
4. [Gross margin](/handbook/finance/financial-planning-and-analysis/#long-term-profitability-targets) > 0.85 
5. [Average days of sales outstanding](/handbook/finance/accounting/#days-sales-outstanding-dso) < 45 
6. [Average days to close](/handbook/finance/accounting/#month-end-review--close) < 10 
7. [Runway](/handbook/finance/accounting/#cash-burn-average-cash-burn-and-runway) > 12 
8. [New hire location factor](/handbook/hiring/metrics/#new-hire-location-factor) < 0.69 
9. [ARR by annual cohort](/handbook/sales/#arr-cohort)
10. [Reasons for churn](/handbook/customer-success/vision/#measurement-and-kpis)
11. [Reasons for net expansion](/handbook/customer-success/vision/#measurement-and-kpis)
12. [Refunds processed as % of orders](/handbook/support/workflows/verify_subscription_plan.html#refunds-processed-as--of-orders)
13. [Capital Consumption](/handbook/finance/accounting/#capital-consumption) < Plan 

## Product KPIs

1. [Activation](/handbook/product/metrics/#activation) 
1. [Adoption](/handbook/product/metrics/#adoption) 
1. [Upsell](/handbook/product/metrics/#upsell) 
1. [Retention](/handbook/product/metrics/#retention) 
1. [Acquisition decision velocity](/handbook/acquisitions/) > 1 per month (out of scope for Q3)
1. [Acquisition success](/handbook/acquisitions/) > 1 new [category](/handbook/product/categories/) shipped per acquistion after 3 months (out of scope for Q3)
1. Product Satisfaction (PSAT) > 4.0 (out of scope for Q3)
1. [Average stages per user](/handbook/product/metrics/#average-stages-per-user)
1. [New hire location factor](/handbook/hiring/charts/engineering-function/) < 0.xx (P1)
1. [Category Maturity Achievement](/handbook/product/metrics/) >70% achievement per quarter

## Support KPIs
1. [Support Satisfaction (SSAT)](/handbook/support/#support-satisfaction-ssat) (P2)
1. [Priority Support Service Level Agreement (SLA)](/handbook/support/#service-level-agreement-sla)  (P2)
1. [Support cost](/handbook/support/) vs. [ARR](/handbook/sales/#annual-recurring-revenue-arr); Also referred to as [Customer Support Margin](/handbook/support/#customer-support-margin) (P0)
1. [ARR](/handbook/sales/#annual-recurring-revenue-arr) per [support team member](/handbook/support/) > $1.175M (P0)
1. [Average daily tickets closed per Support team member](/handbook/support/#monthly-tickets-per-support-team-member) (P1)
1. [Manager to customer support rep ratio](/handbook/support/#customer-support-gearing-ratios)  < 10:1
1. [New hire location factor](/handbook/hiring/charts/engineering-function/) < 0.xx (P1)

## Engineering KPIs

1. [Merge Requests per month per engineer in product development](/handbook/engineering/performance-indicators/#average-mrs-dev-month) > 10 (P2)
1. [Days to merge](/handbook/engineering/performance-indicators/#mean-time-to-merge-mttm) < 7
1. [Uptime GitLab.com](/handbook/engineering/performance-indicators/#gitlab-com-availability) > 99.95% (out of scope for Q2)
1. [Performance GitLab.com](/handbook/engineering/performance-indicators#gitlab-com-performance) (out of scope for Q2)
1. [Days to fix S1/2/3 security issues](/handbook/engineering/performance-indicators/#mttm-mean-time-to-mitigation-for-s1-s2-s3-security-vulnerabilities) (P1)
1. [GitLab.com infrastructure cost per MAU](/handbook/engineering/performance-indicators/#infrastructure-cost-per-gitlab-com-user) (P0)
1. [New hire location factor < 0.58](/handbook/engineering/performance-indicators/#average-location-factor) (P1)
1. [Public Cloud Spend](/handbook/engineering/performance-indicators/#infrastructure-cost-vs-plan) (P0)

## Alliances KPIs

1. [Active SMAU](/handbook/alliances/#alliances-kpis) (out of scope for Q2)
1. [Active installations per hosting platform](/handbook/alliances/#alliances-kpis) (out of scope for Q2)
1. [Product Downloads](/handbook/alliances/#alliances-kpis) (out of scope for Q2)
1. [New hire location factor](/handbook/hiring/metrics/#new-hire-location-factor) < 0.82 (P0)

## GitLab Metrics

We share a spreadsheet with investors called "GitLab Metrics."

## Satisfaction

We do satisfaction scope on a scale of 1 to 5 how satisfied people are with the experience.
We don’t use NPS since that cuts off certain scores and we want to preserve fidelity.
We have the following abbreviation letter before SAT, please don’t use SAT without letter before to specify it:

- P = product (would you recommend GitLab the product)
- T = team-members (would you recommend interviewing here)
- A = applicants (would you recommend applying here)
- S = support (would you recommend our support followup)
- C = unused since customer is ambiguous (can mean product or support, not all users are customers)
- E = unused since employee is used by other companies but not by us
