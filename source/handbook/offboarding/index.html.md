---
layout: markdown_page
title: "GitLab Offboarding"
---

## On this page
{:.no_toc}

- TOC
{:toc}

Should a new hire never start on their first day and either let us know they will not be starting before or soon after their first day that will not be joining, this is not seen as a termination but instead should be deleted entirely from BambooHR. The People Operations Specialist will inform the [team member with ownership rights](/job-families/people-ops/director-people-ops/) of BambooHR to delete this profile entirely. 

## Voluntary Terminations

A voluntary termination occurs when a team member informs his or her manager of a resignation.

If you are a current team member and you are thinking about resigning from GitLab, we encourage you to speak with your manager, People Ops, or another trusted team member to discuss your reasons for wanting to leave. At GitLab we want to ensure that all issues team members are facing are discussed and resolved to result in a great work environment.

If resignation is the only solution after you have discussed your concerns, then please follow these procedures.

### Procedures

1. Team members are requested to provide an agreed upon notice of their intention to separate from the company to allow a reasonable amount of time to transfer ongoing workloads.
1. The team member should provide a written resignation letter or email notification to their manager.
1. Upon receipt of the resignation, the manager will notify the People Business Partner (PBP) by sending a copy of the resignation email/letter. A discussion with the  manager and PBP should also happen if needed to determine what lead up to the resignation.
1. PBP will forward the resignation email to the people operations email inbox. If the team member has a contract with a co-employer, people operations should forward the email to the contact at the co-employer.
1. The PBP (or another member of the People Operations Team) will organize the offboarding, depending on the location of the team member it may mean that another member of the people operations team handles the offboarding. This can be done by one of the People Operations Specialist or People Operations Manager.  Collectively known as "People Ops" for the purposes of the following points:
1. The PBP will post a message in the #terms confidential channel in Slack and provide the name and last day of the individual. The Payroll and Payments Lead should be `@`.
1. After the PBP and Manager have reviewed the circumstances of the resignation, the team member may take the opportunity before the end of their notice period to share their decision to leave during a Company Call.
1. After People Operations is made aware of the upcoming resignation an acknowledgement letter or email (email only for contractors or GitLab Inc team members) confirming last day, the name of the POBP, and details of the [exit survey](#exit-survey). An [example email](https://docs.google.com/document/d/1rZvczqEuyyFDAzjheiF4LpcLuvj9fFd6maPOSDwkYpQ/edit) is available to use for this.
1. If the individual has a statutory holiday allowance, the holiday taken should be confirmed with the manager and team member via email and then filed in BambooHR. Once that has been done an acknowledgement letter can be prepared.  This will depend on the location and employment status of the team member. Examples of these can be found here:
 - [GitLab Ltd (UK) Resignation Acknowledgement](https://docs.google.com/document/d/1zV1qnZmjQaNZ3QUjrLOtod-FbboNCPmYdqCLIAc7aos/edit)
 - [GitLab BV (Netherlands) Resignation Acknowledgement](https://docs.google.com/document/d/1-9hCL2Xs5po4lZ19L2vrcmGxQIYRD-Emyci3F63kt0c/edit)
1. For GitLab UK team members, payroll can pay up to and including the 5th day of the following month. For example: If someone's last day is February 2nd, the team member can receive their final pay for this in January's payroll.
1. Once the acknowledgement email or letter is sent, People Ops will send the exit survey to the team member to complete.
1. People Ops will create an [offboarding issue](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/offboarding.md) on or before the last day.


### Exit Survey

The exit survey provides team members with the opportunity to freely express views about working at GitLab. People Operations will send the [CultureAmp](https://www.cultureamp.com/) survey to all team members who are leaving voluntarily. The PBP & the team member may proactively set up time to discuss their responses and ask for further information.  Exit interviews are not mandatory, however your input will help provide GitLab with information regarding what is working well and what can be improved.


### GitLab Alumni Program

All voluntary terminations will be added to a Slack Channel `gitlab-alumni`. The purpose of this channel is to network with team members who have voluntarily left the company to preserve the relationships built while at GitLab. The channel is voluntary to all former and current team members.

GitLab, the company, monitors the channel and can remove people from it at their sole discretion.

## Involuntary Terminations

Involuntary termination of any team member is never easy. We've created some guidelines and information to make this process as humane as we can. Beyond the points outlined below, make sure to refer to our guidelines on [underperformance](/handbook/underperformance), as well as the [offboarding issue](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/offboarding.md).

### Overall process

Ideally, the manager and the team member have walked through the guidelines on [underperformance](/handbook/underperformance) before reaching this point.

1. Manager: Reach out to the appropriate People Business Partner (PBP) for assistance. PBP will ask about what the performance issues have been, how they have been attempted to be addressed, and will then prepare for the termination.
1. If the team member is employed with a co-employer, that co-employer should be consulted as they can provide advice on the appropriate procedure to follow.
1. PBP: Notify the Director of Legal Affairs, Director of Security, the CPO and payroll of the termination. This can be done via the confidential #terms channel but depending on the situation it may be necessary to discuss this via direct message or video call.
  * Payroll should be notified due to final payment regulations based on each state or country.
1. PBP: With manager and/or CPO determine if a [separation and release of claims agreement](#Separation-and-Release-of-Claims-Agreements) is appropriate. If so, have it prepared prior to the call.
1. PBP: With manager discusses with the Executive responsible for final approval.  If approved, the CEO is included as a cc. 
1. Manager and PBP: Discuss best mode of communicating the bad news to the team member. This discussion can happen via a private chat-channel, but it is best to be done via  video. Set up a private chat-channel in any case, since this is also useful to have during the eventual call with the affected team member. Another member of the People Operations Team should also be added to this channel as they will be responsible for handling the offboarding Issue tasks during the call. This can be the People Operations Specialist or People Operations Manager. Collectively known as "People Ops" for the purposes of the tasks listed below.
1. Manager and PBP: Decide who will handle which part of the conversation, and if desired, practice it.
1. Manager and PBP: If the team member who is being terminated is a people manager a communication plan for the team regarding the departure should be in place before the termination proceeds.  The plan should include identification of an interim leader if possible, scheduled meeting with interim leader, a scheduled team call to announce the departure and the interim leadership, and then an announcement on the company call. Informing the team and answering questions should be the top priority.  No announcement should be made on a company call until a team call has been completed. In most cases a team call can occur the same day of the termination, if necessary the termination can be announced on the company call the following day.
1. Manager and PBP: Decide what offboarding actions need to be taken _before_ the call (e.g. revoke admin permissions), or _during_ the call (e.g. revoke Slack and Gmail access), and which ones can wait until later (see the [offboarding issue](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/offboarding.md) for the full list of actions). Do not create the offboarding issue until _after_ the call, since even confidential issues are still visible to anyone in the team.
1. Manager: Set up a call with the team member in question. Make a separate private calendar event to invite the PBP.
1. On the call: Deliver the bad news up-front, do not beat around the bush and prolong the inevitable pain for everyone involved. A sample leading sentence can be "Thanks for joining the call, ____ . Unfortunately, the reason I wanted to speak with you is because we have decided that we have to let you go and end your employment / contract with GitLab." At this point, hand over the call to PBP to continue. The Manager will make it clear that the decision is final, but will also explain what led to this decision and will point to the process that was followed to reach this decision. PBP will also make it clear that the decision is final, but also will genuinely listen to their side of the story since there may be useful lessons in what they say for the rest of the team e.g. regarding hiring and vetting practices.
1. PBP: Make sure to communicate the [practical points](#offboarding-points) from the termination memo outlined below.
1. People Ops: Create the [offboarding issue](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/offboarding.md) and complete all relevant tasks.
1. People Ops: If an employee, review if any time off needs to be paid on the last paycheck by looking in BambooHR under Time Off.
1. People Ops: If applicable, sign the team member up for a [Mentat](https://thementat.com/) subscription. Credentials, licenses, and pricing are in the People Ops 1Password vault.
1. People Ops: If appropriate (default is that it is appropriate), send a [termination memo](#termination-memo).


### Points to cover during the offboarding call, with sample wording
{: #offboarding-points}

The following points need to be covered for any team member:

1. Final Pay: "Your final check (or invoice period) is for the pay period of X and includes X days of pay”.
1. Company Property: “Please return all property as explained in the handbook, also please delete GitLab’s email connection from your phone”.
1. Business Expenses: “Please create your final expense report to Expensify (for employees), OR, please file any outstanding expenses with your final invoice (for contractors), so these can be reimbursed to you in a timely manner”.
1. Confidentiality and Non-Disclosure: “We know you are a professional, please keep in mind the agreement you signed when you were hired”.
1. If you would like GitLab to share your personal email with the rest of the company, please send an email to People Ops or a farewell message that can be forwarded on your behalf.

The following points need to be covered for US-based employees:

1. COBRA: “Your benefits will cease on last day of the month you are eligible for Consolidated Omnibus Budget Reconciliation Act (“COBRA”), the carrier (Lumity) has been notified and the carrier will send out the paperwork to your home address on file”.
1. PPACA: "You may also be eligible under the Patient Protection and Affordable Care Act (“PPACA”) for subsidized health care options via the marketplace. If you are interested it is important that you sign up with the market place well before the 15th of the month to have coverage for the following month”.
1. HIPAA: " Under the Health Insurance Portability and Accountability Act of 1996 (HIPAA), if you need a certificate of credible coverage please download it from your current carrier's online portal or request it from People Ops”.
1. Unemployment insurance: "It is up to your state's labor agency (in CA: EDD) to decide if you are eligible for unemployment insurance”.
1. Please remember to keep GitLab informed: "If you move I want to be sure your W-2 gets to you at the end of the year. You may also contact X at GitLab (provide phone number and email address) with any other questions that you may have" (consider inviting them to contact you at anytime for any reason).

### Sample termination memo

If appropriate (to be determined by conversation with the manager, the Group Executive, and People Ops), use the following [termination memo](https://docs.google.com/document/d/11Uk8p4VJrLnDD5IAtbTwswPUUEnmeEOazS1kJMhOu70/edit?usp=sharing), which is provided here as an openly viewable Google Doc, but of course needs to be personalized and tailored to each individual's situation. As written, it is applicable to US-based employees only.

### Separation and Release of Claims Agreements

If appropriate (currently only in the case of US based employees, and to be determined by conversation with the manager, the Group Executive, and People Ops) prepare a Separation and Release of Claims Agreement following the steps outlined here. All of these steps are done by People Ops unless specified differently.

1. Prepare the agreement using the standard template.
1. Have the legal team review the agreement.
1. Additional notes:
   1. Separation pay is not paid until the ex-team member signs the document and the revocation period has passed.
   1. Make sure you understand the rules for over 40.
   1. You must use language in your exit meeting that is in no way forceful of the ex-team member to sign the agreement. Use phrasing such as “if you choose to sign”; “you have a right to have legal council review this document before you sign”, etc.

## Departures are unpleasant

As explained briefly in the [offboarding issue](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/offboarding.md), GitLab does not provide any context around why people are leaving when they do. However as mentioned in the [procedures](/handbook/offboarding/#procedures), for voluntary terminations, the team member can share their reasons for leaving if they wish. If they choose not to then we
say: "As of today, X is no longer with GitLab. I would like to take this opportunity to thank X for their contributions and wish them all the best for the future. If you have questions about tasks or projects that need to be picked up, please let me know. If you have concerns, please bring them up with your manager."

If someone is let go involuntarily, this generally cannot be shared since it affects
the individual's privacy and job performance is intentionally kept [between an
individual and their manager](/handbook/general-guidelines/#not-public). Unless it's for extreme behaviour / gross misconduct, the team member will be advised the areas of concern about their performance, given support and guidance on how to improve and provided with an appropriate timeframe to reach and sustain the expectations.

If you are not close to an employee's termination, it may seem unnecessarily swift.  Please remember that these decisions are never made without following the above process to come to a positive resolution first - we need to protect the interests of the individual as well as the company, and terminations are a last resort.  According to our values [negative feedback is 1-1 between you and your manager](/handbook/values/#negative-is-1-1) and we are limited in what we can share about private employee issues.  Please discuss any concerns you have about another employee's termination with your manager or your People Business Partner.

Given the expectations and responsibility that come with a VP and above position, when there is an involuntary termination for one of these positions, additional context for the personnel change will be provided to the organization.  

Silence is unpleasant. It's unpleasant because we are human, which means
that we are generally curious and genuinely interested in the well-being of
our team members.

Is it _more_ unpleasant in a remote setting? Probably not. When people are all in
the same office building, they can "kinda sorta" know what may be coming because
of the grapevine, the water cooler, and so on. When the news hits it might be
less of a shock - only
because of unprofessional behavior in the first place. But at larger companies with
multiple office buildings, departures will tend to come as more of a surprise and
with less context (at least to the people in other buildings).

## Turnover Data

GitLab's [turnover data](https://docs.google.com/a/gitlab.com/spreadsheets/d/1yk_tlu-Qy3j4VbaB8Yt4wMc5Gocxri9cp7F57DVWyYM/edit?usp=sharing) is only viewable internally. This data is updated on a monthly basis by People Operations.

## Offboarding Issue

Before starting an offboarding issue, make sure that the team member's resignation or termination has been discussed and cleared with _at least_ the member of the executive team to whom the team member (in)directly reports.

When it is time for offboarding, [create a new **confidential** issue](https://gitlab.com/gitlab-com/people-ops/employment/issues/new) for former team member using the `offboarding` [template](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/offboarding.md) using the [dropdown](https://docs.gitlab.com/ee/user/project/description_templates.html#using-the-templates), and edit it for applicability to the individual.
Please [update the checklist](https://gitlab.com/gitlab-com/people-ops/employment/edit/master/.gitlab/issue_templates/offboarding.md) as more steps arise.

Note: If the team member is transitioning to a [temporarily positioned contractor](/handbook/general-onboarding/consultants/), please proceed with the full offboarding and create a separate onboarding issue to grant only specific temporary access for what they would need to fulfill their contractual obligations.

### Managing the Offboarding Tasks

#### Returning property to GitLab
{: #returning-property}

As part of offboarding, any GitLab property valued above 1,000 USD needs to be returned to GitLab. One exception to this rule is that the team member can opt to purchase their laptop at a depreciated market price which will be provided by Finance upon request.

To return your laptop to GitLab, please contact itops@gitlab.com immediately upon termination.


#### Expensify

To remove someone from Expensify Log in to [Expensify](https://www.expensify.com/signin) and go to "Settings" in the left sidebar. Select the right policy based upon the entity that employs the team member. Select "People" in the left menu. Select the individual's name and click "Remove".
If the person has a company credit card assigned to them please notify Finance before un-assigning it.

### Evaluation

1. How could this outcome have been avoided?
2. Were there early signs that were missed?
3. In retrospect, what questions should have been asked to bring awareness and
  ownership to performance issues? For example, "How would you compare yourself relative to your peers?" People are surprisingly honest here.

## Announcing Terminations

We strive to maintain personal information regarding all team members private, this includes information regarding a team members voluntary or involuntary departure from GitLab.  However, a manager with the consent and approval of the departing team member can share more details with the GitLab team regarding the decision to leave GitLab.

For a voluntary departure a team member may have chosen to leave for many different reasons, career development, promotion, a new role or career path, dislike remote work, etc. For example, a team member may tell their manager that they really miss being in an office environment and remote work is not suitable for their personality.  Based on that decision they have taken another opportunity that allows them to go to an office.

If the departing team member gives their manager permission to share that information then the manager will share while making the departure announcement on the team call.  We want all GitLab team-members to be engaged, happy and fulfilled in their work and if remote work, the requirements of the job or the role it self are not fulfilling we wish our team members the best of luck in their next endeavor.

Regarding involuntary terminations, certain information can also be shared with the GitLab team regarding the departure.  Some team members do not thrive or enjoy the work that they were hired to do.  For example after starting at GitLab a team member quickly learns they have no desire or interest to learn Git or work with Git.  This doesn't make them a bad person, it just means they don't have the interest for the role and based on that the decision was made to exit the company.  Again, we want all team members to enjoy and thrive in their work and that may or may not be at GitLab.

The departing team member may author a goodbye message for either voluntary or involuntary terminations:

  1. Write your message, for example, "It was a pleasure working with you all. I've decided to pursue an opportunity in a different industry that is more aligned with my interests. I'll be rooting for GitLab from the sidelines. Stay in touch: Cheers!"
  1. Send it to your manager and People Ops representative for approval
  1. The manager can with the permission of the team member post the agreed upon message verbatim as long as the message is deemed appropriate in the company call agenda and slack goodbye announcements
  1. If appropriate, managers are encouraged to thank departing team members for their contributions and wish them the best of luck on their future endeavors

In some instances there will be no further clarification on why a team member has departed, if there are concerns you can address those with your manager. Different levels of transparency will exist based on maintaining respectful treatment for all departures. Having team members leave may be a learning opportunity for some, but should not be a point of gossip for anyone. Managers will need to balance the opportunity for learning with the expectation of privacy and consult their People Business Partner should they have questions.

Transparency is one of our values. In the case of terminations transparency [can be painfully specific, calling out an employee’s flaws, while inviting more questions and gossip](https://outline.com/PTGkER). We opt to share the feedback only with peers and reports of the person since we balance transparency with our value of collaboration and negative is 1-1.

