---
layout: markdown_page
title: Handbook
twitter_image: '/images/tweets/handbook-gitlab.png'
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

The GitLab team handbook is the central repository for how we run the company. Printed, it consists of over [3,000 pages of text](/handbook/about/#count-handbook-pages). As part of our value of being transparent the handbook is [open to the world](https://gitlab.com/gitlab-com/www-gitlab-com/tree/master/source/handbook), and we welcome feedback. Please make a [merge request](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests) to suggest improvements or add clarifications. Please use [issues](https://gitlab.com/gitlab-com/www-gitlab-com/issues) to ask questions.

## CEO

* [CEO](/handbook/ceo/)
  * [Values](/handbook/values/)
  * [CEO Shadow Program](/handbook/ceo/shadow/)
  * [Cadence](/handbook/ceo/cadence/)
  * [Communication](/handbook/communication/)
    * [YouTube](/handbook/communication/youtube/)
  * [E-Group Offsite](/handbook/ceo/offsite/)
  * [Handbook](/handbook/about/)
    * [Handbook Usage](/handbook/handbook-usage/)
    * [Edit This Website Locally](/handbook/git-page-update/)
    * [Handbook Changelog](/handbook/CHANGELOG.html)
    * [Handbook Roadmap](/handbook/about/roadmap/)
  * [KPIs](/handbook/ceo/kpis/)
  * [OKRs](/company/okrs/)
  * [Pricing Model](/handbook/ceo/pricing/)

## People Operations

* [People Operations](/handbook/people-operations)
  * [Labor and Employment Notices](/handbook/labor-and-employment-notices/)
  * [General Guidelines](/handbook/general-guidelines/)
  * [Tools and Tips](/handbook/tools-and-tips/)
  * [Inclusion & Diversity](/company/culture/inclusion/)
  * [Learning & Development](/handbook/people-operations/learning-and-development/)
  * [Gender and Sexual-orientation Identity Definitions and FAQ](/handbook/people-operations/gender-pronouns/)
  * [Unconscious bias](/handbook/communication/unconscious-bias/)
  * [Ally resources](/handbook/communication/ally-resources/)
  * [Code of Conduct](/handbook/people-operations/code-of-conduct/)
    * [Anti-Harassment Policy](/handbook/anti-harassment)
  * [Leadership](/handbook/leadership)
  * [Hiring](/handbook/hiring)
    * [Greenhouse](/handbook/hiring/greenhouse)
    * [Vacancies](/handbook/hiring/vacancies)
    * [Interviewing](/handbook/hiring/interviewing)
    * [Jobs FAQ](/jobs/faq)
  * [Onboarding](/handbook/general-onboarding)
  * [Benefits](/handbook/benefits)
    * [Incentives](/handbook/incentives)
    * [Paid time off](/handbook/paid-time-off)
  * [Offboarding](/handbook/offboarding)
  * [Spending Company Money](/handbook/spending-company-money)
    * [Travel](/handbook/travel)
    * [Visas](/handbook/people-operations/visas/)
  * [Secret Snowflake](/handbook/people-operations/secret-snowflake)

## Engineering

* [Engineering](/handbook/engineering/)
  * [Development Department](/handbook/engineering/development/)
    * [CI/CD](/handbook/engineering/development/ci-cd/)
    * [Defend Section](/handbook/engineering/development/defend/)
    * [Dev Section](/handbook/engineering/development/dev/)
    * [Enablement Section](/handbook/engineering/development/enablement/)
    * [Growth Section](/handbook/engineering/development/growth/)
    * [Ops Section](/handbook/engineering/development/ops/)
    * [Secure Section](/handbook/engineering/development/secure/)
  * [Infrastructure Department](/handbook/engineering/infrastructure/)
  * [Quality Department](/handbook/engineering/quality/)
  * [Security Department](/handbook/engineering/security/)
  * [Support Department](/handbook/support/)
    * [Incident Management for Self-Managed Customers](/handbook/support/incident-management/)
  * [UX Department](/handbook/engineering/ux/)
  * [Security Practices](/handbook/security/)

## Marketing

* [Marketing](/handbook/marketing)
  * [Website](/handbook/marketing/website/)
  * [Blog](/handbook/marketing/blog)
  * [Social Media Guidelines](/handbook/marketing/social-media-guidelines)
  * [Revenue Marketing](/handbook/marketing/revenue-marketing/)
    * [Sales Development](/handbook/marketing/revenue-marketing/xdr/)
    * [Field Marketing](/handbook/marketing/revenue-marketing/field-marketing/)
    * [Digital Marketing Programs](/handbook/marketing/revenue-marketing/digital-marketing-programs/)
        * [Marketing Programs](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/)
  * [Corporate Marketing](/handbook/marketing/corporate-marketing/)
      * [Content Marketing](/handbook/marketing/corporate-marketing/content/)
         * [Editorial](/handbook/marketing/corporate-marketing/content/editorial-team/)
  * [Marketing Operations](/handbook/marketing/marketing-operations/)
  * [Community Relations](/handbook/marketing/community-relations)
  * [Product marketing](/handbook/marketing/product-marketing/)
    * [Technical marketing](/handbook/marketing/product-marketing/technical-marketing)
      * [Demos](/handbook/marketing/product-marketing/demo/)
    * [Partner marketing team](/handbook/marketing/product-marketing/partner-marketing/)
    * [Channel marketing team](/handbook/marketing/product-marketing/channel-marketing/)
    * [Competitive intelligence team](/handbook/marketing/product-marketing/competitive/)
    * [Market research and customer insight team](/handbook/marketing/product-marketing/mrnci/)
      * [Analyst relations (AR)](/handbook/marketing/product-marketing/analyst-relations/)
      * [Customer reference program](/handbook/marketing/product-marketing/customer-reference-program/)
  * [Technical Evangelism](/handbook/marketing/technical-evangelism)
  * [Marketing Career Development](/handbook/marketing/career-development/)

## Sales

* [Sales](/handbook/sales)
  * [Commercial](/handbook/sales/commercial)
  * [Customer Success](/handbook/customer-success/)
  * [Reseller Channels](/handbook/resellers/)
  * Sales Operations - moved to [Business Operations](/handbook/business-ops)
  * [Reporting](/handbook/business-ops/#reporting)
  * [Technical Account Management](/handbook/customer-success/tam/)
  * [Alliances](/handbook/alliances/)

## Finance

* [Finance](/handbook/finance)
  * [Stock Options](/handbook/stock-options)
  * [Board meetings](/handbook/board-meetings)
  * [Business Operations](/handbook/business-ops)
    * [Data Team](/handbook/business-ops/data-team/)
    * [IT Ops Team](/handbook/business-ops/it-ops-team/)
  * [Internal Audit](/handbook/internal-audit/)
    * [Sarbanes-Oxley (SOX) Compliance](/handbook/internal-audit/sarbanes-oxley/)

## Product

* [Product](/handbook/product)
  * [Release posts](/handbook/marketing/blog/release-posts/)
  * [Making Gifs](/handbook/product/making-gifs)
  * [Data analysis](/handbook/business-ops/data-team/#-data-analysis-process)
  * [Technical Writing](/handbook/product/technical-writing/)
  * [Markdown Guide](/handbook/product/technical-writing/markdown-guide/)
  * [Acquisitions](/handbook/acquisitions/)

## Legal

* [Legal](/handbook/legal)
  * [Compliance](/handbook/legal/global-compliance/)

<style>
.md-page h2 i.icon-color {
  color: rgb(107,79,187)
}
.md-page h2:nth-of-type(even) i.icon-color{
  color:rgb(252,109,38);
}
.font-awesome {
  font-size: .70em;
  vertical-align: middle;
  padding-bottom: 5px;
}
ul.toc-list-icons {
  list-style-type: none;
  padding-left: 25px;
}
ul.toc-list-icons li ul {
  padding-left: 25px;
}
ul.toc-list-icons {
  list-style-type: none;
  padding-left: 25px;
}
ul.toc-list-icons li ul {
  padding-left: 35px;
}
ul.toc-list-icons li i,
ul.toc-list-icons li ul li i {
  padding-right: 15px;
  color: rgb(107,79,187);
}
ul.toc-list-icons li:nth-of-type(even) i {
  color:rgb(252,109,38);
}
</style>
