---
layout: markdown_page
title: "Compliance for Your Average Human"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Compliance Topics Demystified 

## 1. Principle of Compliance

I had lunch with Mel Farber the other day.  She is such a healthy person that I thought it would be great to emulate such a pristine lifestyle. 

The waiter took our orders.  

Mel ordered a nice spinach salad.  Protein?  Yes, grilled salmon; complemented with cucumber, radishes, scallions, sprinkled with croutons with a cruet of oil and vinaigrette.  A French baguette on the side.  Mel was ready to embark on her healthy meal.

Me?  I ordered the same thing.  Except…I substituted the grilled salmon with fried chicken.  I replaced the croutons with French Fries and the baguette with biscuits. Oh, yeah, one more substitution - replace the oil and vinaigrette with gravy. Hold the cucumbers, radishes, and scallions.  I can do a spinach salad if modified correctly.

Mel seemed to like her salad.  Mine was okay...except for the spinach.

Sometimes we can find so many loopholes in the rules that we actually alter the underlying principle of the matter…and principles matter.  Everything we do matters. At GitLab, we want you to comply with the policies and procedures because it is the right thing to do; but most importantly, we want you to comply because we value integrity.

Paul Harvey once told an amazing story about a football ploy utilized by Carlisle University in the early 1900’s.  Carlisle was getting ready to play Syracuse.  Carlisle’s Coach Warner was determined to defeat Syracuse so he poured over the rule book to find any loophole he could exploit.  He found one.  Coach Warner had realistic footballs sewn onto the front of every player’s jersey on his team.  The football appliqués were puffed out so they actually appeared 3-dimensional.  The result is that when the game was in play, Syracuse couldn’t tell which Carlisle player actually had the ball.  The deceptive jerseys made it difficult to determine who to tackle.  The result – Carlisle won.

The next game was to be a heated one as it was between Carlisle and Harvard.  The two colleges were rivals and the stakes were high.  Despite the strong rivalry, the coaches respected one another. Harvard’s coach met with Coach Warner and inquired if he planned to have Carlisle don the deceptive uniforms in the upcoming game.  Coach Warner indicated that there were no rules that prevented him from doing so; so, yes, yes his team would be wearing the jerseys.  

Game day came.  Harvard was the home team.  Carlisle was the guest.  Carlisle took the field first in their appliquéd football garb.  Harvard then took the field in their **crimson** colored uniforms. As the home team, Harvard had the duty to bring the game balls.  The refs took the field and grabbed the bag of balls provided by the home team.  They reached in and discovered a bag full of **crimson** colored footballs.  

Harvard won. 

The point of the story is that people who use the rules as the baseline or absolute minimum for integrity are apt to look for loopholes.  Those who follow principles, do not.   Doing the right thing because it is the right thing ultimately wins the long game. 

## 2. GDPR

**GDPR is here…**
The General Data Protection Regulations came into effect in the European Union May, 2018.  Anyone who handles personal information of Europeans must comply.  In the European Union, privacy of personal information is a right – like human dignity, freedom, equality and the opposite of left.  It is a personal right belonging to the owner of the information and cannot be commoditized, utilized, processed without that person’s consent.  The European Union (EU) and European Economic Area (EEA) diligently try to preserve the rights of their citizens but such efforts were without coordinated and express direction – until now.  The GDPR reflects a unified approach to handling personal, sensitive information in the EU/EEA. 

**What is covered by the GDPR?**
The regulation is implemented in all local privacy laws across the entire EU and EEA region. It applies to all companies selling to and storing personal information about citizens in Europe, including companies on other continents (so even offices in Antarctica must comply if they intend to access personal information of Europeans.)  GDPR provides citizens of the EU and EEA with greater control over their personal data and assurances that their information is securely protected. According to the GDPR directive, personal data is any information related to a person such as a name, a photo, an email address, bank details, updates on social networking websites, location details, medical information, financial information or a computer IP address. Ultimately, data that can directly or indirectly identify a person is covered by GDPR.

**What do these regulations involve?** 
There are eight fundamental principles under the GDPR.  Let’s bring it down to a personal application to help navigate. Imagine you left your electronic diary on your Ex’s server.  You remember that diary; the one with all those deep, dark secrets like the time you went into a dressing room and then yelled real loudly, “There’s no toilet paper in here!”?  Those crazy times from your past can haunt you – unless you are European.  Well, they can still haunt you - but not electronically.  You want your diary back, you need it back and you need to know how much damage was done. Congratulations! In our hypothetical, you are European!  (You may be European outside of the hypothetical, as well, but stay with me here.) 

**Applying the GDPR to your diary, what are your rights?**

**The right to access** – Individuals have the right to request access to their personal data and to ask how their data is used by the company after it has been gathered. The company must provide a copy of the personal data, free of charge and in electronic
format if requested. 

* So, your diary is on your Ex's server.  You have the right to see exactly what dates and entries are stored.  And, of equal importance, you want to know, specifically, with whom your Ex shared that information and why. (Details!  You want details!) You would not want to hear a generic answer like “The contents of the diary were shared with the Finance department, the customers at the local pub and numerous bathroom stalls… for everyone’s reading enjoyment.”  You want specificity about what was shared and with whom. And there had better have been a good reason for any sharing that occurred. 

**The right to be forgotten** – Individuals can withdraw their consent from a company to use their personal data; they have the right to have their data erased.

*  You can demand your diary be shredded, burned, flambéed, sautéed with a nice Hollandaise sauce or simply destroyed (for the more likely and less dramatic).  In short, all evidence of the contents of your book must be erased.  There cannot be excerpts of the diary floating about on social media, bathroom walls or billboards.  The existence, use and memory of the information must disappear faster than cash in my wallet.  

**The right to data portability** – Individuals have a right to transfer their data from one service provider to another. And it must happen in a commonly used and machine readable format.

*  Maybe you separated on good terms and you think your Ex’s friend is pretty cute.  You can ask to have your diary delivered to that humorous, single friend in an unaltered, readable format.  Generally, this is a more common request with medical records, academic transcripts and the like but those aren't the subject of this hypothetical.

**The right to be informed** – this covers any gathering of data by companies.  Individuals must be informed before data is gathered. Individuals must opt in for their data to be gathered, and consent must be freely given rather than implied. 

*  If your Ex decides to go find evidence of your past based on your contents, you have to give permission for each planned encounter.  You can allow your Ex to visit your third grade teacher, obtain your 9th grade report card and visit your first childhood address.  Nothing more.  If your Ex decides to get details about your juggling statistics, your Ex crossed the line.  Just because your Ex has your diary- doesn’t mean you intend for the contents to be read, evaluated, processed, extrapolated or shared.  Consent must be specific, clear and express. There can’t be the broad, sweeping comment from your Ex that “I will only share your information on days that start with ‘T’… Today and Tomorrow; and only with people with a pulse.”  There must be clarity in how and with whom the information is shared.

**The right to have information corrected** – Individuals can have their data updated if it is out of date, incomplete or incorrect. 

*  You told your Ex that the diary excerpt from May 16th can be posted on Facebook.  Your Ex posts that you said “I have enough money to live happily the rest of my life...”  You can demand it be corrected to include the last part of the sentence which stated “...if I die on
Thursday.”      

**The right to restrict processing** – Individuals can request that their data is not used for processing. Their record can remain in place, but not be used. 

*  Your diary can stay on your Ex's server but it better not be shared, analyzed or used for anything.  If your Ex decides to compile a little black book blog and wants to invoke a ranking system with stars based on the entries in your diary – you can refuse.   

**The right to object** – this includes the right of individuals to stop the processing of their data for direct marketing. There are no exemptions to this rule, and any processing must stop as soon as the request is received. In addition, this right must be made clear to individuals at the very start of any communication.

*  If you find out your Ex is, in fact, continuing with the little black book blog – you can demand a complete stop.  Your Ex cannot object or try to find some loophole.  The diary is closed.  The conversation is closed.  The end.

**The right to be notified** – If there has been a data breach which compromises an individual’s personal data, the individual has a right to be informed within 72 hours of first having become aware of the breach. 

*  If your Ex left your diary open and unencrypted on a laptop at a cafe, your Ex better tell you quickly and with as much detail as possible of the events surrounding the loss of the sensitive information. Your Ex should expect reprisals for any careless behavior that alerted some third party to your private life.

Marc Antony said in Shakespeare's Julius Caesar, “The evil that men do lives after them.  The good is oft interred with their bones.”  Poor Caesar should’ve waited for the GDPR.  He could have better controlled the information dispersed about him.  Good thing he didn’t have a diary.

## 3. Secrets

I’m only sharing this with you because I trust you.  I know that you know how to keep secrets. Do you realize how many secrets you are entrusted with each day?  There are three primary categories of company secrets that are super special: Trade Secrets, Confidential Information and Personally Identifiable Information (Sensitive Information if you parlez OUS).  Collectively, these things are classified as RED in our Data Classification Policy.  The “Red” designation is for the most sensitive, restricted business information that requires heightened internal controls by limiting access to only those authorized individuals with a need-to-know.  Not everyone gets to know this inside scoop.  

**Trade Secrets** are those extra bits of information that set our company apart – the chocolate chips to our cookies.  It’s that stuff that is so cutting edge that we don’t even want to get a patent on it for fear of letting others know the technology.  If you patent something, the secret ingredients are laid out for the world to see like some crazy technological recipe.  Others are precluded from copying your recipe but once they see the basic ingredients, it doesn’t take long to realize that butter can be replaced with oil;  basic flour and yeast can be replaced with self-rising flour and sugar can be replaced with gummy bears (clearly, you do not want to eat my cooking.)   Competitors quickly mobilize for comparable product offerings.  But! where there is a trade secret, competitors are left scratching their heads. Have you ever wondered what the recipe is for Coca-Cola?  Do you realize that you don’t know the recipe because the employees are very good about protecting its secrets.  There is no patent.  If there was, we would all be drinking knock off Cokes that taste like the real thing.  Once you release a trade secret – there is no protection.  It is all out there for the world to exploit; this is why we have employees sign non-disclosures and occasionally pink swear to protect the information. 

Another set of secrets include **Confidential Information**.  This is more than the general confidential stuff.  This includes the provocative business data that helps us run strategically and includes business development plans/strategies, non-public financial information, third party data and government protected information.  (Trade Secrets technically fall under this category but given its unique stature, it gets celebrity status.)  Failing to protect heightened confidential information could cause all sorts of bad feelings with GDPR, EUCI, SOX, EUEC, DOJ, DOC, SEC, OAIC and others.  In other words, if you fail to execute proper controls and restraint, you will be attacked by nothing short of alphabet soup with a badge. 

Other secrets include **Personally Identifiable Information (PII)** [parfois connu comme Sensitive Information].  This is any information that links a particular piece of sensitive information to a specific person.  For instance, if I tell you someone makes a million dollars a year.  You would probably respond with “Thank you, Captain Obvious.” That statement isn't really PII.  It's rather generic and could apply to anyone (unfortunately, not me.)  If I tell you that Jamie makes a million dollars a year - that information is interesting and even intriguing; but this, in and of itself, in not PII. There are a million Jamies in the world, it could be anyone.  Now, if I told you that Jamie Hurewitz makes a million dollars year, I have now divulged PII.  You are now able to link a specific piece of sensitive information to a particular person.  This information becomes something you can exploit.  Now that you know this information - you are going to want to befriend Jamie, have her take you out to dinner, have her cosign a loan, apply for her position…  She has now been stripped of the privacy that she probably wanted to maintain. If my salary statement were true, I would be in serious trouble. 

There are various types of PII.  There is Personal Health Information (PHI) which includes sensitive information about a person’s health or medical condition; Personal Credit Information (PCI) which includes credit card information; Personally Identifiable Financial Information (PIFI) which includes banking information, account numbers, and account information; PI which is 3.14159265359; and PIE which is a more conservative dessert approach. [The secret is already out on these last two.]  How do you protect PII?  Consider the guidelines in the Data Classification Policy and apply it to all of the PII listed above (except PI whose protection is irrational and PIE whose protection won’t last five minutes unattended in my house).  

Unauthorized disclosure of Red, restricted information, could cause serious adverse impact to GitLab, its clients, business partners, and suppliers.  Your mission, should you decide to accept it…and you should accept it (not really optional), is to ensure proper protection for all data classified as Red.  

As always, feel free to reach out with questions or concerns. This message will self-destruct in five minutes.

## 4. Personal Credit Information (PCI)

My credit card was stolen last week.  My husband isn’t reporting it though because, apparently, the thief spends less than I do.    It’s not that I’m frivolous with my spending, it’s just that there is Nigerian prince that I’ve been helping out.  I don't like to talk about it.  

Credit card information falls into the bucket of Personal Credit Information (PCI).  PCI requires heightened levels of protection - much like Personal Health Information (PHI), Personal Identifiable Information (PII), Personal Financial Information (PFI) and other Sensitive Information.  PCI is restricted information with a data classification of Red. PCI isn’t only governed by law; it is also heavily regulated by the credit card industry. Failure to properly handle credit card or bank information could result in the company not being able to process credit card orders.  Nobody wants to resort to checks – that is sooo 1980’s.  So, please, keep in mind - and apply - the Data Classification requirements for a Red level when dealing with PCI, PHI, PFI, PII, trade secrets and those other juicy bits of information we are bound to protect. 

The fundamental requirements for PCI are:
1. Must have a maintained firewall;
2. Custom passwords and unique security measures;
3. Secure/encrypt cardholder data at rest;
4. Encrypt any cardholder data transmitted;
5. Current, active and updated anti-virus must be installed;
6. Secured systems and applications must be sustained;
7. Access is on a need-to-know basis only;
8. Unique data identifiers are required for anyone who has access to cardholder data;
9. Physical access to cardholder data must be restricted;
10. Access to cardholder data needs to be logged and reported;
11. Ensure that security system and process tests are run frequently; and
12. There should be policies around all of the above.

When dealing with PCI, at no time should an entire credit card number be visible in its full form.  Generally, only the last four numbers of a card should be maintained.  Please make sure that there is no credit information ever jotted down on sticky notes, files, napkins, etc.  If you are aware of any credit card numbers that are not secure, please alert Compliance. 

As a young child, my mother told me I can be anyone I want to be...turns out this is called identity theft.  Don’t go down that road.  Let’s keep things on the up and up.

## 5. Data Protection Impact Assessments or DPIAs

Recently, legislators threw alphabet soup at companies. In short, the EU, under the GDPR, requires DPIAs in order to be an A-OK VIP in the technology world.   In long, the European Union, under the General Data Protection Regulations, require Data Protection Impact Assessments in order to be compliant leaders in the technology world.  Translation: We need to make sure we have our finger on the pulse of any application or system integration involved with GitLab.

While the legal requirements may seem cumbersome, the need is quite apparent.

Consider the controls you would expect from your bank. Do you have locks on the doors? Do you have a safe?  Is the safe code confidential or is posted on a sticky note beside the lock? Can anyone enter the safe or is it limited to select employees?  Just what goes in the safe to begin with? Will there be free lollipops?

You expect a bank to protect your money.  A person’s identity is far more valuable; therefore, should be protected to the highest level.  Therefore any new system or data integration within GitLab must have an analysis conducted to determine if a DPIA is required.  We have combined the analysis and the DPIA to improve efficiency and ensure proper oversight of the sought after application/integration.  

Just like the various assurances with your financial institution, a DPIA ensures all the necessary controls are in place to cover your most important assets - personal information.

## Data Protection Impact Assessment Frequently Asked Questions DPIA FAQ

**What is a Data Protection Impact Assessment (DPIA)?**
A DPIA is an instrument utilized to identify and analyze risks for individuals, which exist due to the use of a certain technology or system by an organization in their various roles (as citizens, customers, patients, etc.). On the basis of the outcome of the analysis, the appropriate measures to remedy the risks should be chosen and implemented. Since the inception of impact assessments there have also been approaches to adapt a model for the area of privacy and data protection." A Process for Data Protection Impact Assessment Under the European General Data Protection Regulation is required. 

**When is a DPIA required?**
A DPIA is only required when the processing is “likely to result in a high risk to the rights and freedoms of natural persons” (GDPR Article 35(1)), but processors must continuously assess the risks created by their processing activities to identify when a type of processing requires them to conduct a DPIA.   The questions in pay to procure process are to assist you in assessing whether a DPIA is required and should be re-evaluated periodically in light of the list of processing operations that the Supervising authority deems subject to the requirement of a DPIA.

The assessment of whether there is a need for a DPIA, as well as any required DPIA should be carried out prior to processing. 

**Why is a DPIA Conducted?**
The GDPR is concerned with "personal data", which it defines as “any information relating to an identified or identifiable natural person (data subject); an identifiable natural person is one who can be identified, directly or indirectly, in particular by reference to an identifier such as a name, an identification number, location data, an online identifier or to one or more factors specific to the physical, physiological, genetic, mental, economic, cultural or social identity of that natural person.” GDPR Article 4.  

The exception to this rule would be if an applicable exception applied. These exceptions are limited and unlikely to apply in most circumstances.  See, GDPR Article 35 (10).

You should limit the collection of the data, consistent with the HIPAA concept of "minimum necessary", to that which is genuinely required for the stated purpose. 

**How is DPIA Conducted?**
The System Owner is required to seek the advice of the Data Protection Officer when carrying out a DPIA.  Also, the System Owner must “seek the views of data subjects or their representatives” (Article 35(9)), “where appropriate”. 

At GitLab, the DPIA process is initiated by the System Owner during the [Procure to Pay Process](https://about.gitlab.com/handbook/finance/procure-to-pay/#procure-to-pay-process), during which the System Owner must complete a [Data Protection Impact Assessment issue template](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/new?issuable_template=Data%20Protection%20Impact%20Assessment). 

The GDPR provides that: 
- those views could be sought through a variety of means, depending on the context (e.g. a generic study related to the purpose and means of the processing operation, a question to the staff representatives, or usual surveys sent to the data System Owner’s future customers) ensuring that the System Owner has a lawful basis for processing any personal data involved in seeking such views. Although it should be noted that consent to processing is obviously not a way for seeking the views of the data subjects; 

- if the data System Owner’s final decision differs from the views of the data subjects, its reasons for going ahead or not should be documented; 

- the System Owner should also document its justification for not seeking the views of data subjects, if it decides that this is not appropriate, for example if doing so would compromise the confidentiality of companies’ business plans, or would be disproportionate or impracticable. 

Specifically, if there are remaining , unacceptable high risks such that,  as a result of the project:  "data subjects may encounter significant, or even irreversible, consequences, which they may not overcome (e.g.: an illegitimate access to data leading to a threat on the life of the data subjects, a layoff, a financial jeopardy) and/or when it seems obvious that the risk will occur (e.g.: by not being able to reduce the number of people accessing the data because of its sharing, use or distribution modes, or when a well-known vulnerability is not patched)". 

**Are there other resources?**
Guidelines on Data Protection Impact Assessment [DPIA] and Determining Whether Processing is "Likely to Result in a High Risk" for the Purposes of Regulation 2016/679. Article 29 Data Protection Working Party (Adopted April 4, 2017).  http://ec.europa.eu/newsroom/article29/item-detail.cfm?item_id=611236 . 

## 6. CAN-SPAM

Since I live in Minnesota, it seems appropriate that I alert people to the nuances of canned spam; but as a lawyer, it is probably more important that I let you know about CAN-SPAM.  One of these items is very salty; the other is meat in a tin.  Controlling the Assault of Non-Solicited Pornography and Marketing Act (CAN-SPAM) came into effect in 2003 and impacts all mass electronic marketing communications (among other things). 

CAN-SPAM sets forth certain requirements that must be honored when sending out mass emails and other marketing materials.  They may seem simple but, much like canned spam, there is more than meets (meats?) the eye. 

*Keep the header honest.*  The reader needs to be able to identify who is sending them messages.  The email address of the sender should properly denote GitLab as the sender of the company material.  

*Keep the subject line honest.*  Don’t mislead the reader or otherwise use deceitful or inaccurate subject lines to compel someone to open the email. Be honest, impactful and short.  You can create urgency but, most importantly, create value.  In fact, the more authentic and clear the email, the less likely it is to be marked as spam or junk mail.

*Admit that it is an ad.*  This doesn’t mean that you have to go over the top and begin each Subject Line with “Hello, I am an advertisement.”  It just means that the sender has to know it is marketing material sent by GitLab. Put it in the subject line or put it in the body - just put it somewhere. 

*Location, location, location.*  Primary rule in real estate and an important rule in spam laws.  There needs to be a valid business address listed in the emails.  This helps ensure that if that Nigerian Prince were to send out an email on GitLab’s behalf - the recipient has a clear and legal point of reference to double check the authenticity of the sender.

*Opt-out options are not oppressive.*  When people receive mail that they don’t want, there must be an easy way to unsubscribe from the lists.  And “easy” means my grandmother should be able to figure it out as quickly as my 15-year old nephew.  In some jurisdictions, every email sent after an opt-out is selected is subject to a fine. 

*Opt-outs need to be quick.*  Ideally, an opt-out will occur automatically or within a business day or two.  If you take more than ten (10) business days to remove an email from a mailing list, things will get messy quickly.   

*Stay Diligent.*  If you use a third party to manage business emails, be aware that GitLab could still be on the hook for any wrongdoing.  Make sure that any mass communications are reviewed by someone knowledgeable in the spam laws.  

We care about our customers and their protection is our focus.  But, if creating transparent and open relationships with our customers isn't your concern, then be aware that failing to comply can cost a lot of money on a per email basis - up to $41,000 per violation.  That adds up quickly.  

The rules are simple and something each one of us would appreciate in our own inbox.    

## 7. Cookies and Consent
 
I was happy to learn that if someone tries to give you a cookie, they have to get your consent.  I was a crazy diet and, despite my attempts to eat healthy, offers of Oreos kept surfacing.  Cookies can be detrimental if not kept under control.
 
Electronic cookies are the same.  
 
Privacy and Electronic Communications Regulations (PECR) sit alongside the Data Protection Act and the GDPR. They give people specific privacy rights in relation to electronic communications.  PECR sets specific rules around marketing, secure communication services and customer privacy (as regards traffic and location data, itemised billing, line identification, directory listings) and - you guessed it - cookies (which is broadly defined).
 
There are important factors to consider with cookies in general. 
If you use cookies you must:
•say what cookies will be sent;
•explain what the cookies will do; and
•obtain consent to store cookies on devices.
 
PECR expands the definition of cookies to include “similar technologies” like fingerprinting techniques. Therefore, unless an exemption applies, any use of device fingerprinting requires the provision of clear and comprehensive information as well as the consent of the user or subscriber.
 
Consent is not required if the cookie is used: “(a) for the sole purpose of carrying out the transmission of a communication over an electronic communications network; or  (b) where such storage or access is strictly necessary for the provision of an information society service requested by the subscriber or user.”
 
Please note that cookies for analytics purposes are not “strictly necessary”. 
 
PECR applies to any technology that stores or accesses information on the user’s device. This could include, for example, HTML5 local storage, Local Shared Objects and fingerprinting techniques.
 
Device fingerprinting is a technique that involves combining a set of information elements in order to uniquely identify a particular device. Examples of the information elements that device fingerprinting can single out, link, or infer include (but are not limited to):
•data derived from the configuration of a device;
•data exposed by the use of particular network protocols;
•CSS information;
•JavaScript objects;
•HTTP header information,
•clock information;
•TCP stack variation;
•installed fonts;
•installed plugins within the browser; and
•use of any APIs (internal and/or external).
 
It is also possible to combine these elements with other information, such as IP addresses or unique identifiers, etc.
 
PECR also applies to technologies like scripts, tracking pixels and plugins, wherever these are used.
 
If you provide cookies that fall in these categories, consent is required.  You never who is on a technological diet.  Ask before you give them a cookie.  







