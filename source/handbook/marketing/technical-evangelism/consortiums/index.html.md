---
layout: markdown_page
title: "Consortium Marketing"
---

## On this page
{:.no_toc}

- TOC
{:toc}

----
### What are Consortiums?

We define Consortiums as (often open source) groups that have come together to further a technology cause. The prototypical Consortium would be the [Linux Foundation (LF)](https://en.wikipedia.org/wiki/Linux_Foundation) which is a non-profit technology consortium founded in 2000 as a merger between Open Source Development Labs and the Free Standards Group to standardize Linux, support its growth, and promote its commercial adoption. It also hosts and promotes the collaborative development of open source software projects. 

### Why is Consortium marketing important?
Consortiums are marketing makers in the enterprise technology ecosystem. The success of the Cloud Native Computing Foundation today, the OpenStack foundation in the past, and many others parallely and before prove the value of a technology thought leadership, branding, and engineering alignment where it makes sense. The Technical Evangelism team focuses on Consortium marketing to bring the GitLab technical perspective to the right conversations. 

### Current Consortiums we work with 
* Linux Foundation - Board seat
* Cloud Native Compution Foundation - Board seat
* FINOS (coming soon)
* Let's Encrypt (coming soon)
* GraphQL (coming soon) 

Would you like to suggest a consortium for us to collaborate with? Email Priyanka Sharma at psharma@gitlab.com 



