---
layout: markdown_page
title: "Influencer Bureau Program"
---

## Goal
The GitLab Influencer Bureau is committed to enabling the mission that “everyone can contribute” by educating, elevating, and celebrating our community of users. GitLab’s global Technical Evangelism Community (TEC) leverages our internal team members, CEO thought leadership platforms, and external Heros, centralizing efforts so that we can achieve and maintain visibility, maximize resource-availability, and leverage the voice of our users.

### What do we do?
The Technical Evangelism Influencer Bureau elevates GitLab’s technical brand and awareness to engage and inspire users and customers of the product. 

It does so by identifying, empowering, training, supporting, collecting, and cataloging the great work put in by GitLab dedicated evangelists (coming soon), subject matter experts, executive team members, and the wider GitLab community.

We will:
1. Identify & own key tech evangelism events and place influencers there
1. Alleviate the CFP and speaker preparation process; 
1. Enable influencers to grow awareness of pillars with speaking, writing, recording opportunities
1. Collaborate with marketing and make accessible their output.

### Technical Evangelism thought leadership pillars
Technical evangelism has identified several key areas where we plan to establish GitLab's thought leadership, leveraging our internal and community experts to increase impressions, engagement, and drive conversation.
![Technical Evangelism thought teadership pillars](/images/handbook/marketing/corporate-marketing/technical-evangelism-thought-leadership-pillars.png)

### Technical Evangelism Influencer Bureau services
In order to achieve the program goals and expand upon the [team's services] (/handbook/marketing/technical-evangelism/#services-we-offer-team-members-and-the-wider-gitlab-community) listed, we have Influencer Bureau-specific offerings that are also available to the greater GitLab community, depending upon time and resources.

#### Calls for proposal
For calls for proposal / papers (CFPs) that pertain to Technical Evangelism's target events, the Technical Evangelism team will manage the process.

![Technical Evangelism pillar event process](/images/handbook/marketing/corporate-marketing/technical-evangelism-pillar-event-process.png)

For individuals who find an event or other contributor opportunity, we also want to empower them to share their voices and unique stories. 

![CFP support request process](/images/handbook/marketing/corporate-marketing/CFP-support-request.png)

### Technical Evangelism Influencer Bureau rubric
Although we aim to support any and all individuals interested in evangelising GitLab, those who measure highly on the Influencer Bureau rubric will have their requests prioritized and opportunities highlighted.

![Technical Evangelism Influencer Bureau rubric](/images/handbook/marketing/corporate-marketing/technical-evangelism-influencer-bureau-rubric.png)

### Partnering across GitLab
Technical Evangelism will work collaboratively across the greater GitLab organization to increase efficacy and impact, reduce redundancy, and elevate key narratives.

The Technical Evangelism team plans to enable an increase and upleveling of technical content posts. We hope to coordinate the production of the following:

* 1 event or contribute article quarterly witht a placed Influencer
* A monthly deep dive blog post, hosted by GitLab
* A bi-weekly technical feature in an article, interview, or op ed 
* Publication of monthly ever-green content, such as webcasts, slideshares, or other repurposing of existing talks
* A portfolio of technical interviewees/ partners that can be utilized for customer meetings, podcasts, and AMAs

We will achieve these goals by partnering closely with PR, Content, Community, and Digital Marketing, opening direct channels of communications with these teams to support them in their goals. We will begin by:

* Compiling and cataloging past talks and content
* Packaging existing resources 
* Training on “everyone can be an evangelist”
* Update and own the “Find a Speaker” page, mapping Influencer Bureau members to Pillars for easy reference

#### Enabling Community
We seek to elevate, strengthen, and broadcast the voice of the user. We want the technical community to not only be excited about GitLab, but also to feel confident and capable of speaking to their usage of and innovation on the GitLab platform. As such, the Technical Evangelism program plans to partner closely with the Community team and their [GitLab Heroes Program] (https://about.gitlab.com/community/heroes/). 

![enabling GitLab Community](/images/handbook/marketing/corporate-marketing/technical-evangelism-community-enablement.png)

Return to the main [technical evangelism page](/handbook/marketing/technical-evangelism/) 