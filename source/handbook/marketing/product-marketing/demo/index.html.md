---
layout: markdown_page
title: "Demos"
---

# On this page
{:.no_toc}

- TOC
{:toc}

# About Demos

There are 3 basic demos types:
* Vision - designed to show where we are going (this might include forward looking ideas not yet developed)
* Use Case Based - designed to show how GitLab solves a particular use case (how we should be selling)
* Feature - designed to enable a deeper dive into details of product features (on prospect request)

These demos come in different formats:
* videos - good for everyone to self-demo by passively watching
* click-throughs (screenshots on slides) - good for disconnected situations (like conferences) and for interactive self demos
* live (instructions for how to set up and run demos live) - enables everyone (especially GitLab sales) to set up and run through the demo live on their own system resources. Allows for spontaneous deep diving - not always a good thing).

# Videos

## Benefits of a Single App (12.1) (Aug 2019)
<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/MNxkyLrA5Aw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

## GitLab in 3 minutes (12.1) (Aug 2019)
<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/Jve98tlZ394" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

## GitLab Overview - Planning to Monitoring (11.3) (Oct 2018) (18 mins)
<figure class="video_container">
 <iframe width="560" height="315" src="https://www.youtube.com/embed/nMAgP4WIcno" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</figure>

## How to set up GitLab groups and projects to run multiple Agile teams with microservices (11.7) (Jan 2019)
<figure class="video_container">
 <iframe width="560" height="315" src="https://www.youtube.com/embed/VR2r1TJCDew" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</figure>

## SAFe and Agile Planning with GitLab (11.7) (Jan 2019)
<figure class="video_container">
 <iframe width="560" height="315" src="https://www.youtube.com/embed/PmFFlTH2DQk" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</figure>

## 2018 Vision Prototype (Feb 2018)
<figure class="video_container">
 <iframe width="560" height="315" src="https://www.youtube.com/embed/RmSTLGnEmpQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</figure>

## Auto DevOps in GitLab 11.0 (June 2018)

<figure class="video_container">
 <iframe width="560" height="315" src="https://www.youtube.com/embed/0Tc0YYBxqi4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</figure>

## GitLab VSM - Issue Boards for Mapping (June 2018)

<figure class="video_container">
 <iframe width="560" height="315" src="https://www.youtube.com/embed/9ASHiQ2juYY" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</figure>

## GitLab VSM - Business Value Monitoring (June 2018)

<figure class="video_container">
 <iframe width="560" height="315" src="https://www.youtube.com/embed/oG0VESUOFAI" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</figure>

## GitLab for the Enterprise (Feb 2018)

<figure class="video_container">
 <iframe width="560" height="315" src="https://www.youtube.com/embed/gcWfUw_Cau4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</figure>

# Click-throughs

Click-through demos can be run in multiple ways:
* [Run them off-line](/handbook/marketing/product-marketing/demo/off-line-click-throughs/) (for conferences, practice on the airplane, etc)
* Run them while online, [from the files](https://drive.google.com/drive/folders/1Qm8Y3oVLRa0nS1BARA631Ex6SKVzYp3C?usp=sharing), same as running a presentation
* Or run them online from the web, using the links bellow:

## Merge Request Approval API (July 2019 - 12.0)

 [<img src="./ct-files/mr-approval.png">](./ct-files/mr-approval.html)


## Agile Project Management (June 2019 - 12.0)

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vR5EWjzgr-Qe-cYdMj4WJnnVDxKyoyirMv0OfOJWbgCzevJoLGTXnbKDYm2TUhpdPAkh-nTcucIgZKx/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

[G-slides file](https://docs.google.com/presentation/d/13Zj83pjpwyq3s4T2fPSTuKO8NwqdCdn827GB7S-3hW8/edit?usp=sharing)

## Cross-project Pipeline Triggering and Visualization (May 2019 - 11.10)

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRZaaHmlJcBQS56SSS1QKyvLJQayJ31ZXqlAJMzQOMckbHt0dSj0KBN2bzg6lwny-lqfvhfhOl7tK8H/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

[G-slides file](https://docs.google.com/presentation/d/1AVYo0HqTrwG59G2a55YzQhbXX2EKIsxQ-9P7-g_2bQc/edit?usp=sharing)  

## Secure Capabilities (March 2019 - 11.8)

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTmtpY92Uun3YsixCJHZu7yt69M9rFB5SuFRSglOBRXoFNTKBdgPZpE4JBTl3LtAAX1zS4zQdBdD6Ga/embed?start=false&loop=false&delayms=60000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>  

[G-slides file](https://docs.google.com/presentation/d/1fdTmdepdaq03OSfcA3pYduDxDnQEyvY4ARPqXEX8KrY/edit#slide=id.g2823c3f9ca_0_9)  
[Sales enablement recording (of shorter version)](https://youtu.be/hwJTiXt5T3w)

## Secure Capabilities (short, guided) (March 2019 - 11.8)

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQF2Neh1h0vFwMapLvhppr_bZZVaxbtnVvTP69xd6YNGreW5dZ43w4w5qQTmYNewmI-3pViilsvbIcX/embed?start=false&loop=false&delayms=60000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
[G-slides file](https://docs.google.com/presentation/d/1cfzdLFWk3hYLw_aocgunVmJCD-TSiOgypr66A_nR8VQ/edit?usp=sharing)  
[Booth Demo Auto Play link](https://docs.google.com/presentation/d/e/2PACX-1vQF2Neh1h0vFwMapLvhppr_bZZVaxbtnVvTP69xd6YNGreW5dZ43w4w5qQTmYNewmI-3pViilsvbIcX/pub?start=true&loop=true&delayms=3000)  
[Sales enablement recording](https://youtu.be/hwJTiXt5T3w)

## Auto DevOps (Oct 2018 - 11.3)

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTNeBj0TJWca1PVwccxUTXWDWYqaIyB6Q1fRYCfjtMzeK8DtpmAcG1o6ipFBi-lhYKVTAA9kYBWyKKu/embed?start=false&loop=false&delayms=600000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
[G-slides file](https://docs.google.com/presentation/d/1oKHU3MsbJmxVQyO-7c6JLMoCOS80uS-0NlcI-mRxAAY/edit?usp=sharing)  
[Sales enablement recording (of shorter version)](https://youtu.be/V_6bR0Kjju8)

## Auto DevOps (short, guided) (July 2018 - 11.0)

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRQ4xltHhYRO_zgbo7exF6BwR09jvPmyFzR4XvjdlpYMRqT4dctx61XCkLjfR-8sq6QyOsoEFBBJjJh/embed?start=false&loop=false&delayms=600000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
[G-slides file](https://docs.google.com/presentation/d/1UkQI_9V-CJZcbZJBDTB7tyOg14XHCKIwNoUHW1K6tC8/edit?usp=sharing)  
[Booth Demo Auto Play link](https://docs.google.com/presentation/d/e/2PACX-1vRQ4xltHhYRO_zgbo7exF6BwR09jvPmyFzR4XvjdlpYMRqT4dctx61XCkLjfR-8sq6QyOsoEFBBJjJh/pub?start=true&loop=true&delayms=3000)  
[Sales enablement recording](https://youtu.be/V_6bR0Kjju8)

## Auto DevOps - Setup (GKE) (Oct 2018 - 11.3)

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQIPrinsvTG1s6ppnUWqSY-fpHnxe6oAwM7g91uBl8Mx3EYQhaejlKUF9_c3GtagIhzwg-8dJlIrNgw/embed?start=false&loop=false&delayms=600000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
[G-slides file](https://docs.google.com/presentation/d/1AGABPlNzMm5-rrYfwGIzueXIbPleVkGpnc2Qk6JtnWk/edit?usp=sharing)

## Auto DevOps - Setup (EKS) (Oct 2018 - 11.3)

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRjhO2VvKf_gWbObwi5APm18gxQrzQk5vvAARD-4vLQeT0NbkrSuP3t4sTVylRYZOD6kINLr37HmtHA/embed?start=false&loop=false&delayms=60000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
[G-slides file](https://docs.google.com/presentation/d/1Ejnho9pqXPj-OHNU2q51cC0xCG5c8pVLmvg-maIA7BQ/edit?usp=sharing)

# Live (instructions)

## [Planning to Monitoring (formerly i2p)](i2p/)
Highlights GitLab’s single platform for the complete DevOps lifecycle, from idea to production, through issues, planning, merge request, CI/CD, and monitoring.

## [CI/CD Deep Dive](cicd-deep/)
Provides a more in-depth look at GitLab CI/CD pipelines.

## [Integration Demos](integrations/)
Demonstrations which highlight integrations between GitLab and common tools such as Jira issues and Jenkins pipelines.

# Conference Booth Setup

We've started using iPad's to run click-through demos at conferences, and also we now have a main screen video to loop that shows our key slides and product clips. You can find details about both of these on the [conference booth setup page](conference-booth-setup/).
