---
layout: markdown_page
title: "PMM Metrics"
---

## Objective:  Manage and track the velocity of the PMM team.

At GitLab, we ship MVC and iterate quickly to deliver value.  The true measure of product marketing is in how effectively customers are able to understand how Gitlab can help solve their problems.  We aspire to measure the value of our activities, but the first start is capturing and tracking our activities. (which hopefully are aligned to business value)

### Approach

We intend to use GitLab to capture key PMM activities / deliverables in order to track, visualize, and measure our velocity.   We will use labels to clearly tag specific issues as they relate to the following high level PMM activities:

For MVC, we'll start with a handful of exclusive TOP level project labels in the [Product Marketing project](https://gitlab.com/gitlab-com/marketing/product-marketing)

| Topic  | Details  |  Label |
|----|---|---| 
| Analyst Relations | briefings, reports, inquiry | pmm::AR |
| Customer Relations | Cab, case studies | pmm::Cust |
| Digital marketing  | blogs, webinars, videos, whitepapers | pmm::Digital |
| Enablement | sales,xdr, partner, etc | pmm::Enable |
| Events  | speaking, booth, writing abstracts, , etc | pmm::Event |
| Research | market research, drafting studies, documenting  | pmm::Research |
| Presentation  |  building, and updating decks pitch deck, product deck, etc    | pmm::Deck |
| PR | briefing, press release, etc  | pmm::PR |
| Sales Support  | Specific sales support meetings, slides,  etc   |  pmm::Sales  |
| Web updates  |  handbook, product page, solution page, etc  |  pmm::Web  |

In the future we MIGHT opt to add more detailed sub categories... but not at first.

- Web updates       (pmm::web)
   - Handbook changes  (pmm::web::Handbook)
   - Product page      (pmm::web::ProdPage)
   - Solution page     (pmm::web::SolPage)
   - other web page    (pmm::web::otherweb)
- Presentation      (pmm::deck)
   - Pitch Deck        (pmm::deck::pitch deck)
   - Product deck      (pmm::deck::product deck)
   - AR deck           (pmm::deck::AR deck)
   - Other Deck        (pmm::deck::misc deck)
- Analyst Relations (pmm::ar)
   - Briefings         (pmm::AR::Briefing)
   - Inquiries         (pmm::AR::Inquiry)
   - Reports           (pmm::AR::Report)
- Digital marketing (pmm::digital)
   - Blogs             (pmm::digital::Blogs)
   - Webinars          (pmm::digital::Webinar)
   - Videos            (pmm::digital::Video)
   - White papers      (pmm::digital::White Paper)
- Enablement        (pmm::enable)
   - Sales             (pmm::enable::Sales)
   - XDR               (pmm::enable::xdr)
   - Partner           (pmm::enable::partner)
- Events            (pmm::event)
   - Speaking (conf)   (pmm::event::presentation)
   - Abstract          (pmm::event::Abstract)
   - ABM Events        (pmm::event::ABM)
   - Booth             (pmm::event::booth)
   - Meetup            (pmm::event::meetup)
- Research          (pmm::research)
- PR                (pmm::pr)
   - Briefings         (pmm::pr::briefing)
   - Release           (pmm::pr::release)
- Customer Relations  (pmm::cust)
   - CAB Meeting        (pmm::cust::cab)
   - Customer meeting   (pmm::cust::meeting)
- Sales Support       (pmm::sales)
   - Prospect meeting    (pmm::sales::prospect)
   - Sales Deck          (pmm::sales::sales deck)

Other key labels will be Region (East,West, EMEA, APJ, LATAM, Pub Sector, WW, etc ) and DevOps Stage

### Boards and workflow

1. Any of the PMM work that applies to one of the defined PMM activities will be tracked via a simple issue in the Product marketing project.
1. The issue will describe the deliverable, key dates, dependencies, and related issues.  
1. The issue will have ONE of the above PMM activity categories associated with it.
1. The issue will have a label for the STAGE it supports
1. The issue will have a label for the NEXT Group Conversation where we will share. We'll use the Month/Year of the GC as a label. (GC-June19, GC-Aug19, etc)
1. We might label issues for which Sales region it supports (WW, East,West, EMEA,APAC, LATAM, PubSector, Commercial, etc.)
1. The workflow will be one of four steps
   1. unassigned backlog
   1. status:plan  - assigned to someone and in the backlog
   1. status:wip   - we're doing it now
   1. status:completed in quarter   - it's done
   1. closed

### Process

In the weekly PMM call, the team will review our current PMM board.  What is in the backlog, what WIP is in flight, and whatever we've completed.  

### Reporting and Tracking.

1.  We can set up a quarterly milestone to track PMM quarterly work.   
1.  We also can set up an Insights dashboard using [GitLab Insights](https://docs.gitlab.com/ee/user/project/insights/) which will give us visibility into work completed.  Such as:
![Stacked Bar](https://docs.gitlab.com/ee/user/project/insights/img/insights_example_stacked_bar_chart.png)

or

![bar chart](https://docs.gitlab.com/ee/user/project/insights/img/insights_example_bar_time_series_chart.png)

or

![Pie Chart](https://docs.gitlab.com/ee/user/project/insights/img/insights_example_pie_chart.png)
