---
layout: markdown_page
title: "Product Marketing"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Product marketing at GitLab

Product marketing is GitLab's interface to the market. The market is made up of customers, analysts, press, thought leaders, competitors, etc. Product marketing enables other GitLab teams such as Sales, Marketing, and Channel with narrative, positioning, messaging, and go-to-market strategy to go outbound to the market. Product marketing does market research to gather customer knowledge, analyst views, market landscapes, and competitor intelligence providing marketing insights inbound to the rest of GitLab.

### Responsibilities of the product marketing department at GitLab include:

![Product marketing functions](/handbook/marketing/product-marketing/images/PMMFunctions.png)

1. [Product marketing team](/handbook/marketing/product-marketing/pmmteam/)
1. [Technical marketing team](/handbook/marketing/product-marketing/technical-marketing/)
1. [Partner marketing team](/handbook/marketing/product-marketing/partner-marketing/)
1. [Channel marketing team](/handbook/marketing/product-marketing/channel-marketing/)
1. [Competitive intelligence team](/handbook/marketing/product-marketing/competitive/)
1. [Market research and customer insight team](/handbook/marketing/product-marketing/mrnci/)
  - [Analyst relations (AR)](/handbook/marketing/product-marketing/analyst-relations/)
  - [Customer reference program](/handbook/marketing/product-marketing/customer-reference-program/)

### Some key resources

#### Customer facing presentations

  Marketing decks linked on this page are the latest approved decks from Product Marketing that should be always be in a state that is ready to present. As such there should never be comments or WIP slides in a marketing deck. If you copy the deck to customize it please give it a relevant title, for example include the name of the customer and an ISO date.

  - [Company pitch deck](https://docs.google.com/presentation/d/1dVPaGc-TnbUQ2IR7TV0w0ujCrCXymKP4vLf6_FDTgVg/)  
    The Pitch Deck contains the GitLab narrative and pitch.

  - [SDR presentation deck](https://docs.google.com/presentation/d/1679lQ7AG6zBjQ1dnb6Fwj5EqScI3rvaheIUZu1iV7OY/edit#slide=id.g42cc3032dd_1_1903)    
    Condensed version of the company pitch deck. It copies linked slides from the pitch deck (so they can stay in sync.) SDR Managers own responsibility for keeping the deck in sync.  

    This deck can be used on occasions where the SDRs feel they should or could prequalify a prospect before setting a discovery meeting with the SAL. This could for example be someone who isn't our typical target persona but who might have an interest in what we do.  

  - [GitLb security capabilities deck](https://docs.google.com/presentation/d/1z4v6v_lP7BHCP2jfRJ9bK_XoUgQ9XW01X2ZhQcon8bY/edit#slide=id.g2823c3f9ca_0_9)  
    This deck introduces GitLab's position and capabilities around security. It covers why better security is needed now and how GitLab provides that better security in a more effective manner than traditional tools. This deck should be used when talking to prospects who are asking about how GitLab can help them better secure their software via GitLab Ultimate.   

    <details>
      <summary>Click here to know more about company pitch deck change process</summary>

      Only the PMM team and CMO have write access to the marketing decks. Changes, comments, and additions should be made via this process.
      <br>

      1. Create a google slide deck with the slide(s) you want to update
      <br>
      2. Create an issue in the [marketing issue tracker](https://gitlab.com/gitlab-com/marketing/general/issues)
      <br>
      3. Add a reason for the proposed changes in the description
      <br>
      4. Label the issue with "Product Marketing"
      <br>
      Discussion takes place in the issue and revisions are made on the slide until it is ready to move to the marketing deck. When it is ready, a PMM will move the slide into the marketing deck and close the issue.

    </details>
<!--
    **Company pitch deck change process**  
    Only the PMM team and CMO have write access to the marketing decks. Changes, comments, and additions should be made via this process:
    1. Create a google slide deck with the slide(s) you want to update
    2. Create an issue in the [marketing issue tracker](https://gitlab.com/gitlab-com/marketing/general/issues)
    3. Add a reason for the proposed changes in the description
    4. Label the issue with "Product Marketing"

    Discussion takes place in the issue and revisions are made on the slide until it is ready to move to the marketing deck. When it is ready, a PMM will move the slide into the marketing deck and close the issue.
-->

#### Key Demos
##### Videos
   - [GitLab in 3 minutes](https://youtu.be/Jve98tlZ394)
   - [Benefits of a Single Application](https://youtu.be/MNxkyLrA5Aw)
   - [GitLab Overview - 18 minutes](https://youtu.be/nMAgP4WIcno)

##### Click through Demos
   - [Auto DevOps](https://docs.google.com/presentation/d/1oKHU3MsbJmxVQyO-7c6JLMoCOS80uS-0NlcI-mRxAAY/edit?usp=sharing)
   -  [GitLab Secure Capabilities](https://docs.google.com/presentation/d/1fdTmdepdaq03OSfcA3pYduDxDnQEyvY4ARPqXEX8KrY/edit#slide=id.g2823c3f9ca_0_9)
   - [GitLab Agile Project Management](https://docs.google.com/presentation/d/13Zj83pjpwyq3s4T2fPSTuKO8NwqdCdn827GB7S-3hW8/edit?usp=sharing)

#### Print collateral   
  The following list of print collateral is maintained for use at trade shows, prospect and customer visits.  The GitLab marketing pages should be downloaded as PDF documents if a print copy for any marketing purposes is needed.
  - [GitLab DataSheet](/images/press/gitlab-data-sheet.pdf)
  - [GitLab Federal Capabilities One-pager](/images/press/gitlab-capabilities-statement.pdf)
  - [How GitLab is Enterprise Class](/solutions/enterprise-class)
  - [Reduce cycle time whitepaper](/resources/downloads/201906-whitepaper-reduce-cycle-time.pdf)
  - [Speed to mission whitepaper](/resources/downloads/201906-whitepaper-speed-to-mission.pdf)


#### Speaker Abstracts    
  To encourage reuse and collaboration, we have a [shared folder of past abstracts](https://drive.google.com/drive/folders/1ODXxqd4xpy8WodtKcYEhiuvzuQSOR_Gg) for different speaking events.  

#### Product marketing - team specific planning and reporting resources  
  - [Product marketing group conversation slides](https://drive.google.com/drive/folders/1fCEAj1HCegJOJE_haBqxcy2NYm0DS1FO)   
  - [Product marketing FY 2020 Vision](https://docs.google.com/presentation/d/1sbpBNy5OpO0QGvkAeobNyyIcEjTRGIkyApKeC1Oa8xY/edit#slide=id.g4a712342f9_0_852)
  - [CMO Triage Board](https://gitlab.com/groups/gitlab-com/-/boards/1160244?label_name[]=CMO%20Staff%20Triage)
  - [Product Marketing Staff Triage Board](https://gitlab.com/gitlab-com/marketing/product-marketing/-/boards/1237365?label_name[]=sm_request)
  - [Technical Marketing Issue Board](https://gitlab.com/gitlab-com/marketing/product-marketing/-/boards/926375?&label_name[]=tech-pmm)
  - [Partner Marketing Issue Board](https://gitlab.com/gitlab-com/marketing/general/-/boards/814970)
  - [Customer Reference Board](https://gitlab.com/groups/gitlab-com/marketing/-/boards/927283?&label_name[]=Customer%20Reference%20Program)
  - [Case Studies Board](https://gitlab.com/groups/gitlab-com/marketing/-/boards/918204?&label_name[]=Case%20study)
  - [Product Marketing Staff Triage Board](https://gitlab.com/gitlab-com/marketing/product-marketing/-/boards/1237365?label_name[]=sm_request)
  - [Sales Enablement Board](https://gitlab.com/gitlab-com/marketing/general/boards/465497?=&label_name[]=Sales%20Enablement)
  - [SDR Coaching Board](https://gitlab.com/gitlab-com/marketing/general/boards/772948?label_name[]=XDR-Coaching)
  - [PMM Budget Planning](https://docs.google.com/spreadsheets/d/1_MN8K9ixdgOp32DKiGPjT-208pr8s-rNKKuuYHsqCdI/edit#gid=1423447843)
  - [PMM Hiring Planning](https://docs.google.com/spreadsheets/d/12mNijMwA8hIG5h3zV5JJ0oxsVh6xzk6-si0eT7L9mvM/edit#gid=1301737184)


#### Other useful resources to help your productivity  
  - [Getting Started - GitLab 101 - No Tissues for Issues](/handbook/marketing/product-marketing/getting-started/101/)  
  - [Getting Started - GitLab 102 - Working Remotely](/handbook/marketing/product-marketing/getting-started/102/)
  - [Markdown style guide for about.gitlab.com](/handbook/product/technical-writing/markdown-guide/#markdown-style-guide-for-aboutgitlabcom)
  - [Searching GitLab](/handbook/marketing/product-marketing/getting-started/searching/)
  - [Other frequently used sales resources](/handbook/marketing/product-marketing/sales-resources/)  

### Requesting Product marketing team help/support
All Product Marketing Work is tracked and managed as issues in the Product Marketing Project. If you need support from the team, the simple process below will enable us to support you.
1. [Open an issue](https://gitlab.com/gitlab-com/marketing/product-marketing/issues/new) and select the *A-PMM-Support-Request* template and fill in what you know.
1. Triage and assignment. The PMM team will prioritize and assign team members to support your requests.
1. If you need more immediate attention please send a message with a link to the issue you created in the `#product-marketing` slack channel. Add an `@reply` to the [PMM responsible](#roles) or you can ping the team with `@pmm-team`.

**Product marketing request review and assignment flow** (note: the label `sm_request` indicates a request for Strategic Marketing support)  
   1. **New requests** start with the label `sm_req::new_request`
   1. Requests are then reviewed, prioritized, and then either
      1. **Triaged** to one of the Strategic Marketing teams (PMM,TMM, Competitive, Partner, AR, ro Cust Ref) `sm_req::triage` and the team label who is working the issue.
      1. **Defered** as a backlog item `sm_req::backlog` for future review
      1. **Declined** as not relevant `sm_req::declined`
   1. The relevant team will review the request and **assign** it to a team member for action `sm_req::assinged`
   1. When **complete**, the team member will update the issue with `sm_req::completed`

The [SM Request Board](https://gitlab.com/gitlab-com/marketing/product-marketing/-/boards/1237365?&label_name[]=sm_request) tracks status of requests

### Sales and partner enablement

Product Marketing Managers serve as subject matter experts and conduct sales trainings scheduled by the [Sales Training team](/handbook/sales/training/). For for info go to the [Sales Enablement page](/handbook/sales/training/sales-enablement-sessions/).
