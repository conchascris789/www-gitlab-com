---
layout: markdown_page
title: "Competitive Intelligence"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Competitive Intelligence at GitLab

Competitive intelligence is the action of monitoring, gathering, analyzing and sharing intelligence about competitors, products, customers.

At Gitlab, one of our core values is [transparency](/handbook/values/#transparency).  

Transparency ensures that anyone looking for a true end-to-end DevOps solution can compare us to any tool on the market, giving them the ability to make realistic and unbiased decisions for their DevOps strategy. Gitlab believes that transparency of competititve intelligence creates a well informed customer and gives  teams the information to be successful during their DevOps journey.

### Competitive Cards
* [Microsoft Azure DevOps + GitHub](https://docs.google.com/presentation/d/1AgPA-2WiHsHdVJABsnYWbBpMHtsi1nVigde1fv9igNU/edit?usp=sharing)
* [Cloudbees + Jenkins](https://docs.google.com/presentation/d/1ILMuOkg0QlFa8zBptQPANqPWh3aceQE4x4yWT_Y5md0/edit?usp=sharing)
* [Micro Focus Fortify](https://docs.google.com/presentation/d/1pBKfUGkjk9AdGT4txeFC03Eo0BJVISoHULgs-a9eLpg/edit?usp=sharing)

### Market analysis
* [Application Security market analysis](/handbook/marketing/product-marketing/competitive/application-security/#application-security-market-overview)
* [Application Security competitor scope](/handbook/marketing/product-marketing/competitive/application-security/#competitor-scope)
* [CI/CD Primer](/handbook/marketing/product-marketing/competitive/cicd/)


### DevOps Tools
GitLab exists in an ecosystem of [DevOps tools](/devops-tools) and might need to interact with any number of these tools. Many have over-lapping capabilities, but that does not mean that we necessarily directly compete with them. A user would need to patch together multiple solutions from this list in order to get all the functionality that is built-in to GitLab as a [single application for end-to-end DevOps](https://about.gitlab.com/). Other places that list tools in various DevOps categories include [CA](https://assessment-tools.ca.com/tools/continuous-delivery-tools/en?embed), [XebiaLabs](https://xebialabs.com/periodic-table-of-devops-tools/), and [CNCF](https://landscape.cncf.io/).

Before a tool shows up on the GitLab DevOps tools web page and the about.gitlab.com homepage, check the [GitLab internal partner spreadsheet](https://docs.google.com/spreadsheets/d/1-EE7vChGkDeyJxoM-LjVmUdwYwboxBmq8_42hjHGw_w/edit#gid=0) for the partnership status. If the tool is on the partner spreadsheet, reach out to Brandon Jung to confirm our partnership status and how they should be depicted on the DevOps tools web page.


- View the Competitive Intelligence Issue Board to see what's currently in progress.

### Whom should I contact for competitive intelligence?

- Listed below are areas of responsibility within the competitive intelligence team:

  - [Clint](/company/team/#Clint_II), Competitive Intelligence Manager
  - [Ashish](/company/team/#kuthiala), Director PMM
