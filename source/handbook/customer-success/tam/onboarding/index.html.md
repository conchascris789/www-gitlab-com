---
layout: markdown_page
title: "Account Onboarding"
---

# Technical Account Management Handbook
{:.no_toc}

## On this page
{:.no_toc}

- TOC
{:toc}

- Account Onboarding *(Current)*
- [Technical Account Manager Summary](/handbook/customer-success/tam/)
- [Account Triage](/handbook/customer-success/tam/triage)
- [Account Engagement](/handbook/customer-success/tam/engagement)
- [Using Salesforce within Customer Success](/handbook/customer-success/using-salesforce-within-customer-success/)
- [Gemstones](/handbook/customer-success/tam/gemstones)
- [Escalation Process](/handbook/customer-success/tam/escalations/)
- [Customer Health Scores](https://about.gitlab.com/handbook/customer-success/tam/health-scores/)
- [Customer Renewal Tracking](https://about.gitlab.com/handbook/customer-success/tam/renewals/)

---

# Customer Onboarding

When a customer with the Gemstone Ruby or above purchases or upgrades GitLab, an [onboarding object](/handbook/customer-success/using-salesforce-within-customer-success/#onboarding-objects) is automatically created.

The Technical Account Management team are subscribed to a Customer Onboarding Report in Salesforce which is sent every Monday. A standing report of [Customer Onboarding (by TAM)](https://gitlab.my.salesforce.com/00O4M000004JP9M) is also available.

_Handy tip: In order to delete an onboarding object, you must first make yourself the owner of it._

Once an account has been assigned to a Technical Account Manager, they must open an onboarding issue in the [Customer Onboarding GitLab project](https://gitlab.com/gitlab-com/customer-success/customer-onboarding). [The issue template](https://gitlab.com/gitlab-com/customer-success/customer-onboarding/issues/new?issuable_template=%5BCustomer%5D%20Onboarding&fds) has instructions.

The goal of the issue template is to help Technical Account Managers more consistently deliver a more thorough onboarding experience for customers, so they are set-up for success. If an item in the checklist doesn't make sense for a particular customer, mark it complete and leave a note in the comments.

In order to complete the onboarding object, the onboarding survey _must_ be sent to the customer. Be sure to send a personalised email to the team of people in the customer's organization that you have been working with throughout the onboarding time-box: [Here is the link to the survey](https://goo.gl/forms/aYOLCCkdM52yfaaD3).

An overview of these tools is [recorded in this video](https://drive.google.com/open?id=1gLYoU9Q07LrxjiZtpRM7wG40J4XfinZm) (internal link).

## Customer Support
GitLab offers [a variety of support options](https://about.gitlab.com/support/) for all customers and users on both paid and free tiers. For customers on Standard and Priority support tiers please address the below items when [submitting a support ticket](https://support.gitlab.com/hc/en-us):

1. Provide as much detail as possible during the first submission of the ticket

2. Summary of issue (when did it start, how frequently, impact on organization, etc.)
    * Detailed steps to recreate
    * Current behavior
    * Expected behavior
    * Any recent changes to Gitlab, its components, dependencies, or the services it's hosted on?
    * Attach logs and screenshots (avoid attaching .doc or .pdf files)

3. Try and avoid requesting a call during the initial ticket submission. We would like to keep all communication within the ticket and attempt to resolve the issue here before going to a call.

4. If a call is necessary, the support engineer will invite your team to a call via the ticket

5. If a support engineer requests follow up items, please make sure to respond back with these items. This will help us resolve the issue as quickly as possible

## Playbooks
The purpose of a playbook is to build standardized, repeatable and scalable processes. A playbook helps you quality assure the basics of your customer success methodology. All of our playbooks will be stored on our GDrive under `Sales/Customer Success/Playbooks`.


