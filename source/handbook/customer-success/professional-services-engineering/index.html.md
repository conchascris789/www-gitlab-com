---
layout: markdown_page
title: "Professional Services Engineering"
---
# Professional Services Engineering Handbook
{:.no_toc}

The Professional Services Engineering group at GitLab is a part of the [Customer Success](/handbook/customer-success) department. Professional Services Engineers have the goal to optimize organization's adoption of GitLab through our professional services, designed to enable other necessary systems in your environment so that customers can move from planning to monitoring.

## On this page
{:.no_toc}

- TOC
{:toc}

## Vision

<div class="alert alert-purple center">
    <h3 class="purple">
        <strong>
            Accelerate our customer’s DevOps Transformation <br/>
            by providing services to improve efficiency, time to market and agility <br/>
            through GitLab product adoption
        </strong>
    </h3>
</div>

## Role & Responsibilities
See the [Professional Services Engineer role description](/job-families/sales/professional-services-engineer/)

### Statement of Work creation

The GitLab Professional Services Engineering group is responsible for maintaining the [GitLab SOW Template](https://docs.google.com/document/d/1X8_EiX8kgJdpaVlydbTJg5pn4RXeDvYOIyok2G1A69I/edit) (internal link) while the  production of new Statements of Work for customer proposals is owned by the [Solutions Architects ](https://about.gitlab.com/handbook/customer-success/solutions-architects/).

To obtain a Statement of Work, open a new SOW issue using the template on the [Professional Services project](https://gitlab.com/gitlab-com/customer-success/professional-services-group/ps-plan/issues/new?issuable_template=NewSOWApproval&issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=) (internal link). 
We prefer customers to mark up our agreement and SOW template if they request changes. If they require the use of their services terms or SOW, please contact the Professional Services Engineering group.

### Long Term Profitability Targets
The long term gross margin target for services is 35%.

### Services Attach Rate for Large Accounts
The percent of Large Parent Accounts that have Professional Services associated with it or any linked SFDC Accounts. GitLab's target is above XX%.

### Services Delivery

#### Implementation Plan

Each services engagement will include an Implementation Plan compiled based on the Statement of Work and other discussions the Professional Services Engineering group has with the customer.  The Professional Services Engineering group also maintains the SOW template located [at this internal link](https://docs.google.com/document/d/1X8_EiX8kgJdpaVlydbTJg5pn4RXeDvYOIyok2G1A69I/edit).

Each services engagement will have a google sheet that shows the contracted revenue and the estimated cost measured in hours or days of effort required to complete the engagement.  The cost estimate must be completed prior to SoW signature and attached to the opportunity in SFDC.

### Professional Services Engineering Workflows

For details on the specific Professional Services Engineering plays, see [Professional Services Engineering workflows](/handbook/customer-success/professional-services-engineering/workflows).

## Selling Professional Services

For details on selling professional services, see [Selling Professional Services](/handbook/customer-success/professional-services-engineering/selling).

## Professional Services Offerings

### Hierarchy

1. **Levels**: There are three levels of service that represent the different services buyer personas.
1. **Categories**: Each level has one or many categories of service that define a set of related services either by type or product area.
1. **Offering**: Each category has one or many offerings that is a single consumable unit that has all of the required pieces to make a complete service offering.
1. **Offering Variants**: Each offering may have one or many variants that allow it to be deployed in different ways.  For example, enterprise versus commercial or remote versus on-site or one-time versus with an embedded engineer.

### Levels of Service
There are three levels of service we talk about when it comes to classifying our service offerings.

#### Product Services
**Installing, scaling & using the tool(s)**

Product services comprise services geared toward GitLab the software itself.  Getting a customer up and running with GitLab in a secure, highly-available environment to ensure their success with the application.

For example:
* Implementation & Migrations
* Training & Certification
* Performance tuning & advanced administration

See [professional services offerings](/handbook/customer-success/professional-services-engineering/offerings) for a detailed listing of offerings.

#### Operational Consulting Services
**Processes aligned with value-added changes**

Operational Consulting Services help teams on the path towards full DevOps maturity adopt industry best-practices and advanced use cases of GitLab to accelerate their time to market.  These services live one level above Product Services in the sense that they are less focused on the tool.  Operational Consulting Services are more focused on the processes in place that helps make the tool a success across a large enterprise.

For example:
* Development & DevOps process consulting
* Increasing DevOps maturity
* Establishing & measuring cycle time

See [professional services offerings](/handbook/customer-success/professional-services-engineering/offerings) for a detailed listing of offerings.

#### Strategic Consulting Services
**Adapt people’s way of thinking to the new paradigm **

Strategic Consulting Services live one level above Operational Consulting Services as they focus on organization changes and changes to the people and their behavior.  These changes are required to get the full value from a DevOps transformation.  We will provide some services in this area though the complete offering will be provided in collaboration with our partner ecosystem.

For example: 
* Cultural transformation
* Executive coaching
* Organizational structure consulting
* Digital strategy

See [professional services offerings](/handbook/customer-success/professional-services-engineering/offerings) for a detailed listing of offerings.


## How to work with/in the Professional Services Engineering group

### Contacting & Scheduling

At GitLab, Professional Services Engineering is part of the [Customer Success department](/handbook/customer-success).  As such, you can engage with Professional Services Engineering by following the guidelines for [engaging with any Solutions Architect](/handbook/customer-success/solutions-architects#when-and-how-to-engage-a-solutions-architect).  This process ensures that the Customer Success department as a whole can understand the inbound needs of the account executive and our customers.

For scheduling specific customer engagements, we currently are slotting implementations while our group grows to support the demand for services.  If you have a concern about scheduling the engagement, this should be discussed at the Discovery phase.  In no case should you commit to dates before receipt of agreements, P.O., etc.

### Contacting Professional Services Engineering

To contact the Professional Services Engineering group, the best way is to follow the guidelines for [Engaging a Solutions Architect](/handbook/customer-success/solutions-architects#when-and-how-to-engage-a-solutions-architect).

You can also reach the group via the [#customer-success Slack Channel](https://gitlab.slack.com/messages/C5D346V08/).

### Professional Services Engineering Issue Board

The [Professional Services Engineering Issue Board is available here](https://gitlab.com/gitlab-com/customer-success/professional-services-group/ps-plan/boards).  This board contains everything that the group is working on - from strategic initiatives to [SOW writing](#statement-of-work-creation), all group activity is available here.

#### Time Tracking

The professional services team will track billable hours within GitLab against each Statement of Work ("SOW") for internal use regardless of the billing structure agreed upon with the customer (such as fixed bid or time and materials). Detailed and accurate time tracking records of billable hours is essential to ensure revenue can be recognized based upon percentage completion of the project scope as outlined in the SOW, and this data is used in the calculation of gross margin. 

Billable hours represent work hours that a staff member reports as being aligned to a specific SOW. The format for daily time tracking for each team member is shown below, and should be reviewed by the professional services leadership for approval and signoff before being submitted each Monday (for the week prior) to the Finance Business Partner for Sales. Rounding to the nearest hour is acceptable, and please ensure data is recorded in numeric format without the use of text strings such as "hrs". 

| Manager Approved Y/N | Finance Accepted Y/N |
| ------------- |-------------|


| Customer Name | SOW#  | mm/dd/yy (Day 1) | mm/dd/yy (Day 2)  | mm/dd/yy (etc...)  | Total Billable Hrs  |
| ------------- |:-------------:| -----:|-----:|-----:|-----:|
| Customer 1     | SOW 1 | X.X |X.X|X.X|X.X| 
| Customer 2     | SOW 2     | X.X |X.X|X.X|X.X|  
| Customer 3 | SOW 3     |  X.X |X.X|X.X|X.X|



#### Project Completion

At the conclusion of the Statement of Work the Professional Services Engineer will prepare a cost vs actual analysis. This analysis will be filed in SFDC.  When filed in SFDC the Professional Services Engineer will @mention the the Controller and Finance Business Partner, Sales in SFDC Chatter.

#### SOW Issues

When requesting a SOW, Account Executives or Professional Services Engineering group members should use the SOW issue template, which automatically shows the required information as well as labels the issue appropriately.

#### Strategic Initiatives
Strategic Initiatives are undertaken by the Professional Services Engineering group to leverage team member time in a given area of the Customer Success Department.  This can include increased documentation, better training resources or program development.

