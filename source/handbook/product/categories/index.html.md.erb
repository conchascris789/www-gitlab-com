---
layout: markdown_page
title: Product stages, groups, and categories
extra_js:
    - listjs.min.js
    - categories.js
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Interfaces

We want intuitive interfaces both within the company and with the wider
community. This makes it more efficient for everyone to contribute or to get
a question answered. Therefore, the following interfaces are based on the
product categories defined on this page:

- [Home page](/)
- [Product page](/product/)
- [Product Features](/features/)
- [Pricing page](/pricing/)
- [DevOps Lifecycle](/stages-devops-lifecycle/)
- [DevOps Tools](/devops-tools/)
- [Product Vision](/direction/#vision)
- [Stage visions](/direction/#devops-stages)
- [Documentation](https://docs.gitlab.com/ee/#complete-devops-with-gitlab)
- [Engineering](https://about.gitlab.com/handbook/engineering/) Engineering Manager/Developer/Designer titles, their expertise, and department, and team names.
- [Product manager](https://about.gitlab.com/handbook/product/) responsibilities which are detailed on this page
- [Our pitch deck](https://about.gitlab.com/handbook/marketing/product-marketing/#company-pitch-deck), the slides that we use to describe the company
- [Product marketing](https://about.gitlab.com/handbook/marketing/product-marketing/) specializations

## Hierarchy
The categories form a hierarchy:

1. **Sections**: These map to sections within the Development
   part of the Engineering function.
1. **Stages**: Stages start with the 7 **loop stages**, then add Manage, Secure,
   and Defend to get the 10 (DevOps) **value stages**, and then add the Growth
   and Enablement **team stages**. Values stages are what we all talk about in
   our marketing.
1. **Group**: A stage has one or more [groups](/company/team/structure/#product-groups).
1. **Categories**: A group has one or more categories. Categories are high-level
   capabilities that may be a standalone product at another company. e.g.
   Portfolio Management. To the extent possible we should map categories to
   vendor categories defined by [analysts](/handbook/marketing/product-marketing/analyst-relations/).
   There are a maximum of 8 high-level categories per stage to ensure we can
   display this on our website and pitch deck.
   ([Categories that do not show up on marketing pages](https://about.gitlab.com/handbook/marketing/website/#working-with-stages-groups-and-categories)
   show up here in *italics* and do not count toward this limit.) There may need
   to be fewer categories, or shorter category names, if the aggregate number of
   lines when rendered would exceed 13 lines, when accounting for category names
   to word-wrap, which occurs at approximately 15 characters.
1. **Features**: Small, discrete functionalities. e.g. Issue weights. Some
   common features are listed within parentheses to facilitate finding
   responsible PMs by keyword. Features are maintained in
   [features.yml](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/features.yml).

Groups may have scope as large as all categories in a stage, or as small as a single category within a stage, but most will form part of a stage and have a few categories in them.
When the group's scope is the same as a stage or category, they can share the same name. This same principles applies when sections and stages [have the same scope](#secure-section).
For groups that have two or more categories, but not *all* categories in a stage, the group name must be a [unique word](/handbook/communication/#mecefu-terms) or a summation of the categories they cover.

Every category listed on this page must have a link, determined by what exists
in the following hierarchy:

Marketing product page > docs page > epic > label query > issue

E.g if there's no marketing page, link to the docs. If there's no docs, link to
the Epic. etc.

[Solutions](#solutions) can consist of multiple categories as defined on this
page, but there are also other ones, for example industry verticals. Solutions typically represent a customer challenge, how GitLab capabilities come together to meet that challenge, and business benefits of using our solution.

Capabilities can refer to stages, categories, or features, but not solutions.

Adding more layers to the hierarchy would give it more fidelity but would hurt
usability in the following ways:

1. Harder to keep the [interfaces](#Interfaces) up to date.
1. Harder to automatically update things.
1. Harder to train and test people.
1. Harder to display more levels.
1. Harder to reason, falsify, and talk about it.
1. Harder to define what level something should be in.
1. Harder to keep this page up to date.

We use this hierarchy to express our organizational structure within the Product and Engineering organizations.
Doing so serves the goals of:
- Making our groups externally recognizable as part of the DevOps lifecycle so that stakeholders can easily understand what teams might perform certain work
- Ensuring that internally we keep groups to a reasonable number of stable counterparts
As a result, it is considered an anti-pattern to how we've organized for categories to move between groups out
of concern for available capacity.

When designing the hierarchy, the number of sections should be kept small
and only grow as the company needs to re-organize for [span-of-control](/handbook/leadership/#management-group)
reasons. i.e. each section corresponds to a Director of Engineering and a
Director of Product, so it's an expensive add. For stages, the DevOps loop
stages should not be changed at all, as they're determined from an [external
source](https://en.wikipedia.org/wiki/DevOps_toolchain). At some point we may
change to a different established bucketing, or create our own, but that will
involve a serious cross-functional conversation. While the additional value
stages are our own construct, the loop and value stages combined are the primary
stages we talk about in our marketing, sales, etc. and they shouldn't be changed
lightly. The other stages have more flexibility as they're not currently
marketed in any way, however we should still strive to keep them as minimal as
possible. Proliferation of a large number of stages makes the product surface
area harder to reason about and communicate if/when we decide to market that
surface area. As such, they're tied 1:1 with sections so they're the
minimal number of stages that fit within our organizational structure. e.g.
Growth was a single group under Enablment until we decided to add a Director
layer for Growth; then it was promoted to a section with specialized
groups under it. The various buckets under each of the non-DevOps stages are
captured as different groups. Groups are also a non-marketing construct, so we
expand the number of groups as needed for organizational purposes. Each group
usually corresponds to a backend engineering manager and a product manager, so
it's also an expensive add and we don't create groups just for a cleaner
hierarchy; it has to be justified from a [span-of-control](/handbook/leadership/#management-group)
perspective or limits to what one product manager can handle.

## Changes

The impact of changes to stages and groups is felt [across the company](/company/team/structure/#stage-groups).
Merge requests with
[changes to stages and groups and significant changes to categories](/handbook/marketing/website/#working-with-stages-groups-and-categories)
need to be created, approved, and/or merged by each of the below:

1. VP of Product
1. VP of Product Strategy
1. The Product Director relevant to the stage group(s)
1. The Engineering Director relevant to the stage group(s)
1. CEO

The following people need to be on the merge request so they stay informed:

1. VP of Engineering
1. Senior Director of Development
1. Director of Quality

## DevOps Stages

![DevOps Loop](devops-loop-and-spans.png)

<%= partial("includes/product/categories") %>

## Maturity

Not all categories are at the same level of maturity. Some are just minimal and
some are lovable. See the [category maturity page](/direction/maturity/) to see where each
category stands.

## Solutions

GitLab also does the things below that are composed of multiple categories.

1. Software Composition Analysis (SCA) = Dependency Scanning + License Management + Container Scanning
1. Application Performance Monitoring (APM) = Metrics + Tracing + Real User Monitoring (RUM)

We are [intentional in not defining SCA as containing SAST and Code Quality](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26897#note_198503054) despite some analysts using the term to
also include those categories. 

## Other functionality

This list of other functionality so you can easily find the team that owns it.
Maybe we should make our features easier to search to replace the section below.



- Projects (project creation)

### Other functionality in Plan

- markdown functionality
- assignees
- milestones
- time tracking
- due dates
- labels
- issue weights
- quick actions
- email notifications
- todos
- GraphQL API foundational code

### Other functionality in Create

- [gitlab-shell](https://gitlab.com/gitlab-org/gitlab-shell)
- [gitlab-workhorse](https://gitlab.com/gitlab-org/gitlab-workhorse)

### Other functionality in Verify

- [Runner](https://docs.gitlab.com/runner/)

### Other functionality in [Secure](/handbook/product/categories/#secure-stage) stage

#### [Static & Dynamic Analysis group](/handbook/product/categories/#static-analysis-group)

- [Project and Group Security Dashboards](https://docs.gitlab.com/ee/user/group/security_dashboard/)
- [Interacting with Vulnerabiilities](https://docs.gitlab.com/ee/user/application_security/index.html#interacting-with-the-vulnerabilities)

#### [Composition Analysis group](/handbook/product/categories/#composition-analysis-group)

- [Pipeline and Merge Request Security reports](https://docs.gitlab.com/ee/user/project/merge_requests/#security-reports-ultimate)
- [Dependency List](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/index.html#dependency-list)

### Other functionality in [Monitor stage](/handbook/product/categories/#monitor-stage)

#### [APM group](/handbook/product/categories/#apm-group)

- [GitLab Self-monitoring](https://gitlab.com/groups/gitlab-org/-/epics/783)

#### [Debugging and Health group](/handbook/product/categories/#debugging-and-health-group)

- [Operations Dashboard](https://gitlab.com/groups/gitlab-org/-/epics/141)
