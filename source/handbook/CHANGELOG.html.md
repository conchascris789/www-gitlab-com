---
layout: markdown_page
title: Handbook Changelog
---

### 2019-08-18
- [!28244](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28244) ak-addresources-pmm
- [!28239](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28239) Ak handbook updates
- [!28238](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28238) ak-addmarkdownguide
- [!28237](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28237) ak-mrnciUpdate
- [!28233](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28233) infra library updates
- [!28081](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28081) Add new video to Sales Training page
- [!27765](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27765) Added definition for Mktg efficiency ration on Rev mktg page

### 2019-08-19
- [!28223](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28223) Add friendly competition to CEO Shadow
- [!28222](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28222) Organize YouTube with playlists and labels.
- [!28194](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28194) Add picture of sun hitting zoom background to CEO Shadow page
- [!28193](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28193) Move Emilie to CEO Shadow Alumni
- [!28073](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28073) Restore public monitoring links to the publicly accessible version
- [!27858](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27858) Add best practice of line per sentence to markdown guide
- [!26742](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26742) Rename SCA into Composition Analysis

### 2019-08-17
- [!28220](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28220) Ak fixdemoslist
- [!28219](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28219) link senior leader
- [!28218](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28218) ak-fixmrnci_title
- [!28217](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28217) Update source/handbook/marketing/product-marketing/customer-reference-program/index.html.md
- [!28216](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28216) ak-fixpartnermkttitle
- [!28215](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28215) Update source/handbook/marketing/product-marketing/technical-marketing/index.html.md
- [!28214](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28214) ak-updatetitle
- [!28213](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28213) Update source/handbook/marketing/product-marketing/pmmteam/index.html.md
- [!28212](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28212) fix spelling
- [!28211](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28211) rmv old newsletter dates in schedule
- [!28208](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28208) Update source/handbook/marketing/product-marketing/sales-resources/index.html.md
- [!28207](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28207) Update source/handbook/marketing/product-marketing/channel-marketing/index.html.md
- [!28206](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28206) update partner mkt handbook
- [!28205](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28205) cust ref program page update
- [!28204](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28204) update cim page
- [!28203](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28203) ak-tmm-update
- [!28199](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28199) rmv dup info that was moved to marketing/events
- [!28198](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28198) Fix the broken "sales demo" links in the Product Marketing section.
- [!28197](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28197) Added two latest videos
- [!28196](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28196) Fix a few broken links on the Community Advocacy page.
- [!28195](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28195) Fix the "homepage card" link on the "Release Posts" page.
- [!28192](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28192) Re-restructure and expand the "Cross posts" section on the "Blog Handbook" page.
- [!28191](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28191) Fix formatting for example agenda items
- [!28189](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28189) add demandbase to tech stack
- [!28188](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28188) add operational work cadence
- [!28186](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28186) add golden circle pic and text
- [!28175](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28175) Ak handbook pmmchanges
- [!26880](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26880) Add CEO skip level description
- [!26775](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26775) Adding Liz Corring to Territory
- [!26646](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26646) short toes

### 2019-08-16
- [!28187](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28187) Add DPIA cross-reference
- [!28184](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28184) Move the Security Breach biggest risk to the appropriate order
- [!28181](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28181) Add risk assessment reference, fix link
- [!28172](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28172) Add Security Breach to biggest risks
- [!28169](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28169) Add info on MRs having branch set to delete and commits not set to squash
- [!28165](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28165) Remove guidance to squash commits when merging job families
- [!28152](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28152) update dwells pair
- [!28151](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28151) Resolve "Create the Influencer Bureau program page"
- [!28142](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28142) Ak addtmmhandbookpage
- [!28141](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28141) update business cards
- [!28139](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28139) Ak pmmhandbookupdates
- [!28137](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28137) update sdr slack channel
- [!28135](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28135) Remove target term from Finance KPIs
- [!28132](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28132) Update KPI page to clarify target vs cap
- [!28131](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28131) Fix 2020 dates for CEO shadow program rotation
- [!28130](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28130) Replace the broken "#peopleops-confidentia" link with corrected text.
- [!28126](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28126) Add recruiting and retention topics to leadership
- [!28125](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28125) Changed Gitlabbers to GitLab team-members
- [!28124](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28124) Adding the SPIFF info to the handbook with updates
- [!28123](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28123) Merged Growth pages; deprecated categories/growth
- [!28120](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28120) Fix broken links in handbook business ops
- [!28113](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28113) Add engineering hiring guidelines to acquisition page
- [!28104](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28104) Fix broken links in handbook ceo sections
- [!28096](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28096) Remove old Verify and Release team pages
- [!28090](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28090) Add mention to Workflow Labels to the design handbook
- [!28084](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28084) added missing library index
- [!28077](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28077) Fix a broken example link and restructure the "Cross posts" section on the "Blog Handbook" page.
- [!28072](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28072) Fix the broken "Sales team onboarding" link and several redirected links on the Onboarding page.
- [!28071](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28071) Fix Kibana link
- [!28069](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28069) Add two missing periods to "Working with Tickets".
- [!28065](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28065) Fix the Account Verification link on the Support Workflows Quick Reference page.
- [!28059](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28059) update to full name
- [!28049](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28049) update to add clarification and links
- [!28027](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28027) Describe CEO Shadow task management process
- [!28024](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28024) Resolve "Update Commercial SA Handbook page with better guidance and expectation explanations"
- [!28009](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28009) add L&D to handbook index
- [!27976](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27976) chore: fixed spelling of deprecated
- [!27959](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27959) Explicitly name that timestamps in the data warehouse should be in UTC
- [!27775](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27775) Updated VUL.2.01 ownership
- [!27590](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27590) Update Handbook - Values - Feedback with proper punctuation
- [!27566](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27566) updates team plan page with more advanced workflow chart, fixes typos
- [!27559](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27559) Adding "The Relationship Between Learning and Development and Promotion"...
- [!27546](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27546) Add Unfiltered to handbook + templates
- [!27018](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27018) Updated direction page, deprecated old Growth page
- [!26982](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26982) Germany Leave Pamphlet
- [!26172](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26172) Adds speedy meetings to Handbook > Tools and Tips > Google Calendar

### 2019-08-15
- [!28055](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28055) Added 70% target to category maturity KPI
- [!28051](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28051) Update handbook organization guidance
- [!28050](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28050) Add #office-today
- [!28048](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28048) Retrying the addition of the Influencer bureau page...
- [!28046](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28046) Fixed board links using newer labels
- [!28044](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28044) Update source/handbook/marketing/technical-evangelism/index.html.md
- [!28042](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28042) Making clearer that Key Reviews are FYI
- [!28040](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28040) Add that Business units are responsible for embedding charts in the Handbook
- [!28039](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28039) update name change additions
- [!28034](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28034) updated BZO page with new labels
- [!28033](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28033) Removed active tests section from DMP page
- [!28028](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28028) Move Verify and Release team pages to CI/CD section
- [!28022](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28022) Fix the who-to-talk-to-for-what links.
- [!28017](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28017) added start date change for peo
- [!28015](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28015) Encourage CEO Shadows to attend breakout calls
- [!28014](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28014) Add efficiency action of `Write things down` to values
- [!28013](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28013) Update source/handbook/use-cases/index.html.md
- [!28008](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28008) update l&D section of peopleops group
- [!28007](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28007) Reducing acquisition soft-landing for non-offered employees from 12 months to 3 months
- [!27999](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27999) add support agent to junior stock
- [!27998](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27998) Consolidate co-working info in the CEO Shadow Handbook
- [!27997](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27997) Add CS resume requirement
- [!27991](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27991) Fix typo and link in Serve smaller users section
- [!27988](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27988) Qualtrics Instructions
- [!27985](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27985) fix more typos in handbook marketing section
- [!27984](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27984) Fix more typos in handbook sales section
- [!27983](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27983) Fix more typos in handbook engineering section
- [!27974](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27974) adding new team member in growth stage
- [!27973](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27973) Update index.html.md for GitLab!!
- [!27972](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27972) Update mentions workflow
- [!27968](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27968) Ak pmm hbook
- [!27967](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27967) Update links and remove one in General Guidelines.
- [!27964](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27964) Update PMM functions
- [!27963](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27963) Fix a couple UX Researcher Onboarding links.
- [!27961](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27961) Fix a few broken links on the Quality Department home page.
- [!27960](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27960) Fix the unicorn2redis link on the Performance page.
- [!27958](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27958) Document Meeting Tuesday
- [!27955](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27955) Add Periscope Project URL to Handbook
- [!27953](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27953) Update index.html.md
- [!27950](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27950) update interview process to include doc for interviews w/ ceo shadows
- [!27949](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27949) lightweight Enablement section prof dev budget application process
- [!27948](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27948) Ak mktpath
- [!27929](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27929) Add Coworking option to CEO shadow page
- [!27927](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27927) add gitlab levels diagram
- [!27909](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27909) add EOR vendors
- [!27901](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27901) Fix typos in handbook business ops section
- [!27880](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27880) Update Finance KPIs to say target
- [!27838](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27838) Add values feed details
- [!27823](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27823) Move Dan to alumni
- [!27815](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27815) Added 'Sharing your Visiting Grant stories' to handbook/incentives, fixed...
- [!27789](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27789) Updated VUL.4.01 ownership
- [!27743](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27743) updating territory alignment for SMB EMEA team
- [!27674](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27674) changing definition of TCV & ACV
- [!27655](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27655) Update index.html.md
- [!27631](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27631) #www-gitlab-com/4006: clean of blueprint and design
- [!27453](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27453) Update Secure section documentation process and labels
- [!27033](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27033) Enable screen lock on your Mac Touch Bar

### 2019-08-14
- [!27951](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27951) Fixed typo
- [!27946](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27946) Add WebEx Test Meeting to Tips and Tricks
- [!27930](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27930) Fix small typo on biggest risks page
- [!27926](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27926) Add candidate interview meetings to CEO Shadow Page
- [!27924](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27924) Add non-overlapping scope to velocity risks
- [!27922](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27922) Clarify non-overlapping scope for groups
- [!27920](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27920) Add clarification about stage and section scope overlap
- [!27907](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27907) Update handbook to note dataStudio doesn't support shared drives
- [!27906](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27906) Fix more typos in handbook CEO section (automated spellcheck)
- [!27904](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27904) Fix typos in handbook sales section
- [!27897](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27897) Add link reference to term Bio Break
- [!27893](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27893) Update the Support Time Off information with PTO Ninja
- [!27890](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27890) Update corporate events handbook with GitLab Commit calendar and move landing...
- [!27887](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27887) Update source/handbook/marketing/technical-evangelism/index.html.md
- [!27885](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27885) Handbook Finance Periscope Dashboard Link
- [!27884](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27884) Update source/handbook/marketing/technical-evangelism/influencer-bureau
- [!27883](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27883) Fix typo on design workflow page
- [!27879](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27879) Update source/handbook/business-ops/index.html.md
- [!27874](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27874) Added LinkedIn Recruiter System Connect How-To
- [!27869](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27869) Remove old ops page
- [!27868](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27868) Fix grammar typo in ceo pricing section
- [!27866](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27866) Add speedy meetings benefits
- [!27865](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27865) Add additional clarity to not using `link` links
- [!27863](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27863) Update confidential merge request mention
- [!27856](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27856) Updated header included per Sids suggestion noting examples
- [!27854](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27854) Fix the Security Release Development link on the Security Releases page.
- [!27853](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27853) Add issue impact dashboard to sensing mechanisms
- [!27852](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27852) Fix and separate separate the 'Create Backend Team' link on the Engineering home page.
- [!27851](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27851) Add missing PMM org roles
- [!27849](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27849) Fix the bad links on the Sales Compensation Plan page.
- [!27846](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27846) Fix the 'GitLab Version Check' links at /handbook/resellers/ and /handbook/sales/.
- [!27822](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27822) Adding myself as Joe Miklos' SDR
- [!27816](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27816) Add Technical Marketing Manager career path
- [!27800](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27800) Clarify process for People Ops Analyst when someone is moving entities.
- [!27790](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27790) Fixed numbering issue and unlinked error message
- [!27755](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27755) Fix typos in handbook tools and tips section
- [!27753](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27753) Fix typos in handbook marketing section
- [!27752](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27752) Fix typos in handbook git page update section
- [!27751](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27751) Fix typos in handbook legal section
- [!27750](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27750) Fix typos in handbook general onboarding section
- [!27749](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27749) Fix typos in handbook product section
- [!27748](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27748) Fix typos in handbook hiring section
- [!27747](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27747) Fix typos in handbook sales section
- [!27731](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27731) Removing broken links and replacing with generic link.
- [!27730](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27730) Update index.html.md to fix numbers
- [!27719](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27719) Corrected typo under Promotions & Compensation Changes section
- [!27704](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27704) Updating link to Rules of Engagements - removing reference to Google Drive in...
- [!27528](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27528) Some typo / grammar fixes to MR labels docs.
- [!27318](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27318) Update to commercial sales handbook
- [!27256](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27256) Updated EMEA regions Aug.html.md
- [!27163](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27163) Fixed some broken support workflows links
- [!27111](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27111) Add support communication updates
- [!27009](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27009) modify NORAM US WEST table

### 2019-08-13
- [!27844](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27844) Add process to update #ceo-shadow channel to CEO Shadow handbook
- [!27841](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27841) Add back backchannel references info
- [!27834](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27834) Revert "Merge branch 'add-backchannel-reference-process' into 'master'"
- [!27821](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27821) Update BC.1.01_business_continuity_plan.html.md
- [!27820](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27820) Add backchannel references section to hiring process
- [!27807](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27807) Add "per month" to MRs per eng on Engineering KPIs
- [!27806](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27806) Adding link for SAO Criteria
- [!27804](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27804) Generalize visual reference to not be specific to the handbook
- [!27802](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27802) fixed a typo on location for professional-services-engineering
- [!27801](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27801) Add e-group link from team structure
- [!27799](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27799) Add underperformance to biggest risks page
- [!27797](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27797) Add possessive punctuation to the underperformance page
- [!27792](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27792) Updated VUL.7.01 ownership
- [!27787](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27787) Fix Capital Consumption Link
- [!27786](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27786) Update handbook allocation
- [!27785](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27785) Add vision and levels of service
- [!27784](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27784) Update document to help on checking if a Docker image is up or outdated
- [!27780](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27780) Corrected broken link to DPIA policy page
- [!27778](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27778) update livestream instructions
- [!27777](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27777) Updated VUL.3.01 ownership
- [!27776](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27776) update live stream instructions
- [!27774](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27774) Add states where EPO is not available.
- [!27770](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27770) Updated VUL.1.01 context and ownership
- [!27766](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27766) Revert "Merge branch 'rnalen-master-patch-31074' into 'master'"
- [!27756](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27756) Fix broken links for Failure Management Rotation
- [!27754](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27754) Fix typos in handbook stock options section
- [!27746](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27746) Fix typos in handbook support section
- [!27744](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27744) Fix typos in handbook people operations section
- [!27742](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27742) Fix typos in handbook finance section
- [!27741](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27741) Fix typos in handbook engineering section
- [!27740](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27740) Fix typos in handbook customer success section
- [!27739](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27739) fix typos in handbook communication section
- [!27738](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27738) Fix typos in handbook ceo section
- [!27737](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27737) Fix typos in handbook business ops section
- [!27736](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27736) fix typos in handbook benefits section
- [!27733](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27733) Update source/handbook/hiring/interviewing/bar-raiser/index.html.md
- [!27732](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27732) Show full team on Growth eng page, improve content
- [!27728](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27728) Cross-link recording everything in video meetings section
- [!27727](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27727) Add reference to product category maturity targets in velocity risk
- [!27726](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27726) Add visual aid check to CEO Shadow roles
- [!27725](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27725) Adds CEO Shadow email best practices
- [!27722](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27722) Add how the handbook doesn't make us rigid, it empowers change
- [!27720](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27720) Add benefits list to CEO shadow program
- [!27718](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27718) communication formatting, code of conduct typo
- [!27712](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27712) Fix the 'How may we be of service?' link on the Reliability Engineering page.
- [!27711](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27711) Update TAM handbook with a link to video overview of the new onboarding process.
- [!27709](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27709) Fix the 'How may we be of service?' link on the Infrastructure Meetings page.
- [!27708](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27708) Fix the git workflow link on Terraform Automation page.
- [!27707](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27707) Remove Navigation from other functionality in Manage
- [!27706](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27706) Remove GitLab.com specific functionality from other functionality in Manage
- [!27705](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27705) Remove Projects from Manage Other Functionality
- [!27680](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27680) Fix gitlab name misspell and usage
- [!27675](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27675) add privacy screens as examples
- [!27671](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27671) Add Frontend and Backend Weight scoped labels to the Create: Source Code group capacity planning section
- [!27649](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27649) typo: prioriy -> priority
- [!27630](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27630) Reorganize ceo shadow page
- [!27592](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27592) Improve Quality Engineering Handbook
- [!27548](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27548) added how to change HSA contribution
- [!27488](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27488) Moved FE plan page to engineering/development/dev/fe-plan/
- [!27442](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27442) Split plan backend teams
- [!27405](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27405) Added DPIA policy page
- [!27292](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27292) IR Phase 2 - Expansion of Incident Handling
- [!27266](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27266) update red team ROE to include handling of emergencies & improve existing link formatting
- [!27077](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27077) Document process for triaging vulnerability reports in ZenDesk
- [!26920](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26920) Update UK Ltd benefits with moratorium and $0 excess

### 2019-08-12
- [!27701](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27701) Fix the broken refund link on the Sales page.
- [!27699](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27699) Fix the broken Brand Guidelines links on the Corporate Marketing page.
- [!27690](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27690) add info on the numerous DNS records for gitlap.com
- [!27683](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27683) Update source/handbook/business-ops/order-processing/index.html.md
- [!27677](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27677) Inputting budget vs. actuals process in HB
- [!27673](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27673) Fix misspelled gitab.com and GibLab words to GitLab in Handbook
- [!27670](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27670) Update RM.1.01 control statement
- [!27669](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27669) added filter parameter to the URL link
- [!27668](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27668) Add pajamas URL to How We Work and remove duplicate bullet point
- [!27665](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27665) Update source/handbook/people-operations/code-of-conduct/index.html.md
- [!27664](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27664) Added /handbook to fix handful of 404s
- [!27662](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27662) Category Maturity Achievement KPI
- [!27625](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27625) Updated Process owner sg.1.01
- [!27624](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27624) Updated ownership of TPM.1.03 in handbook
- [!27623](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27623) Updated context, scope and ownership
- [!27622](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27622) Updated TPM102 ownership section
- [!27621](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27621) Specified Security Compliance in ownership section
- [!27612](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27612) Fixing broken URL Link for Data Analyst Onboarding
- [!27603](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27603) Defining Cost Center, Division, and Department
- [!27601](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27601) Series of small link updates to fix 404s
- [!27547](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27547) Add zoom waiting room tip to the handbook
- [!27544](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27544) Add periscope housekeeping part in readme
- [!27394](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27394) updated corporate structure
- [!26936](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26936) Update with addition to Zoom section, Re GBP.
- [!26823](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26823) Add risk assessments to Security page
- [!26784](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26784) Update design.gitlab.com project links

### 2019-08-11
- [!27620](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27620) fixing typo

### 2019-08-09
- [!27596](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27596) Added links to shadows' gitlab profiles.
- [!27588](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27588) Add guide on using Visuals to our Communication page
- [!27583](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27583) production section updates
- [!27582](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27582) fix misspelling :(
- [!27581](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27581) Allocation link to original location and reporting hierarchy
- [!27578](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27578) adding how to test zoom audio/video
- [!27574](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27574) Add info on social events for the CEO to the Shadow page
- [!27573](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27573) Indicate the CEO Shadow agenda is in the channel + to search for the google doc
- [!27572](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27572) Add a public by default to Deep Dive sessions
- [!27570](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27570) Update SCA definition in all places
- [!27569](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27569) Add info on CEO Shadows participating in media briefings
- [!27564](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27564) Added notes on starting live streams
- [!27562](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27562) Add information on why we work in public
- [!27552](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27552) Add clarifying language about unlinked controls
- [!27550](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27550) Link to livestream docs
- [!27541](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27541) Remove typo on the Sourcing page
- [!27538](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27538) add to searching tips page
- [!27536](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27536) Fix a double-indentation issue.
- [!27524](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27524) Fix the Monitoring and Gemnasium links on the Production Architecture page.
- [!27522](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27522) Fix the On-Call link from the Production page.
- [!27519](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27519) Specify all-remote as the Pick Your Brain interview topic on the CEO page.
- [!27517](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27517) Update the CommonMarker capitalization and URL in the Markdown Guide.
- [!27511](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27511) Updated TPM.1.01 context and ownership
- [!27508](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27508) Updated SG.1.01 context, scope, and ownership
- [!27483](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27483) Added ZenDesk light agent to baseline entitlements for all GitLabbers
- [!27476](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27476) Updating /handbook/ceo to add Pick Your Brain interviews header
- [!27414](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27414) Update "What is the difference..." table for /handbook/business-ops/
- [!27372](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27372) Update the Issues auto-labelling workflow chart
- [!27357](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27357) Add text about following the CEO's GitLab activity log
- [!27350](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27350) APM Add process for milestone planning
- [!27225](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27225) Add SMB Pitch Deck to Handbook
- [!27127](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27127) Update views and descriptions
- [!26357](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26357) Update on-call page re. chatops membership
- [!22777](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22777) Clarify tension between single-application and plays well with others

### 2019-08-08
- [!27509](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27509) Link to the June 2019 San Francisco minimum wage notice from the Labor and Employment Notices page.
- [!27506](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27506) Updated wording of SYS.1.01 control guidance
- [!27505](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27505) cross link dev on-call to on-call page
- [!27504](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27504) Enhanced access review guidelines
- [!27502](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27502) Added note about Snowplow to changes of note
- [!27499](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27499) Minor misspelling - changed estimatd to estimated
- [!27494](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27494) add note about subdirs
- [!27481](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27481) Added elements of Pick Your Brain section back to /handbook/eba
- [!27473](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27473) Update index.html.md for tech stack
- [!27472](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27472) update group conversations expectations
- [!27471](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27471) Added Laptop Vendor Selection Criteria to IT Ops Laptop section
- [!27463](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27463) Avoid use of word "metric"
- [!27461](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27461) added two tools to the website stack
- [!27460](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27460) Make it clear branches should be created from master
- [!27456](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27456) Add average stages per user as a Product KPI
- [!27454](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27454) update slack channel for list uploads
- [!27433](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27433) Clarify that emails are not a source of truth
- [!27420](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27420) Updated 'Diversity' to 'Diversity & Inclusion' to match our revised value
- [!27418](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27418) Fix the AM.1.01 link from the VUL.1.01 page.
- [!27416](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27416) Fix the 'support team does this' link on the Security index.
- [!27415](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27415) Fix the sample template link from the gSIC page.
- [!27413](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27413) Fix the 'OKR Blueprint' link from /handbook/engineering/infrastructure/design/gitlab-okrs/.
- [!27412](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27412) Fix the staging server link and corresponding heading on the Tech Stack page.
- [!27410](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27410) Added link to 'Archive of Pick Your Brain interviews with Sid' to CEO page
- [!27373](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27373) Update markdown guide
- [!27338](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27338) Move ops configure page
- [!27336](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27336) Update source/handbook/engineering/security/data-classification-policy.html.md
- [!27285](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27285) Cleaned up links, fixed spellings, made sections more readable, added link to...
- [!27220](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27220) Document use of shared drive for DMP dataStudio sheets
- [!26922](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26922) Resolve "Create Category Comparison Table for Cloudbees & Jenkins"
- [!26910](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26910) Update Product Leadership Meeting Process
- [!26897](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26897) Adjust SCA solution in Product Categories page
- [!26884](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26884) Adding PMM Request Section to the Handbook
- [!26378](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26378) Pmm 862 update pr boilderplate
- [!26340](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26340) Update index.html.md

### 2019-08-07
- [!27403](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27403) Global Review of Benefits, Not Global Application
- [!27402](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27402) Updating countries
- [!27400](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27400) Show Sid's office
- [!27396](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27396) Kickoff Updates from Feedback
- [!27395](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27395) Update index.html.md
- [!27392](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27392) Engineering OKR procedure
- [!27390](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27390) Update index.html.md
- [!27388](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27388) Fixing broken link to the milestones page (line 73)
- [!27385](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27385) Move note about cancellation.
- [!27380](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27380) Tweak Kickoff Meeting Process
- [!27370](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27370) Add additional details on what Stitch is to the data team page
- [!27368](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27368) Added trailing slash to CS specialties from handbook index
- [!27364](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27364) Clarify GitLab Team Member Training Budget
- [!27363](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27363) updated corp structure
- [!27358](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27358) Added Culture Curation mission statement, OKRs, audience, and channels to...
- [!27355](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27355) Fixed typo on job families page
- [!27354](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27354) Add Link to Jeremy Elder README
- [!27339](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27339) Add a more descriptive instruction for breakout calls
- [!27331](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27331) Fix and update a few of sbouchard1's links.
- [!27329](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27329) Move Internal Audit under Finance on the Handbook home page.
- [!27326](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27326) Correct the spelling of 'organisation' on the Internal Audit index.
- [!27325](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27325) Add cohost detail for promoting folks to panelist on Zoom calls
- [!27324](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27324) Update a couple broken links on the Support index.
- [!27322](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27322) Title + Rotation updates to CEO Shadow Page
- [!27320](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27320) Update index.html.md
- [!27319](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27319) add industry standard to definition for turnover
- [!27316](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27316) Add comment on speaking up when there are technical difficulties for the CEO
- [!27314](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27314) Add more information on the current triage report
- [!27312](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27312) Cleaned up links (relative in place of absolute), added takeaways to alumni chart
- [!27304](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27304) Added specific code to 'Call to action' button example in Markdown Guide
- [!27284](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27284) Removes `Google Chrome` preference from Okta FAQ
- [!27283](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27283) APM - remove duplicate information
- [!27276](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27276) Update index.html.md Added Nadel portal demo video to NORAM swag process
- [!27029](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27029) Update deployment model in Fulfillment page
- [!27025](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27025) Added Culture Curation mission statement, OKRs, audience, and channels to Corporate Marketing Handbook
- [!26864](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26864) Updated definitions for Opportunity Types
- [!26396](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26396) Changing position of instructions to switch to youtube unfiltered to view...
- [!24793](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24793) Consolidate and document common subscription/licensing/billing issues

### 2019-08-06
- [!27303](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27303) moved image to correct location
- [!27296](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27296) fix hyperlinks
- [!27293](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27293) Step ladder location
- [!27289](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27289) Lighting mode to guest setup
- [!27286](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27286) Add info on CEO Interviews to the Shadow page
- [!27281](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27281) Updated /handbook/ea references (broken links) to /handbook/eba
- [!27277](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27277) Move defend up a week
- [!27269](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27269) Update source/handbook/engineering/ux/stage-group-ux-strategy/secure/index.html.md
- [!27261](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27261) Adding a link to the model into the handbook for easy access.
- [!27255](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27255) Infrastruture mStaff updates and main Infra page
- [!27254](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27254) Add linked name as a last step of granting access to Zoom service
- [!27242](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27242) Add @andrewn as a member of sandboxed Zoom account
- [!27240](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27240) Updated youtube stream for day 1 morning
- [!27235](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27235) Fix the 'ccount' typo on the Corporate Marketing index.
- [!27234](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27234) Fix some capitalizations on the Corporate Marketing index.
- [!27233](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27233) Fix 'URL' capitalization on several handbook pages.
- [!27230](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27230) iteration on training doc
- [!27224](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27224) Add more details on performance bug grooming process
- [!27191](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27191) Update QA Rotation
- [!27185](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27185) Update Beamy info
- [!27183](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27183) update to remove BAO from tech stack
- [!27181](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27181) infra mstaff board
- [!27174](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27174) Add recommended reads for database
- [!27161](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27161) Addressing feedback from engineers on framework
- [!27144](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27144) Ps job updates
- [!27142](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27142) Add New Zealand to supported laptop ordering countries
- [!27096](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27096) Update source/handbook/legal/global-compliance/index.html.md
- [!27078](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27078) Add Defend Section group conversation
- [!27031](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27031) Add info about interview trainings for support
- [!26814](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26814) Customer Success OKRs for Health Checks and Renewal Tracking
- [!26716](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26716) Handling non-emergencies
- [!26198](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26198) Add Periscope Links to Product Metrics
- [!25937](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25937) Small formatting changes to the writing conference proposals page
- [!24998](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24998) Light copy edit of acceptable use policy handbook page

### 2019-08-05
- [!27226](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27226) Change number of shifts wording to be more certain
- [!27221](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27221) Update source/handbook/sales/territories/index.html.md
- [!27216](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27216) Add Becka Lippert to Secure UX page
- [!27211](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27211) Update index.html.md Updated NORAM Event Deadlines Process for MPM Tasks
- [!27210](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27210) Livestreaming direct from zoom
- [!27209](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27209) Adding value driver sections
- [!27203](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27203) Fix typo on process.html.md.erb
- [!27199](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27199) Add Beamy shut down instructions to CEO Shadow Page
- [!27198](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27198) Make clear that KPI definitions should only be defined in one place
- [!27195](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27195) Fix outdated service-mapping link
- [!27188](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27188) Fix typo in Secure Engineering Release Process handbook page
- [!27182](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27182) Usecase and GTM
- [!27180](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27180) Link vulnerability update info.
- [!27179](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27179) add character limit to bonus comment
- [!27177](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27177) Add BambooHR and Gsuite as the first step to improve the flow in onboarding.
- [!27176](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27176) Document the 'automation:devops-mapping-disable' label and improve a bit the 'Auto-labelling of Issues' section
- [!27167](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27167) Adding whom to contact on the sales team home page.
- [!27160](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27160) Add frequently asked questions from Safeguard
- [!27158](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27158) Add coordinator details to infrastructure escalation
- [!27157](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27157) Link to plan stage page from plan BE team page
- [!27155](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27155) Add WAZO info about Netherlands Maternity leave.
- [!27153](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27153) Update upcoming CHD events
- [!27147](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27147) Update contracts page in handbook re Notice periods
- [!27132](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27132) add slack channels
- [!27103](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27103) Fix typos
- [!27092](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27092) Add triage guidance for support requests
- [!27069](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27069) Remove inactive CRO test on DMP page
- [!27064](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27064) Mention sandboxed Zoom account for Monitor
- [!27052](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27052) Add Product Management Buddy System
- [!26975](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26975) smb post-sales customer engagement
- [!26887](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26887) Change name from "customer collaboration project" to "Customer Success Plan" Update source/handbook/customer-success/tam/engagement/index.html.md
- [!26861](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26861) Initial information for the FE Configure & Serverless team page
- [!26853](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26853) Foundation for automating release post (changelog style)
- [!26512](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26512) Add Design Review process to handbook
- [!26405](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26405) Changes to prioritization section of Product handbook
- [!25736](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25736) Update DMP page with buyer journey content model
- [!25055](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25055) Adding Bullets to make the table notes more legible
- [!24056](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24056) Weekly Department Audit

### 2019-08-03
- [!27120](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27120) Moved Manage team page to /engineering
- [!27118](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27118) Update the handbook links on the handbook home page.
- [!27116](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27116) Added zoom rooms information
- [!27049](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27049) Update to time tracking language in handbook
- [!25564](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25564) Update Mktg handbook (Ops page) with gearing ratios from marketing model

### 2019-08-02
- [!27115](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27115) Added TOC to Experience Baselines
- [!27114](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27114) Add issue info and links to the Handbook Roadmap.
- [!27113](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27113) Recategorize Alliances and Acquisitions on the handbook home page.
- [!27107](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27107) Recategorize the general items on the handbook home page.
- [!27101](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27101) Fixup formatting Infra-Dev-Escalation
- [!27090](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27090) add a wording modification
- [!27089](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27089) Add 2020 dates through February
- [!27084](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27084) adding reference link to 5 dysfunctions:directness
- [!27082](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27082) Zencaster podcast FAQ
- [!27079](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27079) Update source/handbook/hiring/target-companies/index.html.md
- [!27073](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27073) Adding the transparency link.
- [!27071](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27071) Update tech stack details
- [!27068](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27068) replace png by embedded mermaid flowchart, reword copy-n-pasted text
- [!27063](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27063) Change wording about why groups dont get Gold free
- [!27061](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27061) new CES alignment for August
- [!27060](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27060) add Zero Trust webcast to yml
- [!27058](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27058) Add locality to BambooHR
- [!27054](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27054) Update Triage Ops links throughout handbook
- [!27047](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27047) modify routing rules
- [!27046](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27046) update onboarding tracker link info
- [!27044](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27044) update sdr handbook pg
- [!27026](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27026) CEO shadow page updates
- [!27016](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27016) Add a CEO section to the handbook home page.
- [!27011](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27011) add instruction to update
- [!27006](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27006) Update new hire location factor definition to be based on the last 90 days (instead of last calendar month)
- [!26969](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26969) Remove EA for scheduling, add PeopleOps Specialist
- [!26865](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26865) Add Plan stage team planning page
- [!26848](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26848) add implementing KPIs in Periscope
- [!26840](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26840) Update "Customer Onboarding" page with Playbook definition and location.
- [!26830](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26830) Add a link to the Experience Baseline spreadsheet
- [!26692](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26692) Book club lessons
- [!26613](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26613) Reword problem statement
- [!26442](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26442) Update infrastructure envs w/ gitlap
- [!25860](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25860) Development on-call process for GitLab.com infrastructure escalations

### 2019-08-01
- [!27005](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27005) Revert Personal Mobile Device access only GREEN data
- [!27001](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/27001) Link to Labor and Employment Notices from the handbook home page.
- [!26993](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26993) update tasks to include checking CEO calendar
- [!26991](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26991) Updated two typos and added partner criteria suggestion to the Alliances section in Handbook
- [!26990](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26990) Personal Mobile Device access only GREEN data
- [!26986](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26986) Added link to growth strategy page, fixed typos
- [!26980](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26980) Correct a typo.
- [!26972](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26972) Fixing links to the growth board
- [!26971](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26971) Use another user for experience baselien
- [!26966](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26966) Link DRI from the engineering hanbook to the definition
- [!26963](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26963) Handbook Finance KPIs periscope links
- [!26958](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26958) Add break down of dedicated designer per Release group
- [!26951](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26951) updated the compensation part of the job vacancy
- [!26950](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26950) Added Trust Center link to compliance handbook
- [!26944](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26944) Change anniversary announcements to team-member-updates channel
- [!26943](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26943) Updated board and team meeting descriptions
- [!26937](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26937) Add info on recurring team meetings to Data Team Page
- [!26930](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26930) Add chatops command to how-to-get-help section
- [!26929](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26929) Handbook typo fixes
- [!26926](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26926) Update hackathon section with issue template
- [!26921](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26921) Add printer amend access
- [!26914](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26914) Add benefits not being implemented currently.
- [!26913](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26913) Add Periscope tip about exporting images
- [!26903](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26903) Move portfolio to under review
- [!26895](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26895) Start a Labor and Employment Notices index.
- [!26883](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26883) Reworked IA of the UX Research handbook
- [!26837](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26837) Update Security team OKR board links
- [!26832](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26832) Clarify Grammarly usage
- [!26821](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26821) Add how we work details around UX ready and manager feedback
- [!26729](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26729) Update time off information in UX handbook
- [!26720](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26720) Update product discovery description
- [!26667](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26667) Improve triage documentation
- [!26437](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26437) Adding support requests table to Geo Homepage
- [!26419](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26419) Delete actionable feedback section
- [!26415](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26415) Removing Comm guidelines, GitLab U, and Communicating product vision sections from Product handbook
- [!25314](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25314) Add labels to group that have custom labels, and add categories/features labels for each group

### 2019-07-31
- [!26908](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26908) Remove Meltano as Dept + fix People Ops name
- [!26907](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26907) Calling out casting in base models
- [!26901](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26901) Start a Handbook Roadmap page in the Handbook.
- [!26900](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26900) Update instructions on zoom room log in at Mission Control
- [!26896](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26896) added Tech Marketing workshop links & ansley
- [!26890](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26890) Key meeting update.
- [!26888](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26888) Fix typo
- [!26882](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26882) Vacation Delegate for Expensify
- [!26878](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26878) Re-ordering usecases
- [!26875](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26875) Communicate early and broadly about expected automation impact
- [!26863](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26863) Move location of note
- [!26859](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26859) Update Australia entity
- [!26858](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26858) Fix broken link on How GitLab is Enterprise Class.
- [!26854](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26854) Remove handover process unless ongoing escalation, use phones for backup paging
- [!26849](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26849) Expense approval process
- [!26846](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26846) Add a table of contents to the handbook index.
- [!26843](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26843) Update customer onboarding guide for TAMs to simplify and update the process
- [!26839](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26839) Updating UseCase definition and key Usecases
- [!26838](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26838) Clarify further how coverage works when team member and dependent are employed by GitLab.
- [!26833](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26833) Update source/handbook/business-ops/index.html.md
- [!26829](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26829) change how to transfer ticket from Support to Security
- [!26828](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26828) Update source/handbook/legal/global-compliance/index.html.md
- [!26826](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26826) Added Data Protection Impact Assessment requirement to the Procure to Pay process
- [!26793](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26793) Update buyer journey content model with content mktg link
- [!26787](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26787) Add 2019 Research epic link and copy tweaks
- [!26760](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26760) Added info about Mural and anonymous collaborators
- [!26740](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26740) Removed the potentially outdated sentence.
- [!26734](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26734) Handbook Update: Periscope Links People Group KPIs
- [!26674](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26674) Quality Engineering performance grooming with Development and PM
- [!26566](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26566) Add how to use group labels in throughputs
- [!26417](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26417) Delete meta issues section
- [!26282](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26282) Cost Per Team Member First Iteration
- [!26248](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26248) Move PeopleOps cost per team member to out of scope
- [!26162](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26162) Update MVC content
- [!26022](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26022) Updated to reflect that company calls no longer feature team introductions.
- [!25954](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25954) Request candidates reapply after 6 months, unless they gain the skills required
- [!25907](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25907) contractor process in BambooHR
- [!25475](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25475) Align policy with values of transparency and assuming positive intent
- [!25398](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25398) Update index.html.md with updated labels section
- [!23623](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23623) Add missing links, reformat non links

### 2019-07-30
- [!26834](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26834) Adding detail to Dogfooding
- [!26825](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26825) Added link to the DPIA issue template and corrected formatting
- [!26820](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26820) Update index.html.md
- [!26813](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26813) updates to accounting handbook
- [!26811](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26811) Add new staff to Serverless eng page
- [!26810](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26810) Move portions of product handbook over to direction page
- [!26807](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26807) Link to the new Notices index from the main handbook index.
- [!26806](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26806) Fixing cut/paste error. Adding fourth label
- [!26805](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26805) Start a Notices index.
- [!26802](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26802) update the retention of company call agenda
- [!26800](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26800) Fix typo and update Livestream
- [!26798](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26798) Update source/handbook/legal/global-compliance/index.html.md
- [!26797](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26797) Update index.html.md Added the Nadel portal link to NORAM FM swag process
- [!26794](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26794) Clean up Dogfooding section and link to new board
- [!26792](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26792) Update YouTube directions re: one livestream at a time
- [!26783](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26783) Only one engineer is on call at a time
- [!26781](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26781) Add link styling to Monitor team videos
- [!26780](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26780) Update recommended size of training class
- [!26771](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26771) Fix the "domain experts" link on the Frontend Department page.
- [!26769](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26769) Fix the Product Manager links on the Secure Team page.
- [!26768](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26768) Fix a few Configuration Management links on the GitLab Security Compliance Controls page.
- [!26766](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26766) Fix the "Social response time" link on the KPI Index page.
- [!26756](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26756) Add `Prioritizing for Predictability` to Product Handbook
- [!26755](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26755) Update sales enablement info in PMM handbook
- [!26754](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26754) change sales enablement page
- [!26752](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26752) Fixed minor formatting error
- [!26733](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26733) Add some more motivation to time-to-remediation targets for ~security issues
- [!26730](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26730) Added period to two sentences
- [!26725](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26725) Added link to How-To Chorus videos on Sales Training page
- [!26706](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26706) Fix typos on /handbook/about
- [!26696](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26696) Add #diversity--inclusion link as well as Diversity & Inclusion in a few new areas
- [!26656](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26656) Clarify instructions for reporting a suspected phishing email
- [!26627](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26627) Update stages.yml with new EM for Dynamic Analysis
- [!26603](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26603) Fix spelling of notifcation -> notification on Contracts page
- [!26574](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26574) Remove duplicative content from UX handbook and cross link
- [!26554](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26554) Add spike process to Monitor stage
- [!26447](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26447) Add triage instructions for spam issues
- [!26413](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26413) removing special considerations section
- [!26095](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26095) Marketing KPI changes
- [!26010](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26010) Add scalability to CS inititives
- [!25902](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25902) Replace RCA with Incident Review and set criteria for wen a review is required
- [!21603](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21603) Proposed Import group for Manage stage

### 2019-07-29
- [!26750](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26750) Added Compliance to the title to facilitate searches of the handbook
- [!26748](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26748) Add phone and door to tasks
- [!26745](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26745) SecOps rewrite for detect and response
- [!26744](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26744) fixes broken links
- [!26743](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26743) Revert "Add phone and door."
- [!26737](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26737) Adding Steph Sarff.
- [!26736](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26736) Update links in the introduction section of the handbook index.
- [!26735](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26735) Edit some "some" on "About the Handbook".
- [!26723](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26723) Adding missing content to new SDR Coaching page
- [!26722](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26722) Added Distribution team training link
- [!26721](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26721) Adding content to Sales Enablement Sessions page
- [!26719](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26719) Updates to Sales Training page
- [!26718](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26718) Remove outdated process files
- [!26717](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26717) Roadmap link on EB page
- [!26711](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26711) Update "Development on-call process for GitLab.com infrastructure escalations" FAQ to take account of Nick's changes to process
- [!26709](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26709) updated table employee of GitLab entity
- [!26707](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26707) Make career progression easier to read
- [!26704](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26704) Add GitLab IT BV to the wording.
- [!26703](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26703) Add secure coding training page
- [!26702](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26702) Updates to number of people and location
- [!26698](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26698) Fix Pajamas link and describe Design project in UX Resources
- [!26691](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26691) Added a link to Iain Camacho's GitLab 'About Me' page to the 'About our team'...
- [!26686](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26686) Jj move enablement
- [!26673](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26673) Move test heuristics to main test engineering page
- [!26652](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26652) add gitlab-org to the chart about where labels are located
- [!26634](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26634) Add Self-Serve Analysis in Pericope Documentation
- [!26624](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26624) Remove reference to closed issue in Product Handbook
- [!26623](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26623) Add another link about competitive advantage in security
- [!26617](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26617) Fix broken image link on BizOps Page
- [!26614](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26614) Nick's proposal to "Development on-call process for GitLab.com infrastructure escalations"
- [!26606](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26606) adding language for engaging ongres to the database page
- [!26565](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26565) created new directory for bizops portal page
- [!26491](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26491) benefits for contractors
- [!26473](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26473) Link to IACV definition from incentives page
- [!26373](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26373) remove alliances and add meltano
- [!26161](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26161) Adding strategy page for Enablement and Growth groups
- [!25684](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25684) Fix link for security related software.
- [!25649](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25649) Specify the category under which the IACV Target Dinner Evangelism Reward should be filed in Expensify
- [!22093](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22093) Adds some thoughts on ticket deflection

### 2019-07-28
- [!26683](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26683) Added new directory in preparation for moving sales enablement sessions from Marketing to Sales
- [!26682](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26682) Minor updates to Sales Handbook QRG section
- [!26681](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26681) Sales Training Page updates
- [!26680](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26680) Time tracking data - fixed table formatting
- [!26672](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26672) Including expanded time tracking language in handbook
- [!26640](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26640) Update index.html.md Updates to the NORAM field marketing swag instructions
- [!26638](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26638) Be more specific
- [!26628](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26628) Update dmp section website handbook
- [!25819](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25819) Add active A/B tests, use more descriptive language, add related issues

### 2019-07-27
- [!26666](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26666) Support Handbook: Internal Support: Add contact user case
- [!26662](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26662) 5 dysfunctions
- [!26658](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26658) 5 dysfunctions
- [!26651](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26651) Gather handbook meta-info on a new "About the Handbook" page.
- [!26330](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26330) Fix broken links to RCA resources

### 2019-07-26
- [!26655](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26655) Added Fullstack Eng. baseline entitlements to handbook
- [!26649](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26649) added link
- [!26648](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26648) fix links
- [!26644](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26644) Structure the page with better headers
- [!26643](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26643) adds Clint, Lyle and Marin to Shadow alumni
- [!26642](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26642) Rename AdWords and update link /handbook/business-ops/tech-stack/#google-adwords.
- [!26641](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26641) Fix the campaign request info link at /handbook/marketing/events/#event-execution.
- [!26639](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26639) Fix the campaign request info link at /handbook/marketing/corporate-marketing/#corporate-event-execution-process.
- [!26632](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26632) Fix the payroll deductions link at /handbook/benefits/inc-benefits-us/#us-specific-benefits.
- [!26631](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26631) Add GitLab Unfiltered link for external meeting invites/comms
- [!26618](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26618) Breaking paragraphs into one sentence per line for better diffs in the future
- [!26616](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26616) Move Periscope MR process to data team project
- [!26615](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26615) Updating calculation for Rep Productivity from Gross IACV to Net IACV
- [!26611](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26611) Added Red Team baseline to handbook and corrected one backend entitlement based on pervious MR
- [!26610](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26610) Add Survey Link info to Company Kickoff Process
- [!26609](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26609) update outdated material in instructions
- [!26607](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26607) adding addendum workflow to contract process
- [!26599](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26599) Update Fulfillment dev page to reflect feature freeze date
- [!26586](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26586) Link and TOC fix for red team roe page
- [!26581](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26581) Fix the throughput link at /handbook/business-ops/data-team/#milestone-planning.
- [!26580](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26580) Fix the link to meltano.com at /handbook/business-ops/data-team/#snowflake-permissions-paradigm.
- [!26578](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26578) Removed duplicate "it" and changed "diversity & iteration" to "diversity &...
- [!26575](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26575) Support: Update cross-functional counterpart list
- [!26573](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26573) Add TOC and copy edit product dev flow
- [!26572](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26572) word change to participants
- [!26568](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26568) word change: manager to team members
- [!26550](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26550) Fix the "list of countries…" link at /handbook/marketing/corporate-marketing/.
- [!26546](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26546) explicitly stating excluding certain expenses from sign matrix
- [!26534](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26534) Include SFDC <> ZD sync information
- [!26517](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26517) Removes Jira / Integrations From Plan's Scope
- [!26507](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26507) Update topics lists
- [!26492](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26492) update action items for survey with links/status
- [!26478](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26478) Updating Thresholds for Budgeting
- [!26459](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26459) Social strategy update
- [!26366](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26366) Only include adding metrics on APM page
- [!26185](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26185) initial draft of red team rules of engagment (ROE)
- [!25837](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25837) Use group::distribution label instead of Distribution team label
- [!25708](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25708) Add details on onboarding and offboarding issues for Core Team members

### 2019-07-25
- [!26561](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26561) moved IDSes from it's own line item to after e.g.
- [!26548](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26548) Update source/handbook/sales/territories/index.html.md
- [!26547](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26547) Update spending company money with prof dev certs
- [!26535](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26535) Removing Microsoft from a list of our current Customers
- [!26529](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26529) Swap code review link for a more relevant one
- [!26520](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26520) Adding new recruiters/new functional alignment
- [!26514](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26514) Add links to useful GitLab Unfiltered videos.
- [!26510](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26510) Adding Merge Request buddy to handbook
- [!26509](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26509) Mention that email confirmation from manager is sufficient for relocation
- [!26508](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26508) Fix Youtube invite access link
- [!26505](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26505) Fixing some typos in the swag section
- [!26500](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26500) adding why ultimate to sales resource page
- [!26490](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26490) moved info from other pages here and added weekly task to check onboarding sheet
- [!26489](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26489) removed contract info
- [!26488](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26488) Fix the Pajamas Design System link capitalization and URL at /handbook/engineering/ux/.
- [!26486](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26486) Fix the "Google Sheet" spacing and capitalization at /handbook/business-ops/data-team/.
- [!26485](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26485) Capitalize Google service names at /handbook/business-ops/.
- [!26484](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26484) removed duplicate info
- [!26483](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26483) updated sending contracts to remove duplicates from process framework
- [!26482](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26482) Add description of primary vs secondary fundraising
- [!26474](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26474) Add dogfooding video to engineering handbook
- [!26472](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26472) Fixed broken link
- [!26471](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26471) taxation on stock options
- [!26470](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26470) add principal/group PM
- [!26453](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26453) Jj trainingpage
- [!26420](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26420) Remove absolute links in handbook/engineering/ md files
- [!26339](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26339) Update latest iteration of milestone grooming
- [!26164](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26164) Add docs for additional automations for triage
- [!26092](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26092) Add process sections to Monitor stage

### 2019-07-24
- [!26479](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26479) Remove a stray article in a sentence
- [!26466](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26466) Add Quality to the grooming infra/dev board
- [!26465](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26465) Change "EA" to "EBA", fix a link, link "my EBA", and add periods at /handbook/ceo/.
- [!26463](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26463) Adding "Meetings will begin promptly regardless of shadow attendance".
- [!26458](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26458) Make include_file default summary method for comparison pages
- [!26454](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26454) Include how Account -> Org info is relevant to SLAs
- [!26452](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26452) update CEO/CRO interview scheduling and add email to EBA
- [!26451](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26451) add link to corp mktg job fam
- [!26449](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26449) update wording/formatting for Slack pinging
- [!26445](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26445) Handbook Finance KPI Link Updates
- [!26443](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26443) Update standup description for Monitor
- [!26439](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26439) Added new video from John Jeremiah
- [!26438](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26438) update strategy horizon to 3 years
- [!26436](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26436) Update sales resource page
- [!26433](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26433) Add note about mentioning whole teams in Slack
- [!26430](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26430) Add emphasis formatting to the sentence about onsite vs. offsite
- [!26427](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26427) Change slack link to gaming channel to be correct
- [!26426](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26426) Capitalize "Google" on the Jira workflow page.
- [!26425](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26425) Capitalize "Google" on the Jenkins workflow page.
- [!26424](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26424) Normalize Google Doc capitalization on /handbook/communication/.
- [!26412](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26412) Updated SG.1.01 control statement
- [!26394](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26394) move of stock option tax treatment to separate page
- [!26380](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26380) KPI change requires approval of the function head and the CEO
- [!26377](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26377) CEO approves manager, staff and principal PM hires
- [!26347](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26347) update exec recruiting/interview process
- [!26087](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26087) Updated people ops taking over the generation of on-boarding issues
- [!26082](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26082) Add least privilege process
- [!26066](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26066) Adding MR approval click-through demo to the demo page
- [!26060](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26060) Collaboration: It's impossible to know everything
- [!25614](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25614) Updates to UX How We Work page

### 2019-07-23
- [!26418](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26418) delete gitlab.com metrics section
- [!26414](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26414) Typos in breaking change section
- [!26409](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26409) update strat horizon to three years
- [!26406](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26406) Link to UX dept
- [!26402](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26402) Remove goals section of product handbook
- [!26401](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26401) Add link to Seth readme
- [!26400](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26400) fixes to responsibilities section of product page
- [!26398](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26398) Adding link to Product Development flow page
- [!26393](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26393) taxation when stock option exercised
- [!26387](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26387) handbook: Breaking out Accounting & Legal Departments
- [!26385](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26385) Add interview cancellation info
- [!26383](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26383) Updates IAM.2.02 Control Wording
- [!26374](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26374) Fix typo
- [!26372](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26372) Adds the June and July frontend themed calls
- [!26365](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26365) When to direct to AE vs Online portal
- [!26360](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26360) Add a section to probation about underperformance
- [!26358](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26358) Fix broken image and update link at /handbook/marketing/product-marketing/competitive/cicd/#ci--cd.
- [!26355](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26355) Remove a broken and obsolete Slack link from /handbook/hiring/offers/#next-steps.
- [!26354](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26354) Update timesheet info link at /handbook/people-operations/code-of-conduct/#fair-wages.
- [!26353](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26353) Fix broken e-mail links at /handbook/people-operations/code-of-conduct/.
- [!26348](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26348) fix typos in chat and youtube comm pages
- [!26346](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26346) Update GitLab Positioning everywhere
- [!26328](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26328) Clarify definitions of ~feature and ~backstage throughput labels
- [!26322](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26322) Trial extension update
- [!26318](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26318) Add Agile Deck to Sales Resource Page
- [!26303](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26303) Moves all workflows to root folder; fixes links in workflows index
- [!26231](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26231) Add security compliance handbook page
- [!26210](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26210) move lee matos readme to engineering readmes
- [!26187](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26187) Add Matt Nohr README to list of engineering readme pages
- [!26179](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26179) Updated Quote to Cash Cycle
- [!25691](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25691) Fix broken link in the handbook - "Spending Company Money"
- [!25576](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25576) Update 1password for your private password link

### 2019-07-22
- [!26343](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26343) Added layout and TOC to IAM.2.01
- [!26336](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26336) Updated IAM201 control
- [!26326](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26326) Update Company Kickoff Process to remove the "Merged" status update
- [!26325](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26325) Cleanup Important Dates for PMs for improved Kickoff process
- [!26323](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26323) Clarify PMs responsibility regarding community contributions
- [!26321](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26321) Removing SDR Assesment Criteria because we don't do it this way - TBD for an update in the future.
- [!26320](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26320) Changing "Team Lead" to "Manager"
- [!26319](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26319) Adds further clarity on what Stage Monthly Active Users is defined by.
- [!26319](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26319) Adds further clarity on what Stage Monthly Active Users is defined by.
- [!26316](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26316) Updated page title reflect removal of VPN in control name and add Remote Connections
- [!26307](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26307) Adding image file
- [!26306](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26306) Fixed typo
- [!26300](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26300) Add to review checklist
- [!26298](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26298) Competitive intelligence link updates
- [!26294](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26294) Add Failure Management Rotation as child page
- [!26291](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26291) update contracts round 1
- [!26286](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26286) Add training videos to Sales Training page
- [!26283](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26283) multiple updates needed due to outdated content
- [!26263](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26263) Re-formatted Employee Privacy Policy commits and pushed new MR
- [!26249](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26249) Add Inclusion Section to Greenhouse Page
- [!26191](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26191) Updated Apply to Offer Accept KPI; Updated NPS to SAT
- [!26181](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26181) Update NW territory coverage
- [!26157](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26157) Updated information for CT team members and compliance for anti harassment training
- [!26148](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26148) Add Release UX link to ux department page
- [!26146](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26146) Australia Parental Leave
- [!26140](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26140) Update zero trust links to use Kramdown URL syntax
- [!26065](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26065) Minor improvement of the description regarding the ZenDesk registration.
- [!26004](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26004) Rename `Diversity` value into `Diversity & Inclusion`
- [!25898](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25898) Churn MRR Dashboard Review and add topic link
- [!25615](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25615) Release post - GitLab 12.1

### 2019-07-21
- [!26281](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26281) Update elevator pitch link
- [!26253](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26253) Update XDR issue board
- [!26197](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26197) Update XDR SFDC Report Links
- [!24960](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24960) change salesteam to sales-all

### 2019-07-20
- [!26272](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26272) Add section headings to the handbook index.
- [!26271](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26271) Update the Slack status article link and related info at /handbook/tools-and-tips/#slack-status.
- [!26270](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26270) Fix Hangouts On Air links and capitalization at handbook/tools-and-tips/#hangouts-on-air.
- [!26269](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26269) Fix a 1Password link on the tools-and-tips page.
- [!26265](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26265) Update index.html.md
- [!26261](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26261) Remove redundant sentence.
- [!26260](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26260) Update policy for coverage when a team member's dependent is employed by GitLab.
- [!26256](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26256) Update source/handbook/people-operations/code-of-conduct/index.html.md
- [!26166](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26166) Clarify language
- [!26016](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26016) New product KPIs

### 2019-07-19
- [!26259](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26259) Add time-to-value metrics to the Vision Page
- [!26255](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26255) Update source/handbook/business-ops/data-team/metrics/index.html.md
- [!26252](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26252) Add link to feature proposal template
- [!26246](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26246) Added policy review cadence and approvers
- [!26244](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26244) Cosmetic updates to product dev flow page
- [!26241](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26241) Fix typo
- [!26238](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26238) Key monthly review
- [!26226](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26226) Operating Plan vs Financial Model
- [!26225](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26225) rmv alex from territory table
- [!26220](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26220) added the Candidate Experience Specialist is responsible for changes after contract is signed
- [!26212](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26212) updated and corrected titles
- [!26199](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26199) Clean up Links
- [!26190](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26190) Fixes broken Pajamas link
- [!26151](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26151) Update SMB Sales Territories in handbook
- [!26147](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26147) Add monitor to equipment examples
- [!26139](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26139) Fix URLS for dashboards
- [!26094](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26094) Updated the onboarding issue creation to the people ops team
- [!26034](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26034) Specify how to select throughput label for documentation issues
- [!25877](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25877) New Product Development Flow Page
- [!25375](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25375) Add APM product issue board to handbook
- [!25079](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25079) Updated Geo Team Planning information

### 2019-07-18
- [!26195](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26195) Changed NPS to SAT
- [!26186](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26186) Update Offer Acceptance Rate definition
- [!26174](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26174) stock-refresh-cadence
- [!26170](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26170) Fix typos in Stable Counterparts section of Quality Dept page
- [!26159](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26159) Add when we iterate slowly
- [!26155](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26155) July Eng Key Review meeting
- [!26153](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26153) Update source/handbook/marketing/product-marketing/enterprise-it-roles/index.html.md
- [!26150](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26150) Added BackendEngineer - ProductivityEngineering to baseline entitlement listing in handbook
- [!26145](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26145) Add inbound label to data team workflow
- [!26141](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26141) rmv abso link
- [!26126](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26126) Update Release UX index.html.md
- [!26114](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26114) Update hash for definition of MVC in Value page
- [!26111](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26111) Replace absolute acquisitions link URLs with relative versions.
- [!26110](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26110) Replace absolute acquisition-process link URLs with relative versions.
- [!26109](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26109) Link some URLs at /handbook/engineering/index.html.
- [!26083](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26083) Reorganize support direction, update headers
- [!26073](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26073) Update team meeting doc
- [!26067](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26067) Update mpm handbook
- [!26042](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26042) Fix broken Google Calendar links at /handbook/tools-and-tips/.
- [!25959](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25959) Update source/handbook/people-operations/code-of-conduct/index.html.md
- [!25938](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25938) Fixed blog link for Work In Progress section
- [!25859](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25859) Revamp Kickoff Process
- [!25796](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25796) Minor update on examples for spending company money

### 2019-07-17
- [!26105](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26105) rmv abso links
- [!26104](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26104) Added Vendr language to handbook on Vendor Contracts page
- [!26102](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26102) added link to new events page
- [!26096](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26096) moved the onboarding issue creation steps from the CES pages to the people ops page
- [!26093](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26093) Update index.html.md Added NORAM Field Marketing Venue Search process
- [!26090](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26090) Update index.html.md Added NORAM Event Deadlines Process for MPM Tasks
- [!26086](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26086) Remove link title text that breaks rendered HTML at /handbook/communication/#user-communication-guidelines.
- [!26080](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26080) updating FP&A handbook with purpose, how we will get there and general updates
- [!26076](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26076) remove mixer calls
- [!26074](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26074) Add suggestions on running a book club
- [!26068](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26068) additional language to the code of conduct directions
- [!26058](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26058) Update the Code of Conduct link at /handbook/communication/#user-communication-guidelines.
- [!26057](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26057) Update and reformat the Pomodoro Technique section at /handbook/tools-and-tips/#the-pomodoro-technique.
- [!26056](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26056) Fix, link, and describe the 2FA debugging URLs at /handbook/tools-and-tips/#2fa-debugging.
- [!26055](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26055) Update the page count on the handbook index.
- [!26054](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26054) Update index.html.md Updated Nadel POC for swag reorders
- [!26050](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26050) Add smalls improvements into infrastructure delivery index
- [!26046](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26046) Adding weights standards to the Memory Team page
- [!26019](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26019) Added Field Security Section Under Strategic Sec
- [!26005](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26005) Few more changes to the failure management rotation process
- [!25924](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25924) Updating handbook to reflect new Snowflake roles
- [!25876](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25876) Assigned Scott Larson (me) to MM-Mt-East/Dallas Territory code
- [!25863](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25863) Updated the (Recruiting Process Framework - Sourcer Tasks)
- [!25826](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25826) Describe time sheet/payment frequency for CXC contractors
- [!25816](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25816) degraded and outage def
- [!25516](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25516) Add additional details on data source for Support KPIs

### 2019-07-16
- [!26053](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26053) Fixed typo and corrected the Bar-Raiser page link
- [!26047](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26047) Revert Marketing KPI's on Index Page to Original Set
- [!26041](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26041) update table
- [!26039](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26039) Added Bar-Raiser Page and Updated Interviewing Page
- [!26038](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26038) fixed file path for team org image
- [!26036](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26036) language around promo doc being editable
- [!26030](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26030) Add Heroes detail to EP handbook
- [!26028](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26028) Revert MR 25812 handbook satisfaction definition location
- [!26026](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26026) additional language around CoC
- [!26024](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26024) updated template instructions
- [!26023](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26023) Add instructions for uploading cover pics
- [!26021](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26021) Adding Detail Investor Update Process
- [!26017](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26017) Added AR requirement for groups/repos gitlab and instructions for baseline entitlement updates
- [!26014](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26014) Update index.html.md Added NORAM FM swag details
- [!26011](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26011) correct spelling and move images
- [!26008](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26008) Fix typo 'get hep' -> 'get help'
- [!26007](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26007) Improvement to CEO shadow page
- [!26000](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26000) Update and correct the Freedom section at https://about.gitlab.com/handbook/tools-and-tips/#freedomto
- [!25999](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25999) Update the handbook page count section at /handbook/tools-and-tips/#count-handbook-pages
- [!25996](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25996) Definition of Done for Delivery:Dev Backend engineers
- [!25979](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25979) Fix a link and improve the search URL formatting at handbook/tools-and-tips/#alfred
- [!25976](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25976) update directions for mac users to confirm full disk encryption
- [!25974](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25974) Added IT Exception process and color to administrative access criteria
- [!25971](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25971) Updated VPN security control
- [!25969](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25969) Update to NORAM West SMB Territory
- [!25964](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25964) Follow-up from "Add UX Collaboration Model": Update toolchain image to be GitLab focused
- [!25962](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25962) Added BYOD section to the GitLab Internal AUP. Iteration number one
- [!25922](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25922) Update source/handbook/marketing/product-marketing/messaging/index.html.md
- [!25845](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25845) Adding missing members of team
- [!25783](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25783) Benefits Survey Results
- [!25752](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25752) Add charts and monitor bugs issue board to monitor health handbook page
- [!25733](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25733) update 360 to annual time frame
- [!25693](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25693) Add a snippet about what is not dogfooding
- [!25458](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25458) Add predictable mechanism to engage developers for infrastructure

### 2019-07-15
- [!25972](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25972) Fix Geekbot URL on Data Team Page + GitLab Free for Startups Dashboard Review
- [!25953](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25953) Add EAP to burnoout
- [!25950](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25950) Add a few steps to the probation period process for PeopleOps Specialists
- [!25945](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25945) Update handbook with current Moo tasks
- [!25944](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25944) updated tax treatment of stock options
- [!25928](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25928) Update US benefits page to include SBCs where available.
- [!25911](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25911) Add info about boards and epics
- [!25891](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25891) Adjust release timelines for our new auto-deploy world
- [!25871](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25871) Change gitlab-com/gitlab-docs to gitlab-org/gitlab-docs
- [!25866](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25866) Added warning regarding upcoming service shutdown to "hangouts on air" section of tools page.
- [!25831](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25831) Signing up for coverage via Lumity at a later date
- [!25821](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25821) Add example criteria for MVP
- [!25812](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25812) Handbook Update Satisfaction
- [!25753](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25753) Add section about frontend scheduling for monitor health handbook page
- [!25739](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25739) fix structured services definition link
- [!25508](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25508) Updating wording from Function --> Division
- [!25151](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25151) Updated incident management responsibilities

### 2019-07-13
- [!25936](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25936) Update index.html.md for Okta Application Stack, add new applications and update status of others.
- [!25933](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25933) Fix the probation period link under Other People Policies on the Code of Conduct page
- [!25932](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25932) Capitalize "Linux" at handbook/tools-and-tips/#gitlab-team-members-setups
- [!25931](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25931) Update the Alfred info at handbook/tools-and-tips/#alfred
- [!25930](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25930) Update the Webex download link at handbook/tools-and-tips/#webex
- [!25929](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25929) Board of directors renewal process
- [!25927](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25927) Update index.html.md added a table that describes the labels that bizops uses for issues/MRs
- [!25908](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25908) Add ops week-in-review
- [!25865](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25865) Clarified definition of Sales Efficiency and Field Efficiency Ratios
- [!25807](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25807) Add Periscope links to Engineering KPIs

### 2019-07-12
- [!25915](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25915) Added Test Automation Engineer to baseline entitlements outlined in the handbook
- [!25905](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25905) Board meeting is in a video call
- [!25901](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25901) add enablement to the schedule 7/12/19
- [!25899](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25899) Updating Names in Org
- [!25896](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25896) Update index.html.md.  This change adds the new Public Sector team members as...
- [!25893](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25893) Filter out current month in Periscope dashboards
- [!25890](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25890) Update Secure UX index.html.md
- [!25885](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25885) update offbaorded laptop buy back/return
- [!25884](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25884) Clarify "GitLab" capitalization guidance
- [!25883](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25883) Update index.html.md
- [!25881](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25881) Adding additional comment on preference for virtual/remote delivery
- [!25875](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25875) Add check for meaningful links
- [!25867](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25867) Product manager rather than Project manager
- [!25864](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25864) Add telemetry page
- [!25862](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25862) Another minor change to Sales Onboarding page in the Handbook
- [!25857](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25857) Update source/handbook/customer-success/tam/escalations/index.html.md
- [!25855](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25855) Move personal readme of Marin Jankovski to engineering readme
- [!25854](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25854) Updated Social Selling section with new YouTube GitLab Unfiltered links
- [!25853](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25853) adds product/engineering interaction
- [!25852](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25852) Update source/handbook/customer-success/tam/escalations/index.html.md
- [!25850](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25850) Added a bullet to highlight future plans to extend Sales Onboarding
- [!25848](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25848) adds underperformance quote
- [!25847](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25847) Edits to the CEO shadow page
- [!25846](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25846) Update product metrics page to use "performance indicators" terminology
- [!25842](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25842) Included link to KiwiSaver website
- [!25825](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25825) Add clarity to Services Attach Rate
- [!25824](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25824) Add clarity to self-serve sales ratio
- [!25818](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25818) Added link to Retention Dashboard to Customer Success Vision Page
- [!25803](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25803) Updating user type: adding CISO
- [!25798](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25798) Add preferences for explicit dating methods for SQL style guide
- [!25795](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25795) Update triage schedule to be explicit.
- [!25785](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25785) Fix typos to render headings correctly in Alliances
- [!25772](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25772) Add page for IT Help to BZO handbook
- [!25755](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25755) Add benefit of a single application
- [!25735](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25735) Update source/handbook/people-operations/promotions-transfers/index.html.md
- [!25694](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25694) Add smart speaker best practice
- [!25681](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25681) Best-effort database on-call
- [!25633](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25633) New Workflow: Sending Notices
- [!25579](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25579) QA Failure Management Rotation
- [!25550](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25550) Add a bullet to the Spending page to include business cards as reimbursed office supplies
- [!24382](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24382) New page to request technical evangelism CFP help
- [!24248](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24248) updates secret snowflake page, reorganizes info
- [!23136](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23136) Update Quality triage instructions for support requests

### 2019-07-11
- [!25811](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25811) Add clarity to win rate
- [!25810](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25810) Add clarity to Field Efficiency Definition
- [!25805](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25805) updating instructions for how to get help from SRE with the "~SRE:On-call" label
- [!25802](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25802) Growth engineering pages cleanup
- [!25794](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25794) Small improvements to the SQL style guide
- [!25793](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25793) Update source/handbook/finance/financial-planning-and-analysis/index.html.md
- [!25791](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25791) Add MR instructions to blog hb
- [!25789](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25789) Update Growth Engineering pages
- [!25787](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25787) Zendesk agent signature: add role, remove 'thanks'
- [!25778](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25778) Adding a SOX  sub-section the handbook
- [!25777](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25777) Remove CR from Data Team Triage schedule
- [!25773](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25773) Moving the Acquisition KPIs to the Product section
- [!25769](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25769) KPI Index: Update LTV/CAC ratio to out of scope for Q2
- [!25757](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25757) Added Sales QuickStart info to Sales Onboarding page of the Handbook
- [!25740](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25740) Change boardroom to mission control in the ceo shadow
- [!25703](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25703) Add zoom meetings details and visitor prep details
- [!25682](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25682) Create events page in handbook
- [!25513](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25513) Create New Directories for Misplaced Sales Pages
- [!25465](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25465) New Periscope Dashboard Review process
- [!25297](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25297) Consolidate support team business drivers
- [!24368](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24368) Update index.html.md commented out anniversary swag since program is suspended
- [!24257](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24257) Social image request DRI

### 2019-07-10
- [!25764](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25764) document data team password rotation
- [!25761](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25761) Add Social Selling Basics content to Handbook
- [!25760](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25760) Adding CSAT changes to handbook
- [!25759](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25759) fixing product page links
- [!25758](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25758) FP&A Tips for Planning
- [!25750](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25750) Adding headers for Product team mission and product team principles
- [!25747](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25747) "business operations-update to blueprints page"
- [!25741](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25741) Update source/handbook/offboarding/index.html.md
- [!25732](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25732) Restock fridge point of contact
- [!25731](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25731) Handbook improvement at the request of the on boarding issue
- [!25727](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25727) Update location of pay frequency in BambooHR.
- [!25723](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25723) Emphasize issue pointing to capture complexity, not time spent for the Data Team
- [!25719](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25719) Updated KPI Page >>> LTV/CAC ratio
- [!25718](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25718) Fix broken manager link and add Director position to the Data Team Page
- [!25716](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25716) Update onboarding time notice
- [!25712](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25712) updated markdown in table
- [!25697](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25697) add email alias information to development department page
- [!25690](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25690) Update livestream instructions
- [!25669](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25669) adding links to not-risks
- [!25566](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25566) Update QA debugging guidelines re. updating slack
- [!25552](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25552) Move Engineering Readmes
- [!25533](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25533) Add planning rotation to the Plan team page
- [!25305](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25305) Large Copy Edit of stock options handbook page
- [!25190](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25190) Clarify which field should be used in IACV calculations.

### 2019-07-09
- [!25699](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25699) Fix double spaces
- [!25683](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25683) Update DMCA Handbook Workflow for Abuse integration.
- [!25670](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25670) Clarify Quality gearing ratio wording
- [!25663](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25663) updated tax section for tuition fees
- [!25660](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25660) Update source/handbook/marketing/revenue-marketing/field-marketing/index.html.md
- [!25657](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25657) added link to blog post template
- [!25651](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25651) First Iteration to Adding Definition of Cost Per Team Member
- [!25641](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25641) Don't call it a KPI since some people will have a non KPI performance indicator.
- [!25635](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25635) Periscope Review: Gitlab.com Health
- [!25631](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25631) Note that FRH should focus on premium
- [!25629](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25629) Adding employment branding section to handbook
- [!25619](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25619) Add example to follow data heuristic
- [!25594](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25594) changed link for "gitlab voice" from 404 to a fit
- [!25591](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25591) Update sa few pages with proper people ops terms
- [!25549](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25549) Adding more details on the job approval, intake, and posting process.
- [!25420](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25420) Update transfering with manager inclusion
- [!25384](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25384) Fixed typo
- [!25275](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25275) Incident Response Containment

### 2019-07-08
- [!25642](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25642) Boardroom monitor layout change
- [!25639](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25639) Add links to all items of the cadence page
- [!25628](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25628) Fix link I accidentally broke on the Data Team Page
- [!25623](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25623) modify list import instructions
- [!25622](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25622) Add information about coffee machine to the ceo shadow page
- [!25620](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25620) Add links to the ceo/cadence page
- [!25617](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25617) moves boardroom monitor configuration to table-layout
- [!25616](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25616) Update SLA to SLO on the Data Team Page
- [!25608](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25608) Remove When Labels from Data Team page
- [!25604](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25604) Update scope of KPIs for quarter
- [!25603](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25603) grammar changes to addition
- [!25602](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25602) Remove mention of "core team" from PM Handbook
- [!25599](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25599) prtimary -> primary
- [!25597](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25597) add new label
- [!25596](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25596) Change to as. Typo on ceo shadow page.
- [!25595](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25595) adding Christie's suggestions for underperformance
- [!25592](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25592) Added the definition of underrepresented groups and provided examples.
- [!25587](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25587) Update people-operations page with right people ops terms
- [!25585](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25585) Update term - HRBP -> PBP
- [!25582](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25582) Update compensation and group conversation page with relevant people ops term
- [!25577](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25577) Add due date to onboarding
- [!25556](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25556) Fix links in Group Conversations
- [!25537](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25537) Changed 'employee' to 'team member' under the Diversity value
- [!25531](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25531) Move anniversary announcements to #company-announcements channel
- [!25505](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25505) Update People ops page with appropriate team terms
- [!25409](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25409) Add Perseverance under Results
- [!25280](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25280) Clean up Security Best Practices
- [!25139](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25139) Updating quarantining process
- [!24904](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24904) Light copy edit of the code-of-conduct page

### 2019-07-05
- [!25545](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25545) Added hyperlinks to competitive battlecards
- [!25541](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25541) Handbook Recruiting Gearing Ratios
- [!25539](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25539) Update Support Managers page to talk about Metrics with Explore and Periscope
- [!25528](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25528) Rename People Ops to People Ops Analyst
- [!25526](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25526) Add Italy and Brazil under Safeguard and update hiring process for CXC Poland and Ukraine
- [!25525](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25525) Add Paste app to tools and tips page
- [!25514](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25514) adding thailand
- [!25512](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25512) Fix outdated links in Support KPIs
- [!25503](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25503) Update hiring page with appropriate people terms
- [!25494](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25494) Adding how IACV is calculated in the case of contractual ramps.
- [!25488](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25488) added transfer pricing section and updated corporate structure
- [!25484](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25484) Adding due date for expense reports
- [!25482](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25482) Qualify 'open source' hyphenation guidance with ambiguity and clumsiness
- [!25481](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25481) Correct and clarify guidance to use PT vs. PDT or PST
- [!25480](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25480) Fix an article and capitalize 'iso' in the date-formatting guidelines
- [!25479](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25479) Adjust the serial comma guidance, but leave the joke intact
- [!25478](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25478) Clarify the purpose of Markdown and capitalize 'git' in that context
- [!25459](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25459) Capitalize 'Google Calendar' in the handbook
- [!25427](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25427) Add a sentence about new hire's that should not be seen as terminations
- [!25424](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25424) replace "this" with FRT
- [!25298](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25298) Include onboarding text in intro to support team handbook
- [!25185](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25185) Update source/handbook/finance/financial-planning-and-analysis/index.html.md
- [!25152](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25152) adds Rabois essay to board meeting page
- [!24294](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24294) Community Advocacy handbook updates [Marketing handbook hackathon]
- [!24125](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24125) Link to lists of international crisis numbers

### 2019-07-04
- [!25507](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25507) Update blog labels, move to blog hb
- [!25506](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25506) Added hyperlink
- [!25499](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25499) Update general-onboarding appropriate people ops terms
- [!25498](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25498) Update Finance Page with appropriate people terms
- [!25497](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25497) Update engineering page with right people term
- [!25496](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25496) Update contracts handbook page with appropriate people terms
- [!25493](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25493) Update handbook communications page with the right people terms
- [!25492](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25492) Update business ops page with the right people team
- [!25491](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25491) Update benefits page
- [!25490](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25490) Update handbook anti-harassment page with the right people ops team
- [!25485](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25485) Handbook :: Fixes spelling, punctuation, grammar and linking.
- [!25476](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25476) Added 2 items to Sales Handbook Quick Reference Guide
- [!25474](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25474) remove offboarded people end of June/beginning July
- [!25472](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25472) fix formatting
- [!25470](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25470) added CES Service Desk steps
- [!25469](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25469) Cleanup of Territory table formatting
- [!25464](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25464) Update source/handbook/ceo/shadow/index.html.md
- [!25416](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25416) Fix images link in compliance page
- [!25396](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25396) Re-inserted SMART Goals to PIP requirements
- [!25295](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25295) Sales Qualification Questions Revamp
- [!25217](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25217) Update People Operations Subgroup info and how to reach them
- [!25096](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25096) add app info for bamboohr
- [!25090](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25090) Belgium paternity leave processing
- [!25088](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25088) update effective date for comp changes
- [!24839](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24839) Added notifications to mgrs and stage trans to hiring mgrs
- [!24111](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24111) Introduce a process to request support from Product

### 2019-07-03
- [!25460](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25460) Move territories
- [!25450](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25450) Update source/handbook/engineering/index.html.md
- [!25448](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25448) Add bias towards action to Everything Starts with a MR
- [!25447](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25447) Apply suggestions to how we work section of ux handbook
- [!25446](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25446) Update source/handbook/engineering/index.html.md
- [!25444](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25444) Update source/handbook/engineering/index.html.md
- [!25441](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25441) Capitalize S in Slack on Data Team Page
- [!25440](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25440) Clarify category strategy is now in pages
- [!25437](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25437) Update source/handbook/engineering/index.html.md
- [!25436](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25436) Update source/handbook/engineering/index.html.md
- [!25435](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25435) Resolve "Shane's Readme"
- [!25434](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25434) Making Justification Step Optional
- [!25405](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25405) Changed "Meetups per month quarter" to per month on Community Rel page
- [!25390](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25390) Add in rotation in July
- [!25372](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25372) Update Quality page in the handbook with information AMA sessions
- [!25358](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25358) Add UX Collaboration Model
- [!25357](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25357) Add slack channels to Data Team Handbook
- [!25307](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25307) Adding SMB Advocates as a source where SDR/BDR's create Opportunities for.
- [!25188](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25188) Update source/handbook/business-ops/index.html.md
- [!25178](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25178) Update Give Back for commercial sales issues
- [!25119](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25119) Update to process for tech writer as blog post reviewer backup
- [!25109](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25109) Update issue guidelines with a prescription to regularly update the issue description
- [!24937](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24937) Update source/handbook/business-ops/index.html.md
- [!24834](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24834) Add code block instructions to blog handbook
- [!24727](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24727) Adding Problem Validation process description to Product handbook
- [!24677](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24677) Update time needed for people-ops to onboard a new team member to 4 business days.
- [!24490](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24490) Add Commercial link to Sales handbook

### 2019-07-02
- [!25413](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25413) Specify searching website rather than GitLab Product
- [!25410](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25410) Correct typo in the career matrix page
- [!25407](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25407) Added field marketing alias
- [!25404](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25404) Fix typos and rename file
- [!25401](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25401) add examples+instructions for criticality 4 change
- [!25397](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25397) Update Spending Company Money page (Software)
- [!25394](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25394) Fix incident_management workflow filename
- [!25387](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25387) Handbook KPI Links
- [!25382](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25382) Fixed 'Roadmap' broken link on Release team page
- [!25380](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25380) Reorganize Data Team How We Work
- [!25377](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25377) Updating Sales Handbook to reflect new sales onboarding process
- [!25376](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25376) Defines support role and guidelines for incident management
- [!25371](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25371) Database follows the same process as BE/FE now. Mention there might be...
- [!25366](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25366) Clarify the core.autocrlf config information on git-page-update.
- [!25365](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25365) Update Ruby version numbers and note transience on git-page-update.
- [!25361](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25361) Clarify the RVM section and make it friendlier on git-page-update.
- [!25343](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25343) Applied Danae Villarreal to CEO shadow program
- [!25338](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25338) KPI Index page - mktg kpi's
- [!25335](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25335) Update source/handbook/communication/index.html.md
- [!25330](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25330) COE: Changed "calender" to "calendar" and removed "the"
- [!25321](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25321) Increasing google optimize timeout
- [!25304](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25304) Updates to the Sales Evangelism Reward, removing Dir of Sales Ops and...
- [!25299](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25299) Adding SOX Page
- [!25243](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25243) Updated the definition of an "Ally"
- [!25200](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25200) Update source/handbook/communication/index.html.md
- [!25164](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25164) Added additional context to about.gitlab.com redirect policy
- [!24979](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24979) Update Macros Categories
- [!24815](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24815) Copy edit of the anti-harassment policy page
- [!24148](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24148) Handbook Support: Update label usage for issues
- [!23148](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23148) Update Newly Added Product Principles
- [!22419](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22419) Add recommendations around when a PM should move an issue forward from...

### 2019-07-01
- [!25355](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25355) updated SMAU definition
- [!25351](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25351) Clarify for product kpi's
- [!25348](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25348) cleanup and update SLAs
- [!25346](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25346) Update debugging guidelines to use bundle exec
- [!25344](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25344) Adding diversity tools link to "How to source candidates"
- [!25342](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25342) Add single product guidance to general guidelines
- [!25334](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25334) Update source/handbook/communication/index.html.md
- [!25331](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25331) Simplify Data Team "where" labels
- [!25328](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25328) Deleted 'copy legal@gitlab.com' from the vendor signature approval process.
- [!25324](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25324) Add CFO and VP of Lega
- [!25323](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25323) Add missing "or manager" to Code of Business Conduct & Ethics
- [!25312](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25312) Document Interning for Learning issue template
- [!25310](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25310) Added link to new Secure UX page
- [!25309](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25309) updated time and link corrections
- [!25301](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25301) Update source/handbook/marketing/technical-evangelism/how-to-be-an-evangelist/index.html.md
- [!25293](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25293) Update SQL style guide for Data Team
- [!25292](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25292) Language tightening on the Data Team page
- [!25291](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25291) Add info on fiscal date filtering a dashboard in Periscope
- [!25285](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25285) Pipeline vs. Spend KPI Definition >>> Update source/handbook/marketing/marketing-operations/index.html.md
- [!25269](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25269) Update source/handbook/marketing/product-marketing/index.html.md
- [!25258](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25258) Update default retention metric
- [!25251](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25251) Small improvements to various Fullfillment pages
- [!25226](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25226) Add GitLab PTY Ltd Contract
- [!25214](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25214) Rearrange QA test debugging steps
- [!25208](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25208) Propose new milestone planning process for the data team.
- [!25186](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25186) Add rack attack information to IP block troubleshooting workflow
- [!25161](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25161) Document the automatic stage and group label inference from category labels
- [!25138](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25138) Quality Engineering roadmap epics
- [!24940](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24940) Add Create-wide processes to Knowledge and Editor BE team pages
- [!24421](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24421) Update source/handbook/marketing/revenue-marketing/field-marketing/index.html.md
- [!23859](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23859) Sowworkflow

### 1970-01-01
- [!25352](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25352) Added references to more Distribution work resources
- [!23448](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23448) Space after a comma in 'infrastructure, considering'
- [!23004](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23004) fix typos and grammar
- [!21163](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21163) Fix multiple broken links in the website
- [!20990](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20990) Fix bullet points in Slack section
- [!19080](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19080) Correctly indent sub-bullet points
- [!18847](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18847) Fix a few typos in the storage node blueprint

### 2019-06-30
- [!25287](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25287) Update label usage in source/handbook/engineering/development/enablement/distribution/workflow.html.md

### 2019-06-29
- [!25286](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25286) updating "working with 3rd parties"
- [!25281](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25281) Added How to Update a Start Date After the Contract is Signed and more steps...
- [!25276](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25276) Update source/handbook/business-ops/data-team/metrics/index.html.md
- [!25273](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25273) updated handbook for adding self to teampage

### 2019-06-28
- [!25270](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25270) Update source/handbook/sales/index.html.md - Pipeline Coverage Ratio
- [!25264](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25264) Update source/handbook/marketing/revenue-marketing/digital-marketing-programs/index.html.md
- [!25263](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25263) Handbook KPI links
- [!25262](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25262) Update source/handbook/marketing/community-relations/index.html.md.erb
- [!25260](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25260) Cadence page - fixed typo on word 'Amazon'
- [!25259](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25259) Adding KPI definitions
- [!25254](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25254) Linking Alliances KPIs
- [!25249](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25249) Linking the Hosting Users definition to the MR
- [!25246](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25246) Add @mikelewis readme link to UX index page
- [!25245](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25245) Adding our Secure UX page
- [!25242](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25242) Add manual redirect for use cases
- [!25239](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25239) Adding the Acquisition KPI link to their definitions
- [!25235](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25235) Remove mention of the old "Quality dashboard" as it's not updated anymore
- [!25234](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25234) Handbook Match Company KPI
- [!25230](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25230) Added link to GitLab on AWS solution brief under Datasheets section
- [!25228](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25228) Remove Australia from CXC.
- [!25224](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25224) Engaging SecOps
- [!25223](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25223) Update handbook KPI links
- [!25220](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25220) Fix wording in the "Biggest risks" page
- [!25207](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25207) Add collaboration with FBPs to Data Team Charter
- [!25203](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25203) Linking data team metrics definitions to new YML-driven Eng KPI HB pages
- [!25199](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25199) Updated broken and malformed links on multiple pages
- [!25174](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25174) Added link to training video
- [!25153](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25153) Resolve "Add variants, clarity to POC/POV process"
- [!25126](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25126) Rework info architecture of UX handbook
- [!25113](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25113) Update Senior engineering career development with Staff and Distinguished links
- [!25072](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25072) Add Epic Ownership to Geo Team Page
- [!24974](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24974) Update product handbook for Section based group convos

### 2019-06-27
- [!25201](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25201) update to EBA
- [!25171](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25171) Updating links in Support KPIs
- [!25170](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25170) Fixing the Margin reference
- [!25168](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25168) Add interacting with vulnerabilities to other functionality in secure
- [!25163](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25163) update super in Australia and NZ
- [!25158](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25158) Add database trainee template
- [!25154](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25154) Update source/handbook/business-ops/tech-stack/index.html.md
- [!25149](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25149) adding link to RFP archive to sales resource page
- [!25148](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25148) Add Manage daily stand-up info to How We Work
- [!25146](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25146) 2nd iter on data driven eng kpis
- [!25135](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25135) Updating code of conduct page with compensation email
- [!25122](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25122) Change minize to minimize
- [!25092](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25092) Update the Sourcing page with LinkedIn Recruiter/HM seat info
- [!25073](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25073) Update tax section in Handbook
- [!24972](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24972) Update Onboarding page with I-9 and E-Verify policy
- [!24970](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24970) SecOps Incident Response Guide
- [!24832](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24832) define training timeframe for training
- [!24007](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24007) Added a section on How to Qualify an Account From Afar.
- [!23785](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23785) Infrastructure Design: ZFS for git storage nodes
- [!23461](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23461) Update source/handbook/communication/index.html.md

### 2019-06-26
- [!25127](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25127) Reorder list of social channels on slack to be alphabetical
- [!25121](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25121) Remove Data Engineers, Infrastructure & Architecture from Triage Rotation
- [!25116](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25116) Add info on Data Team Access to Data Sources
- [!25105](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25105) Combine blog bullets and add note re: approvals
- [!25100](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25100) Update Marin's rotation, add an opening to rotation in July
- [!25093](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25093) Add links to header for text items under Fork and Commoditize
- [!25091](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25091) Update source/handbook/people-operations/code-of-conduct/index.html.md
- [!25089](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25089) Update source/handbook/finance/accounting/index.html.md
- [!25083](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25083) Additions to style guide + blog hb
- [!25081](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25081) Added my README link to the UX page
- [!25067](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25067) Moving Engineering performance indicators (regular and keys) to handbook from document
- [!25058](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25058) Updates to the Quick Reference Table on Sales Homepage
- [!25056](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25056) Nnamdi to Alumni & open up a week
- [!25033](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25033) Reduce duplication in writing style guide
- [!25029](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25029) Update frontend RFC header and update URL
- [!25020](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25020) Changed SMART acronym to CREDIT values for PIP.
- [!24992](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24992) Propose updated Data Team triage schedule to accommodate growing team
- [!24923](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24923) Add MR reviewers section
- [!24898](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24898) Light copy edit of people ops handbook page
- [!24891](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24891) Add draft of biggest risks.
- [!24321](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24321) Setting Website to a default value when individuals or free email domain

### 2019-06-25
- [!25051](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25051) handbook Adding target to KPI
- [!25043](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25043) adds note about checking CEO calendar
- [!25040](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25040) Handbook People Ops KPIs team member turnover
- [!25037](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25037) Add dedicated marketing field for categories
- [!25022](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25022) replaces overloaded CSAT term with SSAT
- [!25004](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25004) Add additional info about probation period's and add PEO to onboarding.
- [!25002](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25002) Update the Sourcing page with Reach out templates
- [!24996](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24996) remove further PeopleOps tasks, that was repeated and missed in the previous MR
- [!24991](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24991) Add world time buddy link for data team time zones
- [!24989](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24989) update expense reimbursement process
- [!24968](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24968) remove fedex and direct to ITOps
- [!24967](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24967) Update Netherlands process and explaining the 12 month contract more
- [!24965](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24965) move Announcing Terminations to the bottom of the page
- [!24961](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24961) add targets for percent comp band
- [!24942](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24942) Added clientID update to changes of note
- [!24939](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24939) additional language to management page
- [!24935](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24935) Content handbook update
- [!24908](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24908) Update index.html.md
- [!24889](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24889) Integrate underperformance suggestions and add a few.
- [!24875](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24875) Move introductory text in social media guidelines
- [!24868](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24868) Add section about frontend RFC
- [!24717](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24717) Moving application type maturity to main maturity page
- [!24702](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24702) Update the Spending Company Money and Examples pages
- [!24695](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24695) Update source/handbook/marketing/corporate-marketing/index.html.md
- [!24461](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24461) New UX Resources handbook section
- [!23872](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23872) Collaboration: Specify the date of monthly release and link to relevant blog post

### 2019-06-24
- [!24981](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24981) Update index.html.md with Updated Applications in Okta AppStack.
- [!24969](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24969) Updating verbiage within TRN.1.01 security awareness training
- [!24953](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24953) team page pic
- [!24950](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24950) Update WAAK mid-market territory assignment
- [!24943](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24943) Add Slack channels, remove headline from CRO section, remove active test from CRO section
- [!24941](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24941) additional language regarding exit interviews
- [!24936](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24936) Update handbook KPI Capital Consumption
- [!24933](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24933) Add section on capacity planning to Create:Source Code BE team page
- [!24932](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24932) updated markdown and added Reporting Corporate Income Taxes
- [!24930](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24930) Resolve "Add SMB presales support model to Commercial SA handbook"
- [!24929](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24929) Add info about a 12 month fixed term employment agreement in the Netherlands
- [!24927](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24927) Adding Commissions to Quick Reference Guide
- [!24926](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24926) Adding participant schedule details.
- [!24922](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24922) Add wording on how to find a replacement for a GC
- [!24921](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24921) Update to NORAM EAST SDR Alignments.html.md
- [!24919](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24919) Removing quota from sales handbook. Moved to commissions page.
- [!24918](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24918) Adding content for Commissions page.
- [!24916](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24916) Add verification to development workflow
- [!24915](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24915) new directory for commissions
- [!24914](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24914) Clarify "What we offer" section in acquisition handbook
- [!24913](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24913) Adding DRAFT proposal instructions. Updates to GitLab entities.
- [!24906](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24906) Add Probation period for Australia
- [!24905](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24905) Add information on triage objective and label
- [!24903](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24903) Remove unused reaction rotation
- [!24896](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24896) Adding Renewal ACV calculation tables.
- [!24880](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24880) Update PIP section
- [!24877](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24877) added agile click-through to demos page
- [!24872](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24872) Add meta metrics info to the Data team page
- [!24825](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24825) Add editor career path
- [!24777](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24777) Updates for /handbook/marketing/technical-evangelism/
- [!24751](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24751) Add backend engineer experience factor AMA points
- [!24648](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24648) adding sales note taking guidelines to the handbook
- [!24594](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24594) Update growth engineering pages
- [!24500](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24500) Changed format
- [!24403](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24403) Update july posts
- [!24401](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24401) Update june posts
- [!24366](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24366) add tagline to app modernization (for consistency)

### 2019-07-07
- [!24954](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24954) HackerOne: Specify a 3 business day limit to self-close N/A or Informative reports.

### 2019-06-23
- [!24895](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24895) Update index.html.md
- [!24603](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24603) Update source/handbook/sales/public-sector/engaging-public-sector.html.md

### 2019-06-22
- [!24881](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24881) Update Data Team Charter
- [!24879](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24879) Update People Ops to Compensation team in email templates for bonuses and promotions.
- [!24876](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24876) Adding KPI definitions to FBP responsibilities
- [!22238](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22238) Adjust Group Conversations to Sub-Departments

### 2019-06-21
- [!24870](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24870) Update who the letter of adjustment is cc'd to
- [!24865](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24865) Budget vs Actual Update
- [!24864](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24864) Update source/handbook/engineering/ux/index.html.md
- [!24858](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24858) Remove Periscope Index from the Directory Page
- [!24853](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24853) Update index.html.md
- [!24841](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24841) Various additions to Create:Source Code BE team page
- [!24838](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24838) Expand on relative links
- [!24827](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24827) Added TOC to the Okta FAQ page
- [!24823](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24823) Add links to release post page
- [!24821](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24821) Jj site tip2
- [!24817](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24817) Update Sourcer allignment for Engineering&Product
- [!24816](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24816) Make multimodal communication more prominent
- [!24808](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24808) Jj site tip
- [!24803](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24803) Add interview training dashboard to Periscope Directory
- [!24800](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24800) Added info about meetings, new team board, and new important links
- [!24768](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24768) Add exception reasoning to justification
- [!24726](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24726) Create a handbook page for the Core Team
- [!24692](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24692) Tuition Reimbursement Examples
- [!24641](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24641) Add files and config for zoom live streaming
- [!24370](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24370) Updated SLA and Transcription and general process
- [!24359](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24359) Update CAB meetings
- [!24347](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24347) Jj pmmhandbook4
- [!23909](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23909) Add Give Back Project Section to Commercial Handbook
- [!23705](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23705) Update source/handbook/sales-training/index.html.md
- [!23612](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23612) Better definition of the career framework categories

### 2019-06-20
- [!24779](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24779) Change Brittany to Compensation
- [!24776](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24776) Remove specific syntax requirement for dbt packages
- [!24771](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24771) Fix marketing handbook 404s
- [!24767](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24767) Set the context of a created video, related to the baseline initiative
- [!24763](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24763) MEDDPIC to MEDDPICC
- [!24762](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24762) MEDDPIC to MEDDPICC
- [!24761](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24761) Add comma to effective org range
- [!24753](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24753) Be explicit in what is offered by law and what is an additional benefit in BV
- [!24750](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24750) Rephrase GitLab quiz question about professional services
- [!24749](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24749) Add approval
- [!24748](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24748) Add HN front page integration to Community Response table
- [!24691](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24691) Remove empty sections on monitor group pages
- [!24679](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24679) Add details on how to stay informed in the Engineering organization
- [!24651](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24651) Update index.html.md added CBlake shadow take aways
- [!24604](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24604) Life Assistance Program
- [!24602](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24602) Add Health SPD
- [!24525](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24525) Update forum handbook
- [!24320](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24320) add non-work and hobby channels to the list of channels
- [!24290](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24290) Updates to Acquisitions handbook and process
- [!23788](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23788) Update the renewal process for EDU licenses
- [!23706](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23706) Add Instagram instructions to Markdown guide
- [!23241](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23241) Add video resources to frontend handbook

### 2019-06-19
- [!24713](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24713) Adding social slack groups to chat page
- [!24708](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24708) Changing from Recruiting to Hiring
- [!24705](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24705) Update handbook for KPIs
- [!24704](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24704) Update source/handbook/marketing/corporate-marketing/index.html.md
- [!24701](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24701) added additional tech stack definitions
- [!24699](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24699) Update SQL Style Guide for Data Team
- [!24697](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24697) Updated changes of note with new Google Tag Manager tag
- [!24693](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24693) Update UX Research sensing mechanism info in PM handbook
- [!24683](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24683) Fix sensing mechanisms links in PM handbook page
- [!24681](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24681) Remove net retention, focus on gross retention
- [!24676](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24676) Fixes to management tips
- [!24675](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24675) Add references to UXR_Insights repo
- [!24669](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24669) Add tips for ordering business cards from MOO for Indian team members.
- [!24666](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24666) Update security handbook for headcount rationale
- [!24663](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24663) bamboohr and greenhouse approvals
- [!24662](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24662) Update retention metrics
- [!24659](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24659) Adding IACV calculation table to IACV definition.
- [!24658](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24658) Clarify we have one H1 program
- [!24657](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24657) Update virtual event table with due dates and DRI
- [!24653](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24653) Update source/handbook/business-ops/index.html.md
- [!24652](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24652) Update source/handbook/business-ops/tech-stack-applications/index.html.md
- [!24646](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24646) Updating the send the contract through Greenhouse section
- [!24642](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24642) Removed asking of legal name up front in the application stage
- [!24639](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24639) Add clarity to the experience baseline video
- [!24636](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24636) Update security training section and remove stale training page
- [!24634](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24634) Update source/handbook/finance/financial-planning-and-analysis/index.html.md
- [!24630](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24630) update tax section with reporting indirect tax (GST/ VAT)
- [!24627](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24627) quick additional instructions for the PIP document- once the final process is...
- [!24620](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24620) Add new image caption info
- [!24608](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24608) Update index.html.md removed Cindy from schedule and added Cindy to Alumni
- [!24601](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24601) Update OKR and KPI section of handbook page
- [!24584](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24584) Reordering of the accounting processes
- [!24578](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24578) Update index.html.md for Security Practices to include Okta details
- [!24531](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24531) Adding focus on top 4 Product KPI's
- [!24440](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24440) Adding a link to the relocation process on the Relocation section of the Global Compensation page
- [!24363](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24363) Update  handbook People Ops KPIs and Recruiting KPIs
- [!24317](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24317) Apply some of the beneficial steps from PM process for moving features
- [!24301](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24301) Consolidate instructions on how to bring a feature to a lower tier
- [!24225](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24225) Add trial extension template
- [!24214](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24214) Rename Product Use Cases to Application Types
- [!22446](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22446) Updating Comp and Ramp details.
- [!21605](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21605) Handbook Support: Add correct user mapping instructions
- [!21509](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21509) Clarify hiring terms in China
- [!21240](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21240) Update security workflow for developers

### 2019-06-18
- [!24638](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24638) Add prioritization by VP of Product to PM Weekly Meeting process
- [!24629](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24629) Update index.html.md
- [!24628](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24628) add schedule to e-group off site
- [!24625](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24625) Added opening slash to relative links for handbook
- [!24622](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24622) Add one benchmark score per JTBD
- [!24621](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24621) Update H1 Flow for Critical Vulnerabilities
- [!24616](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24616) fleshing out itops laptops source of truth link
- [!24615](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24615) Steps for setting up a virtual background in zoom
- [!24610](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24610) Fix bug mentioning labels in Maturity plans
- [!24596](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24596) Add schedule for newsletter
- [!24590](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24590) Underperformance - add alternative template
- [!24589](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24589) Expand on the Fast boot page with Delivery team experiences
- [!24585](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24585) Add people ops email for letter of employment request
- [!24581](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24581) Update open epics link for Fulfillment
- [!24580](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24580) add link, instead of just the url
- [!24575](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24575) Switch maturity legend to use SVG icons, GitLab colors
- [!24569](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24569) Update handbook with Quality Engineering headcount rationale
- [!24546](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24546) Add "unlearning" section to Engineering handbook
- [!24536](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24536) Update UX headcount rationale for clarity
- [!24515](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24515) Make IT OPs Laptop process single source of truth
- [!24495](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24495) clean up flie & embed doc
- [!24492](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24492) Add item to goal section - add Head of Corp Strategy to attendee list
- [!24488](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24488) Added changes of note to DMP section
- [!24434](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24434) Creates frontend maintainer trainee template
- [!24365](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24365) Update link for case study initiation process
- [!24319](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24319) Add to editorial style guide
- [!24044](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24044) Add table with all entities
- [!24037](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24037) Fix broken links in Professional Services handbook
- [!23281](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23281) Clarify GitLab quiz questions

### 2019-06-17
- [!24572](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24572) Update index.html.md
- [!24568](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24568) Update index.html.md
- [!24556](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24556) Update to the US East SAL Table.
- [!24554](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24554) Add making yourself uncomfortable to force deprogramming.
- [!24548](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24548) Update the handbook to state we are fully on Airflow and link to the data infra repo
- [!24547](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24547) Update source/handbook/spending-company-money/index.html.md to clarify laptop purchase process
- [!24545](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24545) Add info to data team handbook on sources and periscope
- [!24544](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24544) Update index.html.md
- [!24540](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24540) Add issue board links for the  APM and Health groups
- [!24534](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24534) Update index.html.md
- [!24533](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24533) Remove duplicate anti-flicker entry on DMP page
- [!24532](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24532) Added "Turn the Ship Around" to management books.
- [!24526](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24526) Updating term used for People Business Partner and contacts for a few other process steps
- [!24523](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24523) Move nontechnical info on SheetLoad to the handbook
- [!24518](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24518) Mention that productivity is used as well as throughput
- [!24516](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24516) Add more SQL Style Guides to the data team page
- [!24514](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24514) Update Mattermost support workflow
- [!24508](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24508) Update Support Workflow to include Customer Feedback Issue Process
- [!24506](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24506) Add newlines for proper formatting
- [!24501](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24501) update job family page
- [!24483](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24483) Resolve "Document headcount planning rational in Development handbook"
- [!24452](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24452) updated signing authority for the letters of adjustment and the process of...
- [!24429](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24429) Guidance for avoiding "Not Invented Here" syndrome
- [!24406](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24406) Update source/handbook/marketing/technical-evangelism/writing-cfps
- [!24375](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24375) Add TE speaker logistics page
- [!24369](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24369) Update source/handbook/marketing/technical-evangelism/consortiums
- [!24361](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24361) Resolve "Marketing handbook - Customer facing presentations.People go to this page looking for this so put it higher up"
- [!24358](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24358) Add Useful Links section to TE page
- [!24353](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24353) Update product marketing board links
- [!24350](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24350) Add a How to be an Evangelist page
- [!24348](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24348) Update XDR link to point to pitch deck
- [!24339](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24339) Resolve "Add new team members to the marketing handbook page Which product marketing manager should I contact?"
- [!24307](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24307) Adding additional team morale hazard: Lack of trust.
- [!24272](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24272) Add dependency list to SCA features
- [!24220](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24220) Add a new section for good for 1st time contributors
- [!23811](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23811) Clarify what information should be updated and what the choices are when a PM updates the kickoff document
- [!23796](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23796) Hold merge requests to a higher standard when master is red
- [!23616](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23616) Mention project level dashboard in secure features
- [!23595](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23595) Adding page to handbook for tips on technical interview
- [!23514](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23514) Updated information for CEO Shadow Vault in 1Password. Still need to figure...

### 2019-06-15
- [!24498](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24498) Revert "Merge branch 'steveazz/make-table-responsive' into 'master'"
- [!24462](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24462) Update welcome to the BDR team video
- [!24453](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24453) Make tech stack apps table responsive
- [!24433](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24433) KPI Page - spelling correction
- [!24387](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24387) Update stable counterparts and add headcount rationale
- [!23781](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23781) Added SMB about and responsibilities

### 2019-06-14
- [!24486](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24486) Updating WIR bullet with new slack process
- [!24482](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24482) Add active A/B tests section and fixed formatting
- [!24476](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24476) Updated DMP page to include anti-flicker statement
- [!24466](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24466) make link for password policy clickable
- [!24463](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24463) Cleanup apm and health group pages
- [!24459](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24459) Mmf backend eng entitlements
- [!24456](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24456) Add hiring charts and sections on deep dives and async standups to Create BE team pages
- [!24455](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24455) Update group conversation schedule to remove CS and update Sales
- [!24449](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24449) Lighthouse reports are shared with VP of Legal and CPO
- [!24443](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24443) Review Request: Cycle Time
- [!24442](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24442) adding anchor to new tech stack pg
- [!24439](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24439) Updating process for relocations
- [!24438](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24438) clean up styling and fix links.
- [!24437](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24437) rmv deprecated pub issue trackers
- [!24430](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24430) Add verification step in BambooHR
- [!24425](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24425) Update index.html.md
- [!24423](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24423) Added  for opening frontmatter
- [!24419](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24419) Making justification more specific.
- [!24418](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24418) Add public note to scorecard.
- [!24416](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24416) Added board selection process
- [!24414](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24414) Update source/handbook/marketing/revenue-marketing/index.html.md
- [!24412](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24412) move marketing-programs under digital-marketing-programs
- [!24409](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24409) Add data team snowplow documentation
- [!24392](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24392) Add blurb about interviewing is everyone's job
- [!24391](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24391) Update Christie's one pager
- [!24385](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24385) update CHD zoom call info
- [!24381](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24381) Update publishing topic info
- [!24357](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24357) fix formatting for clarity
- [!24356](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24356) delete empty cell in table
- [!24355](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24355) Update projects list for contribution metrics
- [!24329](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24329) handbook: add customer content responsibility
- [!24328](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24328) How we value and consider customer feedback
- [!24311](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24311) Add link to CXC timesheet automation repository
- [!24310](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24310) Included language on referencing custom terms in cutomer agreements. Deleted...
- [!24285](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24285) Merch updates
- [!24279](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24279) vault design doc updates
- [!24275](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24275) Fix link to issue escalation doc
- [!24263](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24263) Rewording a bit to reduce redunancy
- [!24255](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24255) Update process for bringing features to lower tiers
- [!24241](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24241) Add milestone planner cycle to data team handbook
- [!24172](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24172) Update to the commercial sales handbook page
- [!22001](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22001) Add error budgets to blog hb

### 2019-06-13
- [!24408](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24408) Adding Sarah's CRO changes from a broken MR
- [!24388](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24388) including fde info for apple and linux on itops
- [!24386](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24386) Marketing Programs Handbook Reformat
- [!24384](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24384) Fixed broken handbook links in engineering sub-folder
- [!24380](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24380) Update source/handbook/marketing/revenue-marketing/index.html with new DMP link
- [!24377](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24377) Lower case the P in Snowplow
- [!24374](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24374) SmallBDR changesUpdate index.html.md
- [!24373](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24373) Digital Marketing Programs updates, lots of changes
- [!24372](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24372) Update handbook section on MQ/Wave
- [!24371](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24371) Fixed broken links in CS Handbook folder
- [!24364](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24364) add DMP labels to DMP page in handbook 2
- [!24362](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24362) update python style guide to talk about linting
- [!24360](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24360) Update index.html.md added no cumulative graphs for decision making
- [!24354](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24354) Fixed malformed links on ceo and ea pages
- [!24351](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24351) Jj pmmhandbook5
- [!24349](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24349) Add list pull requests to Marketing ops section on Business Ops Handbook
- [!24346](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24346) Fixed broken links and italics in the email review protocol update
- [!24343](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24343) Fixed broken links related to finance and biz ops
- [!24342](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24342) removed table of contents
- [!24340](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24340) Jj pmmhandbook3
- [!24338](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24338) readd content accidentally deleted with competing MRs
- [!24337](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24337) Add SLA to virtual events requests on MPM handbook
- [!24334](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24334) Jj pmmhandbook2
- [!24332](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24332) Jj pmmhandbook1
- [!24330](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24330) Update job title
- [!24327](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24327) Fix Engineering group conversation schedule
- [!24326](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24326) update itops page inc details on laptop order test
- [!24324](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24324) create revenue marketing directory
- [!24318](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24318) fix formatting
- [!24316](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24316) Update Contact/Admin(s) for Zendesk
- [!24315](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24315) move mktg ops page to own directory
- [!24314](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24314) Pages using Ruby get processed by Ruby
- [!24308](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24308) Add Kyla Gradin to CEO Shadow Program
- [!24303](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24303) Update index.html.md fix link typo
- [!24302](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24302) removed tech stack applications table
- [!24300](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24300) Clarifying ping.
- [!24296](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24296) update structure
- [!24295](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24295) Add new directory
- [!24289](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24289) Add new directory for Okta AppStack
- [!24288](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24288) Add a line about vaping to video calls.
- [!24282](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24282) Update index.html.md for Okta Questions about Okta verify
- [!24278](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24278) Update Eligibility for CEO Shadow Program
- [!24277](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24277) Removing CEO/CPO from offer approval for IC's
- [!24273](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24273) Update source/handbook/hiring/offers/index.html.md
- [!24267](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24267) Bolded compensation philosophy per @Sid's request
- [!24262](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24262) Add Help Desk Responsibilities
- [!24254](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24254) Document variable pay frequency for sales roles.
- [!24252](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24252) Add process for fast boot approval.
- [!24250](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24250) Add purpose of the `#hackerone-feed` Slack channel
- [!24247](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24247) stub new onboarding pages in development
- [!24221](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24221) Handbook Addition - Extending SaaS and on-prem trials
- [!24188](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24188) Update source/handbook/legal/global-compliance/index.html.md
- [!24185](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24185) Handbook Support: Recategorize workflows
- [!24139](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24139) Added a Compliance placeholder section to the Legal FAQs
- [!24134](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24134) Split Create team page into 3
- [!24108](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24108) Update index.html.md for Okta FAQ page to include extra FAQ questions
- [!24071](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24071) Finalize content schedule
- [!24011](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24011) Adding engineering metrics process with stages/group label changes
- [!23905](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23905) Adds .com Subscription Enablement page link
- [!23729](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23729) Removing broken link to #coffee-break-calls
- [!23699](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23699) Improve guidance on how to update stages, groups, and categories
- [!23223](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23223) clarifying language on roles and responsibilities. Attempting to link up with our runbooks page
- [!22411](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22411) Initial Snowplow design document template.

### 2019-06-12
- [!24268](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24268) Update tables
- [!24259](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24259) More committal language and dates. Applying to all potential engineering fast...
- [!24256](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24256) add a template for creating a JF
- [!24253](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24253) quick addition of japan to geo we can order laptops
- [!24249](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24249) Updates to table
- [!24239](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24239) Adding Quick Reference table to Handbook page
- [!24236](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24236) Add text about underperformance
- [!24235](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24235) Adding walkthrough of New Job Family Creation.
- [!24234](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24234) Adding info around how to access these training videos
- [!24231](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24231) Move MAX Employees Logic to more Prominent Spot in Handbook"
- [!24227](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24227) update 2fa instructions for reset
- [!24224](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24224) Fixing OKR board link.
- [!24223](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24223) updates a question for clarity
- [!24222](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24222) Rename slack group javacripters to frontend-team in the handbook
- [!24216](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24216) Add Tab Outliner option to Toby option in SA tools
- [!24208](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24208) Fix the outdated intials with first name
- [!24206](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24206) Update handbook, People Ops KPIs
- [!24202](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24202) Update of the Zuora Subscription Data Management with additional information about active subscription status
- [!24200](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24200) Delete index.html.md - https://about.gitlab.com/handbook/sales-process/accounting/
- [!24199](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24199) Add Matej's README to UX Handbook page
- [!24192](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24192) fixed formatting
- [!24191](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24191) Update handbook Recruiting KPIs
- [!24187](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24187) Updated Memory team page with important links
- [!24178](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24178) Adding Slack App recommendations to Tools-and-Tips page
- [!24175](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24175) added business purpose and in-scope for SOX columns
- [!24168](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24168) Adding a section on engineering hiring practices (including new "one star" requirement)
- [!24144](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24144) add iterations to comp page
- [!24140](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24140) Fix link from engineering/ops page to correct Monitor page
- [!24128](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24128) Clarify that weights on Plan issues don't matter too much
- [!24076](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24076) Remove "Open Work Day" references from handbook
- [!23757](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23757) Add additional info on how to set up a pulse survey
- [!23558](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23558) Update index.html.md

### 2019-06-11
- [!24179](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24179) correcting syntax error to AC agenda
- [!24177](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24177) updated admin for Slack
- [!24176](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24176) Resolve "Add optional, best practice tools for SA's"
- [!24174](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24174) Update source/handbook/ea/index.html.md
- [!24173](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24173) Update to approving from email templates.
- [!24171](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24171) update link to new landing page
- [!24170](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24170) Updates to AC charter
- [!24169](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24169) update tech stack link
- [!24166](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24166) update tech stack link
- [!24164](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24164) update tech stack link
- [!24163](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24163) Making a note that the Website is required on page layouts in SFDC
- [!24162](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24162) Adding Marketing Metric Dashboard to Periscope directory
- [!24161](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24161) Updates to AC agenda
- [!24160](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24160) update tech stack information
- [!24159](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24159) Resolve "POC page cleanup"
- [!24157](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24157) Created Global Compliance landing page
- [!24155](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24155) Updated Image for Recruiting Process Framework
- [!24146](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24146) Improve the URL for the top contributors page
- [!24145](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24145) Add new directory for Tech Stack Applications
- [!24143](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24143) Update index.html.md
- [!24141](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24141) Use meow instead of miaow
- [!24135](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24135) Add link to my personal read me
- [!24133](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24133) Reinforce instructions on selecting the correct vendor/contract approval template by function
- [!24131](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24131) Updating wording to encourage team members to review company call agenda on their return from time off.
- [!24130](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24130) Update other handbook references for Measure/Access
- [!24126](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24126) Update laptop recommendations to include Support Engineers
- [!24124](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24124) Fix LinkedIn message to be across all social media and more clear
- [!24123](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24123) Update Kickoff call instructions
- [!24113](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24113) Replace "a MR" with "an MR"
- [!24110](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24110) Update source/handbook/support/index.html.md
- [!24109](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24109) Consistent punctuation in "tasks" section
- [!24104](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24104) Update source/handbook/offboarding/index.html.md
- [!24103](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24103) Update source/handbook/finance/accounting/index.html.md
- [!24102](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24102) Update index.html.md for Okta page to link to Okta FAQs in handbook.
- [!24098](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24098) Added Okta FAQ questions and page
- [!24093](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24093) Update Product Metrics Page
- [!24091](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24091) Add full email nurture visualization
- [!24083](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24083) Fix broken formatting on the Periscope page
- [!24080](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24080) Fix typo
- [!24075](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24075) Update to accounting and reporting page in the handbook
- [!24065](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24065) Stock for CS roles
- [!23998](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23998) Update Periscope Directory
- [!23995](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23995) FY 20 Audit Committee Agenda Planner
- [!23538](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23538) clean up changes
- [!23493](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23493) Update source/handbook/marketing/career-development/index.html.md
- [!23424](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23424) Updated Responsibilities to Match SSOT Workflow
- [!23420](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23420) Updated Recruiting Framework Image and Table
- [!23416](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23416) Updated Greenhouse Page in Handbook re: Hiring Manager Permissions
- [!23378](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23378) Updating Password Policy verbiage
- [!23246](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23246) [Design Doc] Eliminate shared file server
- [!22949](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22949) Professional Services must have cost estimate prior to processing
- [!22448](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22448) Add a section on top annual contributors

### 2019-06-10
- [!24090](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24090) Add product directors into approvals
- [!24088](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24088) Replaced the RACI markdown table with a Google Sheets link
- [!24085](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24085) Update instances of 'Health' group name
- [!24079](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24079) add upload video tutorial
- [!24078](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24078) Update index.html.md Moved airpods recommendation to this page
- [!24074](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24074) Update summit ref to Contribute on PeopleOps/Visa
- [!24073](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24073) Added clarifying language for contract filing and steps for legal review
- [!24070](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24070) Update index.html.md added recommendation for headphone use
- [!24069](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24069) Small fixes
- [!24059](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24059) Update index.html.md
- [!24055](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24055) Create new APM and Health group pages
- [!24054](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24054) Fix CFP Submission template URL
- [!24052](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24052) Add John Coghlan to alumni
- [!24051](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24051) Reverting back.
- [!24049](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24049) Add link to slack for Tech writers team
- [!24048](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24048) Adding Abuse team aliases to the list.
- [!24047](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24047) Promotion letters
- [!24045](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24045) Remove Confirmation email, to show email confirmation only and update template to reflect the same
- [!24043](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24043) Reformat contributor KPI
- [!24042](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24042) Adding Quick Reference Guide for easier navigating. Adding quota ramps and seasonality.
- [!24038](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24038) add benefits survey
- [!24034](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24034) Updates to the order of events in sales order processing. Differentiating...
- [!24029](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24029) Require notes on referral candidate
- [!24009](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24009) Updating Growth Page to reflect addition of Telemetry and Fulfillment charters
- [!24004](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24004) Subscribing to an on-call schedule
- [!23991](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23991) updated so the recruiting managers can sign contracts and German contracts
- [!23987](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23987) Resolve "Update UX Handbook, Product Designer Workflow"
- [!23982](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23982) Updating handbook re; legal name per Erich W
- [!23980](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23980) Sarah - Manager Read Me
- [!23972](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23972) Create editorial team page
- [!23967](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23967) start Comm Sales POC Guide
- [!23954](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23954) Added type to Snowplow taxonomy
- [!23944](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23944) Update comp principle 15
- [!23942](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23942) Updated additional questions for job posts
- [!23934](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23934) Update source/handbook/marketing/technical-evangelism/index.html.md
- [!23880](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23880) Iterating on the termination announcement
- [!23754](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23754) Add link to documentation for pulse survey
- [!23642](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23642) Add link to Visa page
- [!22764](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22764) Resolve "Improve GitLabbers experience using NextTravel"
- [!19075](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19075) initial commit to npm worfklow

### 2019-06-09
- [!24028](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24028) update territory table.
- [!24027](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24027) Clean up list of desktop monitors and add an LG
- [!24012](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24012) Add changelog message in the handbook
- [!23989](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23989) Update source/handbook/internal-audit/index.html.md
- [!23983](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23983) added links to Sales KPIs
- [!23727](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23727) Update index.html.md to add non-technical or SLA related tickets to TAM responsibility

### 2019-06-08
- [!24013](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24013) Fix 'engineering' typo

### 2019-06-07
- [!24000](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24000) Update the MR coach page name
- [!23997](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23997) Deleted bad link to UX career path doc
- [!23992](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23992) Update source/handbook/business-ops/index.html.md
- [!23990](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23990) Included links for opt-in email lists
- [!23986](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23986) Add exact time for cat feeder
- [!23977](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23977) more updates for KPI definitions
- [!23976](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23976) Indicating that the amounts listed are guidelines and not strict limits.
- [!23973](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23973) Update index.html.md added suggestion for comfy shoes
- [!23968](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23968) Update index.html.md
- [!23965](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23965) Add contribution KPIs section to the handbook
- [!23964](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23964) Updated this section to include when we specifically use a marketo landing page
- [!23963](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23963) add a link to the gitlab letterhead
- [!23962](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23962) Update link to contracts
- [!23953](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23953) When rejecting a candidate, delay sending the rejection email.
- [!23952](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23952) Revised outside project process
- [!23951](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23951) Deprecate handbook maturity page
- [!23947](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23947) Document how to generate calendly one-time links
- [!23938](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23938) Definition updates for KPI related metrics
- [!23933](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23933) add content to the package team handbook page
- [!23927](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23927) Add Valerie Readme to UX handbook
- [!23895](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23895) Add maturity plan to category template
- [!23889](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23889) Make language consistent on handbook home
- [!23875](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23875) Update index.html.md to remove Beta data, and Add YouTube training videos
- [!23873](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23873) Add ability to export career matrix as CSV
- [!23730](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23730) Updating Support Handbook with KPI definitions and gearing ratio definitions
- [!23572](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23572) Fix links to upcoming releases page, standardize some language
- [!22310](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22310) Resolve "Create "Meetup Participants with GitLab presentation" KPI definition"
- [!20790](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20790) Improve docs team page in handbook.

### 2019-06-06
- [!23939](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23939) Replace "Sid" with "CEO"
- [!23937](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23937) removed knowbe4 application
- [!23936](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23936) added header to Systems Diagram & Tech Stack table
- [!23929](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23929) Resolve "update Business-Ops link to deprecated SA Activity Board"
- [!23924](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23924) Replace [Ss]ub-department with [Ss]ection
- [!23923](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23923) Details attending events with the CEO
- [!23904](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23904) Update Exp. Baselines & Recommendations tasks
- [!23902](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23902) Fix broken link to christie readme
- [!23901](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23901) Updated links to Product readme's
- [!23900](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23900) Add KPI to job family description format
- [!23898](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23898) Fix ux-one-page linking issue and make page available
- [!23897](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23897) Add link to namespace docs
- [!23865](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23865) made updates to the PEO contract process
- [!23855](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23855) Added exceptions and new onboarding process, which now uses the AR process
- [!23854](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23854) Fix indented heading
- [!23853](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23853) Cat feeder and boardroom hours
- [!23825](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23825) Onboarding Design Links
- [!23808](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23808) Update source/handbook/ea/index.html.md
- [!23778](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23778) Add internal personas section + Data analyst persona
- [!23774](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23774) Add Heroes to Evangelist Program handbook page
- [!23209](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23209) Update index.html.md
- [!23181](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23181) Add a dedicated maturity field
- [!22848](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22848) Add positive entry about use of emojis in Slack as replacement for body-language responses
- [!22343](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22343) Make "adding yourself to the team page" clearer
- [!22240](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22240) Adds .com subscriptions sales-enablement page
- [!21352](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21352) blueprint and design doc for indexing one namespace with ElasticSearch integration

### 2019-06-05
- [!23851](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23851) Updated Scott W Readme
- [!23848](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23848) Modify description of scheduled calls via support tracker
- [!23844](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23844) Include GDoc in promotions discussions on Slack
- [!23843](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23843) corrected knowbe4 application header
- [!23842](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23842) add systems diagram
- [!23841](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23841) Include 1099 provision in outside contract approval process
- [!23839](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23839) Resolve "Commercial CS Customer management handbook addition/review"
- [!23838](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23838) Removed security control SDM.2.01 as this was determined to be redundant
- [!23837](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23837) Move VP of Product Strategy to approver
- [!23835](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23835) Update source/handbook/finance/accounting/index.html.md
- [!23834](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23834) Remove groups for Snowflake role management
- [!23831](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23831) Added link to current role-based baseline entitlements
- [!23829](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23829) guidelines for commercial account slack channels
- [!23827](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23827) Remove words to ensure we use the complete term
- [!23824](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23824) Update Spending Company Money Page
- [!23822](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23822) Update references to Head of Product
- [!23806](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23806) Remove Virjinia from Meltano page
- [!23792](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23792) Update Monitor team members organization to prepare for group split
- [!23791](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23791) Added anchor link to baseline entitlements and cross-referenced automated group reporting
- [!23784](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23784) Handbook / Updated Tax Team section
- [!23783](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23783) Removing Ukraine from Safeguard
- [!23782](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23782) Add YouTube playlist for High Output Management
- [!23770](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23770) Keyboard and mouse to default to bottom left monitor
- [!23769](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23769) Update planning horizon definitions
- [!23768](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23768) Updating link to Licensed Users in the ARPU definition from general metrics to...
- [!23766](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23766) Updated the link to the customer definition in sales handbook. Also,...
- [!23765](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23765) Update source/handbook/marketing/product-marketing/sales-resources/index.html.md
- [!23759](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23759) Using Apple TV
- [!23756](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23756) Minor improvements on usage ping references/links
- [!23753](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23753) Update Marketing Programs Handbook
- [!23744](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23744) Add instructions for zoom job title
- [!23740](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23740) Update self-managed and .com views to include new format
- [!23739](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23739) Adjusted alliances page to point to integrations page
- [!23712](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23712) Add link reference to Product Managers handbook page
- [!23684](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23684) Adding Territory codes to the handbook for MM.
- [!23645](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23645) Added benefits information for Switzerland
- [!23584](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23584) Updated Product team title page to include a folder for Readme's, as well as a Readme for Scott W
- [!23528](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23528) add-ux-one-pagers
- [!23512](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23512) Add MR coach section in the Contributor Program handbook
- [!23391](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23391) Update source/handbook/engineering/ux/ux-designer/index.html.md
- [!23386](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23386) Infrastructure blueprint: ZFS for Repository Storage Nodes
- [!23229](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23229) 30% ruling
- [!23193](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23193) Remove account management redirects
- [!23155](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23155) Adds "what we're logging" to diagnosing errors workflow
- [!22989](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22989) Update to stock options table for leveling by division
- [!22723](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22723) Info about how UX improvements are prioritized
- [!22659](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22659) Added Control category links
- [!21897](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21897) Handbook oncall updates

### 2019-06-04
- [!23748](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23748) Added @ mention for the Security automation group to GitLab groups and projects section
- [!23743](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23743) Add calendar invite instructions
- [!23732](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23732) Update EA page with Leader Scheduling Preferences
- [!23726](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23726) Eng prod triaging retro improvements
- [!23725](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23725) Fix incorrect header in IP troubleshooting doc
- [!23723](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23723) Adding Christopher and his relevant director to approval steps for product categories
- [!23720](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23720) Update index.html.md
- [!23716](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23716) Add video creds
- [!23710](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23710) fix spelling
- [!23693](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23693) Fix URLs based on header changes
- [!23689](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23689) Out of Budget Business Cases
- [!23674](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23674) Update revised MktgOps labels and descriptions in handbook
- [!23641](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23641) Update Visa page to reflect that we do not offer Work permits.
- [!23638](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23638) Corrected link to "Creating Gitlab test instance" in test_env.html.md
- [!23636](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23636) Add ChatOps to onboarding issue creation.
- [!23633](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23633) Reorganize Support Team KPIs
- [!23617](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23617) Update index.html.md
- [!23544](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23544) Document emoji etiquette for pipeline failures on master
- [!23466](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23466) Update Relocation for processing details
- [!23399](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23399) Clarify communications page
- [!23363](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23363) Add new categories image from direction page to acquisition handbook
- [!23361](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23361) Cleans and updates the production handbook page
- [!22334](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22334) Resolve "Create a demo for cross-project pipeline triggering and visualization"
- [!22246](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22246) Security Automation Handbook Page Beginning
- [!22203](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22203) Rename non-shared 1password vault

### 2019-07-06
- [!23722](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23722) Clarifying Support's Deep Dive Process

### 2019-06-03
- [!23686](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23686) Add Micro Focus Fortify Competitive Card - Update source/handbook/marketing/product-marketing/competitive/index.html.md
- [!23685](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23685) Resolve "Update Commercial CS Handbook page: working agreements/flow"
- [!23683](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23683) Clarifies how/where ICs should add themselves for Support stable counterparts
- [!23681](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23681) Update index.html.md reworded slack search and moved it and fixed heading level for food and drink
- [!23677](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23677) Resolve "Update Handbook: UX Landing Page, Persona Bullet"
- [!23676](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23676) Fixed broken link in content marketing handbook to social marketing handbook.
- [!23666](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23666) Add Clement to CEO Shadow program with updated dates
- [!23662](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23662) Update source/handbook/ceo/shadow/index.html.md,...
- [!23661](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23661) Fix typo in docker command
- [!23656](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23656) Update source/handbook/ceo/shadow/index.html.md
- [!23654](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23654) adjusted daycare contact info
- [!23653](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23653) update Enablement page to remove fulfillment
- [!23646](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23646) Resolve "Shift Fulfillment from Enablement to Growth"
- [!23643](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23643) Infrastructure: Where to create change request issue for production
- [!23634](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23634) Update Tax section
- [!23626](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23626) Fix link to NL work permit salary reqs
- [!23624](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23624) Add reasons why to shadow video
- [!23601](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23601) added additional tech stack applications
- [!23594](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23594) added food recommendations nearby GitLab HQ
- [!23587](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23587) Update source/handbook/marketing/product-marketing/customer-reference-program/index.html.md
- [!23547](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23547) Add video to CEO Shadow page
- [!23540](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23540) Review: Concurrent Usage Dashboard
- [!23460](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23460) Expand on communicate directly
- [!23146](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23146) [Security Team] Instructions for using the Security Management board
- [!19990](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19990) fixed inconsistent sentence

### 2019-06-02
- [!23621](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23621) Updating layout to make page more readable
- [!23605](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23605) Split out support KPIs from Engineering

### 2019-06-01
- [!23613](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23613) fix tech stack table
- [!23610](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23610) update title
- [!23606](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23606) Update source/handbook/board-meetings/bylaws.html.md
- [!23602](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23602) Update source/handbook/finance/financial-planning-and-analysis/index.html.md
- [!23599](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23599) Add instructions to set up for Zoom attendees in boardroom meetings
- [!23597](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23597) Update throughput labels description
- [!23596](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23596) Add conference to schedule, move JJ's rotation
- [!23592](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23592) Values Page Grammar and Punctuation Corrections
- [!23589](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23589) Update CEO Shadow schedule and add Tye to Alumni
- [!23580](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23580) Update - How to source candidates
- [!23565](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23565) Breaking out specific vendor templates
- [!23563](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23563) Add monitor configuration to Shadow page

### 2019-05-31
- [!23591](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23591) Update handbook links
- [!23588](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23588) Crosslink to UX page for easy UX
- [!23586](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23586) Update field name for bonus justification
- [!23570](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23570) Update preferred customer call app
- [!23561](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23561) Replace Victor with Plan PM
- [!23560](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23560) Add link from 'Leadership' to 'Being a public company'
- [!23559](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23559) adds stable counterparts to support page
- [!23553](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23553) Add a description to how and when to use a label.
- [!23546](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23546) Changed  Digital Ocean section, added VirtualBox Testing Env section
- [!23541](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23541) Update index.html.md - points to remember while billing professional services
- [!23539](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23539) Update source/handbook/sales/index.html.md
- [!23534](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23534) Adding internal audit function to handbook
- [!23531](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23531) Added high level standard hourly cost methodology for professional services
- [!23520](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23520) Add learning curve info to UX Handbook
- [!23506](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23506) Adding more relationships to Shadow goals
- [!23504](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23504) Propose split of other functionality in secure
- [!23502](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23502) Kk biz ops
- [!23500](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23500) Update index.html.md
- [!23499](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23499) Removal of former team member in alumni section
- [!23495](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23495) Update index.html.md
- [!23478](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23478) Add instruction for handling concerns over departures to handbook
- [!23456](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23456) Use Zoom webinar over Hangouts for Kickoff call
- [!23439](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23439) Suggest concurrent reviewers for multi-approval projects
- [!23429](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23429) Add justification questions for the offer process.
- [!23362](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23362) Add workflow for delivering FE/BE independently
- [!23247](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23247) Resolve "Add Roadshow notes to Blueprints"

### 2019-05-30
- [!23524](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23524) Fix link to bylaws in board meetings page
- [!23518](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23518) Create new Slack channel for each group
- [!23509](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23509) Update information about the bug bounty council
- [!23508](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23508) Added baseline role entitlements to handbook page
- [!23501](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23501) Br fpa jf
- [!23491](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23491) Add online education
- [!23487](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23487) Added link to Security Automation Engineer baseline entitlements runbook
- [!23484](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23484) Update approval for greenhouse for comp
- [!23483](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23483) Update Finance ref to GitLab Summit to Contribute
- [!23477](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23477) Add missing bold formatting in UX workflow page
- [!23476](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23476) add storyboards
- [!23469](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23469) Add Monthly Health Bill Documentation
- [!23463](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23463) Revert `GitLabber` change in "top misused terms"
- [!23462](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23462) Updates to technical diligence process
- [!23457](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23457) removing link
- [!23454](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23454) Correcting KPI Removal
- [!23450](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23450) Move definitions from operating metrics page
- [!23403](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23403) Update index.html.md
- [!23400](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23400) Adding 'Communicate directly' under Efficiency value
- [!23383](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23383) Update index.html.md
- [!23380](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23380) Customer Support Gearing Ratios
- [!23349](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23349) Update Product Leadership Meeting instructions to reference VP of Product
- [!23339](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23339) Add Childcare info to CEO-Shadow page
- [!23333](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23333) Jlt ar1
- [!23191](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23191) Added SMAU definition to Product Metrics
- [!23046](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23046) Add category epic to structure
- [!22941](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22941) Update section Unauthorized Reseller Deals that involve an unofficial reseller...
- [!22314](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22314) Resolve "Update "Social response time" KPI definition"

### 2019-05-29
- [!23438](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23438) update which EA supports which member of e-group
- [!23436](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23436) Update source/handbook/marketing/technical-evangelism/index.html.md
- [!23434](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23434) Remove links from the KPI mapping page that are not real links
- [!23433](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23433) Revert "Split approval process"
- [!23432](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23432) duplicate kpis and location updates
- [!23430](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23430) Growth Persistence Model logic and example in FP&A handbook
- [!23428](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23428) Fix chart
- [!23427](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23427) Require shipping during a fast boot
- [!23423](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23423) Update index.html.md to add tech evangelism
- [!23422](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23422) Resolve "Handbook link for Professional Services work board is outdated"
- [!23421](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23421) Added approval process for splitting stages into groups
- [!23410](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23410) Update source/handbook/business-ops/data-team/metrics/index.html.md
- [!23404](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23404) Resolve "Adding Slack as a software to Business ops handbook page"
- [!23397](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23397) adding jarvis standing desk link
- [!23395](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23395) redirecting laptop info and forms to itops page
- [!23393](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23393) Add data team triage rotation to handbook with allllllll the details
- [!23389](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23389) Add info on better using Date Filters in Periscope when using automatic date aggregations
- [!23370](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23370) Fix typos in Engineering Handbook
- [!23369](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23369) Add a link to GitLab team-member resources wiki
- [!23367](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23367) Changes GitLabbers to GitLab team-members
- [!23365](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23365) Stock option / US service providers with NSOs
- [!23357](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23357) Update operating metrics
- [!23352](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23352) Increase Tuition Reimbursement dependent on tenure, skill, performance
- [!23348](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23348) ZD workflow fixes
- [!23343](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23343) Create 'Being a Public Company' page
- [!23341](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23341) Recruiting KPI updates
- [!23336](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23336) Adding correct file path for tech evangelism handbook page
- [!23329](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23329) Add candidate selection section
- [!23327](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23327) Parental leave update
- [!23321](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23321) Add "Sprint" to top misused terms
- [!23285](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23285) Add Slack info to Support Team team communication
- [!23255](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23255) Reword DOM manipulation for UX purposes
- [!23232](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23232) Add forecast of renewals by health score dash
- [!23139](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23139) Updated Vacancy Creation Process for GH as SSOT
- [!23067](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23067) Add virtual events process to MPM Handbook
- [!23053](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23053) Bonuses KPI definition
- [!23032](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23032) Add current pillar info
- [!23010](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23010) Add High Output Management time and zoom links
- [!22698](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22698) Added Operations Dashboard as a sub-category under Debugging & Health
- [!22309](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22309) update requesting an email and email review protocol with additional items
- [!21290](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21290) Adding probation periods to the handbook

### 2019-05-28
- [!23331](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23331) Adding process for sharing meeting agendas
- [!23314](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23314) Update analytics schemas for Data Team with sensitive schema info
- [!23311](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23311) Add suggestions to the handbook
- [!23302](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23302) Update number of handbook pages
- [!23300](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23300) Clarify the reference process of team member's positive/negative candidate reference
- [!23299](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23299) Ksnape itopslaptops update
- [!23298](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23298) Bar raiser update: values instead of culture + link
- [!23295](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23295) Clarified salesforce signature process and when legal will review changes to terms
- [!23294](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23294) Rename chief culture officer to chief people officer
- [!23293](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23293) Update bar raiser section with link to Amazon's page
- [!23291](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23291) Adding Resources as misused term
- [!23290](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23290) Moved the section that details filing process under step 6 to add clarity
- [!23289](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23289) Add Mek to CEO shadow rotation for October
- [!23280](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23280) Fix typos and links
- [!23277](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23277) Add hygiene to the Code of Conduct
- [!23276](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23276) Add link to new categories page
- [!23263](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23263) Update handbook average/recommended pricing for keyboard and mouse
- [!23260](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23260) Review request for IACV vs. plan > 1
- [!23257](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23257) Delete data intern blueprint
- [!23245](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23245) Fix link to "What if Someone is Working on a Ticket that I’d Like to Work On?"
- [!23239](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23239) Replace broken link to explanation of .gitkeep
- [!23228](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23228) Chenje Application for CEO shadow rotation
- [!23207](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23207) Removing overlap between quotes that need approval and opportunities that need approval
- [!23205](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23205) Prevent: "I don't want to step on anybodies toes"
- [!23176](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23176) Proper icon for uploading pet photo
- [!23077](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23077) Added confidential disclaimer to our process page
- [!23027](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23027) Removed language indicating that we will easily accommodate customer changes...

### 2019-05-27
- [!23253](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23253) Add missed milestone label to issues in the Data team project before rolling over to the next milestone
- [!23238](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23238) Adds feedback project to support ops section
- [!23233](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23233) Improve a GitLabber's guide to time off
- [!23217](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23217) Add issue grooming link to Data Team milestone planning
- [!23213](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23213) Added additional apps, links, and definitions.
- [!23129](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23129) Update UX Handbook Landing Page
- [!23091](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23091) add commissions to parental leave
- [!22422](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22422) Adds a classification system to organize clusters

### 2019-05-25
- [!23211](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23211) Add CEO shadow rotation schedule through end of 2019
- [!23199](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23199) Add Grzegorz to serverless team page
- [!23196](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23196) Trying to Fix Minor territory tables 2 + Adding 2 More Minor Cuts
- [!23195](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23195) Removing location factor link
- [!23190](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23190) Update source/handbook/general-guidelines/index.html.md
- [!23137](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23137) Add baseline entitlements for all GitLabbers

### 2019-05-24
- [!23204](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23204) Update snowflake user provisioning
- [!23185](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23185) Relocating KPI
- [!23170](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23170) Add weights to Plan page
- [!23160](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23160) Clarify and reference picture instructions.
- [!23154](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23154) handbook / tax / wbso section
- [!23151](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23151) Remove discouraged status from handbook
- [!23144](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23144) Removed temporary redirect instructions and remaining temp redirects
- [!23124](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23124) Zendesk Light Agent credentials
- [!23083](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23083) Replace all references and mentions of Implementation Engineer with...
- [!23072](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23072) Add category/direction updates to cadence
- [!23061](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23061) Update the Other Product Handbook pages link section in /handbook/product
- [!23059](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23059) Product Handbook Updates to Move Category Visions out of Epics

### 2019-05-23
- [!23117](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23117) Onboarding - Include instructions to search within file, not search in the entire tab
- [!23114](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23114) FRT Hawks should also ensure the Problem Type of new tickets is correct
- [!23103](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23103) Remove copyedit step
- [!23096](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23096) Delete account management folder
- [!23088](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23088) tax page / update link in stock option section + added WBSO process
- [!23086](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23086) Update Plan team page
- [!23082](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23082) Fix issues with renamed stage labels
- [!23081](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23081) Add a tip to minimize Zoom during meetings
- [!23078](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23078) Stock options section
- [!23076](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23076) Reporting Taxable Gains on Option Exercises
- [!23068](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23068) Various documentation updates to Interviewing page
- [!23063](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23063) Adding 50-50 definition to handbook.
- [!23051](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23051) Defining Recruiting KPIs
- [!23024](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23024) remove content team review from release post instructions
- [!22991](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22991) Update support team handbook landing page
- [!22847](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22847) Improving 30% ruling documentation and adding transferring 30% ruling topic
- [!22683](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22683) Updates to Acquisitions Handbook
- [!21562](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21562) Add a new handbook section for Commercial Sales

### 2019-05-22
- [!23060](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23060) Update reference to System Usability Score
- [!23047](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23047) attempting to fix formatting on table of minors tables
- [!23044](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23044) adding new gl account process
- [!23040](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23040) Alliances KPI link
- [!23029](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23029) Updated language and linked to sales page outlining customer paper
- [!23028](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23028) Update tax procedures for stock options
- [!23025](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23025) Note that PMs do not prioritize contributions
- [!23023](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23023) Added detail on promo from Interim Manager to full time
- [!23021](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23021) Updated link to projects and changes paid user definition to licensed user definition.
- [!23019](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23019) Cmachado1 master patch 38016
- [!23017](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23017) Add info on automating periscope dashboards going into slack
- [!23016](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23016) Change UX Designer to Product Designer
- [!23012](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23012) Resolve "Adding monitor privacy filter to the spending page"
- [!23011](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23011) Resolve "Add pulse survey details to engineering metrics"
- [!23009](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23009) Update product KPIs to be out of scope for Q2
- [!23008](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23008) modify tbl formatting
- [!23005](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23005) fix table formatting
- [!23002](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/23002) No need to squash commits as matter of course.
- [!22997](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22997) Add Product KPIs and definitions
- [!22988](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22988) Triage duplicate/invalid H1 reports first
- [!22987](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22987) Update rep productivity
- [!22986](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22986) Add Engineering Pulse Survey Dashboard to the Periscope Directory
- [!22982](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22982) Updated index.html.md mostly with typo corrections.
- [!22976](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22976) Update source/handbook/marketing/product-marketing/competitive/application-security/index.html.md
- [!22975](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22975) Updating the language on the Experience Baseline steps
- [!22972](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22972) Add successful product direction items as a product KPI
- [!22963](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22963) adding comm sales section
- [!22956](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22956) Minor typos in communication page
- [!22951](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22951) Update tasks on day of milestone start
- [!22939](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22939) Adds design document for how we will transition traffic from vms to kubernetes
- [!22930](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22930) PIAA Agreements: Fix typo: view -> viewed
- [!22898](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22898) Growth engineering page improvements
- [!22890](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22890) adding shared calendar PTO Ninja instructions for SRE Onboarding
- [!22879](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22879) Add self-managed focus in Quality Engineering handbook and team yaml
- [!22834](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22834) adding books from lean coffee issue 30
- [!22784](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22784) Refactor maturity legend
- [!22735](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22735) typo in link text
- [!22667](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22667) Jj pmm metrics
- [!22560](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22560) Add Greenhouse to business tool list
- [!21939](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21939) Design Document :: Kubernetes Application Configuration Storage
- [!21936](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21936) Move Plan team page to correct place
- [!21440](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21440) Adds team level bonuses

### 2019-05-21
- [!22971](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22971) Add SUS and customer discovery conversations to product KPIs
- [!22970](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22970) Remove ambition percentage from product KPIs
- [!22966](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22966) fixed a misspelling and added rep productivity definition
- [!22965](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22965) Update KPI link to new product metrics page
- [!22959](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22959) add backup rolling duration details
- [!22958](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22958) Add a book clubs page to the handbook
- [!22953](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22953) Clarify how to add your profile to the team page.
- [!22952](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22952) Resolve "add links to ops schedule"
- [!22948](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22948) Add BizOps to the list of teams that are committed to having a female interviewers
- [!22940](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22940) Remove onboarding buddies
- [!22938](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22938) Made edits to "See Something Say Something" under the category diversity and...
- [!22934](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22934) Reorder content of blog hb
- [!22932](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22932) Add support recommendations to customer onboarding
- [!22922](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22922) Gm link
- [!22918](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22918) Update customer escalations page with responsibilities
- [!22907](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22907) delete account manager role in Customer Success handbook
- [!22891](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22891) Explaining lost GitLab Unfiltered invitations
- [!22887](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22887) Resolve "update content schedule"
- [!22883](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22883) Moving a feature down.
- [!22755](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22755) Wrap roles table in a div with auto overflow-x
- [!22741](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22741) Update the 'Asking "is this known"' section
- [!22715](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22715) Add video YouTube video introduction to the Vision page
- [!22713](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22713) Found a few typos.
- [!22668](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22668) Added information about UHC renaming offered plans
- [!22441](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22441) Career Matrix for Engineers - logic to partial files
- [!22400](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22400) Update XDR Quota details
- [!21700](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21700) Added Gender and Sexual-orientation Identity Definitions and FAQ content and links
- [!20515](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20515) Update index.html.md to remove spending limits.

### 2019-05-20
- [!22905](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22905) Fixed typo for links
- [!22900](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22900) Adding Business Drivers & Operating Workflow
- [!22896](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22896) add Comm Sales support details
- [!22892](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22892) Adding GM KPI definition.
- [!22885](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22885) Update KPI definitions for sales efficiency ratio and Magic Number
- [!22884](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22884) Update Recruiter, Coordinator and Sourcer Alignment by Department
- [!22875](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22875) replace 3 Memory team vacancies with new hires
- [!22873](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22873) updated letter of adjustment
- [!22869](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22869) Add social hour call to Monitor group page
- [!22868](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22868) Grammar fix on values page
- [!22865](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22865) update SDD responsibilities on MPM page per discussion with Jackie G. on...
- [!22863](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22863) Remove jarek from templates
- [!22862](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22862) Update country count on spending page
- [!22859](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22859) Update index.html.md with complete process for approval and invoicing of opportunities
- [!22858](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22858) rmv Brandy from territory charts
- [!22830](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22830) Update acquisition process to have an implementation strategy
- [!22829](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22829) Don't add configuration if you can avoid it
- [!22827](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22827) Moving product metrics to their own page, which is yet to be created.
- [!22796](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22796) Patch 19102
- [!22746](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22746) Improve the triage operations and triage policies page
- [!22744](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22744) Move gitlab-com metrics into product page and link
- [!22593](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22593) Update bar-raiser interview

### 2019-05-19
- [!22846](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22846) fix spelling error
- [!22769](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22769) Add book clubs to leadership books page

### 2019-05-18
- [!22842](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22842) Add safety exception to Group Conversations full name policy
- [!22841](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22841) fix "asynchronously" typo
- [!22837](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22837) fix "under standing "typo
- [!22789](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22789) Use full names in GC questions
- [!22557](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22557) Adding long term E/R target for Marketing to handbook

### 2019-05-17
- [!22831](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22831) added hyperlink to accounting reimbursement policy
- [!22819](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22819) Add resources for customer discovery meetings to Product handbook
- [!22818](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22818) added temporary contractor and admin account naming conventions
- [!22817](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22817) recommends BCC for large groups
- [!22816](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22816) Adding Jack as SFDC code & config
- [!22812](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22812) Resolve "Improve Periscope Review Process"
- [!22811](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22811) Add cross link
- [!22794](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22794) Added instructions for updating product managers.
- [!22788](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22788) fix typo
- [!22782](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22782) Added contact information for SecOps
- [!22778](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22778) fixed wording/typo
- [!22770](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22770) Adding Forecast Category definitions.
- [!22763](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22763) Consolidate legend in maturity page
- [!22753](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22753) Update Zendesk Business Rules and People information
- [!22743](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22743) Update index.html.md
- [!22680](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22680) Add books from leadership workshop to handbook
- [!22677](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22677) move definitions to sales handbook
- [!22674](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22674) update product responsibilities for gitlab.com
- [!22653](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22653) Add info on getting dashboard updates from the data team
- [!22580](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22580) Fix formatting on data team intern page
- [!22541](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22541) Update source/handbook/support/index.html.md
- [!21987](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21987) Not requiring test plan issue by default
- [!18747](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18747) Vault design doc

### 2019-05-16
- [!22736](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22736) Add nurture documentation
- [!22733](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22733) Added link to conference booth setup page
- [!22725](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22725) update dir, MO
- [!22724](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22724) Added Mktg Ops - Blocked label entry and explanation.
- [!22722](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22722) Updated DPA link from webpage to PDF
- [!22718](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22718) Update payroll notification step to not include Contractors.
- [!22716](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22716) Add Chorus transcriptions to Product sensing mechanisms
- [!22714](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22714) CMM update 2
- [!22710](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22710) Update SLA settings information & API Token request process
- [!22708](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22708) PMs should schedule user experience improvements.
- [!22707](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22707) Change wording
- [!22690](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22690) Fix broken links and typos in eng handbook
- [!22688](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22688) cc UX Manager on proposed solutions
- [!22687](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22687) Discretionary bonus _update to take Director of People Ops off as final approver
- [!22673](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22673) update gross margin definition and link
- [!22672](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22672) update links for new location for retention
- [!22661](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22661) Update milestone planning meeting docs
- [!22660](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22660) Improve formatting and fix typos in customer results section of values page
- [!22657](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22657) Update message about requesting a full ZD agent account through Access Request.
- [!22652](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22652) Add content schedule
- [!22646](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22646) update attendance section to include conference, off-sites, other meetings
- [!22644](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22644) Data sources as determining segment
- [!22639](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22639) Added UX Managers, UX to Product Designers
- [!22629](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22629) Categorize product sensing mechanisms in product handbook
- [!22625](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22625) Add 3 new principles
- [!22610](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22610) update tax section for option exercises.
- [!22489](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22489) Adding long term profitability for Gitlab.com
- [!22464](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22464) Adjust scaling factor
- [!22245](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22245) Resolve "remove BoD double listing"

### 2019-05-15
- [!22656](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22656) Add Use Case page
- [!22651](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22651) Update source/handbook/marketing/product-marketing/competitive/index.html.md
- [!22650](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22650) add mission control access doc
- [!22648](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22648) Adding current metrics definitions for gitlab.com
- [!22647](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22647) add churn analysis
- [!22640](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22640) add links to the app for modern health
- [!22628](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22628) Add GitLab.com Financial KPIs Dashboard to Periscope Directory
- [!22626](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22626) content hb update
- [!22623](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22623) Add line to acknowledge mistakes in inclusive language section
- [!22622](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22622) minor typo
- [!22616](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22616) Updating ldap debugging to reflect code changes
- [!22606](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22606) Updating reference process
- [!22603](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22603) Add measurement and KPIs to CS
- [!22530](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22530) Competitive and Comparison strategy options
- [!22413](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22413) Add Downloads by Install Type to Periscope Directory
- [!22405](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22405) Add Gitlab.com metrics page
- [!22338](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22338) Add Modern Health to Benefits

### 2019-05-14
- [!22599](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22599) Update index.html.md Parts of a KPI section to correct typo in 50/50 "change" to "chance"
- [!22595](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22595) Update Quality Engineering OKR to new format
- [!22591](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22591) fix tables
- [!22587](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22587) Correcting links in handbook
- [!22540](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22540) Update source/handbook/engineering/development/index.html.md.erb
- [!22539](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22539) Add profitability target for professional services.

### 2019-05-13
- [!22586](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22586) Include engineering group in customer meetings
- [!22583](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22583) Fix release time for GitLab certification program
- [!22582](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22582) Initial draft of Sales & Marketing KPI prioritization
- [!22578](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22578) update verbiage for external meeting links
- [!22577](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22577) remove in-person reference / update schedule section for BoD meeting
- [!22576](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22576) Add prioritization discussion.
- [!22574](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22574) Update source/handbook/business-ops/data-team/metrics/index.html.md
- [!22573](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22573) Update of tech stack ownership, admins, etc.
- [!22571](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22571) Grammar updates
- [!22567](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22567) Create a handbook resource and adapt unconscious bias values text
- [!22562](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22562) Update source/handbook/customer-success/tam/escalations/index.html.md
- [!22558](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22558) Update source/handbook/engineering/security/index.html.md
- [!22549](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22549) Create a Customer Success vision page in the handbook
- [!22519](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22519) correcting typos
- [!22467](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22467) Add intern blue print to the handbook
- [!21795](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21795) Add guidance to use CY for external dates

### 2019-05-12
- [!22559](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22559) Add timeframe for achieving long term ebita target
- [!22555](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22555) Adding precall email and advice for keeping calls from dragging on
- [!22552](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22552) KPI Priority for Engineering/Product
- [!22545](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22545) Update long term percentage of revenue target for G&A.
- [!22543](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22543) Update metrics page with references to Q2 Scope
- [!22525](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22525) corporate structure
- [!22511](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22511) Adding Zendesk customer support dashboard to directory

### 2019-05-11
- [!22533](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22533) Small edits to align with current hiring process
- [!22531](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22531) Updating Sourcer and Recruiter alignments
- [!22527](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22527) stock options
- [!22526](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22526) stock options section - link to tax page
- [!22514](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22514) Update index.html.md Correct spelling of inpired to inspired
- [!22513](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22513) fixed typos Update source/handbook/communication/index.html.md
- [!22512](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22512) Fixed two typos Update source/handbook/handbook-usage/index.html.md
- [!22503](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22503) Resolve "Fix TAM escalation page"
- [!22499](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22499) Updating misspelling of across
- [!22495](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22495) fixed partially typos
- [!22494](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22494) Updating misspelling of their.html.md
- [!22490](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22490) Fixed typo in handbook.html.md
- [!22487](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22487) Update travel section spelling error
- [!22485](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22485) Update spelling error in spending-company-money
- [!22483](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22483) Updated typo on line 153 from accross to across.
- [!22479](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22479) updated spelling of annoucements to announcements
- [!22478](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22478) Updated typo - accross
- [!22477](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22477) Updated typo - thier

### 2019-05-10
- [!22517](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22517) Trust and verify in engineering mgmt issues
- [!22506](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22506) Adding "handbook first" to "everything starts with a merge request"
- [!22491](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22491) Updating Communication section on Engineering handbook page
- [!22476](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22476) Fix typo (recieved to received)
- [!22475](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22475) Fix typo (thier to their)
- [!22474](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22474) Verified Periscope checkmark
- [!22470](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22470) remove js from handbook and team pages
- [!22469](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22469) corporate structure
- [!22292](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22292) Adding Allocation Methodology workflow and diagram

### 2019-05-08
- [!22440](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22440) Add Trueup dashboard link
- [!22435](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22435) Adding definition of Professional Services Value.
- [!22417](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22417) Remove Data Blueprint
- [!22406](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22406) Resolve "Move Team Members KPI"
- [!22363](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22363) Add escalations page to TAM Handbook, Fix relative links on TAM handpage
- [!22213](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22213) Handbook: fix broken asset tracking links

### 2019-05-07
- [!22433](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22433) Brings more concise language to the use of canary
- [!22429](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22429) Change "gratis" to "unpaid"
- [!22426](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22426) Add MVP section to the contributor program handbook
- [!22415](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22415) Add Periscope Directory JS notes
- [!22414](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22414) Adds CSAT info to support page
- [!22412](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22412) Adding two .com metrics to operating metrics page
- [!22409](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22409) remove references of #general and replace with #company-announcements
- [!22407](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22407) Adding GitLab Metrics associated with Sales
- [!22402](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22402) Update source/handbook/business-ops/data-team/metrics/index.html.md
- [!22397](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22397) Update source/handbook/support/workflows/services/gitlab_com/verify_subscription_plan.html.md
- [!22396](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22396) Adding link to location factor KPI to charts
- [!22395](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22395) Add link to definition of Location Factor
- [!22388](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22388) Adding details on reimbursement for meetup expenses
- [!22387](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22387) Update how time is used during egroup offsite
- [!22386](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22386) Add attendees and MR responsibility to egroup offsite page
- [!22384](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22384) Fix broken link to gl-security compliance issue tracker
- [!22377](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22377) Add ZD suppression removal documentation to workflow
- [!22376](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22376) team member definition
- [!22375](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22375) add pay date for contractor
- [!22370](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22370) Document dbt config and analytics schema better
- [!22357](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22357) fixing the links on engineering/infrastricture/team page
- [!22311](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22311) update marketing program managers responsibility section to include Sarah Daily
- [!22302](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22302) Resolve "Move Average Days to Close definition"
- [!22294](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22294) Add detail about customer discovery meetings to Product handbook
- [!22208](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22208) Update to acquisition process page
- [!22114](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22114) Build vs. Buy to Handbook
- [!22065](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22065) Career Matrix for Engineering's Individual Contributors
- [!21277](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21277) Add Limit to Product Leadership Meeting and clarify Discussion items
- [!19426](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19426) Sre team stable p2

### 2019-05-06
- [!22373](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22373) Clean up MR first improvements.
- [!22362](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22362) remove mf
- [!22361](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22361) Fixes for malformed links
- [!22359](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22359) Data team handbook typos
- [!22356](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22356) Add note to specifiy delivery method
- [!22351](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22351) security policy for internal requests
- [!22342](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22342) Link to the epic of the single docs codebase effort
- [!22340](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22340) updated typo in link to handbook
- [!22337](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22337) add sales changes
- [!22332](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22332) fix: small syntactical issue
- [!22325](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22325) Add more icons to data page, fix BI refs
- [!22324](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22324) Add Periscope Dashboard review steps
- [!22306](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22306) Resolve "Remove MQL from Op Metrics Page"
- [!22280](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22280) Add dbt test coverage rotation
- [!22115](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22115) Add Data Team Charter
- [!21958](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21958) Everything starts with a Merge Request
- [!21325](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21325) Resolve "TMM on-boarding plan - Creating GCP K8s cluster 102"
- [!18432](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18432) Adding ContractWorks process

### 2019-05-03
- [!22317](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22317) General cleanup of the Data Team Page
- [!22315](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22315) Add info on when to use SheetLoad to the Data team page
- [!22299](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22299) Resolve "Move Support SLA Metric"
- [!22297](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22297) Add Periscope Editor Training to the Handbook
- [!22295](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22295) Add New Hire Location Factor
- [!22293](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22293) Add roles to baseline entitlements
- [!22276](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22276) Update source/handbook/marketing/marketing-sales-development/field-marketing/index.html.md
- [!22275](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22275) Promo Processes
- [!22274](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22274) Adding P&L Allocation Methodology Image
- [!22265](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22265) Move maturity plans to handbook
- [!22263](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22263) Add a column representing current state to Maturity table
- [!22256](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22256) Update CRO reporting in the sales handbook
- [!22254](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22254) fix typo, correct spelling is (arch review)
- [!22251](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22251) Updated online growth job family URL to digital marketing
- [!22242](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22242) On boarding handbook update.
- [!22221](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22221) add process for no tuition deferment
- [!22215](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22215) Merge quarterly maturity table into main maturity page
- [!22062](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22062) Add Frontend Themed Call for May
- [!21687](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21687) Clarify Zapier usage and process for flagging community content
- [!21644](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21644) Adding additional details around MM Majors and Minors to the handbook, prep to add some Minor Maps
- [!21637](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21637) Move growth direction to handbook
- [!20860](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20860) Specify format of issues in Secure standups
- [!20720](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20720) Tracking category maturity

### 2019-05-04
- [!22285](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22285) Maintainers who raise objections against someone being a maintainer should help develop/mentor a long term plan
- [!22264](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22264) Update source/handbook/business-ops/data-team/periscope-directory/index.html.md
- [!22258](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22258) Moved OBS_Studio_Stream.jpeg to where it's accessible and updated link. Corrected some spelling.

### 2019-05-02
- [!22249](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22249) Link to the Pick Your Brain interview playlist on YouTube.
- [!22241](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22241) lockout changes durring summit
- [!22237](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22237) Replace | with & on the BizOps page
- [!22225](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22225) Add Periscope Data Training to the handbook
- [!22219](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22219) Update source/handbook/marketing/marketing-sales-development/online-marketing/index.html.md
- [!22218](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22218) CLeanup the KPIs.
- [!22214](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22214) Update to confirm DRI for  communication moderator
- [!22211](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22211) update marketing
- [!22209](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22209) Add issue-reproduce group to handbook
- [!22206](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22206) simple typo change to wording
- [!22205](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22205) Fix KPI links on CEO page
- [!22204](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22204) add file for booth setup instructions
- [!22197](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22197) Link directly to new access request template
- [!22194](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22194) Improvement on KPIs pages
- [!22190](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22190) Update complete ⬤ to ●
- [!22182](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22182) Added Azure DevOps and Cloudbees Jenkins competitive cards to CI page
- [!22155](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22155) Add expenses, travel and lodging details to CEO shadow page
- [!22104](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22104) Add x-functional collaboration process, vision, mission, and common links
- [!21992](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21992) Remove reviewer step and update CE/EE reviewers
- [!20700](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20700) Add cross-stage features guidance

### 2019-05-01
- [!22183](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22183) Change Harvey Balls
- [!22173](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22173) Update Memory team page and Enablement vision
- [!22170](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22170) Added all custom events to marketing ops page
- [!22168](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22168) Update How-to-be-a-FRT-Hawk.html.md to include priority
- [!22167](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22167) Content handbook fix
- [!22149](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22149) Add maturity by quarter
- [!22147](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22147) Visiting Grant
- [!22146](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22146) Contractor process
- [!22138](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22138) Updates related to personnel changes eff 1 May
- [!22135](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22135) Jj hiddengroup2
- [!22134](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22134) This adds links and a connection between our rules of engagement and the...
- [!22130](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22130) Update tools and tips for more clarity around how and why to set Calendly availability
- [!22129](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22129) update in-person meeting preferences, add section for each exec
- [!22126](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22126) Start table of baseline entitlements
- [!22074](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22074) Update source/handbook/business-ops/it-ops-team/index.html.md
- [!22068](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22068) Start moving GitLab KPIs page into the data team's company metrics mapping page.
- [!22067](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22067) Updating interview timeframe to be more realistic
- [!22037](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22037) Update superannuation percentage for Australian employees
- [!22031](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22031) Remove Looker Reference from Writing Guidelines
- [!22016](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22016) Add section on expiring trials for new subscriptions
- [!21881](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21881) Fix link in documentation index page
- [!21823](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21823) First iteration on Country Conversion process
- [!21732](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21732) Add `no favoritism, be fair` to the permission to play section
- [!21707](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21707) Add qualifier on data that relates to customer.

### 2019-04-30
- [!22112](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22112) Update CEO Shadow - minor edits
- [!22090](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22090) Update page to indicate we can not hire in Spain right now
- [!22084](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22084) Add Zendesk API Request process
- [!22080](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22080) Update UXD title to Product Designer
- [!22075](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22075) Jj sales resource pageupdate
- [!22070](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22070) Minor copy tweaks for accuracy
- [!22061](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22061) Update How-to-be-a-FRT-Hawk.html.md add Need Org and Triage view responsibility
- [!22059](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22059) Fix 'catagories' -> 'categories' typo
- [!22058](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22058) Update eligibility-ceo-shadow
- [!22057](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22057) update ceo-shadow page
- [!22052](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22052) Update source/handbook/marketing/website/index.html.md
- [!22047](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22047) Add Clinton Sprauve to Rotation starting 2019-06-17
- [!22045](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22045) Changed CON to CFG for security controls
- [!22044](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22044) Add second video discussing OpMetrics page + placeholder for e-group KPIs
- [!22040](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22040) Update CEO Shadow page with notes from call with Erica
- [!22035](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22035) Resolve "Add Items to the CEO Shadow page"
- [!22030](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22030) Monthly metrics meeting updates
- [!22026](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22026) Updates to Data Blueprint
- [!22025](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22025) Adding `OOO` to your status message will keep you out of the reviewer roulette
- [!22023](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22023) Fix misspelled word on values page
- [!22019](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22019) Setting the ground work to eliminate metrics pages
- [!21961](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21961) infra management team
- [!21956](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21956) Making info around web directs and the original opp more understandable
- [!21887](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21887) Update engineering page structure
- [!21861](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21861) adding video around submitting quotes for approval for both the submitters and the approvers
- [!21752](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21752) Adding POfulfillment@gitlab.com to the handbook on how to complete a PO
- [!21242](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21242) Add process to relocate to the Netherlands
- [!20163](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20163) Add Support for "Non-Linear" Career Paths to the Job Families Template

### 2019-04-29
- [!22039](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22039) Clarify that bizops is both a product + service function
- [!22038](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22038) Add sentence to the data team page clarifying when to move missed issues
- [!22033](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22033) add Nnamdi Iregbulem to rotation
- [!22013](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22013) Fix security guidance files that fail to checkout on Windows
- [!22011](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22011) Add Blueprint for Business Ops and Data
- [!22009](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22009) fix dual table
- [!22008](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22008) Update index.html.md added reviews for top 3 features
- [!22006](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22006) Add pronouns link
- [!22005](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22005) Update description for when to create release post
- [!22003](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22003) Fix Partnerships typo on Stock Options page
- [!21997](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21997) acquisition handbook hyperlink fixes and duration update
- [!21996](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21996) People can nominate themselves as trainee maintainers
- [!21994](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21994) Fix link to team retrospectives by using full path rather than relative path
- [!21986](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21986) Improve triage steps from TAE. Be specific what domain expert means, get help from #triage
- [!21981](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21981) Proposed corrections to typos on PIP page
- [!21867](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21867) Add ecosystem specialties and team homepage
- [!21026](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21026) Resolve "Update SA triage handbook"

### 2019-04-26
- [!21973](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21973) Add transfer restriction and update dual class sunset
- [!21972](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21972) Clarify customer meetings
- [!21970](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21970) Canada Medical Allowance
- [!21955](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21955) Stock Options Reporting Process
- [!21953](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21953) People Operations Metrics
- [!21948](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21948) Remove CCO outdated reference
- [!21945](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21945) additional language for promo process
- [!21941](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21941) Update source/handbook/business-ops/it-ops-team/index.html.md
- [!21934](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21934) Removed barbie from Manager Review section of global comp
- [!21932](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21932) Renaming San Diego/SoCal to Southern California
- [!21931](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21931) fix team member tables stable counterparts
- [!21928](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21928) Update SDR Manager referral email
- [!21926](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21926) Add nurture process to handbook
- [!21925](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21925) Add handbook links to group conversations schedule
- [!21919](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21919) Docs/interviewing cleanup
- [!21902](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21902) Added group membership reporting links and link to access management policy and process
- [!21900](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21900) Add Dual Class
- [!21885](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21885) Adding Sourced and Prospect definitions.
- [!21884](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21884) Add SAL->ASM Holdover Account Policy to Sales Handbook
- [!21875](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21875) fix typo "onlune" to "online"
- [!21846](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21846) Add some more detail to SAO criteria
- [!21837](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21837) Fix typo exiting = existing
- [!21829](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21829) Add Fulfillment dev workflow
- [!21824](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21824) Adding reference to "ready for development" column
- [!21781](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21781) Dormant Namespace Workflow: fixed minor issues
- [!21724](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21724) Clarify UX debt label
- [!18571](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18571) Update link to Go contribution guide

### 2019-04-27
- [!21959](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21959) Add Pricing Approval Process for Add-on Features
- [!21950](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21950) Fix broken link by changing the relative path to anchor
- [!21933](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21933) Encourage using labels as part of Manage planning

### 2019-04-25
- [!21910](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21910) Updated line about canonical URLs in cross-posted articles
- [!21908](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21908) Update index.html.md added Cindy Blake to CEO Shado June 3-14
- [!21906](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21906) Analyst Documentation
- [!21905](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21905) Remove out of date pipeline reset info
- [!21898](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21898) Update to password policy
- [!21891](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21891) add cross posting instructions for partners
- [!21886](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21886) Update to my full title
- [!21873](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21873) Fix typo
- [!21872](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21872) fix translation typo
- [!21869](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21869) Fix a couple spelling errors
- [!21864](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21864) formatting
- [!21858](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21858) Update quality roadmap with mutation tests
- [!21818](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21818) Add JJ to CEO shadow in Aug/Sept
- [!21802](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21802) Warning message about disruptive activity
- [!21779](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21779) CEO shadow request
- [!21574](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21574) Made edit to the training section
- [!21544](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21544) A new section for CLAs for EE contributions
- [!18876](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18876) Replace Sentry with Kibana

### 2019-04-24
- [!21860](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21860) removing SDR account assignment from the handbook
- [!21849](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21849) Update source/handbook/finance/accounting/index.html.md
- [!21843](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21843) Update meeting preferences for Sid.html.md
- [!21841](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21841) Update index.html.md with internal email send message change
- [!21836](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21836) Fix typo dyed == dried
- [!21831](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21831) Updated CEO Shadow request - Danielle Morrill 9/23
- [!21820](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21820) Update instructions for TAE triage
- [!21817](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21817) Update index.html.md to add Zero Trust fundamentals
- [!21815](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21815) Moved Groups to Control
- [!21811](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21811) Add Tye Davis to 2019-04-29
- [!21788](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21788) More broken link and redirect fixes
- [!21755](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21755) jburrows001 Add laptop refresh info
- [!21747](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21747) Initial draft of Development Department homepage
- [!21715](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21715) Updated instruction, as UI had changed.
- [!21300](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21300) Updating the security section with Comms info

### 2019-04-23
- [!21777](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21777) update instructions for forming breakout groups
- [!21775](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21775) Clarify accessing YouTube brand accounts
- [!21768](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21768) Jj workshops
- [!21761](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21761) Fix typo in job title on CEO Shadow Page
- [!21751](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21751) PMM write intro, TMM reviews feature messaging
- [!21712](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21712) Link to "www-gitlab-com project"
- [!21433](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21433) Universal Interview Feedback Scoring Definition

### 2019-04-22
- [!21742](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21742) fixing 404s with redirects and page updates
- [!21741](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21741) Add Investor Relations pg
- [!21737](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21737) Update index.html.md Fixed typo of "relevent"
- [!21727](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21727) Add a reminder of commit message guidelines to handbook usage page
- [!21697](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21697) Typo fix
- [!21688](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21688) Release post doc updates from 11 9 retro
- [!21681](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21681) Add guidance for product content in group conversations
- [!21626](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21626) Compensation adjustment for Quality Engineering
- [!21566](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21566) Add Dashboard review process
- [!21374](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21374) Linking product feedback and issue creation in the support handbook. Also...
- [!21312](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21312) Create a new MVC issue

### 2019-04-19
- [!21703](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21703) Add Kenny's 2nd week to rotation table
- [!21699](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21699) Add accountability as a sub value of transparency
- [!21690](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21690) modify formatting
- [!21680](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21680) Elaborate on pricing advantages
- [!21678](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21678) Added zero trust model testing to red team
- [!21671](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21671) remove mg reference
- [!21653](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21653) rename all Department to Sub-department

### 2019-04-20
- [!21667](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21667) Experience baseline recommendations

### 2019-04-18
- [!21662](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21662) Added public engagement section
- [!21659](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21659) Add quality mandate for triage SLA
- [!21655](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21655) Add sub value to iteration
- [!21654](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21654) Updates to process
- [!21646](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21646) Added reference from main handbook page, fixed broken link
- [!21642](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21642) Added bullet on rapid iteration of small tasks.
- [!21639](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21639) Added CEO introduction section
- [!21638](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21638) Create acquisition handbook
- [!21633](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21633) Hiring copy edits
- [!21627](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21627) Handbook update: Calendly setup
- [!21625](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21625) Revert "Merge branch 'tatkins-add-lulu-info' into 'master'"
- [!21624](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21624) Remove private healthcare as it is inaccurate
- [!21621](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21621) Add info about Lulu, a Little Snitch alternative.
- [!21612](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21612) Update source/handbook/business-ops/it-ops-team/index.html.md, source/handbook/index.html.md files
- [!21611](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21611) Update source/handbook/index.html.md
- [!21610](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21610) Update source/handbook/business-ops/it-ops-team/index.htm.md
- [!21608](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21608) Update source/handbook/business-ops/blueprints/index.html.md
- [!21607](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21607) Add reasons for churn / expansion dollar weighted metric
- [!21601](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21601) Add renewal guidelines for EDU/OSS licenses
- [!21595](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21595) Add line about childcare being covered for CEO shadow if needed
- [!21593](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21593) additional language around taking time off
- [!21587](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21587) We already had this policy but I wanted to make it explicit inspired by...
- [!21584](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21584) Added Gold to trial follow up messaging
- [!21570](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21570) Follow-up on filling Enablement homepage
- [!21554](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21554) SSOT cleanup
- [!21506](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21506) Amend Product OKR Update Process
- [!21454](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21454) MVC for diverse interviews
- [!21390](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21390) Update retrospective process to have action items documented coming to meeting
- [!21168](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21168) Update SAL/SDR Pairings / Territory Table

### 2019-04-17
- [!21604](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21604) clarify university routing
- [!21597](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21597) Uplevel Clarification and Process Requirements
- [!21594](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21594) update to include reschedule window and update to contribute schedule
- [!21592](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21592) quick typo fix
- [!21588](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21588) Jj resourcepg
- [!21585](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21585) Add references in handbook to triage training videos
- [!21583](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21583) Update US West SDR Pairing
- [!21577](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21577) Broken link on Engineering Growth Page
- [!21576](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21576) fix additional bullet as it read weirdly.
- [!21567](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21567) Reorganize defined and undefined metrics.
- [!21565](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21565) Update index.html.md for Okta, added the Getting Started and FAQ links
- [!21556](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21556) Investor Update Verbiage and Prelim/Actual close process
- [!21538](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21538) Add context around changing MRR
- [!21534](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21534) Clarify meaning of 2x rule for dogfooding
- [!21510](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21510) Fix typos in values
- [!21445](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21445) Small batch of 404 updates
- [!21422](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21422) Update documentation pointing
- [!21405](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21405) Focus direction page by moving some info to handbook
- [!20872](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20872) Clarify CSAT calculation

### 2019-05-26
- [!21560](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21560) aligning lead and account segmentation to be connected and how to work with them

### 2019-04-16
- [!21551](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21551) Adding the nist.gov reference
- [!21545](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21545) Added Red Team description to the Security page
- [!21539](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21539) Update index.html.md - fix offboarding link
- [!21536](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21536) quarter to 2
- [!21535](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21535) Fix ally page name
- [!21531](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21531) Remove duplicate content and improve intro and SSOT section to link to docs
- [!21528](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21528) Update Visa page with SO info for Contribute
- [!21513](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21513) Remove reference to Azure canary environment.
- [!21511](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21511) Update definition for licensed users.
- [!21502](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21502) Adds Lyle to CEO Shadow Program
- [!21500](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21500) Add Eric Brinkman to CEO Shadow Program
- [!21499](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21499) Update source/handbook/finance/accounting/index.html.md
- [!21495](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21495) Add scoping and issue type details to data team page
- [!21493](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21493) Add SUS to ux page
- [!21484](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21484) Update broken link and replace "Summit" with "Contribute"
- [!21482](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21482) initial updates to 4188-fill-enablement-department-homepage-2
- [!21478](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21478) Update to Professional Fees accrual process Update source/handbook/finance/accounting/index.html.md
- [!21465](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21465) Reformat competitive campaign calendar in handbook
- [!21412](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21412) Add info about vulgar language during the interview process
- [!21378](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21378) Resolve "enhance POC page with LITE process"
- [!21334](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21334) Quote Approval Process addition
- [!21330](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21330) Adding in that Segments on our customer accounts are defined at the Parent Account Level
- [!21297](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21297) Add product vision planning guidance
- [!20985](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20985) Add product leadership page
- [!20702](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20702) Update the broken diversity link

### 2019-04-15
- [!21498](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21498) Revert "Adds Lyle to CEO Shadow program"
- [!21497](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21497) Revert "Add EricB to rotation"
- [!21494](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21494) Add serverless deck
- [!21491](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21491) Fixed some minor typos and grammatical errors
- [!21488](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21488) Add Clement to CEO Shadow
- [!21487](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21487) Add note about linking to Slack threads in public GitLab.com issues
- [!21486](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21486) Rewrite Monitor Calendar/PTO paragraph
- [!21483](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21483) Remove links for metrics that don't have definitions
- [!21471](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21471) Fixed Interview training broken link
- [!21467](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21467) Add instructions for Youtube on how to switch account
- [!21463](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21463) Fix grammar "time well spend" ~> "time well spent".
- [!21462](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21462) Update maturity change approver to VP of Product Strategy
- [!21456](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21456) New Milestone process proposal
- [!21449](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21449) Update index.html.md.erb - Fix typos re AU/NZ process
- [!21429](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21429) Correcting the harassment instructions and links
- [!21408](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21408) Update Canada and fix grammer
- [!21380](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21380) Changed Wording on Promotions/Transfers
- [!21329](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21329) Update adding comments on existing issues
- [!21267](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21267) Includes ICs to the harassment training requirement
- [!21254](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21254) Add Free Cash Flow metric placeholder

### 2019-04-13
- [!21448](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21448) Add note to the distribution support request process regarding advance notice
- [!21435](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21435) Add RCA Link to SecOps On-Call
- [!21434](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21434) Add Link to SecOps on-call guide
- [!21431](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21431) Include link to RCA evangelism page

### 2019-04-12
- [!21438](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21438) Add mobile messaging section
- [!21421](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21421) Update source/handbook/meltano/index.html.md
- [!21419](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21419) Improve main onboarding page -added lists of known templates and pages
- [!21418](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21418) Make implication of Global Optimization and Collaboration values explicit
- [!21414](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21414) Add the country conversion process to the handbook
- [!21409](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21409) Add it-pos
- [!21407](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21407) Update CHD dates
- [!21406](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21406) Adding currency selection for Moo docs
- [!21403](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21403) Turn the value Transparency purple
- [!21402](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21402) Clarify info is from Just Commit campaign
- [!21394](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21394) Took off Looker links which aren't active anymore and changed titles from...
- [!21391](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21391) update rotation table through october
- [!21379](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21379) Added section on bulk access criteria to access management procedures
- [!21364](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21364) Add community engagement section w/ conferences
- [!21034](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21034) Fix data classification page formatting
- [!20800](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20800) Update SLAs section to Data Team Handbook

### 2019-04-11
- [!21387](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21387) Update Onboarding Processes to take out Git Going.
- [!21384](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21384) Removing reference to previous director dev-backend
- [!21383](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21383) Removed blank Geo back from Ops department
- [!21375](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21375) Resolve "point SA handbook page to security pages"
- [!21371](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21371) Adjusting description of travel grant for clarity
- [!21348](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21348) add calendar to competitive campaign on Mktg Handbook
- [!21346](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21346) Bullet Business card ordering to make it easier to read
- [!21344](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21344) Updated Planned rotation and added CEO Shadow Program Alumni list
- [!21339](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21339) Revert "Merge branch 'jlh-add-saasterms' into 'master'"
- [!21335](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21335) Added to qualifying description
- [!21333](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21333) Add speaker resources and Zaps, clarify meetup speaker protocol
- [!21331](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21331) Resolve "Exit plan of engineering global optimization"
- [!21308](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21308) Geo: Moving files to match reorg
- [!21302](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21302) Add Fulfillment team page
- [!21291](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21291) Fix typo in values
- [!21174](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21174) Add additional guidance on small primitives
- [!21167](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21167) Update some legacy links on the on-call page
- [!21166](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21166) Some grammar/spelling/wording/formatting fix ups on the change management page, for easier reading

### 2019-04-10
- [!21326](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21326) Add HN good faith guideline
- [!21316](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21316) Correct link to blog calendar
- [!21315](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21315) updated password policy guidelines
- [!21314](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21314) Move the distribution team pages to the new reorg path under development/enablement
- [!21310](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21310) Changing copy to reflect background checks happening during process, not post-offer
- [!21309](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21309) Added a couple of FAQs re: squatting & trademarks
- [!21307](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21307) Expand on default-on
- [!21305](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21305) Added section on Info Sec policies to Security Resources section of Security page
- [!21304](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21304) Add SaaS Addendum
- [!21301](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21301) Changed some groupings, added content, cosmetic changes
- [!21296](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21296) Remove Tommy Morgan's Manager README
- [!21286](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21286) Add Okta information. page to Handbook
- [!21278](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21278) Add Kenny Johnston to CEO Shadow Schedule in August
- [!21276](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21276) Add new dashboards to Periscope Directory
- [!21275](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21275) Add links to data team communication from BO pg
- [!21273](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21273) Added buyer personas
- [!21270](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21270) Add Data Team Milestone Planning process to the Handbook
- [!21268](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21268) Update for Digital Marketing handbook page with noindex
- [!21265](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21265) Update HN post template on source/handbook/hiring/vacancies/index.html.md
- [!21245](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21245) Add details on what to do when master is broken: revert or quarantine
- [!21239](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21239) Make @dennis an official manager!
- [!21195](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21195) Add learnings from 8 Apr kickoff call
- [!21171](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21171) Try to keep video on for all calls
- [!21156](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21156) Propose a concrete solution, not just a problem
- [!21102](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21102) Add Reproducibility to our Transparency value
- [!20608](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20608) Further specifying the junior level moratorium in engineering
- [!20507](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20507) Support: Refresh macros workflow and add missing categories

### 2019-04-09
- [!21266](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21266) Improve and link got inspired page
- [!21251](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21251) Update gitter channel references
- [!21249](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21249) Update source/handbook/marketing/product-marketing/sales-resources/index.html.md
- [!21248](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21248) Update sales qualification questions.
- [!21247](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21247) add video tutorial for using Web IDE to update team page
- [!21238](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21238) Added youtube enablement links and re-ordered click-throughs
- [!21237](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21237) Update OTE table in BambooHR
- [!21234](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21234) Included Stage Step in the Edit the Handbook section.
- [!21231](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21231) Changed iacv to IACV as it is an acronym.
- [!21222](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21222) Remove R.A.D. section; obsoleted by ongoing work
- [!21209](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21209) IP block troubleshooting doc update
- [!21197](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21197) Create handbook page to list companies inspired by GitLab
- [!21154](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21154) Add Growth pages for reorg
- [!21144](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21144) present day pessimists
- [!21121](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21121) SecOps On-Call Guide
- [!21109](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21109) Improve secure department page
- [!21078](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21078) unpaid leave of absence
- [!19696](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19696) first iteration on infra career development

### 2019-04-08
- [!21226](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21226) Update schedule for monthly investor update
- [!21217](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21217) Updated number formatting
- [!21215](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21215) Revert "Update index.html.md"
- [!21214](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21214) Improve content/structure detail on Documentation Handbook page and links back to dev docs
- [!21205](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21205) Tweaks to priority view descriptions
- [!21203](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21203) fix header tag
- [!21202](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21202) Adding use of "Next Step Date" and "Next Steps" fields
- [!21193](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21193) Add Joe Z to rotation
- [!21185](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21185) Clarify external process for paging security.
- [!21183](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21183) Update Create "What to work on" section around Deliverable and Stretch
- [!21178](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21178) Added corporate events mission statement
- [!21177](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21177) Link to instructions on setting up the kickoff hangout
- [!21176](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21176) Add note about durations/timekeeping
- [!21173](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21173) Transparency about non-transparency
- [!21162](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21162) Update the duration of GitLab CI/CD for external repos
- [!21143](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21143) Added tips for First Response Time Hawk Fine Tuning
- [!21118](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21118) Add recurring meetings to monitor page
- [!21067](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21067) Add in RCA evangelism page to Engineering.
- [!21049](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21049) Add details on external plugin prioritization.
- [!20923](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20923) Added link to redirect process in repository from DMP handbook page
- [!20698](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20698) Defined ARPU
- [!20468](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20468) blueprint for self service 301s on about.gitlab.com

### 2019-04-07
- [!21161](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21161) Remove pages from index

### 2019-04-06
- [!21146](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21146) Routing changes for small segment to BDR team
- [!21145](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21145) add customer brief template and subject for requesting meetings
- [!21142](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21142) Fellow is equivalent to a senior leader
- [!21141](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21141) Added the Slack recruiting channel naming convention
- [!21086](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21086) Removing "page has moved" pointers

### 2019-04-05
- [!21133](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21133) fix punctuation
- [!21132](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21132) update slack channel list
- [!21131](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21131) added new slack channel
- [!21128](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21128) Removeak
- [!21122](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21122) Removed "legal intern" as a contact for necessary information
- [!21119](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21119) Add bronze-silver trial note to internal support page
- [!21115](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21115) Update formatting
- [!21095](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21095) Holiday party
- [!21087](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21087) Expand what SLO is
- [!21084](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21084) added dashboards to online-growth handbook page
- [!21079](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21079) Updated the videos with quick links to topics.  Added two sentences describing...
- [!20772](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20772) Add government institutions exception to CA handbook
- [!20708](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20708) Add finance handbook entry for how to pick a budget
- [!20467](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20467) L&D new manager enablement handbook page

### 2019-04-28
- [!21089](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21089) Update debugging procedures

### 2019-04-04
- [!21083](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21083) Add step for CES new hire
- [!21076](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21076) Adding new manager program page and small fixes to harassment training
- [!21075](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21075) Update steps to send Team and Pets MRs to your manager
- [!21068](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21068) Adding sections for Feedback and Vision headers as well as context to each of those sections (to be added)
- [!21066](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21066) Added suggestion to use freedom.to
- [!21057](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21057) update next steps for Regeling Internet Thuis
- [!21054](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21054) jburrows001 Replaced data classification link
- [!21050](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21050) Update onboarding links
- [!21044](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21044) Add expense reporting instructions
- [!21042](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21042) Update source/handbook/marketing/marketing-sales-development/field-marketing/index.html.md
- [!21041](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21041) Add more details to the Periscope Directory + Update ARR by Annual Cohort Definition
- [!21035](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21035) Adding "materials" attribute to template and all roadmaps
- [!21033](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21033) Update the asdf commands on the linux tools page, to match current reality
- [!21032](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21032) Add Data Classification Policy to handbook
- [!21029](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21029) new share levels and stock split announcement
- [!20999](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20999) Add info about communicating with CAs to blog handbook
- [!20971](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20971) Add link to ZFS design to index
- [!20939](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20939) Sweep Handbook for old SFDC links
- [!20919](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20919) Initial cut: "Refactor handbook for development department re-org"
- [!20912](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20912) Update blog hb and issue template - due dates
- [!20779](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20779) Define Light PM Responsibilities and Expectations

### 2019-04-03
- [!21031](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21031) Clarify list import
- [!21028](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21028) Add clarity to claiming the visiting grant
- [!21021](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21021) Revert "clarify list import process"
- [!21019](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21019) Test Engineering Process: Remove role headings from TOC
- [!21016](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21016) add rotation table
- [!21014](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21014) Update index.html.md to reflect current onboarding issue nextravel account setup guidance
- [!21012](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21012) Update cards round 2
- [!21010](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21010) Update link to Engineering Hiring information
- [!21009](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21009) Add Reporting to Finance page
- [!21008](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21008) Jj hiddengroup
- [!21003](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21003) Update marketing handbook button text explanation.
- [!21002](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21002) Sid interest label and participants
- [!21000](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/21000) business cards update
- [!20997](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20997) Changes to the Periscope Directory Page
- [!20995](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20995) Update Meltano markdown page to summarize current team and project
- [!20994](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20994) Jj gettingstarted
- [!20987](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20987) Jj 102
- [!20984](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20984) Edits to blog handbook
- [!20978](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20978) Add Google Docs tips to handbook
- [!20975](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20975) Resolve "Update CAB handbook page"
- [!20973](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20973) workers_comp_insurance request
- [!20927](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20927) Updated security best practices
- [!20866](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20866) Add Meta Dashboards
- [!20827](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20827) Revise test planning process
- [!20808](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20808) Remove preferred from pronouns https://gitlab.com/gitlab-com/people-ops/recruiting/issues/120
- [!20770](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20770) Secure data travel
- [!20759](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20759) GC calendar invites should include stream type

### 2019-04-02
- [!20967](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20967) Add more specifics about Design System Push
- [!20965](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20965) Updated interview training issue to include verification that issues are complete
- [!20951](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20951) Updates to biweekly Sales Call format and agenda
- [!20948](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20948) add training section to CEO shadow page
- [!20937](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20937) Info abt Campaigns in SFDC/Mkto
- [!20935](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20935) update public sector table w/ new team mmbr
- [!20934](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20934) Fix tense in time off recommendation
- [!20921](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20921) Update source/handbook/marketing/product-marketing/getting-started/102/index.html.md
- [!20909](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20909) Second iter on comp roadmaps
- [!20906](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20906) Eng comp roadmaps
- [!20904](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20904) Revert "Merge branch 'remove-a.h' into 'master'"
- [!20902](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20902) Update redirect process in handbook
- [!20896](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20896) Update HRBP contacts
- [!20858](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20858) additional language for promo process- experience factor worksheet
- [!20813](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20813) Add Periscope User Roles to the Handbook
- [!20781](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20781) Added the word Guidelines only to password policy guidelines
- [!20743](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20743) Add Community Relations GC to schedule
- [!20682](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20682) Add to tips for Slack
- [!19835](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19835) Update Letter of Adjustment to include title changes and signing power to Carol
- [!19804](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19804) Cleanup Paid Tiers product section including tier determination process

### 2019-04-01
- [!20890](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20890) Added "What to expect" section
- [!20886](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20886) update boardroom TV instructions
- [!20859](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20859) Document the /h1import command correctly
- [!20857](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20857) Move gl-website content after other content.
- [!20849](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20849) Add clarity on whether 30% tax ruling can be expensed.
- [!20846](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20846) Add anchor
- [!20840](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20840) Updated the offer process and added business ops to laptop order process
- [!20839](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20839) Removed time marker from maintainership guideline
- [!20794](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20794) Sales ops operating metrics rep productivity
- [!20791](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20791) Removed the offer email step in Contract area
- [!20544](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20544) 401(k) limit
- [!20487](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20487) Update source/handbook/ea/index.html.md
- [!20437](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20437) Correct the name of the #loc_valley chat room
- [!20429](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20429) Add section to handbook explaining the process of requesting maintainer access to www-gitlab-com
- [!19438](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19438) corrected dates per feedback from a team member
- [!19388](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19388) Added "find a time" guidance in Tools and Tips
- [!19386](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19386) Modified Calendly guidance
- [!18417](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18417) Update "wider community" term in quiz

### 2019-03-30
- [!20841](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20841) Removed offer email
- [!20834](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20834) add meeting doc template per Sid
- [!20833](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20833) Update zoom recording section
- [!20824](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20824) Moved feedback workflow from Services to Support Workflows and added product feedback.

### 2019-03-29
- [!20829](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20829) Fix quality department roadmap and hiring vacancies
- [!20809](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20809) Avoid vague reasons that are actually opaque when making a change
- [!20796](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20796) Both public and private content
- [!20795](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20795) Update  hostname to localhost
- [!20780](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20780) Add PM responsibility allocation chart
- [!20777](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20777) Add Product Leadership Meeting to Product Handbook
- [!20763](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20763) Add instructions for archiving company call agenda
- [!20683](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20683) Add Portugal hiring info
- [!20561](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20561) Fixing a broken link to the Marketing Issues tracker.

### 2019-03-31
- [!20815](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20815) Add Month End Review

### 2019-03-28
- [!20787](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20787) update campaing type statuses
- [!20785](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20785) Update website handbook on gl-website group.
- [!20783](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20783) update kaiser deduction rates
- [!20774](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20774) Add pipeline minutes reset request to internal support page
- [!20768](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20768) Added content to handbook Greenhouse section regarding social referrals
- [!20767](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20767) Added a link and note to the lifecycle revamp issue.
- [!20761](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20761) Add note to clarify tuition reimbursement limits
- [!20751](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20751) updating engineering recruiter alignment
- [!20748](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20748) Add the Sendoso workflows to the handbook
- [!20742](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20742) Remove instruction re: not attending company call
- [!20727](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20727) A work in progress but updated the contract process to reflect no more offer...
- [!20696](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20696) Updates to Periscope Directory Page
- [!20692](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20692) Add product weekly meeting
- [!20669](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20669) New section for resetting CI minutes plus minor edits
- [!20571](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20571) Location factor charts
- [!20474](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20474) 2019 UK Benefits
- [!18500](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18500) Add Marketing Rapid Response Team Process to Marketing Handbook
- [!18216](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18216) Adjust Product KPIs

### 2019-03-27
- [!20724](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20724) Fix link to monitor stage
- [!20712](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20712) Added requested 10 day turnaround to Sec. Q process
- [!20711](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20711) Add guide about using discussions in issues
- [!20709](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20709) Encourage small comments in video call documents
- [!20705](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20705) add HB links to CEO shadow page
- [!20697](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20697) Add inclusiveness as part of the reason why we use the Q&A document for zoom call
- [!20686](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20686) replace rent index with location factor
- [!20684](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20684) Modify incident definition
- [!20677](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20677) Adding a ‘Company’ hiring chart
- [!20672](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20672) Sales comp page revert in finance handbook
- [!20668](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20668) Document daily stand up process for Data Team
- [!20651](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20651) Adding roles & responsibilities for FP&A team.
- [!20649](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20649) Update BI ref on feature implementation page
- [!20632](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20632) Resolve "Create Periscope Directory"
- [!20599](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20599) Add Distribution team Support Request to handbook
- [!20508](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20508) Support: Refresh feedback and complaints workflow
- [!20505](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20505) Support: Minor fixes/changes to confirmation email workflow
- [!20452](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20452) Be more proactive with issue progress/activity
- [!20445](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20445) Adding details for meetups speakers and improving TOC on Evangelist Program Handbook page

### 2019-03-26
- [!20660](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20660) Resolve "Update data page with Snowflake Permission Paradigm"
- [!20656](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20656) Update index.html.md
- [!20650](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20650) rmv deprecated tool
- [!20643](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20643) Update Infrastructure design docs links
- [!20638](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20638) Fix test plan links
- [!20634](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20634) Update CI to CI/CD tag
- [!20633](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20633) Remove duplicate links in Infrastructure design index.
- [!20631](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20631) Update index.html.md
- [!20614](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20614) fix table tag
- [!20606](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20606) Adjusting segment notes so that ^ For NYC is on Northeast and Great lakes and...
- [!20593](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20593) Update time to UTC and add marketing activity examples
- [!20568](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20568) Update source/handbook/marketing/marketing-sales-development/online-marketing/index.html.md
- [!20567](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20567) Include more useful links
- [!20566](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20566) Monthly calls update.
- [!20565](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20565) Note need to enable Waiting Room for Personal Meeting ID
- [!20523](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20523) Consolidate repack blueprints
- [!20390](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20390) Add a new section for Contribute for prize.
- [!20165](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20165) Add an SLO for Community Contribution First Response
- [!19915](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19915) Sponsored Billable Travel

### 2019-03-25
- [!20610](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20610) Update nomgov
- [!20602](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20602) add vp app dev
- [!20595](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20595) Update with information on Linux Laptops
- [!20587](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20587) Update email address to dmca@
- [!20577](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20577) Handbook update with notes for using Gitlab team accounts with accountless...
- [!20575](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20575) Resolve "Add Periscope as Data Team Visualization Tool"
- [!20377](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20377) Add a line about batteries and small repairs to ensure we stay frugal
- [!20375](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20375) Quality Engineering Roadmap
- [!20367](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20367) Clarify the release post manager scheduling

### 2019-03-23
- [!20554](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20554) Udpate mm cali map for consistency
- [!20552](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20552) OPS handbook Maintenance
- [!20551](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20551) Delete test landing page used to troubleshoot webex issue
- [!20549](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20549) Adding Image to Handbook for MM territory in CA
- [!20545](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20545) Expense reimbursement
- [!20328](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20328) Jj 102
- [!19891](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19891) Document the process for new stages, groups, categories

### 2019-03-22
- [!20541](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20541) Update monitor group name on Monitor page
- [!20539](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20539) Handbook: Add Slack check emoji use
- [!20535](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20535) Use UTC for release post
- [!20530](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20530) remove UTC
- [!20522](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20522) Transparency means saying why, not just what
- [!20516](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20516) Add updated Gross IACV defintion and fix true-up hyphens
- [!20512](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20512) Reinforce values
- [!20479](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20479) Add virtual background to tools and tips
- [!20469](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20469) Fix calendly link
- [!20461](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20461) Add focus on improvement to iteration value
- [!20456](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20456) Update handbook with Anniversary Swag information
- [!20414](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20414) Fix link to pages domain verification info in docs
- [!20378](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20378) Add clement ho manager readme

### 2019-03-21
- [!20513](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20513) Add snowflake warehouse and dbt config details
- [!20502](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20502) Update with auto-renewal process.
- [!20500](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20500) Add another plan change section to internal support page
- [!20493](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20493) cleanup of `design` directory
- [!20486](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20486) Update grammar under "Collaboration"
- [!20484](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20484) Devops_Maturity_Model-Not-Found
- [!20483](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20483) Resolve "Document why sharing a camera video calls are worse than desk ones"
- [!20480](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20480) revised-calculator-link
- [!20478](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20478) Fixed Typo
- [!20472](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20472) Mention slack themes in handbook (tools and tips)
- [!20463](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20463) Thrive -> strive
- [!20449](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20449) Clarify OTE split with adjustments
- [!20424](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20424) Move the adding section from Merch Ops to Workflows handbook

### 2019-03-20
- [!20455](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20455) adds instructions for seeing who is on-call for support
- [!20450](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20450) Add counts to maintainer assignments
- [!20438](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20438) Soften language of not re-opening issues
- [!20435](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20435) Add new security filter in blog
- [!20427](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20427) add correct video link to shadow page
- [!20423](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20423) Updating to reflect BE tech interview
- [!20421](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20421) Fix Plan playlist link
- [!20416](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20416) Jj organize
- [!20411](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20411) Avoid re-opening issues
- [!20409](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20409) Update source/handbook/ceo/index.html.md
- [!20408](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20408) Resolve "update python style guide"
- [!20384](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20384) Clarify temp contractor
- [!20247](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20247) Clarifying recommended equipment for tech writers
- [!19487](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19487) Information around who can change account ownership, to who and the email...

### 2019-03-19
- [!20392](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20392) Add links to incident and change management. Remove table for stable.
- [!20385](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20385) Small primitives
- [!20379](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20379) Add missing slash
- [!20376](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20376) Update source/handbook/tax/index.html.md
- [!20369](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20369) Add note that Retention can change because MRR can change.
- [!20368](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20368) Add notes on assignee vs approver for data team MRs
- [!20350](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20350) adding the python style guide for data team
- [!20346](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20346) Adding that MSP fees are not covered in BC, Canada
- [!20344](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20344) Adding the initial, simplified, CI/CD architecture diagram.
- [!20343](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20343) Updating interview training issue template urls
- [!20337](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20337) Fix typo (propietary -> proprietary)
- [!20305](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20305) Updating wording for female team member interview
- [!19929](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19929) Geo board and milestone process update

### 2019-03-18
- [!20364](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20364) Corrected links to point to Corporate Marketing project
- [!20363](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20363) Adding Matt Allen to Meltano
- [!20358](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20358) Clarify the expected amount of time in San Francisco for CEO shadow program
- [!20354](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20354) Resolve "continuous delivery for newsjacking"
- [!20352](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20352) Fix typo "CI/CE" -> "CI/CD" found during interview
- [!20345](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20345) add pricing links
- [!20341](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20341) Resolve "CEO shadow handbook additions"
- [!20335](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20335) Add integrated campaigns to Handbook
- [!20331](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20331) Updated the note on currency translation account (CTA)
- [!20316](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20316) Update QA debugging guidelines
- [!20307](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20307) Update recruiting Alignment
- [!20276](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20276) PubSec owned accounts clarification
- [!20158](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20158) Add the External Shopify orders section to the Handbook
- [!20084](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20084) DMCA Handoff from Support to Abuse
- [!19893](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19893) Updated a few small typos in the product handbook.
- [!19791](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19791) Remove trailing _ typo in eng handbook
- [!19618](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19618) Mona- You're quick review please.. trying to help SDRs get in front of leads that the SALs want to...

### 2019-03-16
- [!20324](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20324) Fix missing Oxford commas , punctuation, and capitalization.
- [!20321](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20321) Update the blog handbook on promoting integrations

### 2019-03-15
- [!20303](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20303) Specify where to find conversion rates for expenses in foreign currencies
- [!20298](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20298) Add interview guidelines
- [!20297](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20297) Fix identiation issue on Pajamas handbook page
- [!20295](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20295) Update Safeguard locations and process.
- [!20286](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20286) Update pricing page with assessment of geo based pricing suggestion
- [!20279](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20279) Remove skeleton
- [!20278](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20278) Update source/handbook/engineering/ux/index.html.md
- [!20277](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20277) Consolidate and update application security review process
- [!20069](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20069) Exec review of director+ promotions 3 months in advance
- [!19191](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19191) first run on CMOC checklist

### 2019-03-14
- [!20275](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20275) Updates #support slack channel(s) in engineering communication docs
- [!20271](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20271) Small merge requests
- [!20265](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20265) jburrows001 Added links to RACI chart page
- [!20261](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20261) add cloud native decks
- [!20252](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20252) Add DLV003 to the training listing
- [!20249](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20249) Update TP's pronouns
- [!20237](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20237) Update comms for unexpected or emergency situations
- [!20222](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20222) Add DLV002 recording link for Delivery team training session
- [!20218](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20218) Update interview training location
- [!20210](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20210) Update SM IM Process
- [!20207](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20207) jburrows001 Add RACI chart page and feedback links
- [!20192](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20192) Adding section to detail how we track our work
- [!20176](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20176) Clarify the release post manager for the Kickoff
- [!20155](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20155) Add calendar to Community Relations handbook page
- [!20148](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20148) Add recommendation to exclude default off features
- [!20132](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20132) Steps to debug QA pipeline failures
- [!20116](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20116) Adding calendar, office hours, and recording info to Evangelist Program Handbook page
- [!20073](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20073) Update deprecated slides link to closest match from https://about.gitlab.com/training/
- [!20072](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20072) Fix typos in WIR Podcast Workflows
- [!20040](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20040) Update source/handbook/communication/index.html.md to note that slack is for informal communication
- [!19999](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19999) Desired Split COLA
- [!19994](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19994) Document Compensation Questions
- [!19901](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19901) fixing some misspellings, formatting, and dead links
- [!19781](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19781) Add pajamas to handbook
- [!19702](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19702) Add Canceling orders section to the handbook
- [!19458](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19458) Fixing typo - Update index.html.md.erb
- [!19258](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19258) update to processing vacation requirements

### 2019-03-13
- [!20204](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20204) Update offer process to exclude offer email
- [!20202](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20202) Handbook: Minor updates to import workflow
- [!20200](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20200) Updated job notifications to match the current offer process and slack announcement
- [!20184](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20184) Resolve "Updates to the data team page"
- [!20181](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20181) Reorg release post scheduling
- [!20179](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20179) Fix release post scheduling table
- [!20171](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20171) Make sure there is a pure open source repo since people in...
- [!20146](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20146) Fix broken link to infrastructure blueprints
- [!20099](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20099) Support: update diagnose errors workflow
- [!19884](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19884) Adding link to team and stage labels
- [!19846](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19846) Update references from GitLab Summit to Contribute
- [!18657](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18657) Add a Slack access section in the handbook

### 2019-03-12
- [!20161](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20161) Update source/handbook/marketing/marketing-sales-development/field-marketing/index.html.md
- [!20160](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20160) jburrows001 Added remaining guidance docs to handbook
- [!20145](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20145) Consolidate Finance Handbook to appropriate Finance Functions
- [!20144](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20144) Link to blog post "Beware of private conversations"
- [!20143](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20143) Resolve "Remove broken "data analysis" link under Product"
- [!20142](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20142) Resolve "Updates to the Data Page"
- [!20141](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20141) docs: process if a vuln becomes irrelevant after triage
- [!20139](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20139) Avoid private messages on Slack
- [!20136](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20136) Update index.html.md
- [!20131](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20131) added tax section to Finance
- [!20121](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20121) Quality handbook - revise test planning and quarantine process
- [!20117](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20117) Add best practice of "whoever recorded it, uploads it" when adding meetings to YouTube
- [!19916](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19916) Corrections of typos and grammatical errors in handbook pages
- [!19911](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19911) Fixes and minor maintenance of team page
- [!19511](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19511) update how to report sick time
- [!18195](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18195) Updates to workflow from username to namespace

### 2019-03-11
- [!20118](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20118) jburrows001 Started adding guidance documentation to handbook 2019-03-11
- [!20109](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20109) updated the handbook linking to new issue.
- [!20107](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20107) Second tentative to fix broken link on social media guidelines page
- [!20106](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20106) FP&A update
- [!20105](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20105) Fix broken link on social media guidelines page
- [!20101](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20101) Explain when the top priority issue in the Create issue board can be skipped
- [!20100](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20100) Adding charts link to hiring overview page
- [!20093](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20093) Jj 102
- [!20090](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20090) Fix release post draft link
- [!20088](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20088) Jj 102
- [!20087](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20087) Dynamically generating hiring chart pages in new location
- [!20085](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20085) Fix wrong link for relocation
- [!20083](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20083) Fix links in the Quality handbook
- [!20076](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20076) fixed typo
- [!20075](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20075) fixed typos
- [!20071](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20071) Add Product Designer under Delaney, Dev Team Lead
- [!20070](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20070) Jj 102
- [!19995](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19995) Fixed Broken link to Gitlab Values page from...

### 2019-03-09
- [!20068](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20068) added short(er) version of new secure demo
- [!20067](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20067) upgraded Secure demo
- [!20061](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20061) Update source/handbook/board-meetings/index.html.md
- [!20060](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20060) Quality eng team pages
- [!20059](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20059) Add measurements and owners to Journeys + add Contribute to Buyers Journey
- [!20058](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20058) modify list view explanation
- [!20056](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20056) Update handbook confidentiality guidelines
- [!20054](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20054) Quality handbook update - polish department page with department members

### 2019-03-10
- [!20066](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20066) Add obligations for internal confidential information
- [!20065](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20065) Update the GitLab address in community/sweepstakes/ and CA Merchandise Handbook
- [!19596](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19596) Adding process of how to handle duplicate web directs.

### 2019-03-08
- [!20048](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20048) Update 'ops' dept name and link to exclude 'backend'
- [!20047](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20047) Update Development department with proper labels
- [!20046](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20046) Updating GC schedule
- [!20045](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20045) Move 'monitoring' to 'monitor' for team page
- [!20034](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20034) Organize recorded AMAs to encourage more empathy
- [!20028](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20028) Update index.html.md corrected sonarqube and added twistlock and aqua
- [!20027](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20027) Resolve "Update SA Handbook with working agreements template and renewed engagement details"
- [!20009](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20009) Financial information not public
- [!19998](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19998) Earnings Committee
- [!19997](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19997) Extend stable_counterparts to take another manager
- [!19992](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19992) Intro eng mgmt board
- [!19989](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19989) Add more details on broken master actionable steps
- [!19983](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19983) Fix broken link on 'git pages update' page
- [!19974](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19974) Replace the use of 'crew' with 'working groups'
- [!19952](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19952) Updated the legal page to include Requests for Insurance Certificates
- [!19868](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19868) Update UX Desginer onboarding page to include buddy template and process info
- [!18636](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18636) Engineering/infrastructure/blueprints/testing environments

### 2019-03-07
- [!19996](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19996) Resolve "Broken link to kickoff doc in product handbook page"
- [!19987](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19987) Update stock levels
- [!19965](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19965) Update internal trial request note
- [!19960](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19960) Update Frontend Themed Call section
- [!19902](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19902) update some recruiter/coordinator alignment
- [!19861](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19861) Updated that we send email & webinar invites regardless or not if an opp is in flight
- [!19726](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19726) Support Workflows: updates to ip-blocks guide
- [!19711](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19711) Update Group Conversation Schedule with new groups
- [!19707](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19707) Update onboarding-processes to new Swag Store process
- [!19447](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19447) Update source/handbook/engineering/infrastructure/production/index.html.md
- [!19205](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19205) Support Handbook: Use of Admin Notes Update
- [!19146](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19146) adds steps for admin imports

### 2019-03-05
- [!19886](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19886) More examples of public and not public
- [!19885](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19885) Update source/handbook/marketing/product-marketing/competitive/index.html.md
- [!19883](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19883) Update to add nom gov charter
- [!19882](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19882) Update source/handbook/support/index.html.md
- [!19876](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19876) Link to stock option grant levels for clarity
- [!19875](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19875) add a way to navigate to the sales resource page
- [!19873](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19873) Update PMM team
- [!19862](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19862) Add frontmatter to Support IM page
- [!19858](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19858) Update index.html.md with Support Incident Management link
- [!19857](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19857) First iteration of Incident Management process for Support
- [!19855](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19855) Team yml validation of start_date
- [!19843](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19843) Added Krisp to Tools
- [!19837](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19837) Added GitLab Executive Interview Scheduling Process
- [!19768](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19768) Updated Authorization Matrix to clarify vendor contracts approval requirements
- [!19731](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19731) Updates CI/CD team pages to reflect new names and accurately load members
- [!19703](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19703) Add the preprod environment
- [!19656](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19656) Update Availability Section
- [!19623](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19623) Security Team contact information
- [!19580](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19580) Production team on call update to include new manager rotation and DBREs
- [!19252](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19252) modify ownership rules

### 2019-03-06
- [!19860](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19860) Update the sourcing page
- [!19821](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19821) added training
- [!19792](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19792) Add Prioritization Consideration for Community Building
- [!19645](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19645) Guidance for converting ideas into MVCs quickly
- [!19566](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19566) Design: Required changes for automated scheduled deploys
- [!19522](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19522) Adds structure and a changes section to category maturity
- [!19358](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19358) Adding references for heuristic usage

### 2019-03-04
- [!19840](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19840) Fix typo of incluing
- [!19832](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19832) updated Dutch tax treatment of stock options
- [!19829](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19829) Update index.html.md to point to correct transgender non-discrimination link
- [!19828](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19828) Fix formatting for triage handbook
- [!19813](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19813) Fixing the german hiring process
- [!19812](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19812) Update removed duplicate entry
- [!19798](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19798) Fixed typo and adjusted spaces
- [!19663](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19663) add prometheus section to monitoring

### 2019-03-02
- [!19787](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19787) update label error & initials

### 2019-03-01
- [!19780](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19780) Issues should be non-confidential by default
- [!19776](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19776) recruiting metrics report monthly
- [!19774](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19774) Update index.html.md with just commit calendar
- [!19770](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19770) Why today
- [!19764](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19764) typo
- [!19762](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19762) Add unique values of our crispness between pm and eng
- [!19753](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19753) Update internal hiring process
- [!19750](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19750) Update approval for outside work
- [!19745](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19745) Removing Tinggly to avoid confusion, until we have chosen a new provider.
- [!19744](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19744) Quality - Triage Ops - Convert list items to headings for packages
- [!19727](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19727) GS 102
- [!19598](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19598) move personas from design system to handbook and change related links
- [!19590](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19590) Add ally resources to the handbook
- [!19348](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19348) Geo - Moving next gen information from doc to handbook
- [!19310](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19310) Added guidance on application review for interviewers
- [!19245](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19245) Update Lg & MM Tables
- [!18650](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18650) Updates to compensation plan

### 2019-02-28
- [!19730](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19730) use emoji reactions
- [!19724](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19724) Fix no toc for positive intent
- [!19714](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19714) Add thank you and best wishes
- [!19705](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19705) jburrows001 Fixed broken link in acceptable use
- [!19701](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19701) Clarify that decision tree is a guide
- [!19699](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19699) Clarifying that PTO Ninja now updates Bamboo HR.
- [!19693](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19693) Add form for new macbook orders
- [!19691](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19691) DCO update
- [!19685](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19685) Add CI/CD architecture page
- [!19680](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19680) spacing
- [!19658](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19658) Update repairs
- [!19616](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19616) Restructure quality dept okrs
- [!19588](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19588) Tweak trainee maintainer process
- [!19576](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19576) Add instructions for executing scripted changes
- [!19531](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19531) Make relevant engineering group aware of new test failures

### 2019-02-27
- [!19669](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19669) rmv looker
- [!19665](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19665) linked infra meetings from infram main
- [!19664](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19664) fix typo
- [!19662](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19662) Update index.html.md
- [!19661](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19661) Jj 102
- [!19655](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19655) Fix formatting of DQP page
- [!19653](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19653) typos
- [!19652](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19652) deleted page
- [!19651](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19651) spelling errors and job updates
- [!19649](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19649) Added Service Account Request to Access Management Process
- [!19647](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19647) candidate in interview stages that are deleted
- [!19640](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19640) Template helper for chart that visualizes headcount and vacancies by department
- [!19639](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19639) Add GCK to engineering productivity stewardship
- [!19636](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19636) Fixing the SDR Practice to what we actually do
- [!19633](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19633) update recruiter alignment
- [!19630](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19630) Adding directions to mark an event as a Featured Event
- [!19613](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19613) Remove mentions of our old BI tool
- [!19584](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19584) Update KPIs and Operating Metrics with Links and Placeholders
- [!19523](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19523) Iteration on Data Team issue management and prioritization
- [!19512](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19512) Fix misspelling of ‘dialogue’ in Update index.html.md
- [!19463](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19463) Jj 101

### 2019-02-26
- [!19619](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19619) updating documentation for features.yml details changes
- [!19607](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19607) Resolve "Update Customer Reference page in Handbook"
- [!19605](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19605) how to change group slack DMs to private channels
- [!19604](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19604) exported to imported
- [!19603](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19603) Update Frontend theme call for March
- [!19599](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19599) Include next.gitlab.com in the canary section
- [!19569](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19569) Give all images on about.gitlab.com alt values
- [!19530](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19530) Add more information regarding triage packages to the handbook.
- [!19401](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19401) Publish and explain our metrics in the handbook

### 2019-02-25
- [!19575](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19575) Measure to Framework for Manage
- [!19571](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19571) Update release day tasks
- [!19559](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19559) Point out that Create issue board should be filtered by milestone
- [!19551](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19551) Fix typo "an merge -> a merge"
- [!19547](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19547) Revised P5- MQL- Trial note
- [!19543](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19543) Added internal use only
- [!19490](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19490) Resolve "Remove everything about non-Delivery teams doing releases"
- [!19481](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19481) sendoso updates
- [!19380](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19380) Update @dennis' title again

### 2019-02-22
- [!19529](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19529) Update source/handbook/engineering/ux/index.html.md
- [!19519](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19519) Add google docs notifications tip
- [!19517](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19517) Summit to contribute
- [!19506](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19506) Adds "Assume Positive Intent" behavior to collaboration
- [!19502](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19502) Add PYB template and update handbook
- [!19494](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19494) Update supplies ordering.
- [!19489](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19489) add sec-ops
- [!19485](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19485) Add Shift-Command-5
- [!19483](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19483) Added a section for UI
- [!19480](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19480) Jburrows001 updated sec controls page
- [!19479](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19479) Fixing typo on index.html.md
- [!19475](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19475) Re-order operating metrics page
- [!19433](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19433) Add docs vs blog post instructions
- [!19429](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19429) Clarify UK pension contributions are gross
- [!19397](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19397) Update the evangelist merchandise workflow section of the handbook
- [!19392](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19392) Update evangelist merchandise operations section of the handbook
- [!19322](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19322) Update Interview Scheduling process for executives
- [!19149](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19149) Adding headcount DRI
- [!19118](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19118) Add experience factor to salary update
- [!18999](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18999) Add info on providing large files
- [!18270](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18270) Design Document for GitLab Service Inventory Catalogue. Reference:...

### 2019-02-21
- [!19477](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19477) fixing links
- [!19474](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19474) fixing typo
- [!19471](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19471) Improved estimation issue example for Manage
- [!19465](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19465) Update index.html.md
- [!19462](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19462) Add examples to Customer counts
- [!19457](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19457) Fix bad HTML on operating metrics page
- [!19443](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19443) Change URLs to 'sponsorship-request' template
- [!19440](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19440) Explain hierarchy design
- [!19439](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19439) Update index.html.md revised whitepaper for secure
- [!19436](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19436) Added "advocate for a day" workflow to handbook
- [!19427](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19427) XDR requested updated
- [!19413](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19413) Add email review protocol for 1000+ target list
- [!19405](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19405) Add troubleshooting section to support workflows
- [!19327](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19327) Clarify remote vs distributed
- [!19309](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19309) Update index.html.md to correctly align services support roles
- [!19223](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19223) Introducing panel format for kickoff meeting
- [!19138](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19138) Discuss with Security Team before changing priority for sec issues
- [!19000](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19000) Adding evaluation criteria for community events
- [!18914](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18914) Resolve "Increase specificity of how we count subscription in Operating Metrics"

### 2019-02-20
- [!19398](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19398) ZFS Blueprint
- [!19395](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19395) update to date
- [!19391](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19391) Update onboarding for deployer
- [!19382](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19382) Simplify acquisition process
- [!19378](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19378) Add note about making sure your pipelines pass if you commit direct to master
- [!19374](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19374) Fix typo

### 2019-02-19
- [!19364](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19364) bdr updates & test
- [!19363](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19363) New bullet under transparency value
- [!19361](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19361) updated the timeline for the reviews
- [!19360](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19360) Adding Zoom webinars link to EA page
- [!19352](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19352) add "competitive" as a reason to not be public by default
- [!19338](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19338) Update chat channel info
- [!19333](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19333) Update source/handbook/support/index.html.md
- [!19332](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19332) Update index.html.md
- [!19330](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19330) Update index.html.md
- [!19320](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19320) Update source/handbook/ceo/pricing/index.html.md
- [!19316](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19316) Add serial form
- [!19303](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19303) Adding pre-fix bounty review process.
- [!19257](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19257) remove TriNet from sick time
- [!19212](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19212) Add OKR section
- [!19063](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19063) Add in TAM top 10 process MVC
- [!18846](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18846) Clarify the scope of 'outside projects' that require prior approval
- [!18720](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18720) Guidance for link unfurling in Slack.
- [!17796](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17796) Fix typo under "Equal Employment Laws" section of Code of Conduct

### 2019-02-18
- [!19317](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19317) Lbrown link doc & presentation to group conversations

### 2019-02-17
- [!19314](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19314) fixed typo in basics section
- [!19270](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19270) A little Oxford comma humor

### 2019-02-15
- [!19297](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19297) update last name
- [!19295](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19295) rmv fmr tmmbr + new sdr pair
- [!19285](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19285) Mentions instead of assign
- [!19281](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19281) Jj salespage2
- [!19280](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19280) Add Zendesk light agent info
- [!19276](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19276) Encourage people to use merge requests even if merging themselves
- [!19273](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19273) fixing format error
- [!19267](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19267) Updating Sales Resource Page
- [!19264](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19264) Add label links to sensing mechanisms section
- [!19261](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19261) update campaign type w/status progs
- [!19254](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19254) Add ci/cd resources
- [!19225](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19225) Changing the product handbook
- [!19166](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19166) Explain why focusing on a single instance is better

### 2019-03-24
- [!19293](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19293) Updated Frontend Plan and Create pages to correct manager roles

### 2019-02-14
- [!19243](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19243) Fix broken links to backend engineering management
- [!19237](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19237) Rename instance monitoring to self-monitoring
- [!19230](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19230) updating FY2020 function & cost center changes
- [!19220](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19220) Update broken link
- [!19216](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19216) Adding link to the 'Rebuild in GitLab' label view to dogfooding section of PM handbook
- [!19188](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19188) Update Internal Department Transfers to not be contradicting.
- [!19153](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19153) Modifying goodbye procedure for feasibility
- [!19150](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19150) Closing thought 1-3 feature highlight
- [!19122](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19122) Added a how to read this data section for Throughput
- [!19049](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19049) Jj sales resource page
- [!18973](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18973) Remove "above and beyond" from discretionary bonus criteria language

### 2019-02-13
- [!19195](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19195) Regrouping thoughts in the Group Conversations section
- [!19184](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19184) Update EA support
- [!19183](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19183) Add clarification on non-traditional coworking spaces
- [!19179](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19179) Examples: ETL update and Looker update
- [!19178](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19178) Change header size and colors from handbook/values
- [!19177](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19177) Clarify filename format
- [!19169](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19169) Updating territories and assignments
- [!19160](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19160) Adjusting Sales segment on Lead to match SFDC rules
- [!19158](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19158) Update source/handbook/finance/prepaid-expense-policy/index.html.md
- [!19155](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19155) values fit
- [!19151](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19151) adding additional kpi to kpi page and updating investor update process
- [!18926](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18926) Add traveling to relocation
- [!18878](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18878) How to evaluate and assign community events
- [!18820](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18820) few typo edits
- [!18716](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18716) Fixed formatting issue in Mac Tips
- [!18609](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18609) fix a typo
- [!18549](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18549) Minor edits on interviewing page
- [!18226](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18226) Fix broken Looker links on Feature Instrumentation page
- [!17793](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17793) Bug: Fix outstanding broken links for all-remote page
- [!17722](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17722) Added USB-C hub for New MBPs

### 2019-02-12
- [!19143](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19143) Add 30 days Cash Collections escalation email to AE
- [!19141](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19141) Update index.html.md Handbook Equipment Example update w/ new monitor suggestion
- [!19136](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19136) How to verify that Canary is enabled on GitLab.com
- [!19130](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19130) Add workflow for CA suspended tickets
- [!19128](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19128) Update How-to-be-a-FRT-Hawk.html.md add view sort info
- [!19111](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19111) Update UX support rep
- [!19014](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19014) Handbook customer success tam 70
- [!18982](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18982) Quality dept guidelines
- [!18790](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18790) Blueprint for taking CI/CD oncall in SRE
- [!18277](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18277) Blueprint+Design: Database bloat analysis + pg_repack

### 2019-02-11
- [!19108](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19108) Jburrows001 update security awareness training
- [!19096](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19096) Update link to dormant namespace requests.
- [!19095](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19095) Fix broken and missing design docs links
- [!19094](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19094) Updated primary contact
- [!19092](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19092) Fix broken link to design document
- [!19090](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19090) Update CXC Canada duration
- [!19087](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19087) Clarify dogfooding process
- [!19085](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19085) rmv emoji tags
- [!19064](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19064) DPA standard terms
- [!18983](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18983) Add blog calendar to handbook
- [!18941](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18941) Add instance monitoring category epic to other monitor functionality
- [!18891](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18891) Update source/handbook/ea/index.html.md
- [!18851](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18851) Update index.html.md
- [!18803](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18803) Emphasize iteration and issues when outlining category vision
- [!18648](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18648) Add specific example for importing repos from GitHub.
- [!18546](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18546) Remove upgrade barometer section from the release blog posts
- [!18474](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18474) Added notes on estimation

### 2019-02-09
- [!19082](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19082) add back a necesary label
- [!19081](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19081) modify mktgops section w updated info
- [!19039](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19039) add new campaign channel types
- [!19013](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19013) Update source/handbook/finance/prepaid-expense-policy/index.html.md
- [!18961](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18961) internal dmca request workflow

### 2019-02-08
- [!19059](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19059) Update broken link to infra team slack channels
- [!19056](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19056) Updates to Kickoff Process and Adds Closing Reminder
- [!19054](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19054) Support workflow update for W9 and DPA
- [!19047](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19047) create email templates in greenhouse
- [!19022](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19022) new changes to how we eval corp events
- [!19007](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19007) quick typo and format updates
- [!18963](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18963) Add more detail to Zuora Subscription management
- [!18919](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18919) Update index.html.md
- [!18128](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18128) Update evangelist program page section on content contributions

### 2019-02-07
- [!18997](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18997) Please do not use the GitLab logo as your personal avatar on social
- [!18996](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18996) Updated Ux Research Processes
- [!18989](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18989) Clarifying the mobile policy
- [!18988](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18988) Add CD product competitors table
- [!18984](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18984) Remove secure onboarding doc
- [!18978](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18978) Revert "Update index.html.md"
- [!18977](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18977) Submitting weekly forecasts
- [!18976](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18976) add ryan rmv michael
- [!18975](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18975) Add Headers and Anchors to Behavior Lists in Values
- [!18960](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18960) Update HackerOne process
- [!18952](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18952) Remove search functionality from Plan
- [!18897](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18897) Update engineering workflow

### 2019-02-06
- [!18967](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18967) update spelling
- [!18957](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18957) Update source/handbook/ceo/shadow/index.html.md
- [!18956](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18956) Add 2/6/19 training
- [!18954](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18954) Update setting_ticket_priority.html.md
- [!18953](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18953) Fix block quotes
- [!18951](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18951) Update recruiter alignment
- [!18949](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18949) Updates to CI and CD details
- [!18947](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18947) Resolve "Make clearer that dbt bullet points are coming soon on data team page"
- [!18946](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18946) Clarify gitlab VPS info
- [!18938](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18938) Update internal-support/index.html.md
- [!18935](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18935) draft of new corp events goals
- [!18934](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18934) Add yearly clarifier to 401k match
- [!18933](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18933) Jj itgroups2
- [!18930](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18930) Twitter is a publishing channel
- [!18922](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18922) Update source/handbook/incentives/index.html.md
- [!18921](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18921) Update index.html.md
- [!18917](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18917) Updates to territory AEs.
- [!18913](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18913) Update CEO Phishing Instructions
- [!18910](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18910) Explain grant options acceptance
- [!18901](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18901) Security release development design
- [!18899](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18899) Handbook update for Geo team issue weights
- [!18889](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18889) Add process for milestone cleanup
- [!18888](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18888) updating intro to engr metrics and adding links
- [!18873](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18873) Add updating Kickoff doc to timeline
- [!18771](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18771) Update SAO Criteria
- [!18746](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18746) Update sub-departments, groups, and categories

### 2019-02-05
- [!18937](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18937) adds note to communicate ticket merge to customer
- [!18928](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18928) No longer require a google doc for discretionary bonuses
- [!18915](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18915) Add link to new videos in Slack
- [!18908](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18908) Adding a new setup guide to tools
- [!18904](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18904) Update index.html.md
- [!18902](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18902) Document tax exemption process
- [!18890](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18890) Clarify project not product
- [!18883](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18883) Update source/handbook/marketing/product-marketing/index.html.md
- [!18882](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18882) Jburrows001 fix sec controls format
- [!18875](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18875) Add MRR change documentation
- [!18866](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18866) Add 200% limit for ramping SDR quota (last MR was just for BDR quota)
- [!18841](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18841) Removed Andrew and added Brandy to the EMEA territory table
- [!18837](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18837) Add trademark link
- [!18827](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18827) Add customer discovery and sharing to customer meetings for Product handbook
- [!18787](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18787) link to shared gdoc for incident and close RCA on end of discussion
- [!18781](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18781) add qualifies on what is inclusive language
- [!18705](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18705) Fixed Manage dev-backend stable_counterparts issue caused by my title change
- [!18666](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18666) Prioritize Internal Requests at 10x Customer Request
- [!18568](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18568) Tech Writing handbook updates including FY20 Vision and main epic links

### 2019-02-04
- [!18879](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18879) Update source/handbook/engineering/security/index.html.md,...
- [!18870](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18870) Update source/handbook/marketing/corporate-marketing/index.html.md
- [!18869](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18869) Change order
- [!18864](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18864) Fix FE Team pages for Secure, Monitor and Configure
- [!18860](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18860) Remove Move Calc
- [!18859](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18859) how to apply to internal job board plus help article
- [!18858](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18858) update pto ninja link
- [!18843](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18843) adjusting language around word tribe to IT group
- [!18839](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18839) Small improvements inside 360 feedback handbook page
- [!18819](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18819) new hires on interview teams
- [!18815](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18815) fixed broken link on cs salesforce page
- [!18774](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18774) Global Optimization of critical issues process addition
- [!18761](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18761) Update Content Hack Day hb
- [!18664](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18664) Update Kibana.html.md
- [!18577](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18577) creating internal support quick reference
- [!18494](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18494) Add a design document for merging CE and EE codebases
- [!18083](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18083) Small improvements inside compensation page

### 2019-02-03
- [!18852](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18852) Gerir/infra/design/okr
- [!18848](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18848) Update SAO criteria and definition
- [!18811](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18811) Fix "definition of done" link to go directly to description.

### 2019-02-01
- [!18833](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18833) PTO Ninja
- [!18832](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18832) Edit Support Handbook's documentation-related text and add links to docs policies
- [!18829](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18829) Add language around renewal gaps for subscriptions
- [!18828](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18828) how to apply as internal applicant
- [!18825](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18825) bgc process same time as references
- [!18824](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18824) added additional language that is recommended under the NY guidelines.
- [!18821](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18821) Add promotion & transfer process for internal applicants
- [!18813](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18813) Update sheetload documentation
- [!18810](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18810) Link to more useful issue boards from Create team page
- [!18806](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18806) Add SFDC metrics for EDU and OSS programs
- [!18802](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18802) Update source/handbook/marketing/marketing-sales-development/field-marketing/index.html.md
- [!18794](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18794) Update #infrastructure Slack channel is now #production
- [!18778](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18778) Add fast boot handbook page
- [!18758](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18758) Reorganize Product Handbook
- [!18731](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18731) Don't force people to attend meetings or pay attention to them.
- [!18715](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18715) Update Create 'What to work on' process
- [!18714](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18714) Mind the oxford comma
- [!18685](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18685) Clarify breadth over depth

### 2019-02-02
- [!18822](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18822) refining blueprints
- [!18701](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18701) add details related to BDR priority view process

### 2019-01-31
- [!18786](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18786) adding Area Sales Manager role to stock
- [!18783](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18783) Remove Reference to TriNet
- [!18777](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18777) Add in criteria for the issue template description, and provide details on milestone and due date.
- [!18773](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18773) Add CXC Canada doc link
- [!18772](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18772) clarify recruiting tasks for vacancies and add internal job board instructions
- [!18770](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18770) Upload New File for handbook
- [!18766](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18766) Quick update to 360 updates
- [!18750](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18750) #movingtogitlab
- [!18744](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18744) jburrows001 Fixed format of the security compliance controls page
- [!18743](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18743) Enablement live stream
- [!18729](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18729) Fixed two small typos
- [!18728](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18728) Add announcing new job openings to current team on Slack
- [!18718](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18718) Clarify two way door decisions

### 2019-01-30
- [!18737](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18737) Link from Discretionary Bonus Page to Process
- [!18736](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18736) Update index.html.md
- [!18733](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18733) jburrows001 Add controls page and linked it in the security handbook
- [!18727](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18727) Added two new demo videos
- [!18723](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18723) Add CI/CD primer back to page
- [!18722](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18722) updated 360 language
- [!18719](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18719) Jj tribes2
- [!18712](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18712) add link to connect slack and greenhouse
- [!18708](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18708) Update EDU workflow to make sure quotes are received
- [!18699](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18699) Standardize on Likely Buyer terminology
- [!18692](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18692) Updating emergency chat instructions to match the newest version of Zoom
- [!18689](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18689) adds emergency chat protocol
- [!18668](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18668) Update Distribution and Delivery team tables
- [!18634](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18634) Update source/handbook/customer-success/tam/index.html.md

### 2019-01-29
- [!18683](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18683) Adjustments to Comp Page, currency conversion
- [!18680](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18680) how to link greenhouse users to their profiles
- [!18679](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18679) Update source/handbook/marketing/product-marketing/analyst-relations/index.html.md
- [!18673](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18673) Updated Buyer to Primary Buyer and fixed link
- [!18672](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18672) Update source/handbook/marketing/marketing-sales-development/field-marketing/index.html.md
- [!18670](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18670) Exaplain how to request Greenhouse access
- [!18656](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18656) Update source/handbook/tools-and-tips/index.html.md
- [!18644](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18644) Adding method to post a goodbye message upon termination
- [!18642](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18642) remove ellinger from territory chart
- [!18635](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18635) what to do if a candidate reapplies
- [!18633](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18633) Add weight guidelines to Geo team handbook
- [!18581](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18581) Update confidential youtube videos setting in handbook
- [!18564](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18564) Link to categories explainer
- [!18542](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18542) Fix the order of interview steps
- [!18528](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18528) Remove post-MVC epics and mention epic relations
- [!18383](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18383) Adding note about simplicity, and link to Robert C. Martin's SOLID principles
- [!18320](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18320) Make page simpler and shorter.
- [!18283](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18283) additional language to promotions- managers not informing team member until...
- [!18153](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18153) Updated referral language
- [!18064](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18064) Update onboarding template names

### 2019-01-28
- [!18621](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18621) greenhouse access requests
- [!18619](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18619) Update source/handbook/marketing/marketing-sales-development/field-marketing/index.html.md
- [!18612](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18612) Fix YouTube link for 360 culture amp
- [!18604](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18604) Change Distribution EM
- [!18603](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18603) add pops analyst to job appproval flow for comp
- [!18593](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18593) Explain canary environment and update for opt-out
- [!18585](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18585) Resolve "Adding Customer Reference Board to CRP handbook page"
- [!18584](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18584) Update source/handbook/marketing/product-marketing/customer-reference-program/index.html.md
- [!18578](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18578) corrected  few typos and added the new video link to be uploaded as youtube
- [!18545](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18545) Update Recruiter Alignment
- [!18544](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18544) Add executive level hiring page
- [!18532](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18532) Updated "Involving Experts Workflow"
- [!18335](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18335) Capture our "Product Structure over Group Capacity" notion in Handbook
- [!18271](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18271) Dutch law changed since Jan 1st - we now have five days of paternity leave instead of two
- [!18209](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18209) Changed Art Nasser name change
- [!18063](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18063) Remove outdated reference to scheduling slack channel

### 2019-01-26
- [!18580](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18580) rmv terminus from tech stack
- [!18572](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18572) Fix minor typo
- [!18495](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18495) Add missing space after comma (,) on index.html.md
- [!17933](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17933) Add 200% ceiling for ramping SDRs

### 2019-01-25
- [!18567](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18567) add key messages to just commit framework
- [!18565](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18565) Added MOM Motto, "the mom mot"
- [!18555](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18555) grammar edits to contracts page, add ping to update
- [!18551](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18551) 360 feedback
- [!18548](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18548) interviewers can't view closed jobs
- [!18543](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18543) Add workflow for users over license (EDU and OSS programs)
- [!18533](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18533) add updated intn bc info
- [!18531](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18531) add repairs + loaner sections
- [!18520](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18520) Fix Categories Maturity milestone from Summit to May 2019
- [!18511](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18511) Updated Quality dept links and career path
- [!18507](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18507) Update boards to new product marketing project
- [!18337](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18337) Add increased emphasis to dogfooding
- [!18258](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18258) Add documentation on how zuora subscriptions are linked

### 2019-01-24
- [!18529](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18529) updated design index for OKRs
- [!18526](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18526) OKR design: first iteration
- [!18506](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18506) additional language around managers communicating a promo
- [!18488](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18488) first iteration on Infra meetings
- [!18482](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18482) Link secruity blueprint page and Delivery training page to their index pages
- [!18481](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18481) Actually link to the calculator's home page
- [!18479](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18479) workload workflow: blueprints and designs
- [!18478](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18478) Add a page for Delivery team trainings
- [!18476](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18476) Update entities and locations
- [!18470](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18470) Update index.html.md with integrated campaign and updates to event processes for MPMs
- [!18462](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18462) updates to reliability workload workflow
- [!18452](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18452) - 360 Section
- [!18426](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18426) Define a logo request workflow in CA handbook
- [!18401](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18401) updated timeline for 360 survey
- [!18334](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18334) Emphasize when to close an issue and why
- [!18261](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18261) Clarify process on broken e2e test that will be introduced to master
- [!18245](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18245) Security Release Blueprint: Current situation and path forward

### 2019-01-23
- [!18446](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18446) updated DMP page to document keyword volume usage
- [!18444](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18444) Add clarification around Fiscal dates for GitLab and Looker
- [!18439](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18439) Adding event decision tree to marketing handbook pages
- [!18438](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18438) director notes: workload management, initial draft
- [!18435](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18435) Update Community Relations workflows and guidelines handbook pages to remove .html extension
- [!18429](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18429) Replaced request to contact a specific person
- [!18428](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18428) offers slack channel
- [!18423](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18423) Update involving experts links and copy
- [!18398](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18398) Updated CEO scam section for SSOT
- [!18346](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18346) Add Kibana slide deck
- [!18294](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18294) Contract type factor to contract factor to simplify things.
- [!18123](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18123) Groups for Manage

### 2019-01-22
- [!18403](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18403) Jj refine trbs
- [!18395](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18395) Update index.html.md
- [!18394](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18394) Fix link to "How to add events to the events page"
- [!18393](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18393) Updated security handbook for CEO fraud instructions
- [!18392](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18392) Updated old ceo scam link
- [!18389](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18389) clarification on initials and note about future alignments as we grow
- [!18387](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18387) Add missing word to the internet subscription expense
- [!18384](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18384) Resolve "Add Looker workflow overview"
- [!18378](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18378) handbook updates documenting form formatting
- [!18358](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18358) Support: Add note on merging tickets
- [!18354](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18354) Add to PR section of hb
- [!18351](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18351) Fix get-help link in distribution handbook
- [!18348](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18348) Clarifies Gold incentive availability and links directly to issuable template.
- [!18313](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18313) Get Fancy on Data Team Page
- [!18310](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18310) adding language with manager skills requirements
- [!18297](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18297) Add Looker video to the handbook page
- [!18265](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18265) Added churn definition to operating metrics

### 2019-01-21
- [!18344](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18344) Added missing frontmatter to Yorick's README
- [!18339](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18339) Deleted deprecated page about finding tickets in ZD
- [!18338](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18338) clarify git to gitlab
- [!18333](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18333) Update Education and Open Source program handbook pages
- [!18331](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18331) Clean up format
- [!18330](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18330) Fixed link, added templates, and cleaned up text
- [!17940](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17940) Added a personal employee README

### 2019-01-20
- [!18323](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18323) Revamp legal page and add Attorney-Client privilege content
- [!18322](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18322) Update Conga process
- [!18217](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18217) International payroll process

### 2019-01-19
- [!18319](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18319) update form number
- [!18311](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18311) add content for testing
- [!18309](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18309) Add new directory

### 2019-01-18
- [!18293](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18293) Add information regarding the Beamy.
- [!18292](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18292) Greenh 118
- [!18291](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18291) add SA Manager stock options level
- [!18281](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18281) Add link to CFP Submission template
- [!18263](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18263) Preference for product categories which map to vendor categories from analysts
- [!18256](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18256) Add guidance for title length on release post
- [!18248](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18248) Additional 401(k) Match Details
- [!18147](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18147) Expanded awareness training description
- [!18021](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18021) Changing the way to do compensation for current TM.
- [!17772](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17772) Typo correction, "assests" should be "assets".

### 2019-01-17
- [!18229](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18229) Update package specialty
- [!18222](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18222) update backup snapshot details to be current re: GCP
- [!18213](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18213) For the handbook, some tips on using the phone with Zoom
- [!18206](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18206) Fix categories typo
- [!18197](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18197) Added link to BitBar and GitLab Plugin
- [!18191](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18191) Add Target audience and experience to category epic template
- [!18163](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18163) Resolve "Update CS Handbook for POC"
- [!18122](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18122) Fix link to setup NextTravel account
- [!18050](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18050) Include persona labeling information towards PM and UX handbooks
- [!18010](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18010) Prioritization of Broken master and Tests
- [!17955](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17955) Add documentation for distribution usage of dependencies.io

### 2019-01-16
- [!18200](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18200) Move Gitter to Create
- [!18199](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18199) Revert "Merge branch 'brendan-add-amazing' into 'master'"
- [!18196](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18196) Remove "start on Monday" recommendation in interview process
- [!18192](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18192) Add amazing as a maturity level
- [!18187](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18187) Add frontend call theme for February
- [!18186](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18186) Add fulfilment to @dennis title
- [!18170](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18170) Start any day -
- [!18168](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18168) Add instructions for categories
- [!18117](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18117) Adopt label renaming from ~"feature proposal" to ~feature
- [!18087](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18087) Changed team name: Sync to Fulfillment
- [!18076](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18076) Remove deprecated workflow
- [!18002](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18002) Add additional Sketch admin
- [!17824](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17824) Update distribution infrastructure maintenance: SSH public keys
- [!17742](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17742) Fold country factor into location factor
- [!17719](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17719) updating team's focus to how we work and current / oncall notes about triage

### 2019-01-15
- [!18167](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18167) Update competitive assessment sensing mechanism to include reference to missing features
- [!18162](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18162) Update index.html.md
- [!18160](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18160) Patch 10236
- [!18137](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18137) Remove SMAU tracking
- [!18118](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18118) Add maintainer review to Distribution Triage handbook entry
- [!18102](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18102) add 401(k) match
- [!18074](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18074) Update CA knowledge base
- [!18037](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18037) Any change request that involves running a script must have a dry-run...
- [!18028](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18028) Updated Customer At Risk (Emergency Room) documentation
- [!17982](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17982) Jj add tribes
- [!17977](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17977) Add lost instances definition to operating metrics
- [!17928](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17928) Add tech onboarding for secure team
- [!17901](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17901) Fix broken gitlab link on onboarding issue tasks page

### 2019-01-14
- [!18126](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18126) Further updates to Territory Assignment tables.
- [!18120](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18120) Update index.html.md with some updates
- [!18115](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18115) Add link to Serverless from ops-backend/index
- [!18110](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18110) Change milestone start/end date
- [!18107](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18107) Remove SDR and BDR Lead, this no longer exists
- [!18104](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18104) expense report due date
- [!18046](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18046) Fix some broken links to MR coach
- [!17858](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17858) update emea territory table
- [!17829](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17829) Add dogfooding to Serverless team mission

### 2019-01-13
- [!18092](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18092) Fix typo: s/BitBucket/Bitbucket/g
- [!18086](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18086) Fix links to guides on issues and MRs
- [!18085](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18085) Fixed an outdated feature flags documentation link in the Engineering workflow
- [!18033](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18033) Refine enablement

### 2019-01-12
- [!18081](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18081) Add links to OKR and Epic Roadmap for Data Team

### 2019-01-11
- [!18075](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18075) Update Wave/MQ section with MP suggestion
- [!18066](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18066) Fix link name for Terraform Automation design doc
- [!18060](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18060) Updates for new Data Team group
- [!18052](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18052) handbook: align corporate marketing ordered lists to styleguide
- [!18047](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18047) bonus process changed to CCO
- [!18039](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18039) Broken master issues need to be worked on
- [!18031](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18031) Update index.html.md
- [!18026](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18026) Technical Account Management Handbook Updates
- [!18023](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18023) Update wording on wave/mq section
- [!18022](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18022) Add new section on AR page responding to Waves and MQs
- [!18020](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18020) Absorb non-development info from the Looker project README
- [!18015](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18015) Google search console malware process
- [!18013](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18013) Language regarding terminating a people manager and informing the team before...
- [!18009](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18009) Fixing broken link on spending-company-money page
- [!18008](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18008) point severity doc link to new location
- [!17995](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17995) Add new FRT Hawk File
- [!17941](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17941) add milestone framework to marketing process
- [!17842](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17842) Add design for deployer

### 2019-01-10
- [!18004](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18004) Correct links to livestream section of the handbook
- [!18000](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18000) Update company policy on conference speaker travel
- [!17994](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17994) Add double-check to ensure Vacancy posted correctly
- [!17985](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17985) Add Chart Operator training to Distribution team training page.
- [!17984](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17984) Jarv/instance groups
- [!17976](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17976) Update to #recruiting channel
- [!17969](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17969) Fix broken link in Engineering Career Development
- [!17966](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17966) Fix link to Create team retrospectives
- [!17822](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17822) Add Casey Allen Shobe to team page

### 2019-01-09
- [!17954](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17954) Updated for comp table and "partially" definition
- [!17944](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17944) Update index.html.md to clarify AMER Tues Crush session -> Casual
- [!17943](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17943) Clean up cm handbook
- [!17942](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17942) Added sec-ops-team and sec-compliance-team tags to the Slack Channels section
- [!17938](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17938) update url for xdr project
- [!17936](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17936) Update DMCA doc
- [!17931](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17931) add board for xdr project
- [!17925](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17925) Updated tech stack for UX team
- [!17924](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17924) Update CHD dates
- [!17922](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17922) infra structure 3rd iter
- [!17920](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17920) add commuter benefits
- [!17917](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17917) More clearly define MRR definition
- [!17903](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17903) fix redirect
- [!17773](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17773) Add Sensing Mechanisms to Product Handbook

### 2019-01-08
- [!17906](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17906) SRE team is now Reliability Engineering
- [!17905](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17905) add links to individual projects
- [!17893](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17893) Update index.html.md
- [!17888](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17888) Fix typos on Marketing Handbook page.
- [!17887](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17887) Update email alias list to include analyst relations
- [!17886](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17886) Update André Luís's job title
- [!17881](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17881) Fix one rogue link
- [!17877](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17877) update recruiter/coordinator alignment
- [!17875](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17875) Add post kickoff direction items
- [!17870](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17870) Make 1Password TOTP guide to be up-to-date
- [!17866](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17866) Adding quote coverage definition.
- [!17859](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17859) Update index.html.md
- [!17818](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17818) Dormant Username Policy - Minor Updates
- [!17771](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17771) Add campaign reporting to MPM section
- [!17701](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17701) Adding approval of work-related travel

### 2019-01-07
- [!17846](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17846) Resolve "Update data team looker repos links"
- [!17841](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17841) Updated link to the Responsible Disclosure Policy on the security page
- [!17810](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17810) updates to website in handbook with new CTA guidelines
- [!17809](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17809) doc: Correct link on Engineering Workflow doc
- [!17797](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17797) Fixed code review link
- [!17781](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17781) Extract Serverless to separate team page for ops-backend

### 2019-01-05
- [!17817](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17817) Shorten title for better url

### 2019-01-06
- [!17812](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17812) Update compliance goals in the handbook
- [!17798](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17798) Add anti-mining entry to the Internal Acceptable Use Policy

### 2019-01-04
- [!17808](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17808) fix territory tabls
- [!17792](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17792) Fix broken link typo
- [!17788](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17788) Update source/handbook/communication/index.html.md
- [!17778](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17778) Add label definition
- [!17748](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17748) Add Tech Stack Support Heirarchy

### 2019-01-03
- [!17758](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17758) Moved release doc from Quality dept page to release docs
- [!17753](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17753) Update index.html.md - New W-9 2019
- [!17749](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17749) Add source for exchange rate for 2018 Annual review.
- [!17744](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17744) Change 2FA Workflow
- [!17738](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17738) adding some language about utilizing the travel grant and obtaining manager approval first.
- [!17735](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17735) Fix frontend monitor team members page
- [!17728](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17728) Resolve "Update extractor information in the handbook (data team)"
- [!17726](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17726) Resolve "Update data team pointing"

### 2019-01-02
- [!17730](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17730) doc: Fix typo in people ops CoC
- [!17727](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17727) doc: Fix markdown formatting for people-ops CoC
- [!17725](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17725) doc: Fix typo in anti-harassment page
- [!17721](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17721) Added links to SMAU dash
- [!17720](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17720) remove salary source
- [!17717](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17717) doc: Remove outdated Slack Cert mention from tools
- [!17715](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17715) Update index.html.md, added possessive 's to legal language.

### 2019-01-01
- [!17702](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17702) Adding a note about interim responsibilities