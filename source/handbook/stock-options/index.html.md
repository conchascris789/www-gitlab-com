---
layout: markdown_page
title: "Stock Options"
---

----

## On this page
{:.no_toc}

- TOC
{:toc}

----

## About Your Ownership in GitLab

At GitLab we strongly believe in employee ownership in our Company. We are in business
to create value for our shareholders and we want our employees to benefit from that shared success.

*In this [document](https://docs.google.com/document/d/1tS-HBzy9kYH1tIGPV3ivilyrmWAGPeqowV45m1zkxuQ/edit) (only accessible to GitLab team-members and candidates),
you can find some more details on the number of shares outstanding and the most recent valuations.*

This guide is meant to help you understand the piece of GitLab that you’re going to own!
Its goal is to be more straightforward than the full GitLab 2015 Equity Incentive Plan
(the “2015 Equity Plan”) and your stock option agreement which you are advised to read,
which both go into the full legal details.  Please note, however, that while we hope that
this guide is helpful to understanding the stock options and/or stock issued to you under
the 2015 Equity Plan, the governing terms and conditions are contained in the 2015 Equity
Plan and the related stock option agreement. You should consult an employment attorney
and/or a tax advisor if you have any questions about navigating your stock options and
before you make important decisions.

Three things must happen for your stock options to be meaningful:

1. You must vest the stock (we have a 1 year cliff and 3 years of vesting after that).
2. You must stay until we have a liquidation event (or the lock-up period passes in the event we have an IPO), or you have the cash to [exercise your stock after termination](#exercise-window-after-termination).
3. We must make the company worth more than the liquidation preference.

## Stock Options

At GitLab, we give equity grants in the form of Incentive Stock Options (ISOs) and
Non-Qualified Stock Options (NSOs). The difference in these two types of grants are,
generally, as follows: ISOs are issued to US employees and carry a special form of
tax treatment recognized by the US Internal Revenue Service (IRS). NSOs are granted
to contractors and non-US employees. It’s called an option because you have the option
to buy GitLab stock later, subject to vesting terms, at the exercise price provided at
the time of grant. Solely for the purposes of example, if you are granted stock options
with an exercise price of $1 per share of common stock today, and if GitLab grows later
so its common stock is worth $20 per share, you will still be able to buy the common
stock upon exercise of your option for $1 per share.

The reason we give stock options instead of straight stock is that you do not need to
spend any money to purchase the stock at the date of grant and can decide to purchase
the stock later as your options vest. In addition, we do not provide straight stock grants
since this may subject you to immediate tax liabilities. For example, if we granted you
$10,000 worth of GitLab stock today, you would have to pay taxes on the value of the stock
(potentially thousands of dollars) for this tax year. If we give you options for $10,000
worth of stock, you generally don’t have to pay any taxes until you exercise them (more
on exercising later). Please read the Stock Option section of the [Tax Team](https://about.gitlab.com/handbook/tax/#tax-procedure-for-reporting-taxable-gains-on-option-exercises).

### Stock Option Grant Levels

Standard Option Grants are broken out by level for each division. If you have any questions on what grant should be offered to a new hire, please reach out to the Compensation and Benefits team by email to compensation@gitlab.

| Engineering                 | G&A                                | Marketing                      | Sales                                            | Product/Alliances      | Option Grant |
| ----------------------------|------------------------------------|--------------------------------|--------------------------------------------------|------------------------| ------------:|
| Fellow / [Senior Leader](/company/team/structure/#senior-leaders)      | [Senior Leader](/company/team/structure/#senior-leaders)            | [Senior Leader](/company/team/structure/#senior-leaders)                  | [Senior Leader](/company/team/structure/#senior-leaders)                                   | [Senior Leader](/company/team/structure/#senior-leaders)          | 45,000 |
| Distinguished / Director    | Director                           | Director                       | Regional Director                                | Director               | 30,000 |
| N/A                         | N/A                                | Staff PMM                      | N/A                                              | Principal/Group Product Manager | 20,000 |
| N/A                         | N/A                                | Senior PMM                     | Area Sales Manager / Managers in Customer Success (SA, TAM, IE) | Senior Product Manager | 15,000 |
| Staff / Manager (of people) | Staff (Lead) / Manager (of people) | Manager, (of people) / PMM     | Strategic Account Leader / Customer Success (SA, TAM, IE)   | Product Manager        | 7,500 |
| Senior                      | Senior / Partner                   | Senior Marketing Manager       | Account Executive                                | Senior Product Ops     | 6,000 |
| Intermediate                | Intermediate / Billing Specialist  | Marketing Manager / Senior XDR | SMB Customer Advocate                            | Product Operations     | 4,500 |
| Junior/Support Agent        | Junior / Specialist / Coordinator  | Associate / XDR                | Analyst/Other                                    | Analyst/Other          | 3,000 |

> Note: All stock option grants are subject to approval by the Board of
> Directors and no grants are final until such approval has been made. The
> Company reserves the right in its own determination to make any adjustments to
> stock option grants at its sole discretion including the decision not to make
> a grant at all.

## Promotions

If you are promoted, you are eligible to receive a new stock option grant for the
difference between your old level and your new level based on the grant levels currently in effect.
If you currently have more options than what your old level was eligible for
(for example, if you joined early in the history of the company), you will still
receive the difference between the two levels as an additional grant. Contact by email
Compensation@domain if that wasn't done.

## Option Refresh Program

Team members who have reached 3.5 years of vesting against their initial option
grants are eligible to be considered for a refresh grant. The team member list is reviewed each quarter. Refresh grants are made
at the current [stock option grant levels](#stock-option-grant-levels) and vest
over 4 years with a one year cliff. Refresh grants may be adjusted to account
for grants made subsequent to the initial grant. All proposed grants are subject
to review and approval by the Board of Directors prior to being awarded. Once approved, team members are notified directly. CEO
direct reports will be reviewed separately by the Compensation Committee of the
Board of Directors.

## Vesting

Vesting means that you have to remain employed by, or are otherwise a service provider
to, GitLab for a certain period of time before you can fully own the stock purchased
under your stock option. Instead of giving you the right to purchase and own all of
the common stock under your stock option on day one, you get to own the stock under
your stock option in increments over time. This process is called vesting and different
companies offer vesting schedules of different lengths. At GitLab, our standard practice
is to issue options with a four year vesting schedule so you would own a quarter of your
stock after 12 months, half of your stock after two years, and all of it after 4 years.
Vesting happens on a monthly basis (so you vest 1/48 of your options each month), but
many vesting schedules include a cliff. A cliff is a period at the beginning of the
vesting period where your equity does not vest monthly, but instead vests at the end of
the cliff period. At most companies, including GitLab, this cliff period is generally
one year. This means that if you leave your job either voluntarily or involuntarily
before you’ve worked for a whole year, none of your options will be vested. At the end
of that year, you’ll vest the entire year’s worth (12 months) of equity all at once.
This helps keep the ownership of GitLab stock to folks who have worked at the company
for a meaningful amount of time.

## Dilution

This section deals with dilution which happens to all companies over time. In general
companies issue stock from time to time in the future. For example, if company XYZ needs
to raise money from outside investors, it may need to create new stock to sell to those
investors. The effect of additional stock issuances by company XYZ is that while you
will own the same number of shares as you did before such issuance, there will be more
total shares of outstanding and, as a result, you will own a smaller percent of the
company -- this is called dilution.

Dilution does not necessarily mean reduced value. When a company raises
money the value of stock will stay the same because the company’s new valuation will be equal
to the old value of the company + the new capital raised. For example, if company XYZ
is worth $100m and it raises $25m, company XYZ is now worth $125m. If you owned 5%
of $100m before, you now own 4% of $125m (20% of the company was sold, or, said differently,
your 5% stake was diluted by 20%). The 5% stake was worth $5m before the fundraising, but the 4% stake is still worth $5m after.

Funding rounds can have both primary components and secondary components.
Primary components result in dilution as described here, but secondary rounds
are a bit different. Rather than new shares being issued, participants in a
secondary exchange their funding for existing shares. You can read more in
[this Quora article](https://www.quora.com/What-is-a-secondary-component-to-a-VC-round/answer/Elad-Gil).

## Stock Splits

Companies from time to time undertake stock splits. A stock split has no economic impact on the stockholder. The split simply increases the number of shares by a certain amount and reduces the price by an equal, offsetting amount. For example, if a stockholder had 1,000 shares at $10.00 per share (total value $10,000), and the Company executed a 2 for 1 stock split, the shareholder would then have 2,000 shares at $5.00 (total value still $10,000). Nothing has changed. Public companies have historically split their stock to lower the stock price so that a broader set of investors could hold shares in the company. The theory being a lower price would make it easier for an individual investor to buy shares. Private companies perform stock splits to make themselves more comparable to other private companies. Companies that undertake IPOs typically go public in accepted trading ranges and then start trading from that point. So private companies will adjust their shares to be able to trade in those ranges at IPO. For further information, read [this article](https://smartasset.com/investing/what-is-a-stock-split).

Effective February 28, 2019 the GitLab Board of Directors approved a 4:1 stock split. All common stock, preferred stock and options were treated exactly the same in the split. The stock split will be reflected in Carta by the end of April. Why 4:1? We chose that ratio so that our shares will be comparable to other companies in our peer group, so that candidates considering coming to work at GitLab can make an informed choice on an “apples to apples” basis. We also took care to not split at too high of a ratio which could result in a [reverse stock split](https://www.bizjournals.com/sanjose/stories/2001/01/15/focus3.html) prior to IPO and that is something we would like to avoid (but can't guarantee in any scenario).

## Exercising Your Options

"Exercising your options" means buying the stock guaranteed by your options. You pay
the exercise price that was set when the options were first granted and you get stock
certificates back.
To give employees an opportunity to benefit from any existing tax incentives that may
be available (including under the US and the Dutch tax laws) we have made the stock
immediately exercisable. This means you can exercise your right to purchase the unvested
shares under your option to start your holding period. However, the Company retains a
repurchase right for the unvested shares if your employment or other services ends for
any reason. An early exercise of unvested stock may have important tax implications and
you should consult your tax advisor before making such decision.

Also, while the company has the right to repurchase the unvested shares upon your
termination of services, the company is not obligated to do so. Accordingly you could
lose some or all of the investment you made. Because we are a young company there are
lots of risks, so be aware and informed of the risks. Please read this [quora thread about most startups failing](https://www.quora.com/What-is-the-truth-behind-9-out-of-10-startups-fail) and this story of [people paying more in tax for their stock than they get back](http://www.nytimes.com/2015/12/27/technology/when-a-unicorn-start-up-stumbles-its-employees-get-hurt.html).

### Option Expiration

If you leave the company, you will generally have [90 days to exercise your option](#exercise-window-after-termination)
for any shares that are vested (as of the last day of service).

In addition, if not otherwise expired (through termination of your employment), your stock
options expire 10 years after they were issued.

### Exercise Window After Termination

Please note that until the [post IPO lockup period](http://www.investopedia.com/ask/answer/12/ipo-lockup-period.asp) has expired (or we are bought) company stock is not liquid. If your employment ends
for whatever reason, you have a 90 day window to exercise your vested options, or lose them. During this window you would have to pay
the exercise price and in some cases the tax on the gain in value of your stock options, which could be considerable.
If the company stock is not liquid this money might be hard to come by. The 90 day window is an industry standard but
there are [good arguments against it](https://zachholman.com/posts/fuck-your-90-day-exercise-window/).
You may not purchase unvested shares after your service has ended.

At GitLab the stock options are intended to commit our team members to get us to
a successful IPO. We want to motivate and reward our people for reaching that goal. Therefore we will consider exercise
window extensions only on a case by case basis at our discretion. An example of a situation we'll consider is a valued
team member quitting because of personal circumstances. In most cases there will be no extension and you will either have
to pay for shares and the taxes yourself, or lose the options, even when you are fully vested. And, of course, [an IPO in 2020
is our public ambition](/company/strategy/), but neither the timing, nor whether it happens at all, is guaranteed.

## Administration

Option grants are approved by the Board of Directors at regularly scheduled quarterly board meetings. After your grant has been approved by the Board you will receive a grant notice by email from [Carta](https://carta.com/) containing information relevant to the grant including the number of shares, exercise price, vesting period and other key terms. We use Carta to administer our stock option program.

You will receive the grant notice to your GitLab email address. Clicking through that email will enable you to set up a user account at Carta. You can find all of the terms and conditions of the stock program as well as your specific grant within the Carta system. You are safe to go ahead and click "Accept" for your grant, no payment or taxes will be incurred to you since you are not [Exercising Your Options](#exercising-your-options), you are simply confirming to GitLab that you accept the grant that was allocated to you.

As a helpful hint we suggest that you add a second, personal email address to your profile. This can be added by clicking on Profile and Security in the bottom left hand corner of the home screen after logging in to Carta.

### How to Exercise Your Stock Options<a name="how-to-exercise"></a>

There are two methods to exercise your shares:

1. Electronic (US Residents Only)
   - Log into your Carta account
   - Follow the directions to enable ACH payments from your bank
   - After ACH has been enabled select exercise option grants and follow the prompts
2. Manual (Non ACH and Non US Residents)
   - Log into your Carta account
   - Click on "View" (under the arrow on right side of the screen)
   - Click on "Form of Exercise Agreement"
   - Complete the form, sign, and return as PDF to the CFO
   - Send payment in US dollars by wire transfer. You will be provided the wire transfer info.

> Note for US residents: whichever method you choose, be sure to download the 83-b election form provided by Carta and file with the IRS within 30 days of exercise. Send a copy of the election form to the CFO.

You will most likely want to include the following letter when sending in the 83-b election to the IRS:

```
<<Date Filed>>

Department of the Treasury

<<Address provided from Carta 83-b instructions>>

To whom it may concern:

Please find enclosed two copies of the 83-b election in connection with my purchase of shares of GitLab Inc. common stock. Please return one copy stamped as received to my attention in the enclosed self addressed stamped envelope.

Yours Truly,

//signature
```

## Exercise Prices and 409A Valuations

Generally, the exercise price for options granted under the 2015 Equity Plan will be
at the fair market value of such common stock at the date of grant. In short, “fair
market value” is the price that a reasonable person could be expected to pay for the
common stock, but because GitLab is not “public” (listed on a large stock exchange),
the Board is responsible for determining the fair market value. In order to assist
the Board, the company retains outside advisors to undertake something called a “409A
valuation”. In general, the lower a valuation for the shares
the better for employees as there is more opportunity for gain. Additionally, a lower
exercise price reduces the cash required to exercise the shares and establish a holding
period which can have tax advantages in some countries. We describe those in this document
but as always check with your financial or tax advisor before taking any action.

## Taxes

Tax law is complex and you should consult a tax attorney or other tax advisor who is
familiar with startup stock options before making any decisions. Please go to the [tax team's stock options](https://about.gitlab.com/handbook/tax/stock-options/) page for more information on taxation of stock options.

### US employees with Incentive Stock Options (ISOs)

Taxation from the US perspective is not as straightforward as you might like. You aren’t taxed when you exercise
your options. Tax is due on the gain or profit you make when you sell the stock. Depending on your holding period,
the tax may be treated as ordinary income or capital gain. Moreover, when you hold the options long enough you may be subject to 0% capital gains tax. To outline the five possibilities of the different scenarios that may apply:

1. Exercise your options to purchase shares, and hold.
1. Exercise your options to purchase shares, and sell the shares within the same year.
1. Exercise your options to purchase shares, and sell the shares in less than twelve months, but during the following year.
1. Sell shares at least one year and a day after you exercised the options, but less than two years from the original grant date.
1. Sell shares at least two years from the original grant date.

Please note, however, that any gain upon exercise of an ISO (difference between the exercise price and fair market value
at date of exercise), even if you do not sell the shares, may be counted as a "tax preference"
towards the Alternative Minimum Tax limit. For instance, under scenario 1 above you have to make an adjustment in your tax return for the Alternative Minimum Tax (AMT) that equals the so-called bargain element. Each scenario has a different tax treatment, so be careful of the tax consequences when you exercise your options. In the long term, holding onto your stock does save taxes, however be aware of the AMT that you will be confronted with. It is strongly advised that you contact a tax advisor to be aware of the US tax consequences.

In addition to the benefits of a longer holding period, the IRS does have an additional benefit for holders of Qualified Small Business Stock (QSBS for short). Currently, GitLab meets the criteria for QSBS treatment, however (again), the Company is not in a position to offer tax or legal advice, so check with your own tax and financial advisors. We found [this article](https://blog.wealthfront.com/qualified-small-business-stock-2016/) helpful in describing the QSBS program in greater detail.

### US service providers with Nonqualified Statutory Options (NSOs)

For non-employees of GitLab that have been granted stock options, their stock options are treated as NSOs. NSOs have different tax treatments depending on whether they are actively traded on an established market:

- **when actively traded**: taxed at grant
- **when not actively traded**: taxed at exercise

Since GitLab's stock options are not actively traded on an established market, the NSO is taxed at exercise. The gain of the exercise (fair market value minus the exercise price) has to be reported by [form 1099-MISC (box 7)](https://www.irs.gov/pub/irs-pdf/f1099msc.pdf). Withholding is typically not required, however when the service provider fails to provide a valued tax identification number in form 1099, GitLab has to ensure backup withholding (roughly 25%).

### Netherlands, Germany and Australia

For our employees based in the Netherlands, Germany and Australia, the difference between the exercise price and the fair market value is considered taxable at the date you exercise your stock options. With respect to reporting taxes: the taxable gains are subject to employer tax withholding. The tax payable is therefore deducted from your gross payroll with respect to the exercise of your stock options.

### The United Kingdom

In the United Kingdom there is a small difference in the tax treatment of exercising your stock options as opposed to the other entities; the difference between the fair market value and the exercise price is taxed at the date of exercise if there is a liquid market for the stock at time of exercise. For more information please check [our memo on this](https://about.gitlab.com/handbook/tax/#tax-procedures-for-reporting-taxable-gains-on-option-exercises).

## Dual-Class Stock

Often companies will create multiple classes of stock, with each class having different voting rights, in order to provide protection to the company's founders, early investors, and early employees, whose long-term vision for the company may not align with that of later stage investors. At the GitLab Board of Directors meeting held on January 31, 2019, the Board approved the creation of such a dual-class structure.

### Effect of Dual-Class Stock

- Class B stockholders retain significant influence over stockholder votes and actions.
- The Influence of long-term Class B stockholders increases as other pre-IPO stockholders sell or distribute their Class B stock, which coverts into Class A common stock upon sale or distribution.

### Dual-Class Voting  

| Type                     | Detail             |
| :----------------------- | :----------------- |
| **Class A Common Stock** | 1 vote per share   |
| **Class B Common Stock** | 10 votes per share |

### Creation of Dual-Class Stock

**Step 1: Pre-IPO**

- Create two classes of common stock.
- Outstanding common stock converts to Class B common stock.
- Options and RSUs subsequently issued exercise into Class A common stock.

**Step 2: At IPO**

- Preferred stock converts to Class B common stock.
- Class A common stock sold in IPO.

**Step 3: Post-IPO**

- Class B converts to Class A in sale or distribution unless “permitted transfer” (see below).
- Additional sales of Class A common stock.
- Permitted transfers would be transfers to one or more family members, transfers to a trust for the benefit of the stockholder or in favor of a family member of the stockholder, or transfers to a general partnership, limited partnership, limited liability corporation, or other entity controlled by the stock holder or a family member of the stockholder.

### Sunset

The stock structure will automatically convert and become a single-class structure upon (whichever comes first):

- A date specified by a vote of 66.66% of the outstanding shares of Class B common stock
- Death or Permanent Disability of Sid Sijbrandij
- At such time as when the Class B represents less than 5% of the outstanding Common Stock

## Transfer Restrictions on Common Stock

On January 31, 2019 the Board of Directors approved the amendment to the company's bylaws regarding the transfer of shares of Common Stock. Effective as of that date, Stockholders will not be able transfer, sell, or assign any shares of Common Stock without the prior written consent of the Board. This restriction does not apply to the following permitted transfers:

- the transfer is a gift or pursuant to a domestic relations order, to the stockholder's immediate family member;
- the transfer is executed pursuant to the stockholder's will or the laws of intestate succession;
- the transfer by an entity stockholder is made to an affiliate of such entity stockholder;
- the transfer by an entity stockholder of all Common Shares is made to a single transferee in accordance with the terms of any bona fide merger, consolidation, or acquisition;
- the transfer is made for no consideration by a stockholder that is a partnership to such stockholder's limited partners in accordance with the partnership interests of such limited partners;
- any repurchase or redemption of the Common Shares by the corporation (a) at or below cost, upon the occurrence of certain events, such as the termination of employment or services, or (b) at any price pursuant to the corporation's exercise of a right of first refusal to repurchase such Common Shares; or
- in the event of a transfer or deemed-transfer that is approved, or in the event the restriction is waived, and the Common Shares of the transferring stockholder are subject to co-sale rights, any transfers by the persons and/or entities who are entitled to and have exercised the co-sale rights in conjunction with such approved transfer or deemed-transfer giving rise to the exercise of such co-sale right.

## Questions?

Everyone is always welcome to ask our CFO any questions they have about their options,
GitLab’s fundraising, or anything else related to equity at GitLab. However, everyone should
also consult a lawyer before making important financial decisions, especially regarding
their equity because there are complex legal and tax requirements that may apply.

### References

Our team member Drew Blessing [wrote about what he learned about stock options](http://blessing.io/startups/stock-options/2016/02/15/navigating-your-stock-options.html) after starting to research them because he received them when joining us. His article is greatly appreciated, but any advice is his own, and it is not officially endorsed by GitLab Inc.

## Stock Options Board Meeting Reporting

Each quarter before the board meeting, the People Operations Analyst will run a report for the CFO outlining the options pending approval. The process should be:

1. Run the Stock Options Report from BambooHR.
1. In the Stock Options Folder, create a new Google sheet titled "Stock Options as of YYYY-MM-DD".
1. Paste the BambooHR Report into a new tab in the sheet for historical reference.
1. Add new tabs for options that have already been approved (they will be denoted by Yes, No, or N/A).
1. Any blank "Approved by the Board" options should be added to a new sheet.
1. Use the pending options to population the "Option Grant" google doc by creating a new tab with the date of the board meeting.
1. Update the Fully Diluted Shares as shown in Carta and ensure all formulas are functioning properly.
1. Copy and paste the information from the pending approval tab to the proper column.
1. Enter ISO or International as the option type (ISO is for US employees).
1. Enter country for each participant.
1. Enter functional group for each participant.
1. For low/high this should reflect the highest and lowest number of options someone in that level was contracted. Leave this blank for promotion.
1. Run a report on [refresh grants](/handbook/stock-options/#option-refresh-program) and add these to the sheet.
1. Ping the CFO that the report is ready for review.
1. Verify all options were approved (or not) with the CFO after the board meeting and change the approval in BambooHR to avoid duplications on the next report.

## Procedures for Issuing Options to Team Members

1. In Carta Download the Bulk Import Sheet.
1. Cut and paste the Option Grant sheet approved by the Board into the Bulk Import Sheet.
1. Use the concatenate function to merge team member names into a single cell.
1. Designate options as ISO, NSO or INTL. Carta will automatically convert ISOs over the limit into NSOs.
1. Issue date relation - Enter all team members, advisors and board members as Employees. GitLab early adopted ASU 2018-07 (w.r.t accounting for stock options issued to non-employees) which is why we designate all team members as employees.
1. The standard vesting schedule is 1/48 monthly with a 1 year cliff.
1. Enter YES for Early Exercise.
1. Check with Fenwick on Federal and State Exemption whether Rules 701 and 21052(f) remain applicable.
1. Ensure any non-standard vesting terms are correctly input on bulk import or individually in Carta.
1. Document Set is "2015 Equity Incentive Plan".
1. All other optional fields can be left blank as default settings have been pre-populated in Carta.
1. When successfully uploaded make announcement in Team Call. All hires and promotions through xx date; all refresh grants for hires through yy date.
