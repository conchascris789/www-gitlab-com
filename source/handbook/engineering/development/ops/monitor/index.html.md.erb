---
layout: markdown_page
title: "Monitor Stage"
---

# Monitor Stage

## On this page
{:.no_toc}

- TOC
{:toc}

## Groups

The groups within this stage are:

* [APM](apm/)
* [Health](health/)

## Common links

* Slack channel: [#g_monitor](https://gitlab.slack.com/archives/g_monitor)
* Slack alias: @monitor-group
* Google group: monitor-stage@gitlab.com

## Vision

Using GitLab, you automatically get broad and deep insight into the health of your deployment.

## Mission

We provide a robust monitoring solution to give GitLab users insight into the performance and availability of their deployments and alert them to problems as soon as they arise. We provide data that is easy to digest and to relate to other features in GitLab. With every piece of the devops lifecycle integrated into GitLab, we have a unique opportunity to closely tie our monitoring features to all of the other pieces of the devops flow.

We work collaboratively and transparently and we will contribute as much of our work as possible back to the open source community.

## Responsibilities
{: #monitoring}

The monitoring team is responsible for:
* Providing the tools required to enable monitoring of GitLab.com
* Packaging these tools to enable all customers to manage their instances easily and completely
* Building integrated monitoring solutions for customers apps into GitLab, including: metrics, logging, and tracing

This team maps to [Monitor Stage](/handbook/product/categories/#monitor-stage).

## How to work with Monitor

### Surfacing blockers

To surface blockers, mention your Engineering Manager in the issues, and then contact them via slack and or 1:1's. Also make sure to raise any blockers in your daily async standup using Geekbot. One of the prompts in the standup is, "Is there anything blocking your progress?"

The engineering managers want to make unblocking their teams their highest priority. Please don't hesitate to raise blockers

### Scheduling bugs

When new bugs are reported, the engineering managers ensure that they have proper Priority and Severity labels. Bugs are discussed during our backlog grooming session and are scheduled according to severity, priority, and the capacity of the teams. Ideally, we should work on a few bugs each release regardless of priority or severity.

### Interacting with community contributors

Community contributions are encouraged and prioritized at GitLab. Please check out the [Contribute page](/community/contribute) on our website for guidelines on contributing to GitLab overall.

Within the Monitor stage, Product Management will assist a community member with questions regarding priority and scope. If a community member has technical questions on implementation, Engineering Managers will connect them with engineers within the team to collaborate with.

### Using spikes to inform design decisions

Engineers use spikes to conduct research, prototyping, and investigation to gain knowledge necessary to reduce the risk of a technical approach, better understand a requirement, or increase the reliability of a story estimate (paraphrased from [this overview](https://www.scaledagileframework.com/spikes/). When we identify the need for a spike for a given issue, we will create a new issue, conduct the spike, and document the findings in the spike issue. We then link to the spike and summarize the key decisions in the original issue.

### Preparing UX designs for engineering

Product designers generally try to work one milestone ahead of the engineers, to ensure scope is defined and agreed upon before engineering starts work. So, for example, if engineering is planning on getting started on an issue in 12.2, designers will assign themselves the appropriate issues during 12.1, making sure everything is ready to go before 12.2 starts.

To make sure this happens, early planning is necessary. In the example above, for instance, we'd need to know by the end of 12.0 what will be needed for 12.2 so that we can work on it during 12.1. This takes a lot of coordination between UX and the PMs. We can (and often do) try to pick up smaller things as they come up and in cases where priorities change. But, generally, we have a set of assigned tasks for each milestone in place by the time the milestone starts so anything we take on will be in addition to those existing tasks and dependent on additional capacity.

The current workflow:

* Though Product Designers make an effort to keep an eye on all issues being worked on, PMs add the UX label to specific issues needing UX input for upcoming milestones.

* The week before the milestone starts, the Product Designers divide up issues depending on interest, expertise and capacity.

* Product Designers start work on assigned issues when the milestone starts. We make an effort to start conversations early and to have them often. We collaborate closely with PMs and engineers to make sure that the proposed designs are feasible.

* In terms of what we deliver: we will provide what's needed to move forward, which may or may not include a high-fidelity design spec. Depending on requirements, a text summary of the expected scope, a balsamiq sketch, a screengrab or a higher fidelity measure spec may be provided.

* When we feel like we've achieved a 70% level of confidence that we're aligned on the way forward, we change the label to ~'workflow::ready for development' as a sign that the issue is appropriately scoped and ready for engineering.

* We usually stay assigned to issues after they are ~'workflow::ready for development' to continue to answer questions while the development process is taking place.

* Finally, when development is complete, we conduct UX Reviews on the MRs to ensure that what's been implemented matches the spec.

## Repos we own or use
* [Prometheus Ruby Mmap Client](https://gitlab.com/gitlab-org/prometheus-client-mmap) - The ruby Prometheus instrumentation lib we built, which we used to instrument GitLab
* [GitLab CE](https://gitlab.com/gitlab-org/gitlab-ce) and [GitLab EE](https://gitlab.com/gitlab-org/gitlab-ee) - Where much of the user facing code lives
* [Omnibus](https://gitlab.com/gitlab-org/omnibus-gitlab) and [Charts](https://gitlab.com/charts/charts.gitlab.io), where a lot of the packaging related work goes on. (We ship GitLab fully instrumented along with a Prometheus instance)

## Service accounts we own or use

### Zoom sandbox account

In order to develop and test Zoom features for the [integration with GitLab](https://gitlab.com/groups/gitlab-org/-/epics/1439) we now have our own Zoom sandbox account.

#### Requesting access

To request access to this Zoom sandbox account please open [an issue](https://gitlab.com/gitlab-com/access-requests/issues/new?issuable_template=New%20Access%20Request) providing your **non-GitLab email address** (which can already be associated an existing non-GitLab Zoom account).

The following people are owners of this account and can [grant access](https://zoom.us/account/user) to other GitLabbers:

* [Andrew Newdigate](https://gitlab.com/andrewn)
* [Peter Leitzen](http://gitlab.com/splattael)

#### Granting access

1. Log in to [Zoom](http://zoom.us/) with your non-GitLab email
1. Go to [**User Management > Users**](https://zoom.us/account/user)
1. Click on `Add User`
1. Specify email addresses
1. Choose `User Type` - most likely `Pro`
1. Click `Add` - the users receive invitations via email
1. Add the linked name to [the list in "Requesting access"](#requesting-access)

#### Documentation

For more information on how to use Zoom see theirs [guides](https://marketplace.zoom.us/docs/guides) and [API reference](https://marketplace.zoom.us/docs/api-reference/introduction).


## Async Daily Standups
The purpose of our async standups is to allow every team member to have insight into what everyone else is doing and whether anyone is blocked and could use help. This should not be an exhaustive list of all of your tasks for the day, but rather a summary of the major deliverable you are hoping to achieve. All question prompts are optional. We use the [geekbot slack plugin](https://geekbot.io/) to automate our async standup in the [#g_monitor_standup](https://gitlab.slack.com/archives/g_monitor_standup) channel. By joining the [#g_monitor_standup](https://gitlab.slack.com/archives/g_monitor_standup) channel, you will be included in the standup and Geekbot will prompt you once a day for your standup report.

## Recurring Meetings
While we try to keep our process pretty light on meetings, we do have a few recurring meetings to keep in sync and to keep our backlog in good shape. We hold the [Monitor Stage Weekly Meeting](https://docs.google.com/document/d/1yADA1ZfnflQjgSkbgJEEB6ziwE3iyb-_SCpjOOSHEPo/edit) to discuss agenda items that have been added over the course of the week and to walk through our current issue board together. We also hold a [Monitor Backlog Grooming](https://docs.google.com/document/d/1YWpzwlLVvciuHlpT1ALfixMHRYcWt7oqD4HftkVE5w8/edit#) meeting weekly to triage and prioritize new issues, discuss our upcoming issues, and uncover any unknowns. Both meetings are held on Thursday.

There is also an optional Monitor Social Hour meeting every week. This call has no agenda and alternates times every other week to be more inclusive of team members in different time zones.

## Monitor Stage PTO
Just like the rest of the company, we use [PTO Ninja](https://about.gitlab.com/handbook/paid-time-off/#pto-ninja) to track when team members are traveling, attending conferences, and taking time off. The easiest way to see who has upcoming PTO is to run the `/ninja whosout` command in the `#g_monitor_standup` slack channel. This will show you the upcoming PTO for everyone in that channel.

## Useful Resources

* For an introduction to Prometheus, see this guide: [https://www.youtube.com/watch?v=8Ai55-sYJA0](https://www.youtube.com/watch?v=8Ai55-sYJA0).
* For setting up a kubernetes cluster for local development, consult this tutorial: [https://www.youtube.com/watch?v=dFIlml7O2go](https://www.youtube.com/watch?v=dFIlml7O2go).
