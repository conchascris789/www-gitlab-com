---
layout: markdown_page
title: "Secure Team"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### Secure Team

The Secure Team (previously known as the _Security Products Team_) is responsible for the security checks features in the GitLab platform, and maps to the [secure](/handbook/product/categories/#secure) transversal stage.
You can learn more about our approach on the [Secure Vision](/direction/secure/) page.

The features provided by the Secure Team are mostly present at the pipeline level, and mostly available as [Docker](https://www.docker.com/) images. 
This particularity shapes our processes and QA, which differs a bit from the other backend teams.

#### Security Products

We still refer to "_Security Products_" as the tools developed by the Secure Team. Hence the home of our projects in GitLab: [https://gitlab.com/gitlab-org/security-products/](https://gitlab.com/gitlab-org/security-products/)

#### Domains of expertise

##### SAST

[SAST](https://docs.gitlab.com/ee/user/project/merge_requests/sast.html) (_Static Application Security Testing_) refers to static code analysis.
GitLab leverages the power of various opensource tools to provide a wide range of checks for many languages and support.
These tools are wrapped inside docker images which ensure we get a standard output from there.
An orchestrator, [developed by GitLab](https://gitlab.com/gitlab-org/security-products/sast), is in charge of running these images, and gathering all the data needed to generate the final report.

##### DAST

[DAST](https://docs.gitlab.com/ee/user/project/merge_requests/dast.html) (_Dynamic Application Security Testing_) is used to hit a live application.
Because some vulnerabilities can only be detected once all the code is actually running, this method complements the static code analysis.
DAST is relying on [OWASP Zed Attach Proxy Project](https://gitlab.com/gitlab-org/security-products/zaproxy), modified by GitLab to enable authentication.

##### Dependency Scanning

[Dependency Scanning](https://docs.gitlab.com/ee/user/project/merge_requests/dependency_scanning.html) is used to detect vulnerabilities introduced by external dependencies in the application.
Because a large portion of the code shipped to production is actually coming from third-party libraries, it's important to monitor them as well.
Dependency Scanning is relying mostly on the Gemnasium engine.

##### Container Scanning

[Container Scanning](https://docs.gitlab.com/ee/user/project/merge_requests/container_scanning.html) is used when the application is shipped as a Docker image.
It's very common to build the final image on top of an existing one, which must be checked like every other portion of the application.
For that, Container Scanning is relying on the [clair scanner](https://gitlab.com/gitlab-org/security-products/clair-scanner).


##### License Management

[License Management](https://docs.gitlab.com/ee/user/project/merge_requests/license_management.html) helps with the licenses introduced by third-party libraries in the application.
Licence management relies on the [LicenceFinder](https://github.com/pivotal-legacy/LicenseFinder) gem.

#### Label Usage

If you are submitting an issue about a Secure Stage feature, use `~devops::secure` and one of the following group labels to get the issue in front of the most appropriate team members.

| Label 						| Use        									|
| ----------------------		|---------------------------					|
| `~devops::secure`				| All issues related to the Secure Stage      	|
| `~group::static analysis`      | SAST, Secret Detection 									|
| `~group::dynamic analysis`  	| DAST, IAST, Fuzzing and Security Dashboard related   |
| `~group::software composition analysis`            | Container or Dependency Scanning, Vulnerability Database, License Management  |

Additional labels should be added according to the [Workflow Labels Documentation](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/development/contributing/issue_workflow.md#workflow-labels). 


#### Release process

Our release process is specified in this [project](https://gitlab.com/gitlab-org/security-products/release). The vulnerability database is [updated on a regular basis](https://docs.gitlab.com/ee/user/application_security/#maintenance-and-update-of-the-vulnerabilities-database).

#### Skills

Because we have a wide range of domains to cover, it requires a lot of different expertises and skills:


| Technology skills | Areas of interest         |
| ------------------|---------------------------|
| Ruby on Rails     | Backend development       |
| Go                | SAST, Dependency Scanning |
| SQL (PostgreSQL)  | Dependency Scanning       |
| Docker            | Container Scanning / all  |

Our team also must have a good sense of security, with at least basic skills in [application security](https://en.wikipedia.org/wiki/Application_security).

We provide tools for many different languages (ex: [sast](https://docs.gitlab.com/ee/user/project/merge_requests/sast.html#supported-languages-and-frameworks), [dependency scanning](https://docs.gitlab.com/ee/user/project/merge_requests/dependency_scanning.html#supported-languages-and-dependency-managers), [license management](https://docs.gitlab.com/ee/user/project/merge_requests/license_management.html#supported-languages-and-package-managers)). It means our team is able to understand the basics of each of these languages, including their package managers. We maintain [tests projects](https://gitlab.com/gitlab-org/security-products/tests) to ensure our features are working release after release for each of them.

#### QA process

Our [release](https://gitlab.com/gitlab-org/security-products/release) project also defines our [QA process](https://gitlab.com/gitlab-org/security-products/release/blob/master/docs/qa_process.md).

#### Engineering Rhythm

In order to be able to ship 100% of our Deliverables, we give a lot of attention to our planning. For that, we try to plan in advance as much as possible, and to freeze everything once the iteration starts. To avoid changes and uncertainty during the release, we must make sure everything is ready to start the development on the first day.

Meetings schedule:

- _Before the 1st of the next month:_ **Planning meeting** with our [Product Manager](/job-families/product/product-manager/), the [UX](/handbook/engineering/ux/) team, and the [FrontEnd](/handbook/engineering/frontend/) team. The goal of the meeting is to understand and discuss the scope of the next iteration. Each issue from the product vision is explained and detailed.
- _After the Planning meeting, and still before the 1st of the next month:_ **Evaluation meeting** with the [FrontEnd](/handbook/engineering/frontend/) team.
- _Finally, on the 8th of the month:_ **Kickoff meeting** where we make the list of Deliverables didn't change, and we assign developers to each one of them 

The [Product Manager](/job-families/product/product-manager/) and the 
[Engineering Manager](/job-families/engineering/backend-engineer/#engineering-manager) 
will do the milestone grooming during their 1:1 following the kickoff.
Every issue still open will be evaluated for rescheduling (in the following milestone or not). 

#### Product Documentation

As the product evolves, it is important to maintain accurate and up to date documentation for our users. If it is not documented, customers may not know a feature exists. 

To update the documentation, the following process should be followed:

1. When an issue has been identified as needing documentation, add the `~Documentation` label, outline in the description of the issue what documentation is needed, and assign a Backend Engineer and Technical Writer(TW) to the issue (find the appropriate TW by searching the [product categories](/handbook/product/categories/)).
1. If the task is documentation only, apply a `~Px` label. 
1. For documentation around features or bugs, a backend engineer should write the documentation and work with the technical writer for editing. If the documentation only needs styling cleanup, clarification, or reorganization, this work should be lead by the Technical Writer with support from a BE as necessary.  The availability of a technical writer should in no way hold up work on the documentation. 
[Further information on the documentation process](https://docs.gitlab.com/ee/development/documentation/feature-change-workflow.html). 

#### Async Daily Standups

Since we are a [remote](/company/culture/all-remote/) company, having daily stand-ups meetings would not make any sense, since we're not all in the same timezone.
That's why we have async daily standups, where everyone can give some insights into what they did yesterday, what they plan to do today, etc.
For that, we rely on the [geekbot](https://geekbot.io/) slack plugin to automate the process.

Use the "`description in backquote` + [link to issue](#)" format when mentioning issues in your standup report.

#### Recorded meetings

Our important meetings are recorded and published on YouTube, in the [GitLab Secure Playlist](https://www.youtube.com/playlist?list=PLFGfElNsQtha-9T1ywH9qRvi1cnCmt8u_).
They give a good overview of the decision process, which is often a discussion with all the stakeholders. As we are a [remote](/company/culture/all-remote/) company, these video meetings help to synchronize and take decisions faster than commenting on issues. We prefer asynchronous work, but for large features and when the timing is tight, we can detail a lot of specifications. This will make the asynchronous work easier, since we have evaluated all edge cases.

### Technical onboarding

New hires should go through these steps and read the corresponding documentation when onboarding in the Secure Team.
Every new hire will have an assigned [onboarding issue](https://gitlab.com/gitlab-org/security-products/onboarding/blob/master/.gitlab/issue_templates/Technical_Onboarding.md) that will guide them through the whole process.

#### Workflows

- [GitLab development guides](https://docs.gitlab.com/ee/development/README.html)
- [Workflow labels](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/development/contributing/issue_workflow.md#workflow-labels)
- [Issues board](https://gitlab.com/groups/gitlab-org/-/boards/364216?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Secure): Add a `milestone` filter with the [current milestone +1](/releases/) (the current milestone is already released).
- [Merge Requests workflow](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/development/contributing/merge_request_workflow.md)
- [Code Review process](https://docs.gitlab.com/ee/development/code_review.html)
