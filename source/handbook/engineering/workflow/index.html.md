---
layout: markdown_page
title: "Engineering Workflow"
---

This document explains the workflow for anyone working with issues in GitLab Inc.
For the workflow that applies to everyone please see [PROCESS.md](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/PROCESS.md).

## On this page
{:.no_toc}

- TOC
{:toc}

## GitLab Flow

Products at GitLab are built using the [GitLab Flow](https://docs.gitlab.com/ee/workflow/gitlab_flow.html).

We have [specific rules around code review](/handbook/engineering/workflow/code-review/).

## Broken master

If you notice that pipelines for the `master` branch of GitLab [CE] or [EE] are failing (red) or broken (green as a false positive), fixing this takes priority over everything else development related, since everything we do while tests are broken may break existing functionality, or introduce new bugs and security issues.

[CE]: https://gitlab.com/gitlab-org/gitlab-ce/commits/master
[EE]: https://gitlab.com/gitlab-org/gitlab-ee/commits/master

An issue that is labelled as `~"master:broken"` will be assigned the highest priority and severity `~P1` `~S1`.

All tests (unit, integration, **and** E2E QA) that fail on master are treated as `~"master:broken"`.

Any test failures or flakiness (either false positive or false negative) causes productivity impediments for all of engineering and our release processes.
If a change causes new test failures, the fix to the test should be made in the same Merge Request.
If the change causes new QA test failures, in addition to fixing the QA tests, the `package-and-qa` or `review-qa-all` job must be run to validate the fix before the Merge Request can be merged.

The cost to fix test failures increases exponentially as time passes. Our aim should be to keep master free from failures, not to fix master only after it breaks.

[Issues labeled with ~"master:broken"][broken-master-issues] need to have a short lifespan. If the problem described by an issue is not addressed within one working day, the issue labels need to be changed:

* When `master` build was failing and the underlying problem was quarantined / reverted / temporary workaround created but the root cause still needs to be discovered: apply a label ~"master:needs-investigation" and remove ~"master:broken"
* When `master` build had a flaky failure that cannot be reliably reproduced: apply ~"master:flaky" and remove ~"master:broken" label

Everyone who is a member of the project should triage the issues with the [~"master:broken" label][broken-master-issues].

### Failed pipeline status updates

Failing pipelines on `master` are reported to the `#development` channel in
Slack, and this is how we normally notice them. Since investigating them is a
high-priority task, multiple people look out for this!

To avoid both the bystander effect **and** duplication of effort, it's useful to
report actions you're taking in the `#development` channel, as you're taking
them. For brevity, communicate by applying emoji to pipeline failure messages:

* When investigating a failing pipeline, add the `:eyes:` emoji.
* When the pipeline contains a **new** failure, add the `:boom:` emoji,
  create the issue, and reply to the message with a link to the issue.
* When the pipeline is failing because of issues that are already known, add a
  `:fire_engine:` emoji.
* When a system failure (e.g., Docker failure) is responsible, retry the failed
  jobs and add a `:retry:` emoji.

This scheme makes it obvious to see, at a glance, whether someone is already
looking at a pipeline failure on master. In turn, this encourages people to jump
in and help, while helping to avoid duplication of effort.

An example:

![pipeline failures example](pipeline-failure-emoji.png)

### As a Developer

1. Look for [an issue labelled ~"master:broken"][broken-master-issues] that would track the failure
1. If it doesn't exist, [create it][new-issue], ping the relevant people, and post it in `#development` so that other developers are aware of the problem and can help
1. If you have the time, assign it to yourself and start working on it by either:
   - Identifying the merge request that introduced the failures and revert it as soon as possible.
   - [Quarantining](https://docs.gitlab.com/ee/development/testing_guide/flaky_tests.html#quarantined-tests) the
     failing test if you think it's flaky (e.g. it wasn't touched recently and passed after retrying the failed job).
1. Reply to the automatic "Pipeline failed" notifications in `#development` by mentioning the issue
1. If you did not assign the issue to yourself, ask in `#development`, `#backend` and `#frontend` if someone can assign the issue to themselves
1. If the problem isn't fixed within a few hours, `@mention` the relevant Engineering Leads and CTO in the issue and on Slack, so that resources can be assigned to fix it as quickly as possible.

[new-issue]: https://gitlab.com/gitlab-org/gitlab-ce/issues/new

### As a Maintainer

When `master` is red, we need to try hard to avoid introducing **new** failures,
since it's easy to lose confidence if it stays red for a long time. However,
it's wasteful and impractical to completely stop development. We need to
compromise between the two priorities.

It's ok to merge a merge request with a failing pipeline if the following
conditions are met:

1. The failures also happen on `master`
1. Only a small number of the tests have failed
1. They are not directly related to functionality touched by the merge request
1. There is:
    1. [An issue labelled ~"master:broken"][broken-master-issues] for it, see the "As a developer" steps above for more details
    1. That issue is already assigned to someone
    1. The assignee is actively working on it (i.e. they are not on PTO, at a conference, etc.)

Before merging, add a comment mentioning that the failure happens in `master`,
and post a reference to the issue. For instance:

```
Failure in <JOB_URL> happens in `master` and is being worked on in #XYZ, merging.
```

Whether the pipeline is failing or not, if `master` is red, check how far behind
the source branch is. If it's more than 100 commits behind, ask for it to be
brought up to date before merging. This reduces the chance of introducing new
failures, and also acts to slow (but not stop) the rate of change in `master`,
helping us to make it green again.

[broken-master-issues]: https://gitlab.com/groups/gitlab-org/-/issues?state=all&label_name[]=master%3Abroken

## Security Issues

Security issues are managed and prioritized by the security team. If you are assigned to work on a security issue in a milestone, you need to follow these steps:

1. **Do not push code relating to the fix to GitLab.com.** The changes should be kept confidential until the release, and there is no way to make merge requests on GitLab.com confidential at the moment.
1. Instead, work against the equivalent project on [dev.gitlab.org](https://dev.gitlab.org/gitlab). For instance, GitLab
   Community Edition on dev is at [gitlab/gitlabhq](https://dev.gitlab.org/gitlab/gitlabhq).
1. [Create a new issue on dev.gitlab.org project](/handbook/communication/#everything-starts-with-an-issue) using the *Security developer workflow* template and follow the process described in it.
1. In most cases you will need to create merge requests against master and backports to [last 3 stable branches](https://docs.gitlab.com/ee/policy/maintenance.html#security-releases).
1. Make sure you add all necessary links to the issue and merge requests you created.
   1. **Note**: you do not have to create the backport merge requests until the merge request against `master` has been reviewed and is ready to merge.
1. Link the merge requests with all the backports and relevant issue to a security release issue as well. You can find the security issue link in `#security` chat channel.
1. After all your merge requests have been reviewed and all pipelines pass they can be assigned to `@gitlab-release-tools-bot`.

If you find a security issue in GitLab, create a **confidential issue** mentioning the relevant security and engineering managers, and post about it in `#security`.

If you accidentally push security commits to GitLab.com, we recommend that you:
1. Delete the relevant branch ASAP
2. Inform a release manager in `#releases`. It may be possible to execute a garbage collection (via the Housekeeping task in the repository settings) to remove the commits.

For more information on how the entire process works for security releases, see the
[documentation on security releases](https://gitlab.com/gitlab-org/release-tools/blob/master/doc/security.md).

## Basics

1. Start working on an issue you’re assigned to. If you’re not assigned to any issue, find the issue with the highest priority you can work on, by relevant label. [You can use this query, which sorts by priority for the started milestones][priority-issues], and filter by the label for your team.
1. If you need to schedule something or prioritize it, apply the appropriate labels (see [Scheduling issues](#scheduling-issues)).
1. If you are working on an issue that touches on areas outside of your expertise, be sure to mention someone in the other group(s) as soon as you start working on it. This allows others to give you early feedback, which should save you time in the long run.
1. You are responsible for the issues assigned to you. This means it has to ship with the milestone it's associated with. If you are not able to do this, you have to communicate it early to your manager and other stakeholders (e.g. the product manager, other engineers working on dependent issues). In teams, the team is responsible for this (see [Working in Teams](#working-in-teams)). If you are uncertain, err on the side of overcommunication. It's always better to communicate doubts than to wait.
1. You (and your team, if applicable) are responsible for:
  - ensuring that your changes [apply cleanly to GitLab Enterprise Edition][ce-ee-docs].
  - the testing of a new feature or fix, especially right after it has been merged and packaged.
  - shipping secure code, (see [Security is everyone's responsibility](#security-is-everyones-responsibility)).
1. Once a release candidate has been deployed to the staging environment, please verify that your changes work as intended. We have seen issues where bugs did not appear in development but showed in production (e.g. due to CE-EE merge issues).

Be sure to read general guidelines about [issues](https://docs.gitlab.com/ee/development/contributing/issue_workflow.html) and [merge requests](https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html).

[priority-issues]: https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&milestone_title=%23started&assignee_id=None
[ce-ee-docs]: https://docs.gitlab.com/ee/development/automatic_ce_ee_merge.html#avoiding-ce-gt-ee-merge-conflicts-beforehand
[gitlab-workflow]: /handbook/communication/#gitlab-workflow

## Working in Teams

For larger issues or issues that contain many different moving parts, you'll be likely working in a team. This team will typically consist of a [backend engineer](/job-families/engineering/backend-engineer/), a [frontend engineer](/job-families/engineering/frontend-engineer/), a [Product Designer](/job-families/engineering/product-designer/) and a [product manager](/job-families/product/product-manager/).

1. Teams have a shared responsibility to ship the issue in the planned release.
    1. If the team suspects that they might not be able to ship something in time, the team should escalate / inform others as soon as possible. A good start is informing your manager.
    1. It's generally preferable to ship a smaller iteration of an issue, than ship something a release later.
1. Consider starting a Slack channel for a new team, but remember to write all relevant information in the related issue(s). You don't want to have to read up on two threads, rather than only one, and Slack channels are not open to the greater GitLab community.
1. If an issue entails frontend and backend work, consider separating the frontend and backend code into separate MRs and merge them independently under [feature flags](https://docs.gitlab.com/ee/development/feature_flags.html). This will ensure frontend/backend engineers can work and deliver independently.
    1. It's important to note that even though the code is merged behind a feature flag, it should still be production ready and continue to hold our [definition of done](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/development/contributing/merge_request_workflow.md#definition-of-done).
    1. A separate MR containing the integration, documentation (if applicable) and removal of the feature flags should be completed in parallel with the backend and frontend MRs, but should only be merged when both the frontend and backend MRs are on the master branch.

## Convention over Configuration

Avoid adding configuration values in the application settings or in
`gitlab.yml`. Only add configuration if it is absolutely necessary. If you
find yourself adding parameters to tune specific features, stop and consider
how this can be avoided. Are the values really necessary? Could constants be
used that work across the board? Could values be determined automatically?
See [Convention over Configuration](/handbook/product#convention-over-configuration) for more discussion.

## Choosing Something to Work On

Start working on things with the highest priority in the current milestone. The priority of items are defined under labels in the repository, but you are able to sort by priority.

After sorting by priority, choose something that you’re able to tackle and falls under your responsibility. That means that if you’re a frontend developer, you work on something with the label `frontend`.

To filter very precisely, you could filter all issues for:

- Milestone: Started
- Assignee: Unassigned
- Label: Your label of choice. For instance `CI/CD`, `Discussion`, `Quality`, `frontend`, or `Platform`
- Sort by priority

[Use this link to quickly set the above parameters][priority-issues]. You'll still need to filter by the label for your own team.

If you’re in doubt about what to work on, ask your lead. They will be able to tell you.

## Triaging and Reviewing Code from the rest of the Community

It's every [developers' responsibilities] to triage and review code contributed by the rest of the community, and work with them to get it ready for production.

Merge requests from the rest of the community should be labeled with the `Community Contribution` label.

When evaluating a merge request from the community, please ensure that a relevant PM is aware of the pending MR by mentioning them.

This should be to be part of your daily routine. For instance, every morning you could triage new merge requests from the rest of the community that are not yet labeled `Community Contribution` and either review them or ask a relevant person to review it.

Make sure to follow our [Code Review Guidelines](https://docs.gitlab.com/ee/development/code_review.html).

[developers' responsibilities]: /job-families/engineering/backend-engineer/#responsibilities

## Workflow Labels

Labels are described in our [Contribution guide][contrib-labels-guide].

[contrib-labels-guide]: https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md#workflow-labels

## Working with GitLab.com

GitLab.com is a very large instance of GitLab Enterprise Edition. It runs release candidates for new releases, and sees a lot of issues because of the amount of traffic it gets. There are several internal tools available for developers at GitLab to get data about what's happening in the production system:

### Performance Data

There is extensive [monitoring](https://dashboards.gitlab.com/) publicly available for GitLab.com. For more on this and related tools, see the [monitoring handbook](/handbook/engineering/monitoring).

More details on [GitLab Profiler](http://redash.gitlab.com/dashboard/gitlab-profiler-statistics) are also found in the [monitoring performance handbook](/handbook/engineering/monitoring).

### Error Reporting

- [Sentry](https://sentry.gitlap.com/) is our error reporting tool
- [log.gitlab.net](https://log.gitlab.net/) has production logs
- [prometheus.gitlab.com](https://prometheus.gitlab.com/alerts) has alerts for the [production team](/handbook/engineering/infrastructure/#production-team)

### Feature Flags

If you've built feature flags into your code, be sure to read about [how to use the feature flag to test a feature on GitLab.com](https://docs.gitlab.com/ee/development/feature_flags.html).

## Scheduling Issues

GitLab Inc has to be selective in working on particular issues. We have a limited capacity to work on new things. Therefore, we have to schedule issues carefully.

Product Managers are responsible for scheduling all issues in their respective [product areas](/handbook/product/categories/#devops-stages), including features, bugs, and tech debt. Product managers alone determine the [prioritization](/handbook/product/#prioritization). The UX Lead and Engineering Leads are responsible for allocating people making sure things are done on time. Product Managers are _not_ responsible for these activities, they are not project managers.

Direction issues are the big, prioritized new features for each release. They are limited to a small number per release so that we have plenty of capacity to work on other important issues, bug fixes, etc.

If you want to schedule an `Accepting merge requests` issue, please remove the label first.

Any scheduled issue should have a team label assigned, and at least one type label.

### Requesting Something to be Scheduled

To request scheduling an issue, ask the [responsible product manager](/handbook/product/categories/#devops-stages)

We have many more requests for great features than we have capacity to work on.
There is a good chance we’ll not be able to work on something.
Make sure the appropriate labels (such as `customer`) are applied so every issue is given the priority it deserves.

## Product Development Timeline

[![](gitlab-release-timelines.png)](https://gitlab.com/gitlab-org/gitlab-ce/snippets/1731455)

Teams (Product, UX, Engineering) continually work on issues according to their respective workflows.
There is no specified process whereby a particular person should be working on a set of issues in a given time period.
However, there are specific deadlines that should inform team workflows and prioritization. Suppose we are talking about milestone `m` that will be shipped in month `M` (on the 22nd).
We have the following deadlines:

- By month `M-1, 1st`:
  - Release scope is finalized. In-scope issues marked with milestone `m`.
- On month `M-1, 8th` (or next business day): 
  - Kickoff call
  - Release post (WIP merge request) created
- On or around `M, 15th`: [team retrospectives](/handbook/engineering/management/team-retrospectives) should happen so they can inform the [public retrospective](#retrospective)
- By month `M, 17th`: 
  - Completed `m` issues with docs have been merged into master.
  - Individual [release post entries](/handbook/marketing/blog/release-posts/index.html#contributing-to-a-section) merged for all relevant issues.
  - Un-started or unfinished `m` issues are de-scoped from `m`, with `m` being removed from them. These issues should be considered for the next release, rather
- By month  `M, 18th`: Merge latest master into release post branch, aggregate release post entries
- On month `M, 22nd`: Release shipped to production. Release post published.
  - The next Release has been tentatively planned by Product and has been shared with engineering for discussion.
- On month `M, 23rd`: Milestone `m` is marked as close (see [Milestone Cleanup](#milestone-cleanup)). The patch release process for milestone `m` starts. This includes regular and security patch releases.
  - All unfinished `m` issues and merge requests are automatically moved to milestone `m+1`, with the exception of `~security` issues.
- On or around `M, 26th`: The [public retrospective](#retrospective) is held.

Refer to [release post due dates](/handbook/marketing/blog/release-posts/#due-dates) for additional deadlines.

Note that release timelines are overlapping. For example, when a release is shipped to production on the 22nd, the scope for the following release has already been established earlier in that same month.

Engineers should create and merge in the docs as part of completing an issue by the 7th.

Refer to [Feature freeze on the 7th for the release on the 22nd](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/PROCESS.md#feature-freeze-on-the-7th-for-the-release-on-the-22nd) for further timeline details of code releases, including major/minor version releases, as well as patch releases.

## Updating Issues Throughout Development

Team members use labels to track issues throughout development. This gives visibility to other developers, product managers, and designers, so that they can adjust their plans during a monthly iteration. An issue should follow these stages:

- `workflow::In dev`: A developer indicates they are developing an issue by applying the `In dev` label.
- `workflow::In review`: A developer indicates the issue is in code review and UX review by removing the `In dev` label, and applying the `In review` label.
- `workflow::verification`: A developer indicates that all the development work for the issue has been done and is waiting to be deployed and verified.

When the issue has been verfied and everything is working, it can be closed.

## Kickoff

At the beginning of each release, we have a kickoff meeting, publicly livestreamed to YouTube. In the call, the Product Development team (PMs, Product Designers, and Engineers) communicate with the rest of the organization which issues are in scope for the upcoming release. The call is structured by [product area](/handbook/product/categories/#devops-stages) with each PM leading their part of the call.

The notes are available in a publicly-accessible [Google doc](https://docs.google.com/document/d/1ElPkZ90A8ey_iOkTvUs_ByMlwKK6NAB2VOK5835wYK0/edit?usp=sharing). Refer to the doc for details on viewing the livestream.

## Retrospective

After each release, we have a retrospective meeting, publicly livestreamed to YouTube. We discuss what went well, what went wrong, and what we can improve for the next release.

The format for the retrospective is as follows. The notes for the retrospective
are kept in a publicly-accessible [Google
doc](https://docs.google.com/document/d/1nEkM_7Dj4bT21GJy0Ut3By76FZqCfLBmFQNVThmW2TY/edit?usp=sharing).
In order to keep the call on time and to make sure we leave ample room to
discuss how we can improve, the moderator may move the meeting forward with the
timing indicated:

1. **How we improved since last month.** 2 minutes. The moderator will review
   the improvements we identified in the last retrospective and discuss progress
   on those items.
1. **What went well this month.** 5 minutes. Teams are encouraged to celebrate
   the ways in which we exceeded expectations either individually or as a team.
1. **What went wrong this month.** 5 minutes. Teams are encouraged to call out
   areas where we made mistakes or otherwise didn't meet our expectations as a
   team.
1. **How can we improve?** 18 minutes. Teams are encouraged to discuss the
   lessons we learned in this release and how we can use those learnings to
   improve. Any action items should be captured in a GitLab issue so they can
   receive adequate attention before the next release.

The purpose of the retrospective is to help Engineering at GitLab learn and
improve as much as possible from every monthly release. In line with our value
of [transparency](/handbook/values/#transparency), we livestream the meeting to
YouTube and monitor chat for questions from viewers. Please check the
[retrospective notes](https://docs.google.com/document/d/1nEkM_7Dj4bT21GJy0Ut3By76FZqCfLBmFQNVThmW2TY/edit?usp=sharing)
for details on joining the livestream.

### Triaging retrospective improvements

At the end of each retrospective the [Engineering Productivity team](/handbook/engineering/quality/#engineering-productivity-team) is responsible for triaging improvement items identified from the retrospective. 
This is needed for a single owner to be aware of the bigger picture technical debt and backstage work. The actual work can be assigned out to other teams or engineers to execute.

## Milestone Cleanup

Engineering Managers are responsible for capacity planning and scheduling for their respective teams with guidance from their counterpart Product Managers.

To ensure hygiene across Engineering, we will close out a milestone when it has expired.

When a milestone is closed, automatic grooming of unfinished work (open issues and merge requests) associated with the expired milestone will be moved to the next milestone.
Incomplete issues with `~Deliverable` is also labelled with `~missed-deliverable`.
This is currently implemented as part of our [automated triage operations](https://gitlab.com/gitlab-org/quality/triage-ops/blob/master/policies/move-milestone-forward.yml).

The milestone cleanup is currently applied to the following projects:
* [GitLab CE](https://gitlab.com/gitlab-org/quality/triage-ops/pipeline_schedules/10512/edit)
* [GitLab EE](https://gitlab.com/gitlab-org/quality/triage-ops/pipeline_schedules/10515/edit)
* [GitLab Runner](https://gitlab.com/gitlab-org/quality/triage-ops/pipeline_schedules/29681/edit)
* [GitLab Gitaly](https://gitlab.com/gitlab-org/quality/triage-ops/pipeline_schedules/29054/edit)
* [GitLab QA](https://gitlab.com/gitlab-org/quality/triage-ops/pipeline_schedules/29050/edit)

## Kickoff and Retrospective Livestream Instructions

Before the meeting starts, remind people who plan to speak to join the Google Hangout earlier, since there is a 50 user limit.

Several minutes before the scheduled meeting time, follow the [livestreaming instructions](/handbook/communication/youtube/#how-to) to start a Google Hangout using the `Now` setting. Paste the Google Hangout invite link in the Google doc.

At the scheduled meeting time, start broadcasting live to YouTube. Begin the meeting.

## Use Group Labels and Group Milestones

When working in GitLab (and in particular, the GitLab.org group), use group labels and group milestones as much as you can. It is easier to plan issues and merge requests at the group level, and exposes ideas across projects more naturally. If you have a project label, you can promote it to a group milestone. This will merge all project labels with the same name into the one group label. The same is true for promoting group milestones.

## Technical debt

We definitely don't want our technical debt to grow faster than our code base. To prevent this from happening we should consider not only the impact of the technical debt but also a contagion. How big and how fast is this problem going to be over time? Is it likely a bad piece of code will be copy-pasted for a future feature? In the end, the amount of resources available is always less than amount of technical debt to address.

To help with prioritization and decision-making process here, we recommend thinking about contagion as an interest rate of the technical debt. There is [a great comment](http://disq.us/p/1ros2o9) from the internet about it:

> You wouldn't pay off your $50k student loan before first paying off your $5k credit card and it's because of the high interest rate. The best debt to pay off first is one that has the highest loan payment to recurring payment reduction ratio, i.e. the one that reduces your overall debt payments the most, and that is usually the loan with the highest interest rate.

## Security is everyone's responsibility

[Security](/security/) is our top priority. Our Security Team is raising the bar on security every day to protect users' data and make GitLab a safe place for everyone to contribute. There are many lines of code, and Security Teams need to scale. That means shifting security left in the [Software Development LifeCycle (SDLC)](/stages-devops-lifecycle/). Being able to start the security review process earlier in the software development lifecycle means we will catch vulnerabilities earlier, and mitigate identified vulnerabilities before the code is merged. We are fixing the obvious security issues before every merge, and therefore, scaling the security review process. Our workflow includes a check and validation by the reviewers of every merge request, thereby enabling developers to act on identified vulnerabilities before merging. As part of that process, developers are also empowered to reach out to the Security Team to discuss the issue at that stage, rather than later on, when mitigating vulnerabilities becomes more expensive. After all, security is everyone's job. See also our [Security Paradigm](/direction/secure/#security-paradigm)
