---
layout: markdown_page
title: "Secure UX"
---

## Overview
We’re designing an experience that enables contributors to commit their most secure work. This is done by merging security into the DevOps process, giving development teams more ownership, commonly referred to as DevSecOps. The experience brings cross-functional stakeholders together to make better, faster, and more secure-oriented decisions. We are doing this by focusing the experience on automation, education, empowerment, and shifting security to the left.

**Automation** relates to [convention over configuration](/handbook/product/#convention-over-configuration) that helps draw a clear path for the user to produce meaningful results. When it comes to web security, no application will ever be 100% secure. That’s why we are focused on integrating automation into every step of the user’s journey, taking the guesswork out of configuration to open up more time on what’s important: resolving vulnerabilities.

**Education** for our users so they understand security basics and are aware of security needs in their applications. We want our users to know where vulnerabilities have been detected, visualize the implications, present resources to understand the problem, and provide the tools to facilitate informed decisions about next steps.

**Empowerment** for all users to resolve security issues is essential as cross-functional departments share ownership of security. Our tools strive for an experience where the developer is *responsible* and the security team is *accountable* for the organization's security.

**Shifting left** is taking things like QA and other processes typically found later in the ops cycle and moving them to development. Resulting in security problems being addressed early and often.

### Our customer
Organizations of all sizes benefit from our tool and the experience of bringing teams together. We provide customers value with workflow efficiency, informed team decision-making, lower risk of security breaches, and attaining compliance requirements. We focus on all aspects of the product — starting with the customer experience. When deciding to use our tool, organizations are often considering the following:

* What languages does the tool support?
* What tests do we need to cover?
* What tests does the tool cover?
* Can it be automated?
* How long will setup take?
* What does setup involve?
* How easy is it to use?
* What technologies do you need to use? ex. Docker, Kubernetes
* How lightweight is the tool?
* How does it integrate with our tools?
* What customer support is offered?
* What are the upcoming features? (we are selling contracted services vs monthly)


### Our user
We have different user types we consider in our experience design effort. Even when a user has the same title, their responsibilities may vary by organization size, department, org structure, and role. Here are some of the people we are serving:

* [Software Developer](/handbook/marketing/product-marketing/roles-personas/#sasha-software-developer)
* [Development Tech Lead](/handbook/marketing/product-marketing/roles-personas/#delaney-development-team-lead)
* [DevOps Engineer](/handbook/marketing/product-marketing/roles-personas/#devon-devops-engineer)
* [Security Analyst](/handbook/marketing/product-marketing/roles-personas/#sam-security-analyst)
* Compliance Associate
* Security Engineer
* Chief Information Security Officer

### Our baseline experience
##### Primary Jobs to be done (JTBD)
* **Interacting with vulnerabilities in the MR:** When committing changes to my project, I want to be made aware if I am adding risk through vulnerable code, so that I know my changes can be merged without increasing the risk of my project.
     * Current walkthrough: [view issue](https://gitlab.com/gitlab-org/gitlab-design/issues/400)
     * Recent recommendation: [view issue](https://gitlab.com/gitlab-org/gitlab-design/issues/479)
* **Interacting with vulnerabilities in the security dashboard:** When reviewing vulnerabilities for multiple projects, I want to see them all in one location, so that I can prioritize my efforts to resolve or tirage them while seeing the larger picture..
     * Current walkthrough:  [view issue](https://gitlab.com/gitlab-org/gitlab-design/issues/401)
     * Recent recommendation: [view issue](https://gitlab.com/gitlab-org/gitlab-design/issues/460)
* **Managing licenses (user that is responsible):** When new licenses are added to a project I want to be aware so I can commit work that is compliant with my organization's rules.
     * Current walkthrough: [view issue](https://gitlab.com/gitlab-org/gitlab-design/issues/478)
     * Recent recommendation:[view issue](https://gitlab.com/groups/gitlab-org/-/epics/1618)
* **Managing licenses (user that is accountable):** When my organization has license compliance rules to follow I want to be able to whitelist or blacklist licenses so that I can ensure any new code merged in a project is in compliance.
     * Current walkthrough: [view issue](https://gitlab.com/gitlab-org/gitlab-design/issues/402)
     * Recent recommendation:[view issue](https://gitlab.com/groups/gitlab-org/-/epics/1618)
* **Vulnerability check (user that is responsible):** When a merge request is disallowed, I want to know why, so I can resolve the issue and proceed with the MR.
     * Current walkthrough: [view issue](https://gitlab.com/gitlab-org/gitlab-design/issues/534)
     * Recent recommendation: *in progress*
* **Vulnerability check (user that is accountable):** When new vulnerabilities are detected in a merge request, I want to disallow the merge request, so the team can review the vulnerabilities to resolve or decide on the next steps.
     * Current walkthrough: [view issue](https://gitlab.com/gitlab-org/gitlab-design/issues/533)
     * Recent recommendation: *in progress*
* **Setup with Auto DevOps:** onboarding for new teams
     * Current walkthrough: *in progress*
     * Recent recommendation: *in progress*
* **Setup w/o Auto DevOps:** onboarding for new teams
     * Current walkthrough: *in progress*
     * Recent recommendation: *in progress*

### Our team
Our team continues to grow. We currently have 5 members that contribute to Secure UX efforts:

* [Valerie Karnes](https://gitlab.com/vkarnes) - UX Manager
* [Andy Volpe](https://gitlab.com/andyvolpe) - Product Designer, Static and Dynamic Analysis
* [Kyle Mann](https://gitlab.com/kmann) - Sr. Product Designer, Composition Analysis
* [Tali Lavi](https://gitlab.com/tlavi) - UX Researcher
* [Lorie Whitaker](https://gitlab.com/loriewhitaker) - Sr. UX Research
* [Becka Lippert](https://gitlab.com/beckalippert) - Product Designer, will transition to Defend
* New Designer (starting September) - Product Designer, Secure
* New Designer (starting October) - Product Designer, Secure

Our team meetings:
* Secure UX weekly meeting on Wednesdays at 10:30am EST. Meeting to discuss our stages UX shared efforts, review designs, and iterate on our strategy.
* Product Design and Secure PMs bi-weekly on Wednesdays at 10:00am EST
* Product Design does a bi-weekly grooming session on Tuesdays at 10:30am EST

### Our Structure
We've divided the Secure stage into dedicated experience groups to align with a similar [split](/handbook/product/categories/#secure-stage) undertaken by our engineering and PM counterparts.

| Experience Group |  Security Scanning & Testing | Compliance & Auditing | Vulnerability Management | Status, Reporting & Metrics | IA & Core-functionality |
| ------ | ------ | ------ | ------ | ------ | ------ |
| Engineering Group | Static & Dynamic Analysis | Software Composition Analysis | Shared | Shared  | Shared |
| Dedicated Designer | Andy Volpe | Kyle Mann | Shared | Shared | Shared |

This segmentation gives us a better opportunity to:
- Grow our expertise and knowledge within our subgroup while sharing relevant information with the rest of the team.
- Evolve and maintain relationships with our dedicated engineering team and PMs.
- Serve as the known main point of contact.
- Deeply understand our users' needs by initiating and/or leading research activities specific to our experience group.
- Focus on iterating and progressing our experiences from MVC to Lovable.

#### Workflow adjustments
**Labeling issues**

The best way to implement this is through the use of labels. We created 3 scoped labels to help us identify which experience group a particular issue falls into and which designer should be subsequently assigned.

`Secure UX::Shared`
- Issues relating to Vulnerability management • Status, Reporting & Metrics • IA and Core Functionality.
- Examples: Adding Secure to the left nav, Inline vulnerability management, Dashboard designs.
- Who to ping: If no designer is assigned: Both designers. If a designer is assigned, ping them in the issue.

`Secure UX::Security Testing & Scanning`
- Issues relating to security testing, scanning, and detection of vulnerabilities or weaknesses.
- Examples: MR Widget security report design, Secret detection, DAST - list of scanned URLs in MR.
- Who to ping: Andy Volpe

`Secure UX::Compliance & Auditing`
- Issues relating to features that support Compliance and/or Auditing activities.
- Examples, Security gates, License Management, Dependency list.
- Who to ping: Kyle Mann

**Grooming, planning, and assignments**

Secure UX has a separate grooming session which takes place during the planning phase of a milestone. During grooming, we add the proper label to all issues requiring UX support.

Read more about how we've created these dedicated experience groups [here.](https://gitlab.com/gitlab-org/gitlab-design/issues/458)

### Our strategy
The Secure UX team is working together to [uncover customers core needs](https://gitlab.com/groups/gitlab-org/-/epics/1611), what our users’ workflows looks like, and defining how we can make our users tasks easier. Our strategy involves the following actions:

* [Baseline initiative audit and recommendations](/handbook/engineering/ux/experience-baseline-recommendations/) (quarterly)
* Internal understanding: stakeholder interviews (annually)
* Iterating on Secure product [foundation's document](https://gitlab.com/gitlab-org/gitlab-design/issues/333) (ongoing)
* Performing heuristic evaluations on at least 3 competitors, based competitors the 3 user type is using (annually, ongoing)
* We talk to our customer (ongoing)
* We talk to our users (ongoing)
* We outline current user workflows and improve them (upcoming, ongoing)

Additionally, we value the following:
* Testing our features with usefulness and usability studies
* Drinking our own wine and partnering closely with our internal Security and Compliance teams for feedback and feature adoption
* Partnering with our sales and account team to connect directly with customers and learn why customers did (or didn’t) choose our product
* Collaborating with stakeholders on proof of concept discoveries to better understand future consideration
* Prioritizing issues that are likely to increase our number of active users

The source of truth lives with shipped features, therefore we:
* Make data-driven decisions quickly and thoughtfully
* Optimize to deliver our solutions as soon as possible
* Learn, iterate, test, and repeat
