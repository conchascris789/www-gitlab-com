---
title: "GitLab's Functional Group Updates: November 6th - November 17th" # replace "MMM" with the current month, and "DD-DD" with the date range
author: Chloe Whitestone
author_gitlab: chloemw
author_twitter: drachanya
categories: company
image_title: '/images/blogimages/functional-group-update-blog-cover.jpg'
description: "The Functional Groups at GitLab give an update on what they've been working on"
tags: inside GitLab, functional group updates
---

<!-- beginning of the intro - leave it as is -->

Every day from Monday to Thursday, right before our [GitLab Team call](/handbook/#team-call), a different Functional Group gives an [update](/handbook/people-operations/group-conversations/) to our team.

The format of these calls is simple and short where they can either give a presentation or quickly walk the team through their agenda.

<!-- more -->

<!-- end of the intro -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Backend Team

[Presentation slides](https://docs.google.com/presentation/d/1RmTsArGwILwEUlEmRafSNDKLzwMXSjVX3BXcnBvMhjk/edit)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/6E3cnWdfoMg" frameborder="0" allowfullscreen="true"></iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Product Team

[Presentation slides](https://www.slideshare.net/JobvanderVoort/product-update-nov-7)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/hMCSSQ4FzR8" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### People Ops Team

[Presentation slides](https://docs.google.com/presentation/d/1tomqiks--DvLh7NuhgcDVQUrqL8pAxeWsuTCCYQxOwY/edit#slide=id.g156008d958_0_18)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/vwuFohVhnRY" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Discussion Team

[Presentation slides](http://gitlab-org.gitlab.io/group-conversations/backend-discussion/)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/SYrNBu4b81A" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

Questions? Leave a comment below or tweet [@GitLab](https://twitter.com/gitlab)! Would you like to join us? Check out our [job openings](/jobs/)!
