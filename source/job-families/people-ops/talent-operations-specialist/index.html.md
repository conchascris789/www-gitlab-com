---
layout: job_family_page
title: "Talent Operations Specialist"
---

## Responsibilities

* Manage vendor relationships with partner accounts
* Lead the implementation and optimization of our ATS and other recruiting tools
* Design and monitor key metrics to evaluate the effectiveness of GitLab's employment practices
* Pull recruiting metrics reports on a weekly, monthly, and quarterly basis
* Pull any ad-hoc reports, some examples: stats for a specific role, source data, salary data, archive reasons, referral data
* Develop recommendations by leveraging data from our ATS and market research
* Review and act on candidate and hiring manager survey data 
* Lead demos for potential new services, as well as the research and implementation of new services
* Optimize Recruiting Tools and Processes 
* Partner with Finance to optimize Hiring Plan and Gearing Ratios for recruiter capacity 
* Develop new hire and ongoing training for our recruiting team 
* Deliver hiring manager and interview training
* Assist with initiatives to enhance current programs (Onboarding, Employee Referral program, Internal Movement, etc.)

## Requirements

* 2 + years experience in Operations supporting Recruiting, Human Resources, or People Operations 
* Experience in a startup a plus
* Experience working remotely is a plus
* Proven ability to multitask and prioritize workload
* Excellent communication and interpersonal skills
* Demonstrated ability to work in a team environment and work collaboratively across the organization
* Proficient in Google Suite
* Willingness to learn and use software tools including Git and GitLab, prior experience with GitLab is a plus.
* Organized, efficient, and proactive with a keen sense of urgency
* Ability to recognize and appropriately handle highly sensitive and confidential information
* Prior experience using an applicant tracking system (ATS), such as Greenhouse, is a plus
* Prior experience using a human resources information system (HRIS), such as BambooHR, is a plus
* Ability to recognize and appropriately handle highly sensitive and confidential information
* You share our values, and work in accordance with those values
* Successful completion of a background check
