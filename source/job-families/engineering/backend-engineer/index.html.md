---
layout: job_family_page
title: "Backend Engineers"
---

# Backend Engineering Roles at GitLab

Backend Engineers at GitLab work on our product. This includes both the open source version of GitLab, the enterprise editions, and the GitLab.com service as well. They work with peers on teams dedicated to areas of the product. They work together with product managers, designers, and frontend developers to solve common goals.

Unless otherwise specified, all Backend Engineering roles at GitLab share the following
requirements and responsibilities:

<a id="intermediate-requirements"></a>
#### Requirements
* Significant [professional experience](#professional-experience) with Ruby on Rails **or** [language](#primary-programming-language) required by the [specialty](#specialties)
* Professional experience with any other technologies that may be required by the [specialty](#specialties)
* Proficiency in the English language, both written and verbal, sufficient for success in a remote and largely asynchronous work environment
* Demonstrated capacity to clearly and concisely communicate about complex technical, architectural, and/or organizational problems and propose thorough iterative solutions
* Experience with performance and optimization problems and a demonstrated ability to both diagnose and prevent these problems
* Comfort working in a highly agile, [intensely iterative][iteration] software development process
* Demonstrated ability to onboard and integrate with an organization long-term
* Positive and solution-oriented mindset
* Effective communication skills: [Regularly achieve consensus with peers][collaboration], and clear status updates
* An inclination towards communication, inclusion, and visibility
* Experience owning a project from concept to production, including proposal, discussion, and execution.
* [Self-motivated and self-managing][efficiency], with strong organizational skills.
* Demonstrated ability to work closely with other parts of the organization
* Share [our values][values], and work in accordance with those values
* Ability to thrive in a fully remote organization

[values]: /handbook/values/
[collaboration]: /handbook/values/#collaboration
[efficiency]: /handbook/values/#efficiency
[iteration]: /handbook/values/#iteration

#### Nice-to-haves

* Experience in a peak performance organization, preferably a tech startup
* Experience with the GitLab product as a user or contributor
* Product company experience
* Experience working with a remote team
* Enterprise software company experience
* Developer platform/tool industry experience
* Experience working with a global or otherwise multicultural team
* Computer science education or equivalent experience
* Passionate about/experienced with open source and developer tools

#### Responsibilities
* Develop features and improvements to the GitLab product in a secure,
  well-tested, and performant way
* Collaborate with Product Management and other stakeholders within Engineering (Frontend, UX, etc.) to maintain a high bar for quality in a fast-paced, iterative environment
* Advocate for improvements to product quality, security, and performance
* Solve technical problems of moderate scope and complexity. 
* Craft code that meets our internal standards for style, maintainability, and best practices for a high-scale web environment. Maintain and advocate for these standards through code review.
* Recognize impediments to our efficiency as a team ("technical debt"), propose and implement solutions
* Represent GitLab and its values in public communication around specific projects and community contributions. 
* Confidently ship small features and improvements with minimal guidance and support from other team members. Collaborate with the team on larger projects.

***

#### Professional Experience

At GitLab, due to the pace we're moving, people joining our team will
be expected to have **significant** professional experience in the 
[primary programming language](#primary-programming-language) of the 
[specialty](#specialties) that they'll be joining. Because the majority of our
codebase is written in Ruby, even in specialized roles, a knowledge of Ruby
and Ruby on Rails or a desire to learn and contribute is necessary.

Some history on this policy: [https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/2695]()

***

#### Primary programming language

Some [specialties](#specialties) within GitLab are using primary programming
languages other than Ruby. These programming languages are defined as being 
primary in terms of an engineer's experience and what they'll be writing day
to day. The expecation from the specialty is that engineers will have extensive 
experience in this primary programming language as well as some level of 
understanding of Ruby and, Ruby on Rails.

## Levels

Read more about [levels](/handbook/hiring/#definitions) at GitLab here.

### Junior Backend Engineer

Junior Backend Engineers share the same requirements and responsibilities outlined above, but typically join with less or alternate experience than a typical Backend Engineers.

### Senior Backend Engineer

The Senior Backend Engineer role extends the [Backend Engineer](#intermediate-requirements) role.

#### Responsibilities

* Advocate for improvements to product quality, security, and performance that have particular impact across your team.
* Solve technical problems of high scope and complexity. 
* Exert influence on the overall objectives and long-range goals of your team.
* Experience with performance and optimization problems, particularly at large scale, and a demonstrated ability to both diagnose and prevent these problems
* Help to define and improve our internal standards for style, maintainability, and best practices for a high-scale web environment. Maintain and advocate for these standards through code review.
* Represent GitLab and its values in public communication around broader initiatives, specific projects, and community contributions. 
* Provide mentorship for Junior and Intermediate Engineers on your team to help them grow in their technical responsibilities and remove blockers to their autonomy.
* Confidently ship moderately sized features and improvements with minimal guidance and support from other team members. Collaborate with the team on larger projects.

***

A Senior Backend Engineer may want to pursue the [engineering management track](/job-families/engineering/backend-engineer/#engineering-manager) at this point. See [Engineering Career Development](/handbook/engineering/career-development) for more detail.

**Note:** Staff and above positions at GitLab are more of a role than just a "level". We prefer to bring people in as Senior and let the team elevate them to Staff due to an outstanding work history within GitLab.

***

### Staff Backend Engineer

The Staff Backend Engineer role extends the [Senior Backend Engineer](#senior-backend-engineer) role.

#### Responsibilities

* Advocate for improvements to product quality, security, and performance that have particular impact across your team and others.
* Solve technical problems of the highest scope and complexity for your team.
* Exert significant influence on the overall objectives and long-range goals of your team.
* Shepherd the definition and improvement of our internal standards for style, maintainability, and best practices for a high-scale web environment. Maintain and advocate for these standards through code review.
* Drive innovation on the team with a willingness to experiment and to boldly confront problems of immense complexity and scope.
* Actively seek out difficult impediments to our efficiency as a team ("technical debt"), propose and implement solutions that will enable the entire team to iterate faster
* Represent GitLab and its values in public communication around broad initiatives, specific projects, and community contributions. Interact with customers and other external stakeholders as a consultant and spokesperson for the work of your team. 
* Provide mentorship for all Engineers on your team to help them grow in their technical responsibilities and remove blockers to their autonomy.
* Confidently ship large features and improvements with minimal guidance and support from other team members. Collaborate with the team on larger projects.

### Distinguished Backend Engineer

The Distinguished Backend Engineer role extends the [Staff Backend Engineer](#staff-backend-engineer) role.

* At this level the person's contribution plays to their strength and role on the team. These contributions come in different forms such as: Ship _large_ feature sets with team, completes _feature discovery_ independently, publishes technical blogs and speaks at conferences, interfaces with customers and provides technical direction to stakeholders (Product, Sales, others)
* _Generate_ technical and process improvements
* Contribute to the sense of psychological safety on your team
* Work cross-departmentally
* Be a technical mentor for other backend engineers
* Author architecture documents for epics
* Hold team members accountable within their roles

### Engineering Fellow

The Engineering Fellow role extends the [Distinguished Backend Engineer](#distinguished-backend-engineer) role.

* Advocate for improvements to product quality, security, and performance that impact all of Engineering at GitLab.
* Solve technical problems of the highest scope and complexity for the entire
  organization.
* Exert significant influence on the overall objectives and long-range goals of GitLab.
* Ensure that our standards for style, maintainability, and best practices are suitable for the unique problems of scale and diversity of use represented by the GitLab product. Maintain and advocate for these standards through code review.
* Drive innovation across Engineering with a willingness to experiment and to boldly confront problems of immense complexity and scope.
* Actively seek out and prioritize our toughest technical challenges with a goal of creating significant improvement for GitLab's use, ease of development, and/or technical efficiency.
* Represent GitLab and its values in public communication in all aspects of our software development lifecycle and public relations. Interact with customers and other external stakeholders as a consultant and spokesperson for critical projects and aspects of our technical architecture.
* Provide mentorship for Senior and Staff Engineers at the company to help them grow in their technical responsibilities and to share your great expertise across the organization.
* Confidently ship immense or otherwise extremely high-impact features and improvements with minimal guidance and support from other members of the organization.
* Help create the sense of psychological safety in the department

## Hiring Process
Candidates for this position can generally expect the hiring process to follow the order below. Note that as candidates indicate preference or aptitude for one or more specialties, the hiring process will be adjusted to suit. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).

* As part of the application, candidates are asked to complete a short technical questionnaire, with a possibility of additional technical questions being asked if needed after the application is submitted. 
* Next, candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/#screening-call) with one of our Technical Recruiters
* Next, candidates will be invited to schedule a 90 minute technical interview with one of our Backend Engineers
* Next, candidates will be invited to schedule a 60 minute interview with one of our Backend Engineering Managers
* Next, candidates will be invited to schedule a 60 minute interview with our Director of Engineering
* Successful candidates will subsequently be made an offer.
Additional details about our process can be found on our [hiring page](/handbook/hiring).


## Engineering Management Roles at GitLab

Managers in the engineering department at GitLab see the team as their product. While they are technically credible and know the details of what backend engineers work on, their time is spent hiring a world-class team and putting them in the best position to succeed. They own the delivery of product commitments and are always looking to improve productivity. They must also coordinate across departments to accomplish collaborative goals.

### Engineering Manager

Unless otherwise specified below, all Engineering Manager roles at GitLab share
the following requirements and responsibilities:

#### Responsibilities

* Help your engineers grow their skills and experience
* Author project plans for epics
* Run agile project management processes
* Conduct code reviews, and make technical contributions to product
  architecture as well as getting involved in solving bugs and delivering small
  features
* Actively seek and hire globally-distributed talent
* Conduct managerial interviews for candidates, and train the team to do technical interviews
* Contribute to the sense of psychological safety on your team
* Generate and implement process improvements
* Hold regular [1:1's](/handbook/leadership/1-1/) with all members of their team
* Give regular and clear feedback around the [individual's performance](/handbook/leadership/1-1/suggested-agenda-format)
* Foster technical decision making on the team, but make final decisions when necessary
* Draft quarterly OKRs and [Engineering KPIs](/handbook/business-ops/data-team/metrics/#engineering-kpis)
* Improve product quality, security, and performance

#### Requirements

* Exquisite communication: Regularly achieve consensus amongst departments
* 5 years or more experience in a leadership role with current technical experience
* In-depth experience with Ruby on Rails, Go, and/or Git, in addition to any
  experience required by the position's [specialty](#specialties)
* Excellent written and verbal communication skills
* You share our [values](/handbook/values), and work in accordance with those values

#### Nice-to-haves

* Experience in a peak performance organization
* Deep Ruby on Rails experience
* Golang experience
* Product company experience
* Startup experience
* Enterprise software company experience
* Computer science education or equivalent experience
* Passionate about open source and developer tools

#### Hiring Process

Candidates for this position can generally expect the hiring process to follow the order below. Note that as candidates indicate preference or aptitude for one or more [specialties](#specialties), the hiring process will be adjusted to suit. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).

* Selected candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/#screening-call) with one of our Technical Recruiters
* Next, candidates will be invited to schedule a 60 minute first interview with a Director of Engineering, Backend
* Next, candidates will be invited to schedule a 45 minute second peer interview with an Engineering Manager
* Next, candidates will be invited to schedule a 45 minute third interview with one or more members of the Engineering team
* Next, candidates will be invited to schedule a 45 minute fourth interview with our VP of Engineering
* Finally, candidates may be asked to schedule a 50 minute final interview with our CEO
* Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring).

### Director of Engineering

The Director of Engineering role extends the [Engineering Manager](#engineering-manager) role.

#### Must-have Requirements

* Technical credibility: Past experience as a product engineer and leading teams thereof
* Excellent communication skills
* Expert hiring manager skills and experience
* A strong people management philosophy for managers and engineers
* Agile project management skills
* Ability to understand, communicate and improve the quality of multiple teams
* Demonstrate longevity at at least one recent job
* Ability to be successful managing at a remote-only company
* Humble, servant leader

#### Nice-to-have Requirements

* Be a user of GitLab, or familiar with our company
* Prior Developer Platform or Tool industry experience
* Prior product company experience
* Prior high-growth startup experience
* Experience working on systems at massive (i.e. consumer) scale
* Deep open source experience
* Experience working with global teams
* We value diversity and inclusion in leadership
* Be inquisitive: Ask great questions

### Senior Director of Engineering

The Senior Director of Engineering role extends the [Director of Engineering](#director-of-engineering) role.  This role is defined by the functional area(s) the person manages.

* Organizational credibility: Past experience in managing an entire functional area of Engineering
* Prioritzation of hiring efforts to focus on areas of most need and quickly recruit top engineering talent
* Motivate and communicate across multiple levels of their department
* Have successful peer partnerships with other department leaders in Engineering, and cross-functionally (Product Management, sales, marketing, alliances, etc)
* Provide a consistent/successful interface between Engineering Development and Product Management 
* Development, measurement, and management of key metrics for functional area's performance
* Drive high throughput
* Standardize the development process where needed, allow local differences where advantages 
* Help shift the organization toward CD over time

### VP of Engineering

The VP of Engineering role extends the [Senior Director of Engineering](#senior-director-of-engineering) role.

* Drive recruiting of a world class team
* Help their directors, managers, and engineers grow their skills and experience
* Measure and improve the happiness of engineering
* Make sure the handbook is used and maintained in a transparent way
* _Sponsor_ technical and process improvements
* _Own_ the sense of psychological safety of the department
* _Set_ quarterly OKRs around company goals
* _Define_ the agile project management process
* Spend time with customers to understand their needs and issues
* _Be accountable for_ product quality, security, and performance

## Specialties

Read more about what a [specialty](/handbook/hiring/#definitions) is at GitLab here.

### Distribution

The Distribution team closely partners with our greater engineering organization to build, configure and automate GitLab deployments.
GitLab's distribution team is tasked with creating a seamless installation and upgrade experience for users across a multitude of platforms.

Distribution engineering regularly interfaces with broader development teams in supporting newly created features.
Notably, our infrastructure team is the distribution team's biggest internal customer, so there is significant team interdependency.
The Distribution team is involved with diverse projects and tasks that include assisting community packaging efforts.
This is reflected in the job role:
* Development and maintenance of Cloud Native GitLab deployment tooling and upgrade methods
* Support Omnibus GitLab package installations across multiple Linux based Operating Systems
* Experience with various Cloud provider deployment methods including AWS Cloudformation, GCP Deployment Manager, Azure Resource Manager

#### Requirements
* Experience with Docker and Kubernetes in production use cases
* Chef experience (writing complex cookbooks from scratch, custom providers, custom resources, etc.)
* Extensive Linux experience, comfortable between Debian and RHEL based systems
* Experience building and packaging archives such as .deb and .rpm

### Package

Package engineers are focused on creating the binary repository management
system that will extend our Continuous Integration (CI) functionality to allow access
and management of artifacts manipulated by projects.

By extending the current CI artifacts system, the Package team will expose GitLab as
a package repository allowing access to the most common package managers, e.g.
Maven and APT and similar. Additionally, the Package team is improving
the Container Registry and is responsible for items listed under [Package product category](/handbook/product/categories/#package).

#### Responsibilities
* Develop the architecture by extending existing features
* Work with the Distribution team on replacing their current delivery system
* Create and maintain observability of the newly defined features
* Work with customers on defining their needs to replace existing package repository solutions

### Secure

Focus on security features for GitLab. This role will specifically focus on security; if you want to work with Ruby on Rails and not security, please apply to our Backend Engineer role instead. This role will report to and collaborate directly with the Secure Engineering Manager.

#### Requirements
* Strong Go and/or Ruby engineer with security expertise or proven security interest.
* Passion and interest toward security (scanning, dependencies, etc.).
* Experience in using GitLab and GitLab CI.

#### Responsibilities
* Develop security tools from proposal to polished end result.
* Integrating 3rd party security tools into GitLab.
* Complete our internal Advisories Database.
* Manage metadata related to dependencies.
* Key aspects of this role are focused on security tools and features.
* The complexity of this role will increase over time.
* If you are willing to stick to working on these features for at least a year, then this role is for you.

### Configuration

  The configuration team works on GitLab's Application Control Panel, Infrastructure Configuration features, our ChatOps product, Feature flags, and our entire Auto DevOps feature set. It is part of our collection of Ops Backend teams.

#### Requirements
* Experienced engineer who is capable of leading and growing a team of senior engineers.
* For this position, a significant amount of experience with Ruby is a strict requirement. Experience with Go is a plus as we expect that you will likely work on Go during your journey at GitLab.
* Experience with Docker, Kubernetes platform development.
* Experience or interest in functions-as-a-service.

#### Responsibilities
* Implement and improve upon our constellation of configuration feature set.
* Work with the PM team to execute on the roadmap.
* Ensure we deliver on our commitments to the market by communicating clearly with stakeholders.
* Implement the appropriate monitoring and alerting on new and existing features owned by the team.
* Help others adopt and use the configuration features.

### CI/CD

CI/CD Backend Engineers are primarily tasked with improving the Continuous Integration (CI)
and Continuous Deployment (CD) functionality in GitLab. Engineers should be willing to learn Kubernetes and Container Technology. CI/CD Engineers should always have three goals in mind:
1. Provide value to the user and communicate such with product managers,
2. Introduce features that work at scale and in untrusting environments,
3. Always focus on defining and shipping [the Minimal Viable Change](/handbook/product/#the-minimally-viable-change).

We, as a team, cover end-to-end integration of CI/CD in GitLab, with components being written in Rails and Go.
We work on a scale of processing a few million of CI/CD jobs on GitLab.com monthly.
CI/CD engineering is interlaced with a number of teams across GitLab.
We build new features by following our [direction](/direction/#ci--cd).
Currently, we focus on providing a deep integration of Kubernetes with GitLab:
1. by automating application testing and deployment through Auto DevOps,
1. by managing GitLab Runners on top of Kubernetes,
1. by working with other teams that provide facilities to monitor all running applications,
1. in the future implement A-B testing, feature flags, etc.

Additionally, we also focus on improving the efficiency, performance, and scalability of all aspects of CI/CD:
1. Improve performance of developer workflows, e.g. faster CI testing, by improving parallelization,
1. Improve performance of implementation, ex.:by allowing us to run 10-100x more in one year,
1. Identify and add features needed by us, ex.:to allow us to test more reliably and ship faster.

The CI/CD Engineering Manager also does weekly stand-up with a team and product managers to talk about plan for the work in the upcoming week and coordinates a deployment of CI/CD related services with infrastructure team.

#### Requirements
* Go developer with a lot of Kubernetes production experience is a plus

### Geo

[GitLab Geo](https://docs.gitlab.com/ee/gitlab-geo/README.html)
is an enterprise product feature that speeds up the work of globally distributed
teams, adds redundancy for GitLab instances, and provides Disaster Recovery as
well.

#### Requirements
* Deep experience architecting and implementing fault-tolerant, distributed systems
* Experience building and scaling highly-available systems
* In-depth experience with Ruby on Rails, Go, and/or Git

#### Requirements (Staff Level)
* Architect Geo and Disaster Recovery products for GitLab
* Identify ways to test and improve availability and performance of GitLab Geo at GitLab.com scale
* Instrument and monitor the health of distributed GitLab instances
* Educate all team members on best practices relating to high availability

### Growth

Growth Engineers work with a cross-functional team to influence the growth of
GitLab as a business. In helping us iterate and learn rapidly, these
engineers
enable us to more effectively meet the needs of potential users.

#### Requirements
* Strong self-direction (this team is being bootstrapped)
* Experience with A/B, multivariate, or other data-driven methods of testing
* Comfort multitasking in a highly iterative environment

### Engineering Productivity
Engineering Productivity Engineers are full-stack engineers primarily tasked with improving the productivity of
the GitLab developers (from both GitLab Inc and the rest of the community), and
making the GitLab project maintainable in the long-term.

#### Responsibilities
* Build automated measurements and dashboards to gain insights into Engineering Productivity
to understand what is working and what is not.
* Make suggestions for engineering workflow improvements, monitor the results and iterate.
* Increase contributor and developer productivity by improving the development setup, workflow, processes, and tools.
* Improve the ease of use of our GDK (GitLab Development Kit).
* Improve Review apps for CE/EE (GDK in the cloud).
* Build automated tooling to speed up issue and merge request review and triage.
* Ensure workflow and label hygiene in our system which feeds into our metrics dashboard.
* Build automated tooling to ensure the consistency and quality of the codebase and merge request workflow.
* Help with maintaining GitLab Docs.

#### Requirements
* Experience developing in Ruby (this is a strict requirement).
* Experience in with test automation frameworks for both front-end and back-end testing.
* Experience in designing and developing tools and solutions used across teams.
* Development experience in object-oriented programming languages and patterns.
* Excellent oral and written communication skills.
* Experience with a front-end charting/visualization library.
* Experience using test automation tools. (Selenium, Capybara, Watir).
* Experience using Continuous Integration systems (e.g., GitLab CI, Jenkins, Travis).
* Experience using Docker and containerized architectures (e.g. Kubernetes).

See the description of the [Quality team](/handbook/engineering/quality) for more details.
The position also involves working with the community as
[merge request coach](/job-families/expert/merge-request-coach/), and working together with our
[Backend Engineers](/job-families/engineering/backend-engineer) to respond and
address issues from the community.

#### Engineering Productivity Performance Indicators

Engineering Productivity Engineers have the following job-family performance indicators.

* [Average CE/EE pipeline duration per month](/handbook/engineering/quality/performance-indicators/#average-ce-ee-pipeline-duration-per-month)
* [Successful vs failed CE/EE review app deployments per month](/handbook/engineering/quality/performance-indicators/#successful-vs-failed-ce-ee-review-app-deployments-per-month)
* [Successful vs failed CE/EE master pipelines per month](/handbook/engineering/quality/performance-indicators/#successful-vs-failed-ce-ee-master-pipelines-per-month)
* [New issue first triage SLO](/handbook/engineering/quality/performance-indicators/#new-issue-first-triage-slo)

### Memory

The Memory team is responsible for optimizing GitLab application
performance by managing the memory resources required.  The team is also responsible for changes affecting the responsiveness of the application.

#### Responsibilities
* Identify, troubleshoot, improve and manage memory-intensive aspects of the
  GitLab application.
* Explore alternatives outside of the GitLab application for reducing
  memory consumption through contributions to, e.g., Rails, Ruby, Puma, or other key
  third-party components.
* Define and help implement best practices for creating efficient and performant
  code.
* Setting standards that changes are not affecting memory utilization and can be validated.
* Partner with the Quality team to maintain effective performance reporting and
  monitoring through instrumentation and testing.
  
#### Requirements
An ideal engineer candidate -
* Expert of Ruby on Rails.
* Experience of performance tuning and/or architecture, [Example](https://rubykaigi.org/2018/presentations/tenderlove.html).
* Experience of memory leak troubleshooting, [Example 1](http://www.be9.io/2015/09/21/memory-leak/), [Example 2](https://samsaffron.com/archive/2015/03/31/debugging-memory-leaks-in-ruby).

Alternatively, some of the following qualifications, may not necessarily be all -
1. **Must** be proficient in one or more of the following in preference order
	1. Ruby.
	1. Go.
	1. Similar OOP languages (e.g. Python, C++, Java, C#, etc.).
1. AND/OR proficient in one or more of the following
	1. Proven record of building scalable solutions.
	1. Top notch understanding of DB principles and optimization mechanisms.
	1. Familiar with a framework similar to the concepts of Rails (e.g. CakePHP, Ember, Node.js, Angular, J2EE, etc.) - For reference and apply with due diligence.
	1. High-level principles: has knowledge of existing perf testing tools and test automation with some Ruby.
	1. Low-level principles: Understands internals, how memory works, garbage collection. Sorting algorithms.
1. **Nice to have**
	1. Tuning up performance from architecture/design perspective.
	1. Troubleshooting memory leaks (any language).
	1. Optimizing full stack implementation, e.g. I/O, caching.
	1. Good knowledge of performance testing.
1. Strong problem analysis and solving skills, methodological in problem solving.
1. **Must** be a proven fast-learner and self-starter.

### Ecosystem

The Ecosystem team is responsible for seamless integration between GitLab and 3rd party products as well as making GitLab products available on cloud service providers’ marketplaces such as AWS. The team plays a critical role in developing APIs and SDK and expanding GitLab market opportunities.

#### Responsibilities
* Design, build, and maintain APIs, Webhooks, and SDK of Gitlab products.
* Design, build, and maintain solutions to integrate to partner and 3rd party platforms.
* Design, build, and maintain solutions for integrating with cloud service provider marketplaces, such as AWS.
* Develop documentation and instructions of how to work with GitLab SDK & APIs.
  
#### Requirements
* Previous experience developing REST and/or GraphQL APIs using a variety of technologies.
* Previous experience working with Open API standards such as Swagger.
* Proficient with Ruby.
* Proficient or fluent with one or more of other common languages: Go, Python, Java, Node, JavaScript, etc.
* Familiar with full web technology stack (e.g. HTTP, cookies, asset loading, caching).

### Gitaly

Gitaly is a new service in our architecture that handles git and other filesystem operations for GitLab instances, and aims to improve reliability and performance while scaling to meet the needs of installations with thousands of concurrent users, including our site GitLab.com. This position reports to the Gitaly Lead.

#### Responsibilities
* Participate in architectural discussions and decisions surrounding Gitaly.
* Scope, estimate and describe tasks to reach the team’s goals.
* Collaborate on designing RPC interfaces for the Gitaly service
* Instrument, monitor and profile Gitaly in the production environment.
* Build dashboards and alerts to monitor the health of your services.
* Conduct acceptance testing of the features you’ve built.
* Educate all team members on best practices relating to high availability.

#### Requirements:
* Mandatory: production experience building, debugging, optimising software in large-scale, high-volume environments.
* Mandatory: Solid production Ruby experience.
* Highly desirable: Experience working with Go. It’s important that candidates must be willing to learn and work in both Go and Ruby.
* Highly desirable: experience with gRPC.
* Highly desirable: a good understanding of git’s internal data structures or experience running git servers. You can reason about software, algorithms, and performance from a high level.
* Understanding of how to build instrumented, observable software systems.
* Experience highly-available systems in production environments.

### Meltano (BizOps Product)

[Meltano](https://gitlab.com/meltano/meltano) is an early stage project at GitLab focused on delivering an open source framework for analytics, business intelligence, and data science. It leverages version control, data science tools, CI, CD, Kubernetes, and review apps.

A Meltano Engineer will be tasked with executing on the vision of the Meltano project, to bring the product to market.

#### Requirements
* A passion for data science and analytics
* Experience with doing initial prototyping, architecture, and engineering work
* In-depth experience with Python (no Ruby or Rails experience required for this
  role)
* Experience with Kubernetes, Helm, and CI/CD is a **strict requirement**

### Database

A database specialist is an engineer that focuses on database related changes
and improvements. You will spend the majority of your time making application
changes to improve database performance, availability, and reliability; though
you will also spend time working on the database infrastructure that powers
GitLab.com.

Unlike the [Database Engineer](/job-families/engineering/database-engineer/) position the database
specialist title focuses more on application development and less on knowledge
of PostgreSQL. As such Ruby knowledge is absolutely required, but the
requirements for PostgreSQL knowledge / experience are less strict compared to
the Database Engineer position.

#### Example Projects

* Rewriting the database queries and related application logic used for retrieving [subgroups](https://docs.gitlab.com/ee/user/group/subgroups/index.html#subgroups)
* Rewriting code used for importing projects from other platforms (e.g. [GitHub](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/14731))
* Adding trend analysis to monitoring to better detect performance and availability changes on GitLab.com
* Analyzing tables and optimizing them by adding indexes, breaking them up into separate tables, or by removing unnecessary columns.
* Reviewing database related changes submitted by other engineers
* Documenting database best practices or patterns to avoid

#### Requirements

* At least 2 years of experience running PostgreSQL in production environments
* At least 5 years of experience working with Ruby
* At least 3 years of experience with Ruby on Rails or other Ruby frameworks
  such as Sinatra or Hanami
* Solid understanding of SQL
* Significant experience working in a distributed production environment

### Release Management

Release management specialist is an engineer that focuses on improving the engineering workflows, creates new tools, improves release process
and works closely with the whole Engineering team to ensure that
every GitLab release reaches the public in time.

#### Responsibilities

* Identifies issues in architecture of each component of GitLab and proposes
solutions
* Works with individual teams on defining and implementing solutions
* Enforces frameworks that allow engineers to write code that scales with demand
* Helps teams instrument their code and helps recognize parts of code that could benefit from increased observability
* Prioritize architectural issues that have caused a negative impact in the past
* Works closely with Infrastructure teams to control the impact of application code running in user facing products
* Improve the tools used to create a GitLab release and deploy to GitLab.com
* Enforce the [GitLab Release Process](https://gitlab.com/gitlab-org/release/docs)
* Select [Release Managers](/handbook/engineering/index.html#release-managers) and [Release Trainees](/handbook/engineering/index.html#release-managers) monthly
* Organize Release Managers and Trainees tasks and creates release schedules
* Helps communicate the release schedule clearly with others
* Develop monitoring and alerting to measure release process velocity
* Identify process bottlenecks and introduce optimizations

### Gitter

[Gitter](https://gitter.im) specialists are full-stack JavaScript developers who are able to write JavaScript code that is shared between multiple environments. Gitter uses uses a JavaScript stack running Node.js on the server, and bundled with webpack on the client. The [iOS](https://gitlab.com/gitlab-org/gitter/gitter-ios-app), [Android](https://gitlab.com/gitlab-org/gitter/gitter-android-app), MacOS (Cocoa) and [Linux/Windows (NW.js)](https://gitlab.com/gitlab-org/gitter/desktop/) clients reuse [much of the same codebase](https://gitlab.com/gitlab-org/gitter/webapp) but also require some knowledge of Objective-C, Swift and Java. Gitter uses MongoDB, Redis, and Elasticsearch for backend storage.

#### Requirements

* Strong client-side JavaScript experience
* Strong production Node.js experience
* Highly desirable: MongoDB, Elasticsearch, and Redis experience
* Desirable: Some Java, Objective-C or Swift experience building mobile apps
* Desirable: DevOps experience, working with Linux, Ansible, AWS or similar products

#### Responsibilities

* Fix prioritized issues from the issue tracker
* Triage issues (duplicates, clarification, reproduction steps, prioritization)
* Create high quality frontend and backend code
* Review community contributions
* Provide second-level support to the Production Team to ensure that all Gitter production services remain stable
* Document tribal knowledge, particularly around runbooks and production incident processes
* Keep an eye on Sentry to find regressions and ensure application errors are addressed
* Continually improve the quality of Gitter by using discretion of where you think changes are needed
* Continue to migrate the codebase from old repository locations to GitLab, while open-sourcing as much of it as possible
* Maintain the iOS, Android, and desktop applications
* Provide community support for Gitter via [Gitter rooms](https://gitter.im/gitterHQ/gitter), [Twitter](https://twitter.com/gitchat), Zendesk, etc

### Infrastructure

[Infrastructure](/handbook/engineering/infrastructure/) specialists work alongide DBREs and SREs and are experienced Ruby/GoLang developers who work in the product with a focus on reliability, observability, performance and scalability at the application level, as well as on resource optimization from an Infrastructure perspective and on operationally relevant features.

#### Requirements

- Strong Ruby and Golang experience required
- Strong experience with profiling and metrics analysis
- Strong experience with observability tools, including metrics (Prometheus is a plus), structured logging and distributed tracing
- Desirable: DevOps experience, working with Linux, GCP/AWS, Chef/Ansible, or similar products
- ActiveRecord and SQL expertise

#### Responsibilities

- Fix relevant Infrastructure-related issues from the issue tracker
- Develop operations-related features
- Focus on reliability, performance and scalability, as well as resource optimization

### Search

Elasticsearch engineers are focused on delivering a first class global search experience throughout GitLab products.  They are experienced Ruby/GoLang developers who focus on implementing core Elasticsearch functions while advising other development teams on best practices (e.g. indexing). 

#### Requirements

- Elasticsearch experience - modeling, processing, nodes and index management
- Proficient in Go (Golang) and/or Ruby, Ruby on Rails
- Desirable: DevOps experience, working with Linux, GCP/AWS, Chef/Ansible, or similar products
- Desirable: PostgreSQL experience

#### Responsibilities

- Building a first class global search implementation 
- Improve and implement our indexing strategies
- Own architecture, performance and scaling of the Elasticsearch solution
- Build responsive and scalable services and APIs in Go
- Self-managed installation mechanisms