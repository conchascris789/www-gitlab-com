---
layout: job_family_page
title: "UX Researcher"
---

At GitLab, UX Researchers collaborate with our Product Designers, front/backend engineers, product managers and the rest of the community to assist in determining what features should be built, how they should behave, and what the priorities should be. UX Researchers report to the UX Research Manager.

## Responsibilities

#### UX Research Coordinator

* Manage all aspects of participant recruitment for GitLab’s user experience research studies including, but not limited to, sourcing, outreach, screening, scheduling, participation agreements, and incentives management.

* Grow and administer GitLab First Look, our participant recruitment database.

* Refine, document and maintain processes related to participant recruitment.

* Collaborate with UX/Product Designers, Product Managers and UX Researchers in order to effectively understand their research goals and recruit appropriate participants.

* Review design contributions from the wider GitLab community. Determine whether user research is required. Take the lead on smaller research projects. On larger research projects, support UX Researchers by observing research sessions and taking notes, assisting with analysis and documentation of research insights.

* Be the Social Media Ambassador for UX Research at GitLab. Maintain social media accounts which promote GitLab's research efforts and aid participant recruitment.

* Manage relationships with third-party providers, such as recruitment platforms, digital rewards solutions, etc.

* Respond to enquiries from research participants.

#### Junior UX Researcher

A Junior UX Researcher has some practical experience but needs regular guidance and training to produce their best work and develop their skills. 

* Understands user research methods, when to use them and how to apply them correctly. This may include, but not be limited to: usability testing, user interviews, surveys and competitor analysis.
* Understands basic methods to analyze and synthesize research data. Interprets data correctly. 
* Translates research findings into actionable recommendations in conjunction with the Product team and Product Designers.
* Plans, designs and conducts usability testing and user interviews. Effectively undertakes associated tasks such as participant recruitment and scheduling, producing test materials, setting up test environments, analyzing and delivering findings.
* Assists in the creation of research deliverables such as personas, customer journey maps, etc.
* Responsible for their own personal development, actively seeks out opportunities to improve their skill set. 


#### UX Researcher

* Deeply understand the technology and features of the stage groups to which you are assigned.
* Proactively identify generative user research projects within your stage groups, and engage the UX Research Manager/Senior UX Researchers for guidance and/or collaboration.
* Facilitate evaluative research studies and support Product Designers in the creation of strategic deliverables (such as journey maps, storyboards, competitive analyses, and personas).
* Evangelize research. Share user insights with the broader organisation and externally in creative ways to increase empathy.
* Actively contribute to UX Research processes, documentation and repositories.
* Assist in the maintenance and promotion of GitLab First Look.


#### Senior UX Researcher

* Proactively identify generative user research projects within your stage groups. Work collaboratively with Product Managers to execute research projects.
* Guide, and where necessary train, Product Designers in how to undertake evaluative research projects and in how to create strategic deliverables (such as journey maps, storyboards, competitive analyses, and personas).
* Understand the technology and features of the stage groups to which you are assigned, and have working knowledge of the end-to-end GitLab product.
* Evangelize research. Share user insights with the broader organisation and externally in creative ways to increase empathy.
* Lead strategic user research initiatives that span multiple stage groups (and possibly the entire product).
* Mentor UX Researchers, both inside and outside of your stage groups.
* Actively contribute to UX Research processes, documentation and repositories.
* Engage in social media efforts, including writing blog articles and responding on Twitter, as appropriate.
* Interview potential UX candidates.


## Success Criteria

You know you are doing a good job as a UX Researcher when:

* You collaborate effectively with Product Designers and Product Managers.
* You can quickly build rapport with GitLab's users.
* You contribute ideas for feature improvements based on your research findings.


### UX Researcher Interview Questions <a name="ux-research-interview-questions"></a>

Here are some questions we might ask:

1. What are some existing case studies or research results we can see as an example of your work?
1. When you did `x` project, what was the biggest problem that you had to solve and how did you solve it?
1. What was a really interesting insight or finding that you had from a recent project?


## Relevant links

- [UX Research Handbook](/handbook/engineering/ux/ux-research/)
- [UX Department Handbook](/handbook/engineering/ux/)
- [Engineering Handbook](/handbook/engineering)
- [Product Handbook](/handbook/product)
- [GitLab UX Research Project](https://gitlab.com/gitlab-org/ux-research)
- [GitLab Design System](https://design.gitlab.com/)


## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).

* [Screening call](/handbook/hiring/#screening-call) with a recruiter.

* Interview with UX Manager, Research. In this interview, we will be looking for you to give some real insight into two projects you've worked on. We'll look to understand the size and structure of the team you were a part of, the goals of the project, how you approached research, how you synthesized research data in order to inform design/product decisions and the final output of your research.

* Interview with Product Designer or UX Manager. Overall, this stage aims to understand your research methods, how you choose a methodology to use, how you present results, and how you work with a wider team of counterparts. This interview will also look to understand the softer skills you have as a researcher, and how you apply these in the real world.

* Interview with UX Director. In this final stage interview, we'd like to see the end result of a research study you've been a part of, we want to understand the question(s) you were addressing (and how you determined these questions), your methodology, and how you shared that information with the wider team.

* Successful candidates will subsequently be made an offer.

Additional details about our process can be found on our [hiring page](/handbook/hiring).

## Performance indicators
* [Perception of System Usability](/handbook/engineering/ux/performance-indicators/#perception-of-system-usability)
* [Proactive vs Reactive UX Work](/handbook/engineering/ux/performance-indicators/#ratio-of-proactive-vs-reactive-ux-work)

[groups]: /company/team/structure/#groups

