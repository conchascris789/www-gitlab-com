---
layout: job_family_page
title: "Corporate Development"
---

## Director of Corporate Development

As the Director of Corporate Development, you will be responsible for sourcing,
negotiating, and closing [acquisitions](/handbook/acquisitions/).

### Responsibilities

* Own end-to-end deal management
  * Create a target list of potential acquisitions
  * Validate fit for terms, product roadmap, and other criteria
  * Value the companies in a financial model
  * Negotiate terms of the deal
  * Prepare all acquisition related documents such as Non-Disclosure Agreements, Letters of Intent, and Purchase Agreements with our legal team and legal counsel
  * Integrate the acquisition into the company
* Provide project management leadership over the pre-LOI validation and due diligence stages of the acquisition process with cross-functional teams, including communication of diligence findings
* Establish collaborative, effective, and trusting relationships with key internal functions including Product, Engineering, Legal, Finance, and Marketing to ensure the execution of an efficient acquisition process
* Ensure a proper level of strategic, operational, and organizational alignment.

### Requirements

* Over 5 years of relevant acquisition experience
* Relationship builder with the ability to establish a dialog with leadership of acquisition targets.
* Experience structuring various types of deal terms
* Skilled in corporate valuation, risk management, financial modeling, negotiation, and integration
* Ability to manage multiple priorities and projects cross-functionally with strong organizational skills
* Exemplary verbal and written communication and presentation skills.
* Demonstrated analytical and data led decision-making
* Self-starter and team player with ability to achieve or exceed his/her objectives while working in concert with others
* You are living wherever you want and are excited about the [all remote](https://about.gitlab.com/company/culture/all-remote/) lifestyle
* You share our [values](/handbook/values), and work in accordance with those values

## Senior Director of Corporate Development

As the Senior Director of Corporate Development, you will be responsible for
building the team to source, negotiate, and close [acquisitions](/handbook/acquisitions/).

### Responsibilities

* Grow and manage the corporate development team
* Own end-to-end deal management
  * Create a target list of potential acquisitions
  * Validate fit for terms, product roadmap, and other criteria
  * Value the companies in a financial model
  * Negotiate terms of the deal
  * Prepare all acquisition related documents such as Non-Disclosure Agreements, Letters of Intent, and Purchase Agreements with our legal team and legal counsel
  * Integrate the acquisition into the company
* Provide project management leadership over the pre-LOI validation and due diligence stages of the acquisition process with cross-functional teams, including communication of diligence findings
* Establish collaborative, effective, and trusting relationships with key internal functions including Product, Engineering, Legal, Finance, and Marketing to ensure the execution of an efficient acquisition process
* Ensure a proper level of strategic, operational, and organizational alignment.

### Requirements

* Experience growing and managing an acquisitions team
* 10 years of relevant work experience in acquisitions
* Relationship builder with the ability to establish a dialogue with leadership team members of potential acquisition targets.
* Experience structuring various types of deal terms
* Strong negotiation abilities
* Ability to manage multiple priorities and projects cross-functionally with strong organizational skills
* Exemplary verbal and written communication and presentation skills.
* Demonstrated analytical and data led decision-making
* Self-starter and team player with ability to achieve or exceed his/her objectives while working in concert with others
* You are living wherever you want and are excited about the [all remote](https://about.gitlab.com/company/culture/all-remote/) lifestyle
* You share our [values](/handbook/values), and work in accordance with those values
