---
layout: job_family_page
title: "Finance Business Partner, Sales"
---


We are looking for a trusted finance business partner to join GitLab’s finance team. This position reports directly to the Company’s CFO and will have direct interaction and deliverables to the Sales function at GitLab.

The right candidate for this role will demonstrate strong collaboration skills, financial aptitude, excellent business intuition, and the capability to work at multiple levels, including high level strategy and day-to-day financial details. This candidate must effectively partner with business leaders, yet still adhere to his/her fiduciary duties as a member of the finance organization.

## Responsibilities

- Partnering with leadership to analyze spend, KPIs and financial performance across products, teams and geographies.
- Working with the operations team to understand cost drivers and allocation methodology.
- Lead financial representative to implement financial model for evaluating and measuring effectiveness of functional spend.
- Primary interface between finance and the functions to ensure costs are properly forecasted, budgeted and monthly results are analyzed against targets.
- Engage with leadership regarding business strategy, go to market, spending initiatives, ad hoc financial analysis and quarterly and annual planning.
- Improve on hiring model that incorporates ratios for staffing requirements across all technical functions.
- Assist with contract negotiation, competitive benchmarking and financial modeling to facilitate sound financial decision making.
- Participate in monthly metrics meetings end ensure veracity of financial data and that the financial data is used properly in making business decisions.
- Responsible for Ad-Hoc reporting and financial analysis requests working closely with the leadership teams.
- Collaborate with the Sales Operations team to design, calculate, and implement variable compensation and other sales specific financial data for the Sales organization.

## Requirements

- Bachelor in Accounting or Finance preferred; MBA a plus
- Minimum 7+ years of relevant experience with finance or business analysis background.
- Prior experience in enterprise technology/software/SaaS company required.
- Advanced skills in Google office suite.
- Experience with SQL based data analysis tools. Python a plus.
- Experience with abstract financial modeling, scenario analysis, or monte carlo simulations.
- Experience working in Sales financial modeling.
- Hands on experience with data warehousing technologies and visualization layers.
- Articulate and concise with strong communication skills.
- Ability to adapt to a complex and changing environment.
- Ability to work in a fast-paced environment and manage tight deadlines.
- Ambitious and ability to learn quickly.
- Strong team player and leadership potential.


## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our team page.

- Selected candidates will be invited to schedule a 30 minute screening call with our Global Recruiters
- Next, candidates will be invited to schedule a 45 minute interview with our CFO
- Next, candidates will be invited to schedule a 45 minute interview with our FP&A Lead
- Next, candidates will be invited to schedule a 45 minute interview with a functional leader
- Finally, candidates may be asked to interview with the CEO
- Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our hiring page.
