---
layout: job_family_page
title: "Technology Partner Manager"
---

A Technology Partner Manager in our alliances team is  working with some of the most exciting cloud and tech companies in our space. This role provides a unique opportunity to build mind share and
adoption of GitLab leveraging our ecosystem of partners. Responsibilities
include: owning the execution and evolution of partner programs, driving
sales acceleration/marketing programs directly with our partners, and ensuring maximum technical engagement while orchestrating  with the other GitLab teams. The ideal candidate will have a
business background which will enable them to understand ROI goals but also
someone who can deliver on planning to hit goals consistently. An architect level understanding of GitLab and the DevOps ecosystem is critical to understanding technical fit and opportunities for partnership. You will be
comfortable navigating GitLab as a company, challenge what's working and how we
build to improve as part of our workflow.


## Responsibilities

1. Execute annual partner plans with technology companies including ISVs and cloud companies.
1. Evaluate effectivity of partner relationships and engagements in delivering positive ROI.
1. Be the central point of contact and advocacy at GitLab, orchestrating the partner engagements across the various departments: product, content, marketing, and sales
1. Ensure successful execution of marketing and sales acceleration programs, with a drive for measurable outcomes and revenue yield
1. Facilitate meaningful relationships between GitLab and our technology partners' teams. This includes partners executive leaders, product managers, engineering, sales and marketing.
1. Identify new opportunities for quality, high ROI engagements with partners


## Requirements

1. 5-10  years of sales management and/or business development experience in the technology/cloud services industry, preferably in the DevOps space
1. Demonstrated repeated success leading partner/client engagement plans to fruitful executions, consistently exceeding key performance metrics
1. Strong presentation skills and the ability to articulate complex concepts to cross functional audiences
1. Presentation skills with a high degree of comfort speaking with executives, IT Management, and developers
2. Significant experience with executive presence and a proven ability to lead and facilitate executive meetings including architecture whiteboards
1. Excellent time management and written/verbal communication skills
1. Ability to quickly understand technical concepts and explain them to audiences of varying technical expertise
1. [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#management-group)


### Junior Technology Partner Manager

Junior Technology Partner Managers share the same requirements and responsibilities outlined above, but typically join with less or alternate experience than a typical Technology Partner Manager.
