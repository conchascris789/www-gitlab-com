---
layout: markdown_page
title: "Informal Communication"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

GitLab is an all-remote company with [team members](/company/team/) located in 50+ countries around the world. 
Check out the [main all-remote page](/company/culture/all-remote/) to learn more about all-remote work. 

On this page, we're detailing how informal communication occurs at GitLab, and why it matters in an all-remote culture.

## Formally design informal communications

In colocated environments, informal communication is naturally occuring. When individuals are physically located in the same space, there are ample opportunities to chitchat and carry on conversations outside of formal business settings. 

Informal communication is important, as it enables friendships to form at work related to matters *other* than work. Those who feel they have genuine friends at work are [more likely to enjoy their job](https://onlinelibrary.wiley.com/doi/full/10.1111/peps.12109), perform at a high level, feel invested in the company, and serve others within the organization. At GitLab, we desire those outcomes as well, reinforcing our [Results value](/handbook/values/#results). 

For all-remote companies, leaders should not expect informal communication to happen naturally. There are no hallways for team members to cross paths in, no carpools to the office, etc. 

In an all-remote environment, informal communication should be formally addressed. Leaders should organize informal communication, and to whatever degree possible, design an atmosphere where team members all over the globe feel comfortable reaching out to anyone to converse about topics unrelated to work. 

## Devote time to fostering relationships 

![GitLab marketing team Show & Tell social call](/images/all-remote/marketing-social-call-show-and-tell.jpg){: .shadow.medium.center}
GitLab marketing team Show & Tell social call
{: .note.text-center}

In all-remote environments, there should be a greater emphasis placed on carving out time to get to know one another as humans. To connect and bond as empathetic beings with interests, emotions, fears, and hopes — people, not just colleagues. 

If you've spent any length of time in a corporate setting, you've probably seen a company institute a weekly or monthly "happy hour," designed to gather employees in a shared space to converse about topics unrelated to work. 

For colocated companies, the occasional team offsite — to take in a sporting event, to enjoy a shared lunch, etc. — may be enough to supplement naturally occuring informal communication in the office. 

Below are a number of intentional facets of GitLab's culture, created to foster informal communication. We welcome other all-remote companies to iterate on these and implement as desired. 

- [Company call](/handbook/communication/#company-call): A daily team video call with an agenda where everyone is free to add subjects they'd like to discuss with the whole company.
- [Breakout calls](/handbook/communication/#breakout-call): Following the company call, everyone breaks out into small groups for 10-15 minutes to talk about non-work-related topics.
- [Contribute Unconference](/company/culture/contribute/): An in-person, week-long event where we bring the entire company together in one location to get to know each other better.
- [Group conversations](/handbook/people-operations/group-conversations/): Four times a week the company gets together virtually to discuss an area of the business. Slides are provided for context but not presented.
- Coffee chats: More details below.
- Coworking calls: More details below. 
- Social hours: Informal social calls organized within our immediate teams to get to know each other on a more personal level. 
- [Visiting grants](/handbook/incentives/#visiting-grant): This travel stipend encourages team members to visit each other by covering transportation costs up to $150 per person they visit.
- Local meetups: Co-located team members are encouraged to organize their own meetups, whether it's a coworking space or getting dinner together. 
- [CEO house](/handbook/ceo/#house): Team members can get together in Utrecht, Netherlands, at the CEO's AirBnB, free of charge. 
- [Slack](/handbook/communication/#slack): We use Slack channels for informal communications throughout the company, whether it's a team-specific channel or a channel dedicated to sharing vacation photos with other team members. 
- [Zoom calls](https://about.gitlab.com/handbook/tools-and-tips/#zoom): Not only do we get to know our coworkers better by seeing them in real time during video calls, we also get to know their pets and families too. This visual engagement helps us relate to each other on a more personal level, so when we meet in person, we already know each other. In fact, when our team members meet face-to-face for the first time, the most surprising factor is usually each person's height.

>  **"I’ve been given a tour of team members’ new houses, admired their Christmas decorations, squealed when their pets and kids make an appearance and watched them preparing dinner – glimpses into the personal lives of my colleagues that I’ve never had in any office job."** - [Rebecca](https://about.gitlab.com/company/team/#rebecca), Managing Editor, GitLab

#### Coffee chats

We understand that working remotely leads to mostly work-related conversations
with fellow team members, so everyone at GitLab is encouraged to dedicate **a few hours a week**
to having social calls with anyone in the company. 

It's a great chance to get to know who you work with,
talk about everyday things and share a coffee, tea, or your favorite beverage. We want you to make
friends and build relationships with the people you work with to create a more comfortable,
well-rounded environment. 

The Coffee Chats are different from the
[Random Room](/handbook/communication/#random-room) video chat, they are meant to give you the option
to have 1x1 calls with specific teammates who you wish to speak with and is not a
random, open-for-all channel but a conversation between two teammates.

#### Scheduling a Coffee Chat

GitLab Team Members can easily schedule a Coffee Chat in Google Calendar and link it to Zoom, with a 1-click link to the video call.

Watch this [short video](https://youtu.be/zbbEn_PznK0) to see how it's done.

1. Search for the person in Google Calendar
2. Click the time and name the meeting **Name / Name coffee-chat**
3. Click on *Make it a Zoom meeting*

Google Calendar will warn you if the time is outside the other person's working hours, and you can select from a list of suggested times.

##### The Donut Bot

Team members can join the #donut_be_strangers Slack channel to be paired with a random team member for a coffee chat. The "Donut" bot will automatically send a message to two people in the channel every other Monday. 
Please schedule a chat together, and Donut will follow up for feedback.
Of course, you can also directly reach out to your fellow GitLab team-members to schedule a coffee chat in the #donut_be_strangers Slack channel or via direct message.

#### Social Calls

Some teams at GitLab organize informal social calls on a regular basis in order to build camaraderie. 
The [data team](/handbook/business-ops/data-team/#-contact-us) has them every Tuesday. 
Team members and managers are encouraged to create these calls as a medium for informal, agenda-free interaction between team members. 

Consider creating a shared calendar for social events to more broadly allow people to join and be aware of social events that are in a more convenient time zone. For example, GitLab's [marketing](/handbook/marketing/) team has a "Show and Tell" call where team members are encouraged to display something they've crafted and share the story behind it.

#### Coworking calls

These video calls are scheduled working sessions on Zoom where team members can work through challenging tasks with a coworker, or simply hang out while each person works on their own tasks. 
This recreates a productive working session you might have in person in a traditional office setting, but from the comfort of your own desk. 
Want to try advanced mode? Screen share as you work together (keeping in mind any confidentiality issues).

----

Return to the main [all-remote page](/company/culture/all-remote/).
