---
layout: markdown_page
title: "All Remote"
---

## On this page
{:.no_toc}

- TOC
{:toc}

GitLab is an all-remote company with [team members](/company/team/) located in more than 57 countries around the world.

On this page and subpages, we'll share what "all remote" really means, [how it works at GitLab](/company/culture/all-remote/tips/#how-it-works-at-gitlab), some [tips and tricks](/company/culture/all-remote/tips/#tips-for-leaders-and-other-companies) for remote teams, and [resources](/company/culture/all-remote/resources/) to learn more.

## The Remote Manifesto

All-remote work promotes:

- Hiring and working from all over the world *instead of* from a central location.
- Flexible working hours *over* set working hours.
- Writing down and recording knowledge *over* verbal explanations.
- Written down processes *over* on-the-job training.
- Public sharing of information *over* need-to-know access.
- Opening up every document for editing by anyone *over* top-down control of documents.
- Asynchronous communication *over* synchronous communication.
- The results of work *over* the hours put in.
- Formal communication channels *over* informal communication channels.

## Why remote?

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/NoFLJLJ7abE" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

>  **"Remote is not a challenge to overcome. It's a clear business advantage."** -Victor, Product Manager, GitLab

From the cost savings on office space to more flexibility in employees' daily lives, all-remote work offers a number of advantages to organizations and their people.
But we also recognize that being part of an all-remote company isn't for everyone. Here's a look at some of the advantages and disadvantages.  

### Advantages

#### *For employees*
- You have more flexibility in your daily life (for kids, parents, friends, groceries, sports, deliveries).
- No more time, stress, or money wasted on a commute (subway and bus fees, gas, car maintenance, tolls, etc.).
- Reduced interruption stress and increased [productivity](https://www.inc.com/brian-de-haaff/3-ways-remote-workers-outperform-office-workers.html).
- Ability to travel to other places without taking vacation (family, fun, etc.).
- Freedom to relocate, be location independent, or even [travel with other remote professionals](/company/culture/all-remote/resources/#organizations-for-traveling-remote-work).
- Less exposure to germs from sick coworkers.
- It can be easier to communicate with difficult colleagues remotely, reducing distractions from interpersonal drama or office politics.
- You can [set up and decorate your office or workspace](https://thriveglobal.com/stories/how-remote-work-can-reduce-stress-and-revitalize-your-mindset/) in whatever way works best for you.
- You can choose your working hours based on when you're most productive.
- You have the opportunity to meet and work with people from many locations around the world.
- Onboarding may be less stressful socially.
- Eating at home is better (sometimes) and cheaper.
- Taxes can be cheaper in some countries.
- Work clothes are not required.

From family time to travel plans, there are [many examples and stories](/company/culture/all-remote/stories/) of how remote work has impacted the lives of GitLab team members around the world.

> **“The flexibility makes family life exponentially easier, which reduces stress and makes you more productive and motivated. You can’t put a dollar value on it – it’s priceless.”** - Haydn, Regional Sales Director, GitLab  

#### *For your organization*
- You're able to hire great people [no matter where they live](/jobs/faq/#country-hiring-guidelines).
- Employees are more productive with fewer distractions.
- [Increased savings on office costs](https://globalworkplaceanalytics.com/the-remote-work-roi-calculator-v0-95), [compensation](/2019/02/28/why-we-pay-local-rates/) (due to hiring in lower-cost regions).
- All-remote naturally attracts self-motivated people.
- It's easier to quickly grow your company.
- Employees are [increasingly](https://www.iofficecorp.com/blog/workplace-design-statistics) expecting remote work options from their employers.
- Companies often experience [lower employee turnover](https://www.owllabs.com/blog/remote-work-statistics) and higher morale with remote work.
- You have fewer meetings and more focus on results and output of great work.
- You don't have to pay to relocate someone to join your team.
- With employees located all over the world working asynchronously, contributions can continue even when one time zone's working day is over.
- There's also business continuity in the case of local disturbances or natural disasters (e.g. political or weather-related events).
- Greater flexibility [can mean greater diversity](https://business.linkedin.com/content/dam/me/business/en-us/talent-solutions/resources/pdfs/global-talent-trends-2019.pdf) in your organization. 

#### *For the world*

- With no commuting employees and no office buildings or campuses, all-remote companies have a smaller environmental footprint (except when they host regular companywide summits or gatherings that require significant amounts of air travel).
- There's evidence that [remote work can reduce the effects of urban crowding](https://qz.com/work/1641664/remote-workers-are-the-solution-to-urban-crowding/) for many cities around the world. Some states and countries are even [offering incentives](http://fortune.com/2019/06/22/google-housing-plan-bay-area/) to encourage remote work. Here are [13 examples](https://www.bankrate.com/personal-finance/smart-money/places-that-will-pay-you-to-move/#slide=1) around the world.
- For global companies, bringing better-paying jobs to low-cost regions has positive economic impacts.

### Disadvantages

Despite all of its advantages, all-remote work isn't for everyone. It can have disadvantages for potential employees depending on their lifestyle and work preferences, as well as the organization.

#### *For employees*
- Onboarding can be difficult when you're remote, because it involves more self-learning and you're not physically with your new coworkers and fellow new hires.
- The first month in a remote role can feel lonely, especially if you're transitioning from a traditional office setting. 
- Remote settings can cause a breakdown in communication skills if organizations aren't intentional about creating ways for their people to stay connected.
- Some may find it difficult to work in the same setting as they live and sleep, because a dedicated workspace helps to switch the context from their home life to work.
- Team members in different time zones may have to compromise on meeting times.
- It can be hard to separate your personal and work life. It's important to encourage boundaries and make sure you don't continue to work during your family time.
- Differences in currency and tax requirements around the world can create challenges for the employee.
- Remote work requires you to manage your own time and be self-motivated, disciplined, and organized.

#### *For your organization*
- Because it's non-traditional, all-remote work sometimes concerns investors, partners, and customers.
- Differences in currency as well as tax, immigration, and labor laws around the world can create [compliance challenges](https://www.forbes.com/sites/forbeshumanresourcescouncil/2019/07/12/remote-work-is-here-to-stay-heres-how-to-avoid-three-common-compliance-issues/) for the organization.
- You have to be more intentional about cultivating and sustaining your company culture.

## Why is this possible now?

All-remote work wouldn't be possible without the constant evolution of technology, and the tools that enable this type of work are continuously being developed and improved.

We aren't just seeing these impacts for all-remote companies. In fact, in some organizations with large campuses, employees will routinely do video calls instead of spending 10 minutes to go to a different building.

Here are some of the key factors that make all-remote work possible:

* Faster internet everywhere - 100Mb/s+ cable, 5GHz Wifi, 4G cellular
* Video call software - Google Hangouts, Zoom
* Mobile technology - Everyone has a computer in their pocket
* Evolution of speech-to-text conversion software - more accurate and faster than typing
* Messaging apps - Slack, Mattermost, Zulip
* Issue trackers - Trello, GitHub issues, GitLab issues
* Virtual workspace tools - Remo
* Suggestions - GitHub Pull Requests, GitLab Merge Requests
* Static websites - GitHub Pages, GitLab Pages
* English proficiency - More people are learning English
* Increasing traffic congestion in cities
* More demand for flexibility from new professionals entering the workforce

## What "all remote" does not mean

Let's address some of the common misconceptions about all-remote work.

First things first: An all-remote company means there is *no* office where multiple people are based. 
The only way to not have people in a satellite office is not to have a main office. 
It's not that we don't have a headquarters, it is that we have 800+ (and growing) headquarters!

The terms "remote" and "distributed" are often used interchangeably, but they're not quite the same. We prefer the term "remote" because "distributed" suggests multiple physical offices.
"Remote" is also the [most common term](https://www.google.com/search?ei=4IBsXKnLDIGRggftuqfAAQ&q=distributed+companies&oq=distributed+companies&gs_l=psy-ab.12...0.0..5177...0.0..0.0.0.......0......gws-wiz.6xnu76aJWr4) to refer to the absence of a physical workspace, and being able to do your job from anywhere.

For employees, being part of an all-remote company does not mean working independently or being isolated, because it's not a substitute for human interaction.
Technology allows us to [stay closely in touch](company/culture/all-remote/tips/#we-facilitate-informal-communication) with our teams, whether asychronously in text or in real time with high-fidelity conversations through video.
Teams should collaborate closely, communicate often, [build relationships virtually](/2019/07/31/pyb-all-remote-mark-frein/), and feel like valuable members of a larger team.

Working remotely also doesn't mean you're physically constrained to home.
You're free to work wherever you want. That could be at home with family, a coffee shop, a coworking space, or your local library while your little one is enjoying storytime. It could mean that you're location independent, traveling around and working in a new place each week.
You can have frequent video chats or virtual pairing sessions with co-workers throughout the day, and you can even meet up with other coworkers to work together in person if you're located near each other.

At the organizational level, "all-remote" does not mean simply offshoring work. Instead, it means you're able to hire the best talent from all around the world.
It's also not a management paradigm. You still have a hierarchical organization, but with a focus on output instead of input.

All in all, remote is fundamentally about _freedom_ and _individual choice_. At GitLab, we [value your results](/handbook/values/#results), not where you get your work done.

## Our long-term vision for remote work

There are a few important outcomes we expect to see as remote work becomes even more prevalent around the world:

1. The majority of new startups intentionally forming as all-remote companies.
1. Cities in developing countries, particularly in Africa, enabled by all-remote jobs at companies founded by local leaders.
1. Most startups in the Bay Area with a significant portion of their workforce working remotely.
1. Increased wages for remote work outside of metro areas.

## How we built our all-remote team

As GitLab has grown, we've learned a lot about what it takes to build and manage a fully remote team, and want to share this knowledge to help others be successful.

Find out [how GitLab makes it work](/company/culture/all-remote/tips/#how-it-works-at-gitlab).

## Tips for working remotely

Building a remote team or starting your first all-remote job? Check out our [tips for working remotely.](/company/culture/all-remote/tips)

## Resources

Browse our [resources page](/company/culture/all-remote/resources) to learn more about GitLab's approach, read about remote work in the news, and see what other companies are leading the way.

We've also compiled a [list of companies](/handbook/got-inspired/) that have been inspired by GitLab's culture.

## Hiring

GitLab envisions a world where talented, driven individuals can find roles and seek employment based on business needs, rather than an oftentimes arbitrary location. 

Hiring across the globe isn't without its challenges. There are local regulations and risks unique to countries and regions around the globe. We believe that these challenges are worth overcoming, and opening our recruiting pipeline beyond the usual job centers creates a competitive advantage. We hope to see this advantage wane as more all-remote companies are created. 

Learn more about [hiring in an all-remote environment](/company/culture/all-remote/hiring/).

## Compensation

While there are certain complexities to paying team members who are spread out in over 50 countries, we believe that it's worthwhile. Being an all-remote company enables us to [hire the world's best talent](/company/culture/all-remote/hiring/), not just the best talent from a few cities.

Learn more about [compensation in an all-remote environment](/company/culture/all-remote/compensation/).

## Learning and Development

We believe that all-remote companies are at a competitive advantage when it comes to educating and developing team members.

Learn more on how to make [learning and development a companywide mindset in an all-remote environment](/company/culture/all-remote/learning-and-development/).

## Meetings

Learn how to decide when a meeting is necessary and [how to optimize them in an all-remote environment](/company/culture/all-remote/meetings/). 

## Stories

Read the [stories](/company/culture/all-remote/stories/) of some of our team members and hear how remote work has impacted their lives.

## Interviews

Read and listen to [interviews](/company/culture/all-remote/interviews/) on the topic of working remotely, hosted by GitLab team members. 

## History

Learn about [historical milestones, inflection points, and prescient interviews](/company/culture/all-remote/history/) in the evolution and expansion of remote work.

## Contribute to this page

At GitLab, we recognize that the whole idea of all-remote organizations is still
quite new, and can only be successful with active participation from the whole community.
Here's how you can participate:

- Propose or suggest any change to this site by creating a [merge request](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/).
- [Create an issue](https://gitlab.com/gitlab-com/www-gitlab-com/issues/) if you have any questions or if you see an inconsistency.
- Help spread the word about all-remote organizations by sharing it on social media.
