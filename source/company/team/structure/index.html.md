---
layout: markdown_page
title: "Organizational Structure"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Organizational chart

You can see who reports to whom on our [organizational chart](/company/team/org-chart).

## Layers

| Level | Example(s) | Peer group / shorthand for peer group |
|--------------------------------|---------------------------------------|-------------------------------------|
| Board member | [Chief Executive Officer](/job-families/chief-executive-officer/) | Board |
| Executive | [Chief People Officer](/job-families/people-ops/chief-people-officer/) and [VP of Engineering](/job-families/engineering/backend-engineer/#vp-of-engineering) | Executives / [E-group](/handbook/leadership/#e-group) |
| Senior Leader | Senior Director or [VP of Global Channels](/job-families/sales/vp-of-global-channels/) | Senior leaders / [S-group](/handbook/leadership/#s-group) |  |
| Director | [Director of Engineering](/job-families/engineering/backend-engineer/#director-of-engineering) | Directors / [D-group](/handbook/leadership/#director-group) |
| Manager | [Engineering Manager](/job-families/engineering/backend-engineer/#engineering-manager) | Managers / [M-group](/handbook/leadership/#management-group) |
| Individual contributor (IC) | [Staff Developer](/job-families/engineering/developer/#staff-developer) | ICs |

GitLab Inc. has at most six layers in the company structure (IC, Manager, Director, Senior Leadership, Executives, Board).
You can skip layers but you can never have someone reporting to the same layer since that creates too many layers in the organization.
The CEO is the only person who is part of two levels: the board and the executives.

## Levels

### Executives

The executive layer is structured as follows. There are two primary processes, product (product management, engineering, alliances) and go-to market (marketing and sales). 
Some companies have a Chief Product Officer (CPO) for the former and a Chief Operating Officer (COO) for the latter. 
We have a flatter organization. 
The C-level exec for product is the CEO and the VP's of Product management, Product Strategy, and Engineering report to the CEO. Marketing and sales have separate C-level execs. 
The two enabling functions, finance and people, also each have a C-level executive, the Chief Financial Officer (CFO) and Chief People Officer (CPO). 
Together, these executives consist of the [e-group](/handbook/leadership/#e-group)
They meet weekly, attend quarterly board meetings, have a [public Slack channel #e-group](https://gitlab.slack.com/messages/C5W3VS1C4) for most discussion topics, as well as a private one for rare confidential matters.


### VPs

In product (product management, product strategy, and engineering) a VP is an executive; 
in other functions, a VP is a senior leader.
This  aligns with the industry convention that in sales a VP is a senior leader and a VP of engineering commonly is an executive.

### Senior leaders

In product (product management, engineering, alliances) a senior leader has a senior director title;
in other functions, the title of a senior director can be either VP or senior director. 
They are all members of our [S-group](/handbook/leadership/#s-group).

## Organizational Structure

* **Functions:** the area under one executive. _e.g._ the Engineering function
* **Departments:** lead by Directors and comprise multiple teams _e.g._ the Infrastructure department within the Engineering function
  * Optionally, some departments are so large they are comprised of multiple **sections** _e.g._ The Dev section is within the Development Department
* **Teams:** constitute departments  and are made of a line manager and their direct reports _e.g._ the Security operations team within the Security Department

Finance also has a notion called "departments" for financial planning purposes. But these do not align with our organizational departments. For instance the finance department "product development" rolls up both the PM and Engineering functions. But it excludes the Support department, which is part of the engineering function, but a different budget. This name collision should probably be resolved in the future.

### Product Groups
{: #product-groups}

Our engineering organization is directly aligned to groups as defined in [product category hierarchy](/handbook/product/categories/#hierarchy).
Our groups operate on the principle of [stable counterparts](/handbook/leadership/#stable-counterparts) from multiple functions.

For example, we have a Product Manager, Product Marketing Manager, Engineering Manager, Content Marketer, Backend Developers, Frontend Developers, and Product Designers that are all dedicated to a group called "Package". Collectively, these individuals form the "Package group". The word "Package" appears in their titles as a specialty, and in some cases, their team name.

A group has no reporting lines because we [don't want a matrix organization](/handbook/leadership/#no-matrix-organization).
Instead, we rely on stable counterparts to make a group function well.
In some shared functions, like design, technical writing and quality individuals are paired to one or more stages so that there are stable counterparts.

While everyone can contribute, the scope of a group should be non-overlapping. 
This facilitates [iteration](/handbook/values/#iteration) and [efficiency](/handbook/values/#efficiency).
It is important to ensure a group doesn't have a scope definition that is shared across multiple groups. Here are two examples:
1. We don't have an internationalization group.
   That responsibility is shared across many groups. 
   We might instead have an internationalization tooling group. 
1. We don't have a performance group.
   Ensuring GitLab is performant is the responsibility of all groups. 
   We do have a [Memory group](/handbook/product/categories/#memory-group) focused on the specifics of reducing GitLab's memory footprint because it is hard for individual groups to measure their impact there.

### Working Groups

A [working group](/company/team/structure/working-groups/) is a specific type of group that assembles for a period of time to accomplish a business goal. Working groups have defined responsibilities and meet regularly. Ideally a working group can disband when the goal is complete to avoid accrued bureaucracy.

## Roles

People can be a specialist in one thing and be an expert in multiple things. These are listed on the [team page](/company/team/).

### Specialist

Specialists carry responsibility for a certain topic.
They keep track of issues in this topic and/or spend the majority of their time there.
Sometimes there is a lead in this topic that they report to.
You can be a specialist in only one topic.
The specialist description is a paragraph in the job description for a certain title.
A specialist is listed after a title, for example: Developer, database specialist (do not shorten it to Developer, database).
Many specialties represent stable counterparts. For instance, a "Test Automation Engineer, Create" specializes in the "Create" [stage group](#stage-groups) and is dedicated to that group.
The if you can have multiple ones and/or if you don't spend the majority of your time there it is probably an [expertise](/company/team/structure/#expert).
Since a specialist has the same job description as others with the title they have the same career path and compensation.

### Expert

Expert means you have above average experience with a certain topic.
Commonly, you're expert in multiple topics after working at GitLab for some time.
This helps people in the company to quickly find someone who knows more.
Please add these labels to yourself and assign the merge request to your manager.
An expertise is not listed in a role description, unlike a [specialist](/job-families/specialist).

For Production Engineers, a listing as "Expert" can also mean that the individual
is actively [embedded with](/handbook/engineering/infrastructure/#embedded) another team.
Following the period of being embedded, they are experts in the regular sense
of the word described above.

Developers focused on Reliability and Production Readiness are named [Reliability Expert](/job-families/expert/reliability/).

### Mentor

Whereas an expert might assist you with an individual issue or problem, mentorship is about helping someone grow their career, functional skills, and/or soft skills. It's an investment in someone else's growth.

Some people think of expertise as hard skills (Ruby, International Employment Law, etc) rather than soft skills (managing through conflict, navigating career development in a sales organization, etc).

If you would like to be a mentor in a certain area, please add the information to the team page. It is important to note whether you would like to be a mentor internally and/or externally at GitLab. Examples of how to specify in the expertise section of the team page: `Mentor - Marketing, Internal to GitLab` or `Mentor - Development (Ruby), External and Internal to GitLab`.

### GitLab.com isn't a role

Some of the things we do make are GitLab.com specific.
But the majority of work in any role applies to both ways of delivery GitLab, self-managed and .com.

1. We have a functionally organized company, the functions need to as mutally exclusive as possible to be efficient, .com overlaps with a small part of many functions.
1. Having .com specific people will increase the pressure to get to two codebases, that can be a big hindrance: "splitting development between two codebases and having one for cloud and one for on-prem is what doomed them", and "they split cloud and on-prem early on and it was a 10-year headache with the OP folks feeling left in line to jump in the pool but never could.  While cloud pushed daily/weekly with ease, OP was _easily_ 6-mo behind leaving customers frustrated"
1. The [reasons .com customers churned](https://drive.google.com/file/d/1QhGrofKbiUIJSv7ZI524FshoKnS-6y-P/edit) were all things that occur in both self-managed and .com
1. Improvements we can make in user growth might be informed by .com specific data but can be implemented for both delivery mechanisms.

## Other Considerations

### The word "Manager" in a title doesn't imply people management or structure

Some of individual contributors (without any direct reports) have manager in their title without a comma after it. These titles are not considered a people manager in our company structure nor salary calculator, examples are product manager, accounting manager, account manager, channel sales manager, technical account manager, field marketing managers, online marketing manager, and product marketing manager. People with manager and then a comma are a people manager in our company structure.

### Wider community

GitLab is a project bigger than GitLab the company.
It is really important that we see the community around GitLab as something that includes the people at the company.
When you refer to the community excluding the people working for the company please use: wider community.
If refer to both people at the company and outside of it use community or GitLab team-members.

### Team and team-members

Team is reserved for the smallest group.
It is defined by a manager and their reports.
It does not refer to a [group](#groups) or [a department](/handbook/engineering/development/).

We  refer to all the people working for the company as team-members.
This is a bit confusing since team is reserved for the smallest group but it is preferable over all the alternatives we considered:

1. Employees since we have many contractors working for GitLab Inc.
1. GitLabbers, Labbers, or Gitters since this should include the [wider community](/handbook/communication/#writing-style-guidelines).
1. Incers (referring to GitLab Inc.) since it sounds dull.
1. Staff is what is on [our user profile if we work for GitLab Inc.](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/29480/diffs) but is also an [engineering level](/job-families/engineering/backend-engineer/#staff-backend-engineer).
1. Tanuki (referring to our logo) since [it is confusing to refer to humans with an animal species](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24447/).
