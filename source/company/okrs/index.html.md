---
layout: markdown_page
title: "Objectives and Key Results (OKRs)"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Quarterly OKRs

All our OKRs are public and listed on the pages below.

- [FY20-Q3 (draft)](/company/okrs/fy20-q3/)
- [FY20-Q2 (active)](/company/okrs/fy20-q2/)
- [FY20-Q1](/company/okrs/fy20-q1/)
- [CY18-Q4](/company/okrs/2018-q4/)
- [CY18-Q3](/company/okrs/2018-q3/)
- [CY18-Q2](/company/okrs/2018-q2/)
- [CY18-Q1](/company/okrs/2018-q1/)
- [CY17-Q4](/company/okrs/2017-q4/)
- [CY17-Q3](/company/okrs/2017-q3/)

## What are OKRs?

OKRs are our quarterly goals to execute our [strategy](/company/strategy/). To make sure our goals are clearly defined and aligned throughout the organization. For more information see [Wikipedia](https://en.wikipedia.org/wiki/OKR) and [Google Drive](https://docs.google.com/presentation/d/1ZrI2bP-XKEWDWsT-FLq5piuIwl84w9cYH1tE3X5oSUY/edit) (GitLab internal).

The intention is to ensure everyone in the company knows what the top objectives are.
We do not use it to [give performance feedback](https://about.gitlab.com/handbook/people-operations/360-feedback/) or as a [compensation review](https://about.gitlab.com/handbook/people-operations/global-compensation/#annual-compensation-review).

## Format

`1. Title: Objective as a sentence. Key result, key result, key result. => Outcome, outcome, outcome.`

1. The `=> Outcome, outcome, outcome.` part is only added after the quarter started.
1. Each owner has a maximum of 3 objectives.
1. Each objective has between 1 and 3 key results, if you have less you list less.
1. Each key result has an outcome.
1. The title is of person who is the [directly responsible individual](https://about.gitlab.com/handbook/people-operations/directly-responsible-individuals/)
1. We use four spaces to indent instead of tabs.
1. The key result can link to an issue.
1. The outcome can link to real time data about the current state.
1. The three CEO objectives are level 3 headers to provide visual separation.
1. We number them by starting with `1.` we can more easily refer to them.

## Levels

We only list objectives prefaced with your role title.
We do OKRs up to the team level, we don't do [individual OKRs](https://hrblog.spotify.com/2016/08/15/our-beliefs/).
Although, an individual might have OKRs if they represent a unique function.
For example, individual SDRs don't have OKRs, the SDR team does.
Legal is one person, but represents a unique function, so Legal has OKRs.
Part of the individual performance review is the answer to: how much did this person contribute to the team objectives?
We have no more than [five layers in our team structure](/company/team/structure/).
Because we go no further than the manager level we end up with a maximum 4 layers of indentation on this page.
The match of one "nested" key result with the "parent" key result doesn't have to be perfect.
Every owner should have at most 3 objectives. To make counting easier always mention the owner with a trailing colon, like `Owner:`.
The advantage of this format is that the OKRs of the whole company will fit on three pages, making it much easier to have an overview.

## Updating

The key results are updated continually throughout the quarter when needed.
Everyone is welcome to a suggestion to improve them.
To update please make a merge request and post a link to the MR in the #okrs channel and at-mention the CEO.
At the top of the OKRs is a link to the state of the OKRs at the start of the quarter so people can see a diff.

## Replace with epics

When we have a [tree view of epics](https://gitlab.com/gitlab-org/gitlab-ee/issues/7327) we can use that instead of text.

## Schedule

The EBA to the CEO is responsible for scheduling and coordination of the OKRs process detailed below. Scheduling should be completed at least 30 days in advance of the timeline detailed below. 

The number is the weeks before or after the start of the fiscal quarter.

- -5: CEO pushes top goals to this page
- -4: E-group pushes updates to this page and discusses it in the e-group weekly meeting
- -3: E-group 90 minute planning meeting
- -2: Discuss with the board and the teams
- -1: CEO reports give a [How to achieve presentation](#how-to-achieve-presentation)
- -0: Add Key Results to top of 1:1 agenda's
- +0: Present overview of the OKRs during a [Group Conversations](/handbook/people-operations/group-conversations/)
- +1: Review previous quarter 1:1
- +1: Present 'how to achieve' during a Group Conversation
- +2: Review previous and next quarter during the next board meeting

## How to achieve presentation

1. Detail how you plan to achieve your Key Results.
1. CEO EBA to schedule 25 minute meeting with the E-group (CEO and functional counterpart required, everyone else optional) 1 week before the new quarter begins.
1. Have your updated OKRs on the handbook page 24 hours in advance. Use the handbook to drive the conversation.
1. CEO EBA to link agenda and notes doc to the calendar invite 24 hours in advance. We'll use this to track questions and action items.
1. The meeting is fully interactive with questions being asked by other team members.
1. The meeting will be streamed to YouTube. A private or public stream will be indicated by the presenter to the EBA prior to their scheduled presentation.

## Coordination

The OKRs are a way to coordinate across the company.
For efficient coordination it is important that you can see an overview early.
So please iterate and put your best guess for OKRs in on time instead of delaying.
We can always change an OKR, it is much harder to catch up on coordination time.

## Scoring

It's important to score OKRs after the fiscal quarter ends to make sure we celebrate what went well, and learn from what didn't in order to set more effective goals and/or execute better next quarter.

1. Move the current OKRs on this page to an archive page _e.g._ [2017 Q3 OKRs](/company/okrs/2017-q3/)
1. Add in-line comments for each key result briefly summarizing how much was achieved _e.g._
  * "=> Done"
  * "=> 30% complete"
1. Add a section to the archived page entitled "Retrospective"
1. OKR owners should add a subsection for their role outlining...
  * GOOD
  * BAD
  * TRY
1. Promote the draft OKRs on this page to be the current OKRs

## Critical acclaim

Spontaneous chat messages from team members after introducing this format:

> As the worlds biggest OKR critic, This is such a step in the right direction :heart: 10 million thumbs up

> I like it too, especially the fact that it is in one page, and that it stops at the team level.

> I like: stopping at the team level, clear reporting structure that isn't weekly, limiting KRs to 9 per team vs 3 per team and 3 per each IC.

> I've been working on a satirical blog post called called "HOT NEW MANAGEMENT TREND ALERT: RJGs: Really Just Goals" and this is basically that. :wink: Most of these are currently just KPIs but I won't say that too loudly :wink: It also embodies my point from that OKR hit piece: "As team lead, it’s your job to know your team, to keep them accountable to you, and themselves, and to be accountable for your department to the greater company. Other departments shouldn’t care about how you measure internal success or work as a team, as long as the larger agreed upon KPIs are aligned and being met."

> I always felt like OKRs really force every person to limit freedom to prioritize and limit flexibility. These ones fix that!

## OKRs are stretch goals by default

OKRs should be ambitious but achievable. If you achieve less than 70% of your KR, it may have not been achievable. If you are regularly achieving 100% of your KRs, your goals may not be ambitious enough.

Some KRs will measure new approaches or processes in a quarter. When this happens, it can be difficult to determine what is ambitious and achievable because we lack experience with this kind of measurement. For these first iterations, we prefer to set goals that seem ambitious and expect a normal distribution of high, medium, and low achievement across teams with this KR.

## OKRs are what is different

The OKRs are what initiatives we are focusing on this quarter specifically.
Our most important work are things that happen every quarter.
Things that happen every quarter are measured with [Key Performance Indicators](/handbook/ceo/kpis).
Part of the OKRs will be or cause changes in KPIs.
