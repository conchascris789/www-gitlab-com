---
layout: markdown_page
title: "FY20-Q3 OKRs"
---

This fiscal quarter will run from August 1, 2019 to October 31, 2019.

## On this page
{:.no_toc}

- TOC
{:toc}

### 1. CEO: IACV.

1. VP Engineering: [Enterprise-level availability](https://gitlab.com/gitlab-com/www-gitlab-com/issues/4893)
    1. Development: [Improve Predictability around customer expectations](https://gitlab.com/gitlab-com/www-gitlab-com/issues/4841)
    1. Infrastructure: [Raise GitLab.com Availability from 96.64% to a consistent 99.95%](https://gitlab.com/gitlab-com/www-gitlab-com/issues/4918).
    2. Security: [Mitigate and track H1 spend with secure coding training](https://gitlab.com/gitlab-com/www-gitlab-com/issues/4924)
    1. Support: [Create sustainable processes to achieve 95% performance for First Response SLA](https://gitlab.com/gitlab-com/www-gitlab-com/issues/4920)
    1. Support: [Create sustainable processes to achieve 85% performance to Next Response Time SLA](https://gitlab.com/gitlab-com/www-gitlab-com/issues/4921)
    1. Quality: [Improve self-managed enterprise upgrade robustness](https://gitlab.com/gitlab-com/www-gitlab-com/issues/4852)
1. CRO: [Improve Forecast Accuracy:](https://gitlab.com/groups/gitlab-com/-/epics/130) Forecast and report on all bookings and detail by segment and region; Implement MEDDPICC in sales process, forecast methodology and business systems; Measure and report on iACV forecast accuracy and take action to improve.
1. CRO: [Improve Enablement and Productivity:](https://gitlab.com/groups/gitlab-com/-/epics/131) Complete messaging and train WW sales and marketing teams on Command of Message (CoM); create training content (video, static, etc) and first iteration of certification process including ongoing 1:1 training/development cadence; Benchmark current rep ramp time and implement metrics to measure improvements
1. CFO: Improve investing models for go to market functions
    1. [Complete valuation methodology for acquisitions](https://gitlab.com/gitlab-com/finance/issues/967)
    1. [Fully functional pipeline model for funnel from top through to closed won including conversion rates, cycle times and unit economics](https://gitlab.com/gitlab-com/finance/issues/962)
1. CMO: Build key demand generation capabilities
	1. Revenue: SDR Acceleration Team up and running.  Outreach acquisition sequences built for each campaign, first acquisition campaigns launched in a territory.
	1. Revenue: Simple campaign framework in-market, aligned with Command of Message. Agreed and communicated to sales, campaigns running in each region, 1 additional competitive campaign launched in October.
	1. Revenue: Ramp Digital Demand Gen spend. Minimum $2m spent on digital demand gen, $250 cost per MQL achieved, weekly reporting starting Sept. 1.
	1. Ops: Repeatable campaign performance reporting. Periscope campaign dashboard available, realtime pipe-to-spend reporting visible.
	1. Revenue: ABM programs running. Demandbase implemented, target accounts identified by territory, ABM metrics baseline established, territory campaigns running in 2 territories.

### 2. CEO: Popular next generation product.

1. VP Product: Establish a new [Problem Validation](https://about.gitlab.com/handbook/product-development-flow/#validation-phase-2-problem-validation) process. Complete at least 15 problem validation cycles (one per PM, excluding Growth). Complete at least 5 qualitative customer interviews per PM.
1. VP Product: Improve the quality of product usage data, to enable better data driven product decisions. Finalize top Product and Growth [KPI definitions](/handbook/product/metrics/), including [SMAU](/handbook/product/metrics/#adoption) definitions for each stage.  Get reliable and accurate product usage data from both Self Hosted + SaaS customer environments. Deliver SMAU dashboards with actionable data for each stage.
1. VP Product: Ensure prioritization of work to deliver on UX key results to raise our [SUS score](/handbook/engineering/ux/performance-indicators/#perception-of-system-usability) to 76.2 and deliver 30 foundational UI components in Q3.
1. Director of Product, Dev; Director of Product, Ops; Director of Product, CI/CD: Communicate 1 year product Plans at the Section level to inform product investment decisions. Deliver 1 year plan content for Dev, Ops, and CI/CD sections by end of Q3. Stretch goal for Growth, Enablement, Secure, and Defend (pending Director hires).
1. VP of Product Strategy: Get strategic thinking into the org. 4 [strategy reviews](https://gitlab.com/gitlab-com/Product/issues/379) for key industry categories (project management, application security testing, CD/RA, VSM), 100 5-star reviews of each [strategy blog post](https://gitlab.com/gitlab-com/Product/issues/423), 3 [section strategy reviews](https://gitlab.com/gitlab-com/Product/issues/381). => 50%, 0%, 0%
1. VP of Product Strategy: Get acquisitions into shape; build a well-oiled machine. Complete valuation methodology for acquisitions (with FP&A).
1. VP Engineering: [Dogfood everything](https://gitlab.com/gitlab-com/www-gitlab-com/issues/4894)
    1. Development: [Increase MRs, Maintain MR to Author ratio, first response KPI](https://gitlab.com/gitlab-com/www-gitlab-com/issues/4837)
    1. UX Director: [Create products and experiences that users love and value](https://gitlab.com/gitlab-com/www-gitlab-com/issues/4932)
    1. UX Director: [Increase product depth by improving existing product workflows based on customer validation](https://gitlab.com/gitlab-com/www-gitlab-com/issues/4930)
    1. UX Director: [Provide best-in-class documentation for the top 10 Jobs to be Done (JTBD) by improving discovery and making content more relevant and usable](https://gitlab.com/gitlab-com/www-gitlab-com/issues/4931)
    1. Security: [Improve Abuse ML detection and response](https://gitlab.com/gitlab-com/www-gitlab-com/issues/4925)
    1. Support: [Increase support contributions to troubleshooting documentation to enable ticket deflection](https://gitlab.com/gitlab-com/www-gitlab-com/issues/4922)
    1. Quality: [Increase engineering efficiency and remove test gaps in enterprise features](https://gitlab.com/gitlab-com/www-gitlab-com/issues/4853)
1. CMO: Launch corporate awareness program
	1. Corporate: Launch awareness campaign. Agency selected, business objectives agreed by eGroup and BOD, creative approved and tested, media plan in place, first ads in-market.
	1. Corporate: Host 2 user conferences. 500 users at each event, stay within budget, avg. 4 out of 5 on survey event satisfaction.
	1. CMO: Growth marketing test launched. In-app messaging running on GitLab.com for user events, 1,000 click-throughs and 100 user event sign-ups from in-app messaging.
	1. Community: Scale up GitLab Evangelist community. Automated Heroes application process, including defined SLA for acceptance/rejection/rewards, Heroes collateral and communications channel; 25 wider community members with relevant contributions signed up as Heroes, with updated profile page on gitlab.com/community/heroes, including picture, bio, list of contributions; 30 meetups organized.

### 3. CEO: Great team.

1. VP Product:  Hire at least 18 PM roles in Q3 to catch up with annual hiring plan.
1. VP Engineering: [Scale gracefully to 500+ members](https://gitlab.com/gitlab-com/www-gitlab-com/issues/4895)
    1. Development: [Hire to Plan, New Manager Documenation, Increase Maintainers](https://gitlab.com/gitlab-com/www-gitlab-com/issues/4838)
    1. Quality: [Grow the department to plan and improve onboarding experience](https://gitlab.com/gitlab-com/www-gitlab-com/issues/4854)
    1. Security: [Build next phase of zero-trust roadmap](https://gitlab.com/gitlab-com/www-gitlab-com/issues/4926)
1. CPO: People Group to dogfood our own product
    1. VP Recruiting: Continue to successfully drive full cycle recruiting efforts and partnerships with internal stakeholders to bring in top talent while driving towards achieving [Apply to Offer Accept KPI](/handbook/hiring/metrics/#apply-to-offer-accept-days), [Candidates Sourced vs. Candidates Hired KPI](/handbook/hiring/metrics/#candidates-sourced-vs-candidates-hired), [Hire vs Plan KPI](/handbook/hiring/metrics/#hires-vs-plan),  [New Hire Location Factor KPI](/handbook/hiring/metrics/#new-hire-location-factor) and [Candidate ASAT KPI](/handbook/hiring/metrics/#candidate-asat).
    1. Employment Branding: Increase employment brand awareness on LinkedIn among key candidate audiences by creating targeted career pages within the “life” tab on LinkedIn and activate a recruitment marketing campaign to target specific hiring needs
    1. Diversity & Inclusion: Continue to create an environment where all voices can be heard and feel comfortable speaking, while also ensuring equal access to opportunities. Leverage D&I dashboard for benchmarking/recruiting/sourcing. Create new sourcing strategies/initiatives to support gaps identified. Launch Global D&I Advisory Group & Employee Resource Groups
    1. Learning & Development: Enhance effective communication and development throughout the organization. Implement training on ‘communicating effectively through text’ to including how to address if not in line with our values. Update Code of Conduct and Communication page with moderator explanation in handbook, along with documenting the process to discuss with Compliance anything that goes against our expectations and update career growth section in handbook.
    1. Compensation & Benefits:  Continue the evolution of our Compensation/Benefits Model to work towards the [Percentage of Team Member Over Band KPI](/handbook/people-operations/people-operations-metrics/#percent-over-compensation-band). Implement Phase 2 of Radford consulting proposals for compensation iterations and complete Phase 3 of Radford consulting of compensation iterations. Implement benefits to ensure competitive offerings where we have entities/PEOs based on the results of the Benefits Survey from Q2
    1. People Operations: Maintain and continuously improve company culture in order to drive towards [12 month team member turnover](/handbook/people-operations/people-operations-metrics/#team-member-turnover), [12 month voluntary team member turnover](/handbook/people-operations/people-operations-metrics/#team-member-turnover) , and [Discretionary bonus KPIs](/handbook/incentives/#discretionary-bonuses). Implement Anniversary Swag program and support country conversions and entity creation. Get bonus metrics into Periscope by leader and insure each functional leader has a clear understanding of their budget spend and allocation. Share voluntary attrition data with all GitLab team members quarterly, create process for gathering involuntary attrition data to be shared and understand themes of data to find ways to improve
    1. People Operations: [Monitor GitLabber satisfaction and quickly identify emerging trends](https://gitlab.com/gitlab-com/people-ops/General/issues/460)
    1. Handbook Content: Ensure our handbook remains the central repository for how we run the company. Achieve a clean and maintainable state for handbook links. Establish guidelines for separating pages, with standards for content organization (URLS and breadcrumbs) and index organization. Make substantial new content additions along with substantial updates to existing content.
1. CRO: [Hire a Diverse, High Performing Team:](https://gitlab.com/groups/gitlab-com/-/epics/132) Refactor the recruiting process to recover hiring to plan and stay on plan going forward; Ensure a representative mix of diverse qualified candidates are in the recruiting pool, Train and enable new hires quickly to improve produvtivity ramp.
1. CFO: Improve financial planning processes to support growth and increased transparency.
    1. [Achieve ability to use percentage of completion accounting for professional services](https://gitlab.com/gitlab-com/finance/issues/963)
    1. [Version control for financial model that allows for real time analysis without impacting production model](https://gitlab.com/gitlab-com/finance/issues/964)
    1. [Budget vs Forecast vs Actuals automated thru Periscope, including vendor drill down](https://gitlab.com/gitlab-com/finance/issues/965)
    1. [Performance Indicators complete for all G&A functions](https://gitlab.com/gitlab-com/finance/issues/966)
    1. [Unit economics for GitLab.com costing for all product tiers (free, bronze, silver, gold) and internal usage](https://gitlab.com/gitlab-com/finance/issues/954)
1. CMO: New web design and dev hires operational. V1 of Command of Message changes completed, about.gitlab.com repository and CI process streamlined and separated, SEO analyst hired and up-to-speed.




