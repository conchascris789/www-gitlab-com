**Review Apps**: https://release-X-Y.about.gitlab.com/YYYY/MM/22/gitlab-X-Y-released/

_Release post:_

- **Intro**: https://gitlab.com/gitlab-com/www-gitlab-com/blob/release-X-Y/source/posts/YYYY-MM-22-gitlab-X-Y-released.html.md
- **Items**: https://gitlab.com/gitlab-com/www-gitlab-com/blob/release-X-Y/data/release_posts/YYYY_MM_22_gitlab_X_Y_released.yml
- **Images**: https://gitlab.com/gitlab-com/www-gitlab-com/tree/release-X-Y/source/images/X_Y

_Related files:_

- **Features YAML** link: https://gitlab.com/gitlab-com/www-gitlab-com/blob/release-X-Y/data/features.yml
- **Features YAML Images** link: https://gitlab.com/gitlab-com/www-gitlab-com/blob/release-X-Y/source/images/feature_page/screenshots
- **Homepage card**: https://gitlab.com/gitlab-com/www-gitlab-com/blob/release-X-Y/source/includes/home/ten-oh-announcement.html.haml
- **MVPs**: https://gitlab.com/gitlab-com/www-gitlab-com/blob/release-X-Y/data/mvps.yml

_Handbook references:_

- Blog handbook: https://about.gitlab.com/handbook/marketing/blog/
- Release post handbook: https://about.gitlab.com/handbook/marketing/blog/release-posts/
- Markdown guide: https://about.gitlab.com/handbook/product/technical-writing/markdown-guide/

_People:_

- Release Post Managers: https://about.gitlab.com/handbook/marketing/blog/release-posts/managers/
- Release Managers: https://about.gitlab.com/community/release-managers/

| Release post manager | Tech writer | Messaging | Social |
| --- | --- | --- | --- |
| `@mention` | `@mention` |`@mention` | `@mention` |

### Initial checklist (Author)

**Due date: YYYY-MM-DD** (14th)

The PM leading the post is responsible for adding and checking the following items: `@mention`

- [ ] Label MR: ~"blog post" ~release ~"release post" ~P1
- [ ] Assign the MR to yourself
- [ ] Add a release post retrospective issue ([example](https://gitlab.com/gitlab-com/www-gitlab-com/issues/3936)) and provide a link to it at the top of this MR description with the text `Process Improvements? Have suggestions for improving the release post process as we go? Capture them in the [Retrospective issue]().`
- [ ] Check the reviewers on Slack and add their names (and your own) replacing each `@mention` in the MR description checklists
- [ ] Add milestone
- [ ] Update the links and due dates in this MR description
- [ ] Make sure the blog post have all initial files, as well as this MR template, contains the latest template
- [ ] Add authorship (author's data)
- [ ] Add [cover image](https://about.gitlab.com/handbook/marketing/blog/release-posts#cover-image) (`image_title`) (compressed)
- [ ] Add [social sharing image](https://about.gitlab.com/handbook/marketing/blog/release-posts#social-sharing-image) (`twitter_image`) (compressed)
- [ ] Mention managers to remind them to add their team performance improvements: `@gitlab-com/backend-managers` and `@gitlab-org/frontend/frontend-managers`
- [ ] Alert people one working day before each due date (post a comment to #release-post Slack channel)

### General contributions

**Due date: YYYY-MM-DD** (17th)

All contributions added by team members, collaborators, and Product Managers (PMs).

**Note**: the release post has shifted to a changelog style system, to reduce the time and effort required to assemble the post after freeze. Contributions
are made through individual feature YML files, which are reviewed prior to merge.

#### Contribution instructions

- Create a new branch from `master` for each feature/deprecation, and open an MR targeted at the `master` branch using the `Release Post Item` template.
- One YML file for each contribution in `data/release_posts/unreleased/`, see `data/release_posts/unreleased/feature_sample.yml` for format and content. Images should be placed in `/source/images/unreleased/`.
  - Include which section the block belongs to, for example `primary`, `secondary`, or `deprecations`
- Perform the [reviewer checklist](#reviewer-checklist)
- Ask the relevant PMM and tech writer for the group to [review](#reviewer-checklist) the MR. The MR should be reviewed and approved in time to be merged alongside the feature itself, with the PM performing the merge once reviews are complete. If you don't have access to merge, assign the MR to someone who does.
- Content area reminder:
  - Primary features
  - Improvements (secondary features)
  - Deprecations
  - Community contributions
- Primary or potential top feature? Engage PMM early to generate compelling videos, content, etc.

#### Reviewer checklist

- Make sure feature description is positive and cheerful
- Check Feature name
- Check Feature availability (Core, Starter, Premium, Ultimate badges)
- Feature is added to [`data/features.yml`](https://about.gitlab.com/handbook/marketing/website/#adding-features-to-webpages) (with accompanying screenshots)
- Check all images size < 300KB ([compressed](https://tinypng.com/), max width 1000 pixels)
- Check that documentation is updated and easy to understand
- Check Documentation links (all feature blocks contain `documentation_link`)
- Run the content through an automated spelling and grammar check
- Validate all links are functional

#### Feature blocks

**Due date: YYYY-MM-DD** (17th)

The Product Managers are responsible for adding their feature blocks to the
release post by the due date for general contributions. PMs are also responsible
for adding any notable Community Contributions.

PMs: please check your box only when **all** your features and deprecations are added with completion (documentation links, images, etc.). Please don't check if there are still things missing.

Note: be sure to reference your Direction items and Release features. All items which appear
in our [Upcoming Releases page](https://about.gitlab.com/upcoming-releases/) should be included in the relevant release post.

- [ ] Mention the below individuals after feature freeze
- [ ] Manage - Access (`@jeremy`)
- [ ] Manage - Measure (`@valexieva`)
- [ ] Plan (`@ebrinkman`)
- [ ] Create  - Source Code, Gitaly & Gitter (`@jramsay`)
- [ ] Create - Editor & Knowledge (`@phikai`)
- [ ] Verify (`@jlenny`)
- [ ] Package (`@trizzi`)
- [ ] Release (`@cstasik`)
- [ ] Secure - Static Analysis & Dynamic Analysis (`@kencjohnston`)
- [ ] Secure - Composition Analysis (`@NicoleSchwartz`)
- [ ] Configure (`@danielgruesso`)
- [ ] Monitor - APM (`@kencjohnston`)
- [ ] Monitor - Debug & Health (`@sarahwaldner`)
- [ ] Fulfillment (`@tipyn`)
- [ ] Distribution (`@ebrinkman`)

#### Recurring blocks

**Due date: YYYY-MM-DD** (17th)

The following sections are always present and managed by the PM or Eng lead
owning the related area. Each should be :

- [ ] Mention the below individuals after feature freeze
- [ ] Add GitLab Runner improvements: `@jlenny`
- [ ] Add Omnibus improvements: `@ebrinkman`
- [ ] Add Mattermost update to the Omnibus improvements section: `@ebrinkman`

### Content assembly (Author)

The PM leading the post is responsible for adding and checking the following items: `@mention`

**Due date: YYYY-MM-DD** (18th)

- [ ] Pull `master`, resolve any conflicts
- [ ] Aggregate content from all feature blocks in `data/release_posts/unreleased/` into release post YML. Delete individual files when complete.
- [ ] Move referenced images in `/source/images/unreleased/` to `/source/images/X_Y/`.
- [ ] Make sure all features listed in the [direction](https://about.gitlab.com/direction/)
page are included in the post
- [ ] Mention the [Distribution PM](https://about.gitlab.com/handbook/product/categories/#distribution-group) asking them if there's any relevant info to be added to the [upgrade warning](https://about.gitlab.com/handbook/marketing/blog/release-posts#upgrade-warning) section. If not, delete the `upgrade` block in the release post data file
- [ ] Confirm team performance improvements have been included: @gitlab-com/backend-managers and @gitlab-org/frontend/frontend-managers
- [ ] Ask the messaging lead to add the introduction by the 2nd working day before the 22nd
- [ ] Check with the [Messaging Lead](https://about.gitlab.com/handbook/marketing/blog/release-posts/#messaging-lead) on what the top features should be.
- [ ] Check if deprecations are included
- [ ] Add [MVP](https://about.gitlab.com/handbook/marketing/blog/release-posts/#mvp) (MVP block)
- [ ] Make sure the PM for the MVP feature adds a corresponding feature block if applicable, linking from the MVP section
- [ ] Add MVP to `data/mvps.yml`
- [ ] Make sure all images (png, jpg, and gifs) are smaller than 300 KB each
- [ ] Run the release post through an automated spell and grammar check
- [ ] Perform the [content review](#content-review)

### Review

Ideally, complete the review until the **4th** working day before the 22nd,
so that the 3rd and the 2nd working day before the release
could be left for fixes and small improvements.

#### Content review

**Due date: YYYY-MM-DD** (18th)

Performed by the author of the post: `@mention`

- [ ] Label MR: ~"blog post" ~release ~review-in-progress
- [ ] Check all comments in the thread (make sure no contribution was left behind)
- [ ] Reorder primary and secondary features according to their relevance to the user (most impactful features come first)
- [ ] Make sure all discussions in the thread are resolved
- [ ] Mention `@markpundsack`, `@kencjohnston`, `@jlenny`, and `@ebrinkman` to remind them to review
- [ ] Mention `@sytse` in #release-post on Slack when the post has been generated for his review
- [ ] Assign the MR to the next reviewer (structural check)
- [ ] Lock [`features.yml`](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/features.yml) with File Locking **on the 21st**

#### Structural check

**Due date: YYYY-MM-DD** (19th or next business day)

The [structural review (https://about.gitlab.com/handbook/marketing/blog/release-posts/#structural-check) is performed by a technical writer: `@mention`

- [ ] Add the label ~review-structure
- [ ] Pull `master` into the release post branch and resolve any conflicts
- [ ] Check [frontmatter](https://about.gitlab.com/handbook/marketing/blog/release-posts/#frontmatter) (entries, syntax)
- [ ] Check `image_title` and `twitter_image`
- [ ] Check image shadow applied correctly in release post data. Look for `image_noshadow: true` when an image already has a shadow
- [ ] Ensure videos and iframes are wrapped in `<figure class="video_container">` tags (for responsiveness)
- [ ] Ensure all text is consistently hard wrapped at a suitable column boundary
- [ ] Check headings are in sentence case
- [ ] Check feature and product names are in capital case
- [ ] Check if images are harmonic/consistent
- [ ] Check any `.gitkeep` files and delete them if any
- [ ] Add or check cover img reference (at the end of the post). Data in `cover_img` in release post Yaml file
- [ ] Ensure content is balanced between the columns (both columns are even)
- [ ] Ensure links have [meaningful text](https://about.gitlab.com/handbook/communication/#writing-style-guidelines) for SEO (e.g., "click here" is bad link text)
- [ ] Check for badges consistency (applied to all headings)
- [ ] Double check that all features are documented
- [ ] Check that all links to about.gitlab.com content are relative URLs
- [ ] Check the anchor links in the intro. All links in the release post markdown file should point to something in the release post Yaml file
- [ ] Remove any remaining instructions (comments) from the release post files
- [ ] Run a dead link checker, e.g., [Check my Links](https://chrome.google.com/webstore/detail/check-my-links/ojkcdipcgfaekbeaelaapakgnjflfglf) and ping reporters directly on Slack asking them to fix broken links
- [ ] Update the release post templates and release post handbook with anything that comes up during the process
- [ ] Pull `master` into the release post branch and resolve any conflicts (again)
- [ ] Report any problems from structural check in the `#release-post` channel by pinging the reporters directly for each problem. Do NOT ping `@all` or `@channel` nor leave a general message to which no one will pay attention. If possible, ensure open discussions in the merge request track any issues
- [ ] Post a comment in the `#company-announcements` channel linking to the review app + merge request reminding the team to take a look at the release post and to report problems in `#release-post`. Template to use (replace links):
  ```md
  Hey all! This month's release post is almost ready! Take a look at it and report any problems in #release-post.
  MR: https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/1234
  Review app: https://release-X-Y.about-src.gitlab.com/YYYY/MM/DD/gitlab-X-Y-released/
  ```
- [ ] Remove the label ~review-structure.
- [ ] Assign the MR to the next reviewer (Marketing).

#### Further reviews

**Due date: YYYY-MM-DD** (the 2nd working day before the 22nd)

- [ ] Marketing review (PMMs - [messaging lead](https://about.gitlab.com/handbook/marketing/blog/release-posts/#messaging-lead)): `@mention`
  - [ ] Write the introduction (**by the 6th working day before the 22nd**)
  - [ ] Write social sharing text for the tweet announcement after the release post is live
  - [ ] Check/copyedit feature blocks
  - [ ] Check/copyedit `features.yml`
  - [ ] Check/copyedit homepage blurb (check ten-oh-announcement.haml)
  - [ ] Mention Mark P (`@markpundsack`) for final check
  - [ ] Remove the label ~review-in-progress
  - [ ] Assign the MR back to the author

### On the 22nd

The author of the post is responsible for merging the MR and following up
with possible adjustments/fixes: `@mention`.

#### At 12:00 UTC

- [ ] Read the [important notes](#important-notes) below
- [ ] At ~12:30 UTC, ping the release managers on the `#releases` Slack
channel asking if everything is on schedule, and to coordinate timing with them:
  - If anything is wrong with the release, or if it's delayed, you must ping
  the messaging lead on `#release-post` so that they coordinate anything scheduled
  on their side (e.g., press releases, other posts, etc).
  - If everything is okay, the packages should be published at [13:30 UTC](https://gitlab.com/gitlab-org/release-tools/blob/master/templates/monthly.md.erb#L155), and available
  publicly around 14:10 UTC.
  - Ask the release managers to ping you when the packs are publicly available (and GitLab.com is up and running on the release version)
- [ ] Mention `@sumenkovic` to remind him to send the swag pack to the MVP
- [ ] Check if all the anchor links in the intro are working
- [ ] Check if there are no broken links in the review app (use a dead link checker, e.g., [Check my Links](https://chrome.google.com/webstore/detail/check-my-links/ojkcdipcgfaekbeaelaapakgnjflfglf))
- [ ] Pull `master` and fix any conflicts
- [ ] Check if there isn't any alert on Slack's `#release-post` and `#company-announcements` channels
- [ ] Check if there isn't any alert on this MR thread
- [ ] Check if the tweet copy is ready and someone is ready to share on social media

#### At 13:50 UTC

- [ ] Check if there aren't any conflicts. If there are any, pull `master` again and fix them.

Once the release manager confirmed that the packages are publicly available:

- [ ] Unlock [`features.yml`](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/features.yml) just before merging.
- [ ] Merge the MR at 14:10-14:20 UTC.
- [ ] Wait for the pipeline. It should take ~40min to complete.
- [ ] Check the look on social media with [Twitter Card Validator](https://cards-dev.twitter.com/validator) and [Facebook Debugger](https://developers.facebook.com/tools/debug/).
- [ ] Check for broken links again once the post is live.
- [ ] Share on social media (or make sure someone else does) only when you're sure everything is okay.
- [ ] Share the links to the post and to the tweet on the `#release-posts` and
`#company-announcements` on Slack.

#### Important notes

- The post is to be live on the **22nd** at **15:00 UTC**. It should
be merged and as soon as GitLab.com is up and running on the new
release version (or the latest RC that has the same features as the release),
and once all packages are publicly available. Not before. Ideally,
merge it around 14:20 UTC as the pipeline takes about 40 min to run.
- The usual release time is **15:00 UTC** but it varies according to
the deployment. If something comes up and delays the release, the
release post will be delayed with the release.
- Coordinate the timing with the [release managers (https://about.gitlab.com/community/release-managers/). Ask them to
keep you in the loop. Ideally, the packages should be published around
13:30-13:40, so they will be publicly available around 14:10 and you'll
be able to merge the post at 14:20ish.
- Once the release post is live, wait a few minutes to see if no one
spot an error (usually posted in #company-announcements), then share
on Twitter, Facebook, and LinkedIn, or make sure someone
(Emily vH, JJ, Marcia) does. Coordinate the social sharing with
them **beforehand**.
- Keep an eye on Slack and in the blog post comments for a few hours
to make sure no one found anything that needs fixing.
