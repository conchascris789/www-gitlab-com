stages:

  # Stages are defined in the product categories handbook https://about.gitlab.com/handbook/product/categories/#hierarchy

  manage:
    display_name: "Manage"
    marketing: true
    image: "/images/solutions/solutions-manage.png"
    description: "Gain visibility and insight into how your business is performing."
    body: |
          GitLab helps teams manage and optimize their software delivery lifecycle with metrics and value stream insight in order to streamline and increase their delivery velocity.   Learn more about how GitLab helps to manage your end to end <a href="https://about.gitlab.com/vsm">value stream.</a>
    vision: /direction/manage
    roadmap: "https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Amanage&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS"
    established: 2016
    lifecycle: 4
    contributions: 34
    analyst_reports:
      - title: "Whitepaper: Reduce Cycle Time"
        url: "/resources/whitepaper-reduce-cycle-time/"
    related:
      - "plan"
      - "create"
      - "verify"
      - "package"
      - "release"
      - "configure"
      - "monitor"
      - "secure"
      - "defend"
    dept: dev
    groups:
      access:
        name: Access
        pm: Jeremy Watson
        pmm: John Jeremiah
        cm: Suri Patel
        backend_engineering_manager: Liam McAndrew
        frontend_engineering_manager: Dennis Tang
        support: Blair Lunceford
        test_automation_engineers:
          - Sanad Liaquat
        uxm: Shane Bouchard
        ux:
          - Matej Latin
        tech_writer: Evan Read
        internal_customers:
          - Support Department
          - Security Department
          - Infrastructure Department
        categories:
          - audit_management
          - authentication_and_authorization
          - administration
          - groups
          - users
          - workflow_policies
      import:
        name: Import
        pm: Jeremy Watson (Interim)
        pmm: John Jeremiah
        cm: Suri Patel
        backend_engineering_manager: Liam McAndrew
        frontend_engineering_manager: Dennis Tang
        test_automation_engineers:
          - Sanad Liaquat
        uxm: Shane Bouchard
        ux:
          - Matej Latin
        tech_writer: Evan Read
        internal_customers:
          - Support Department
          - Product Department
        categories:
          - importers
          - templates
          - internationalization
      analytics:
        name: Analytics
        pm: Virjinia Alexieva
        pmm: John Jeremiah
        cm: Suri Patel
        backend_engineering_manager: Liam McAndrew
        frontend_engineering_manager: Dennis Tang
        support: Blair Lunceford
        test_automation_engineers:
          - Désirée Chevalier
        uxm: Shane Bouchard
        ux:
          - Chris Peressini
        tech_writer: Evan Read
        internal_customers:
          - Engineering Department
          - Product Department
          - Quality Department
        categories:
          - devops_score
          - code_analytics
          - value_stream_management

  plan:
    display_name: "Plan"
    marketing: true
    image: "/images/solutions/solutions-plan.png"
    description: "Regardless of your process, GitLab provides powerful planning
      tools to keep everyone synchronized."
    body: |
      GitLab enables portfolio planning and management through epics, groups (programs) and milestones to organize and track progress.  Regardless of your methodology from Waterfall to DevOps, GitLab’s simple and flexible approach to planning meets the needs of small teams to large enterprises.

      GitLab helps teams organize, plan, align and track project work to ensure teams are working on the right things at the right time and maintain end to end visibility and traceability of issues throughout the delivery lifecycle from idea to production.
    vision: /direction/plan
    roadmap: "https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Aplan&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS"
    established: 2011
    lifecycle: 5
    contributions: 44
    analyst_reports:
      - title: Gartner Magic Quadrant for Enterprise Agile Planning Tools
        url: "https://about.gitlab.com/analysts/gartner-eapt/"
    related:
      - "create"
    dept: dev
    groups:
      team_planning:
        name: Team Planning
        pm: Gabe Weaver
        pmm: John Jeremiah
        cm: Suri Patel
        backend_engineering_manager: Sean McGivern
        frontend_engineering_manager: Donald Cook
        support: TBD
        test_automation_engineers:
          - Walmyr Lima e Silva Filho
        uxm: Shane Bouchard
        ux:
          - Annabel Dunstone Gray
          - Alexis Ginsberg
        tech_writer: Russell Dickenson
        internal_customers:
          - Engineering Department
          - Product Department
        categories:
          - issue_tracking
          - project_management
          - kanban_boards
          - time_tracking
      portfolio_management:
        name: Portfolio Management
        pm: Gabe Weaver (Interim)
        pmm: John Jeremiah
        cm: Suri Patel
        backend_engineering_manager: John Hope
        frontend_engineering_manager: Donald Cook
        support: TBD
        test_automation_engineers:
          - Walmyr Lima e Silva Filho
        uxm: Shane Bouchard
        ux:
          - Annabel Dunstone Gray
          - Alexis Ginsberg
        tech_writer: Russell Dickenson
        internal_customers:
          - Engineering Department
          - Product Department
        categories:
          - agile_portfolio_management
      certify:
        name: Certify
        pm: Gabe Weaver (Interim)
        pmm: John Jeremiah
        cm: Suri Patel
        backend_engineering_manager: John Hope
        frontend_engineering_manager: Donald Cook
        support: TBD
        test_automation_engineers:
          - Walmyr Lima e Silva Filho
        uxm: Shane Bouchard
        ux:
          - Annabel Dunstone Gray
          - Alexis Ginsberg
        tech_writer: Russell Dickenson
        internal_customers:
          - Quality Department
          - Support Department
        categories:
          - requirements_management
          - quality_management
          - service_desk

  create:
    display_name: "Create"
    marketing: true
    image: "/images/solutions/solutions-create.png"
    description: |
      Create, view, and manage code and project data through powerful branching
      tools.
    body: |
      GitLab helps teams design, develop and securely manage code and project
      data from a single distributed version control system to enable rapid
      iteration and delivery of business value.  GitLab repositories provide a
      scalable, single source of truth for collaborating on projects and code
      which enables teams to be productive without disrupting their workflows.
    vision: /direction/create
    roadmap: "https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Acreate&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS"
    established: 2011
    lifecycle: 7
    contributions: 34
    analyst_reports:
    related:
      - "plan"
      - "verify"
    dept: dev
    groups:
      source_code:
        name: Source Code
        pm: James Ramsay
        pmm: John Jeremiah
        cm: Suri Patel
        backend_engineering_manager: Douwe Maan
        frontend_engineering_manager: André Luís
        backend_team: engineering/development/dev/create-source-code-be
        support: TBD
        test_automation_engineers:
          - Mark Lapierre
        uxm: Shane Bouchard
        ux:
          - Pedro Moreira da Silva
        tech_writer: Marcia Ramos
        internal_customers:
          - Engineering Department
          - Marketing Department
          - Product Department
        categories:
          - source_code_management
          - code_review
      knowledge:
        name: Knowledge
        pm: Kai Armstrong
        pmm: John Jeremiah
        cm: Suri Patel
        backend_engineering_manager: Darva Satcher
        frontend_engineering_manager: André Luís
        backend_team: engineering/development/dev/create-knowledge-be
        support: TBD
        test_automation_engineers:
          - Tomislav Nikić
        uxm: Shane Bouchard
        ux:
          - Jarek Ostrowski
        tech_writer: Marcia Ramos
        internal_customers:
          - UX Department
        categories:
          - design_management
          - wiki
      editor:
        name: Editor
        pm: Kai Armstrong
        pmm: John Jeremiah
        cm: Suri Patel
        backend_engineering_manager: Darva Satcher
        frontend_engineering_manager: André Luís
        backend_team: engineering/development/dev/create-editor-be
        support: TBD
        test_automation_engineers:
          - Tomislav Nikić
        uxm: Shane Bouchard
        ux:
          - Marcel van Remmerden
        tech_writer: Marcia Ramos
        internal_customers:
          - Marketing Department
          - Engineering Department
          - Finance Department
          - People Operations
          - Product Department
        categories:
          - web_ide
          - live_coding
          - search
          - snippets
      gitaly:
        name: Gitaly
        pm: James Ramsay
        image: "/images/solutions/solutions-gitaly.png"
        description: |
          Gitaly is a Git RPC service for handling all the git calls made by
          GitLab.
        body: |
          GitLab makes Gitaly
        vision: "https://gitlab.com/groups/gitlab-org/-/epics/710"
        backend_engineering_manager: ZJ (Interim)
        frontend_engineering_manager: Tim Z (Interim)
        support: Will Chandler
        test_automation_engineers:
          - Mark Lapierre
        uxm: Shane Bouchard
        ux:
          - Pedro Moreira da Silva
        tech_writer: Axil
        internal_customers:
          - Quality Department
          - Infrastructure Department
        categories:
          - gitaly
      gitter:
        name: Gitter
        pm: James Ramsay
        body: |
          Gitter is an open source instant messaging application for
          developers, and is the place to connect the open source and software
          development community.
        vision: "https://gitlab.com/groups/gitlab-org/-/epics/850"
        backend_engineering_manager: ZJ (Interim)
        frontend_engineering_manager: Tim Z (Interim)
        support: TBD
        test_automation_engineers:
          - Mark Lapierre
          - Tomislav Nikić
        uxm: Shane Bouchard
        ux:
          - Jarek Ostrowski
        tech_writer: Axil
        categories:
          - gitter

  verify:
    display_name: "Verify"
    marketing: true
    image: "/images/solutions/solutions-verify.png"
    description: "Keep strict quality standards for production code with automatic testing and reporting."
    body: |
      GitLab helps delivery teams fully embrace continuous integration to automate the builds, integration and verification of their code.  GitLab’s industry leading CI capabilities enables automated testing, Static Analysis Security Testing, Dynamic Analysis Security testing and code quality analysis to provide fast feedback to developers and testers about the quality of their code.  With pipelines that enable concurrent testing and parallel execution, teams quickly get insight about every commit, allowing them to deliver higher quality code faster.
    vision: /direction/verify
    roadmap: "https://gitlab.com/groups/gitlab-org/-/roadmap?label_name[]=devops%3Averify&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS"
    established: 2012
    lifecycle: 7
    contributions: 65
    analyst_reports:
      - title: "Forrester Wave: Continuous Integration Tools"
        url: "https://about.gitlab.com/analysts/forrester-ci/"
    related:
      - "create"
    dept: cicd
    groups:
      continuous_integration:
        name: "Continuous Integration"
        pm: "Jason Lenny (Interim)"
        pmm: William Chia
        cm: Aricka Flowers
        backend_engineering_manager: Elliot Rushton
        frontend_engineering_manager: John Hampton (Interim)
        support: Namho Kim
        test_automation_engineers:
          - Zeff Morgan
        uxm: Valerie Karnes
        ux:
          - Dimitrie Hoekstra
        tech_writer: Evan Read
        internal_customers:
          - Quality Department
          - UX Department
          - Infrastructure Department
          - Engineering Department
          - AutoDevOps and Kubernetes Group
        categories:
          - continuous_integration
      runner:
        name: "Runner"
        pm: "Joshua Lambert (Interim)"
        pmm: William Chia
        cm: Aricka Flowers
        backend_engineering_manager: Elliot Rushton
        frontend_engineering_manager: John Hampton (Interim)
        support: Namho Kim
        test_automation_engineers:
          - Zeff Morgan
        uxm: Valerie Karnes
        ux:
          - Dimitrie Hoekstra
        tech_writer: Evan Read
        internal_customers:
          - Infrastructure Department
          - Engineering Department
        categories:
          - runner
      testing:
        name: "Testing"
        pm: "Orit Golowinski (Interim)"
        pmm: William Chia
        cm: Aricka Flowers
        backend_engineering_manager: Elliot Rushton
        frontend_engineering_manager: John Hampton (Interim)
        support: Namho Kim
        test_automation_engineers:
          - Zeff Morgan
        uxm: Valerie Karnes
        ux:
          - Dimitrie Hoekstra
        tech_writer: Evan Read
        internal_customers:
          - Quality Department
          - UX Department
          - Infrastructure Department
          - AutoDevOps and Kubernetes Group
        categories:
          - code_quality
          - performance_testing
          - system_testing
          - usability_testing
          - accessibility_testing

  package:
    display_name: "Package"
    marketing: true
    image: "/images/solutions/solutions-package.png"
    description: "Create a consistent and dependable software supply chain with built-in universal package management."
    body: |
      GitLab enables teams to package their applications and dependencies, manage containers, and build artifacts with ease. The private, secure container registry and artifact repositories are built-in and preconfigured out-of-the box to work seamlessly with GitLab source code management and CI/CD pipelines. Ensure DevOps acceleration with automated software pipelines that flow freely without interuption.
    vision: /direction/package
    roadmap: "https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Apackage&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS"
    established: 2016
    lifecycle: 3
    contributions: 3
    analyst_reports:
    dept: cicd
    groups:
      package:
        name: Package
        pm: Tim Rizzi
        pmm: William Chia
        cm: Aricka Flowers
        backend_engineering_manager: Daniel Croft
        frontend_engineering_manager: John Hampton
        support: Sara Ahbabou
        uxm: Valerie Karnes
        ux:
          - Iain Camacho
        tech_writer: Axil
        internal_customers:
          - Distribution Team
          - Infrastructure Department
        categories:
          - container_registry
          - package_registry
          - helm_chart_registry
          - dependency_proxy

  secure:
    display_name: "Secure"
    marketing: true
    image: "/images/solutions/solutions-secure.png"
    description: "Security capabilities, integrated into your development lifecycle."
    body: |
      GitLab provides Static Application Security Testing (SAST), Dynamic Application Security Testing (DAST), Container Scanning, and Dependency Scanning to help you deliver secure applications along with license management.
    vision: /direction/secure
    roadmap: "https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Asecure&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS"
    established: 2017
    lifecycle: 3
    contributions: 3
    analyst_reports:
      - title: "Forrester Wave: Software Composition Analysis"
        url: "https://about.gitlab.com/analysts/forrester-sca/"
    related:
      - "create"
      - "defend"
    dept: secure
    cta: includes/ctas/security-whitepaper
    groups:
      static_analysis:
        name: Static Analysis
        pm: Sam Kerr (Life Support)
        pmm: Cindy Blake
        cm: Erica Lindberg
        backend_engineering_manager: Thomas Woodham
        frontend_engineering_manager: Lukas Eipert (Interim)
        support: Thiago Presa
        test_automation_engineers:
          - Aleksandr Soborov
        uxm: Valerie Karnes
        ux:
          - Andy Volpe
          - Kyle Mann
        tech_writer: Axil
        internal_customers:
          - Security Department
          - Engineering Department
        categories:
          - static_application_security_testing
          - secret_detection
          - language_specific
      dynamic_analysis:
        name: Dynamic Analysis
        pm: Sam Kerr (Life Support)
        pmm: Cindy Blake
        cm: Erica Lindberg
        backend_engineering_manager: Seth Berger
        frontend_engineering_manager: Lukas Eipert (Interim)
        support: Thiago Presa
        test_automation_engineers:
          - Aleksandr Soborov
        uxm: Valerie Karnes
        ux:
          - Andy Volpe
          - Kyle Mann
        tech_writer: Axil
        internal_customers:
          - Security Department
          - Engineering Department
        categories:
          - dynamic_application_security_testing
          - interactive_application_security_testing
          - fuzzing

      composition_analysis:
        name: Composition Analysis
        pm: Nicole Schwartz
        pmm: Cindy Blake
        cm: Erica Lindberg
        backend_engineering_manager: Olivier Gonzalez (Interim)
        frontend_engineering_manager: Lukas Eipert (Interim)
        support: Greg Myers
        test_automation_engineers:
          - Aleksandr Soborov
        uxm: Valerie Karnes
        ux:
          - Andy Volpe
          - Kyle Mann
        tech_writer: Axil
        internal_customers:
          - Security Department
          - Engineering Department
        categories:
          - dependency_scanning
          - container_scanning
          - license_management
          - vulnerability_database
        feature_labels:
          - security reports
          - dependency list

  release:
    display_name: "Release"
    marketing: true
    image: "/images/solutions/solutions-release.png"
    description: "GitLab's integrated CD solution allows you to ship code with zero-touch, be it on one or one thousand servers."
    body: |
      GitLab helps automate the release and delivery of applications, shortening the delivery lifecycle, streamlining manual processes, and accelerating team velocity. With zero-touch Continuous Delivery (CD) built right into the pipeline, deployments can be automated to multiple environments like staging and production, and the system just knows what to do without being told - even for more advanced patterns like canary deployments. With feature flags, built-in auditing/traceability, on-demand environments, and GitLab pages for static content delivery, you'll be able to deliver faster and with more confidence than ever before.
    vision: /direction/release
    roadmap: "https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Arelease&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS"
    established: 2016
    lifecycle: 4
    contributions: 9
    analyst_reports:
      - title: "Gartner Magic Quadrant for Application Release Orchestration"
        url: "https://about.gitlab.com/analysts/gartner-aro/"
      - title: "Forrester Wave: Continuous Delivery and Release Automation"
        url: "https://about.gitlab.com/analysts/forrester-cdra/"
    related:
      - "configure"
    dept: cicd
    groups:
      progressive_delivery:
        name: "Progressive Delivery"
        pm: Orit Golowinski
        pmm: William Chia
        cm: Aricka Flowers
        backend_engineering_manager: Darby Frey
        frontend_engineering_manager: John Hampton
        support: TBD
        uxm: Valerie Karnes
        ux:
          - Rayana Verissimo
          - Mike Nichols
        tech_writer: Marcia Ramos
        internal_customers:
          - Delivery Team
          - Distribution Team
          - Gitter
          - Security Department
          - Tech Writers
          - AutoDevOps and Kubernetes Group
        categories:
          - continuous_delivery
          - incremental_rollout
          - review_apps
          - feature_flags
      release_management:
        name: "Release Management"
        pm: Corrina Stasik
        pmm: William Chia
        cm: Aricka Flowers
        backend_engineering_manager: Darby Frey
        frontend_engineering_manager: John Hampton
        support: TBD
        uxm: Valerie Karnes
        ux:
          - Rayana Verissimo
          - Mike Nichols
        tech_writer: Marcia Ramos
        internal_customers:
          - Distribution Team
          - Gitter
          - Security Department
          - Tech Writers
          - Marketing Department
          - UX Department
        categories:
          - release_orchestration
          - secrets_management
          - release_governance
          - pages

  configure:
    display_name: "Configure"
    marketing: true
    image: "/images/solutions/solutions-configure.png"
    description: "Configure your applications and infrastructure."
    body: "GitLab helps teams to configure and manage their application environments.  Strong integration to Kubernetes reduces the effort needed to define and configure the infrastructure required to support your application.   Protect access to key infrastructure configuration details such as passwords and login information by using ‘secret variables’ to limit access to only authorized users and processes."
    vision: /direction/configure
    roadmap: "https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Aconfigure&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS"
    established: 2018
    lifecycle: 3
    contributions: 10
    analyst_reports:
    related:
      - "configure"
    dept: ops
    groups:
      autodevops_and_kubernetes:
        name: "Auto DevOps & Kubernetes"
        pm: Daniel Gruesso
        pmm: William Chia
        cm: Aricka Flowers
        backend_engineering_manager: Dylan Griffith
        frontend_engineering_manager: Jean du Plessis
        support: Lewis Brown
        test_automation_engineers:
          - Dan Davison
        uxm: Aaron White
        ux:
          - Taurie Davis
        tech_writer: Evan Read
        internal_customers:
          - Quality Department
          - Site Availability Engineering
          - Site Reliability Engineering
          - Engineering Department
        categories:
          - auto_devops
          - kubernetes_configuration
          - chatops
          - runbooks
          - chaos_engineering
          - cluster_cost_optimization
      serverless_and_paas:
        name: "Serverless & PaaS"
        pm: Daniel Gruesso (Interim)
        pmm: William Chia
        cm: Aricka Flowers
        backend_engineering_manager: Nicholas Klick
        frontend_engineering_manager: Jean du Plessis
        support: Lewis Brown
        test_automation_engineers:
          - Dan Davison
        uxm: Aaron White
        ux:
          - Taurie Davis
        tech_writer: Evan Read
        internal_customers:
          - Quality Department
          - Site Availability Engineering
          - Site Reliability Engineering
        categories:
          - serverless
          - paas

  monitor:
    display_name: "Monitor"
    marketing: true
    image: "/images/solutions/solutions-monitor.png"
    description: "Automatically monitor metrics so you know how any change in code
      impacts your production environment."
    body: |
      You need feedback on what the effect of your release is in order to do
      release management. Get monitoring built-in, see the code change
      right next to the impact that it has so you can respond quicker and
      effectively. No need to babysit deployment to manually get feedback.
      Automatically detect buggy code and prevent it from affecting the
      majority of your users.
    vision: /direction/monitor
    roadmap: "https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Amonitor&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS"
    established: 2017
    lifecycle: 1
    contributions: 4
    analyst_reports:
    related:
      - "release"
    dept: ops
    groups:
      apm:
        name: APM
        pm: Kenny Johnston (Life Support)
        pmm: William Chia
        cm: Aricka Flowers
        backend_engineering_manager: Matt Nohr
        frontend_engineering_manager: Adriel Santiago (Interim)
        support: Cody West
        uxm: Aaron White
        ux:
          - Amelia Bauerly
          - Sarah Voegeli
        tech_writer: Axil
        internal_customers:
          - Infrastructure Department
          - Engineering Department
        categories:
          - metrics
          - tracing
          - logging
        feature_labels:
          - APM
      health:
        name: "Health"
        pm: Sarah Waldner
        pmm: William Chia
        cm: Aricka Flowers
        backend_engineering_manager: Seth Engelhard
        frontend_engineering_manager: Clement Ho
        support: Cody West
        uxm: Aaron White
        ux:
          - Amelia Bauerly
          - Sarah Voegeli
        tech_writer: Axil
        internal_customers:
          - Infrastructure Department
          - Engineering Department
        categories:
          - error_tracking
          - cluster_monitoring
          - synthetic_monitoring
          - incident_management
          - status_page
        feature_labels:
          - infrastructure monitoring

  defend:
    display_name: "Defend"
    marketing: true
    image: "/images/stages-devops-lifecycle/placeholder.png"
    description: "Defend your apps and infrastructure from security intrusions."
    body: |
      GitLab provides runtime application security, threat detection and management, data security, and application infrastructure security.
    vision: /direction/defend
    roadmap: "https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Adefend&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS"
    established: 2019
    lifecycle: 0
    contributions: 0
    analyst_reports:
    related:
      - "release"
      - "secure"
      - "manage"
    dept: defend
    groups:
      runtime_application_security:
        name: Runtime Application Security
        pm: Sam Kerr
        pmm: Cindy Blake
        cm: Erica Lindberg
        backend_engineering_manager: Thomas Woodham
        frontend_engineering_manager: Lukas Eipert (Interim)
        support: TBD
        uxm: Valerie Karnes
        ux:
          - Rebecca "Becka" Lippert
        tech_writer: Axil
        internal_customers:
          - Security Department
          - Infrastructure Department
        categories:
          - runtime_application_self_protection
          - web_firewall
      threat_management:
        name: "Threat Management"
        pm: Sam Kerr
        pmm: Cindy Blake
        cm: Erica Lindberg
        backend_engineering_manager: Thomas Woodham
        frontend_engineering_manager: Lukas Eipert (Interim)
        support: TBD
        uxm: Valerie Karnes
        ux:
          - Rebecca "Becka" Lippert
        tech_writer: Axil
        internal_customers:
          - Security Department
        categories:
          - threat_detection
          - behavior_analytics
          - vulnerability_management
      application_infrastructure_security:
        name: Application Infrastructure Security
        pm: Sam Kerr
        pmm: Cindy Blake
        cm: Erica Lindberg
        backend_engineering_manager: Thomas Woodham
        frontend_engineering_manager: Lukas Eipert (Interim)
        support: TBD
        uxm: Valerie Karnes
        ux:
          - Rebecca "Becka" Lippert
        tech_writer: Axil
        internal_customers:
          - Security Department
          - Infrastructure Department
        categories:
          - data_loss_prevention
          - storage_security
          - container_network_security

  growth:
    display_name: Growth
    dept: growth
    marketing: false
    related:
      - manage
      - plan
      - create
      - verify
      - package
      - release
      - configure
      - monitor
      - secure
      - defend
    established: 2019
    internal_customers:
      - Sales Department
      - Marketing Department
      - Product Department
      - Finance Department
    groups:
      activation:
        name: Activation
        description: "Acquired and activated users, last 30 days"
        pm: Jensen Stava
        em: Bartek Marnane (Interim)
        uxm: Jacki Bauer
        ux:
          - Kevin Comoli
      adoption:
        name: Adoption
        description: "Adoption of stage cross-use - Stage Monthly Active Users (SMAU)"
        pm: Tim Hey
        em: Bartek Marnane (Interim)
        uxm: Jacki Bauer
        ux:
          - Kevin Comoli
      upsell:
        name: Upsell
        description: "Conversion and Upsell - % of users who went to a (higher) subscription"
        pm: Sam Awezec
        em: Bartek Marnane (Interim)
        uxm: Jacki Bauer
        ux:
          - Kevin Comoli
      retention:
        name: Retention
        description: "Retention including reactivation"
        pm: Scott Williamson (Interim)
        em: Bartek Marnane (Interim)
        uxm: Jacki Bauer
        ux:
          - Kevin Comoli
      fulfillment:
        name: Fulfillment
        pm: Luca Williams
        pmm: John Jeremiah
        cm: Suri Patel
        backend_engineering_manager: James Lopez (Interim)
        frontend_engineering_manager: Dennis Tang (Interim)
        support: Amanda Rueda
        uxm: Jacki Bauer
        tech_writer: Evan Read
        ux:
          - Tim Noah
          - Emily Sybrant
        internal_customers:
          - Sales Department
          - Marketing Department
          - Product Department
          - Customer Success
        categories:
          - licensing
          - transactions
      telemetry:
        name: Telemetry
        pm: Luca Williams (Interim)
        pmm: John Jeremiah
        cm: Suri Patel
        backend_engineering_manager: James Lopez (Interim)
        frontend_engineering_manager: Dennis Tang (Interim)
        support: TBD
        uxm: Jacki Bauer
        tech_writer: Evan Read
        internal_customers:
          - Sales Department
          - Marketing Department
          - Product Department
          - Customer Success
          - Data Team
        categories:
          - collection
          - analysis

  enablement:
    display_name: Enablement
    dept: enablement
    related:
      - "create"
    established: 2012
    groups:
      distribution:
        name: Distribution
        image: "/images/solutions/solutions-distribution.png"
        description: "Add distribution description here"
        body: |
          GitLab helps distribution
        vision: /direction/distribution
        roadmap: "https://gitlab.com/groups/gitlab-org/-/roadmap?label_name[]=devops%3Adistribution&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS"
        pm: Eric Brinkman (Interim)
        pmm: William Chia
        backend_engineering_manager: Steven Wilson
        support: Diana Stanley
        uxm: Jacki Bauer
        tech_writer: Axil
        internal_customers:
          - Quality Department
          - Infrastructure Department
          - Customer Success
        categories:
          - omnibus
          - cloud_native_installation
      geo:
        name: "Geo"
        image: "/images/solutions/solutions-geo.png"
        description: "Add Geo description here"
        body: |
          GitLab makes Geo
        vision: /direction/geo
        roadmap: "https://gitlab.com/groups/gitlab-org/-/roadmap?label_name[]=devops%3Adistribution&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS"
        pm: Fabian Zimmer
        pmm: John Jeremiah
        backend_engineering_manager: Rachel Nienaber
        frontend_engineering_manager: André Luís (Interim)
        support: Aric Buerer
        uxm: Jacki Bauer
        test_automation_engineers:
          - Jennie Louie
        tech_writer: Evan Read
        internal_customers:
          - Infrastructure Department
        categories:
          - geo_replication
          - disaster_recovery
      memory:
        name: Memory
        description: "Responsible for optimizing GitLab application performance by reducing and optimizing memory resources required."
        pm: Eric Brinkman (Interim)
        pmm: Ashish Kuthiala (Interim)
        backend_engineering_manager: Craig Gomes
        uxm: Jacki Bauer
        tech_writer: Mike Lewis
        cm: Erica Lindberg
        test_automation_engineers:
          - Grant Young
        support: Cindy Pallares
        internal_customers:
          - Quality Department
          - Infrastructure Department
      ecosystem:
        name: Ecosystem
        description: "Supporting the success of third-party products integrating with GitLab."
        pm: Patrick Deuley
        pmm: Ashish Kuthiala (Interim)
        backend_engineering_manager: Chun Du (Interim)
        frontend_engineering_manager: Chun Du (Interim)
        uxm: Jacki Bauer
        tech_writer: Mike Lewis
        cm: Erica Lindberg
        qm: Mek Stittri
        internal_customers:
          - Alliances Department
          - Customer Success
        categories:
          - sdk
          - native_integrations
          - marketplace
